# TBO App

The TBO App Flutter project.

## Getting Started
This app is build with Flutter. For help getting started with Flutter, view the Flutter
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Code Style & Quality
In order to keep the same style across the entire project the Effective Dart code style will be followed. In order to enforce that the [lint](https://pub.dev/packages/lint) package is used.

## Project structure
```
app
├── android                             // Android project
├── assets                              // Assets used in the app
│   ├── images
│   ├── fonts
│   └── databases
│   
├── ios                                 // iOS project
├── lib                                 // Project code in folders per feature
│   ├── activity
│   ├── core                            // Shared code for the entire app
│   │   ├── database   
│   │   │   └── sync_db.dart
│   │   ├── app_colors.dart
│   │   └── app_icons.dart
│   ├── home
│   ├── insight
│   ├── overview
│   ├── register
│   │   ├── login_page.dart             // Pages responsible for navigation etc. 
│   │   ├── login_form.dart             // Partial widgets build up a page
│   │   ├── login_scoped_model.dart     // Scoped model tied to the feature
│   │   └── welcome_page.dart
│   └── settings
│
│
├── test                                // Unit tests
├── test_driver                         // Integration tests
├── test_widget                         // Widget tests
├── analysis_options.yaml               // Analyzer settings
├── CHANGELOG.md                        // Log of changes per version
├── pubspec.yaml                        // Project specification
└── README.md                           // This README.md
```


## OTAP

### Support in Flutter
Flutter supports flavor when running and building your application. By running `flutter run flavor=development` the development flavor is run. This corresponds to `productFlavors` in Android and `schemes` in XCode. 

#### Android
In Android projects it is possible to define different flavors of the same app. This is used in Flutter to run and build different versions of the app. For more information please read more about it on [Android documentation](https://developer.android.com/studio/build/build-variants). Setting up the flavors is done as follows;

```
flavorDimensions "flavor-type"


productFlavors {
    development {
        dimension "flavor-type"
        applicationIdSuffix ".dev"
        versionNameSuffix "-dev"
    }

    qa {
        dimension "flavor-type"
        applicationIdSuffix ".qa"
        versionNameSuffix "-qa"
    }

    acceptance {
        dimension "flavor-type"
        applicationIdSuffix ".acceptance"
        versionNameSuffix "-acceptance"
    }

    production {
        dimension "flavor-type"
    }
}
```

By setting the flavors this way this results in the following;
- By running `flutter run --flavor development` the app will be build with the applicationId `com.example.tbo_app.dev` and the version name will be `1.0-dev`. 
- By running `flutter run --flavor production` the app will be build with the applicationId `com.example.tbo_app` and the version name will be `1.0`.

##### Android Studio
By setting up the productFlavors in the `build.gradle` file the Build Variants are shown in Android Studio. This allows for an easier selection of the Build Variant. 
![Build Variants](docs/readme/build_variants.png)

#### iOS
To make the flavors match with the iOS project schemes need to be added. These schemes can be amended with environment variables defined in `*.xcconfig` configuration files. The following is an example of the configuration for the development flavor. 
```
#include "Generated.xcconfig"
#include "Pods/Target Support Files/Pods-Runner/Pods-Runner.debug-development.xcconfig"
#include "Pods/Target Support Files/Pods-Runner/Pods-Runner.release-development.xcconfig"

FLUTTER_TARGET=lib/main_development.dart
bundle_suffix=.dev
bundle_name=TBO
icon_name=-development
```
These environment variables are then used in the `Info.plist` file to adjust the `Bundle identifier` so that per scheme a different version of the same app is built. By adjusting the `FLUTTER TARGET` variable a different entry point is used for the `flutter run` command. In essence the following is executed `flutter run target=<FLUTTER_TARGET>`.

##### XCode
- How to use the schemes in XCode

### VS Code
- How to use the `launch.json` in XCode in combination with the flavors

### Android Studio
- How to use launch configurations in combination with the flavors

## Databases
- Stores the data locally with SQLite. 
- Data is synced with the API

## Api

## Running tests
How to;
- Run normal unit tests
- Run widget tests
- Run driver tests

### Deployment
- Dev; amongst developers
- Test; internal tests within the team and appointed testers
- Acceptance; interviewers
- Production; App Store and Play Store

How to deploy;
- Using terminal
- Using Android Studio
- Using Xcode