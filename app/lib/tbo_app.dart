import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/activities/activity_tree.dart';
import 'package:tbo_app/activities/activity_tree_scoped_model.dart';
import 'package:tbo_app/activities/agendas_scoped_model.dart';
import 'package:tbo_app/core/para_data_scoped_model.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/home/home_page.dart';
import 'package:tbo_app/home/page_model.dart';
import 'package:tbo_app/home/start_questionnaire_scoped_model.dart';
import 'package:tbo_app/insights/insights_scoped_model.dart';
import 'package:tbo_app/register/login_scoped_model.dart';
import 'package:tbo_app/register/welcome_page.dart';
import 'package:tbo_app/settings/settings_scoped_model.dart';

import 'experience_sampling/notification.dart';

class TBOApp extends StatelessWidget {
  final bool initialized;

  const TBOApp({Key key, @required this.initialized}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScopedModel<ParaDataScopedModel>(
      model: ParaDataScopedModel(),
      child: ScopedModel<PageModel>(
        model: PageModel(),
        child: ScopedModel<SettingsModel>(
          model: SettingsModel(),
          child: ScopedModel<ActivityTreeScopedModel>(
            model: ActivityTreeScopedModel(ActivityTree(), ActivityTree()),
            child: ScopedModel<AgendasScopedModel>(
              model: AgendasScopedModel(),
              child: ScopedModel<StartQuestionnaireModel>(
                model: StartQuestionnaireModel(),
                child: ScopedModel<InsightsScopedModel>(
                  model: InsightsScopedModel(),
                  child: ScopedModel<LoginScopedModel>(
                    model: LoginScopedModel(),
                    child: Builder(builder: (context) {
                      return MaterialApp(
                        navigatorObservers: [
                          ParaDataScopedModel.of(context).observer()
                        ],
                        builder: (BuildContext context, Widget child) {
                          initializeUIParemeters(context);
                          final MediaQueryData data = MediaQuery.of(context);
                          return MediaQuery(
                            data: data.copyWith(textScaleFactor: 1),
                            child: child,
                          );
                        },
                        debugShowCheckedModeBanner: false,
                        localizationsDelegates: const [
                          GlobalMaterialLocalizations.delegate,
                          GlobalWidgetsLocalizations.delegate,
                        ],
                        supportedLocales: const [
                          Locale('en', 'US'), // English
                          Locale('nl', 'NL'), // Dutch
                          Locale('sl', 'SL'), // Slovenian
                          Locale('fi', 'FI'), // Finnish
                        ],
                        theme: ThemeData(
                            primarySwatch: const MaterialColor(
                              0xFF00A1CD,
                              {
                                50: Color(0xFF00A1CD),
                                100: Color(0xFF00A1CD),
                                200: Color(0xFF00A1CD),
                                300: Color(0xFF00A1CD),
                                400: Color(0xFF00A1CD),
                                500: Color(0xFF00A1CD),
                                600: Color(0xFF00A1CD),
                                700: Color(0xFF00A1CD),
                                800: Color(0xFF00A1CD),
                                900: Color(0xFF00A1CD),
                              },
                            ),
                            fontFamily: 'Source Sans Pro'),
                        home: StartPageSelector(initialized: initialized),
                      );
                    }),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class StartPageSelector extends StatelessWidget {
  const StartPageSelector({this.initialized});
  final bool initialized;

  @override
  Widget build(BuildContext context) {
    initializeNotificationPlugin();
    configureDidReceiveLocalNotificationSubject(context);
    configureSelectNotificationSubject(context);
    return initialized ? HomePage() : WelcomePage();
  }
}
