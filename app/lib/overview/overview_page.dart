import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tbo_app/activities/agendas_scoped_model.dart';
import 'package:tbo_app/core/database/synchronise.dart';
import 'package:tbo_app/core/translate.dart';
import 'package:tbo_app/overview/mid_section_widget.dart';
import 'package:tbo_app/overview/overview_page_tutorial.dart';
import 'package:tbo_app/overview/top_bar_widget.dart';
import 'package:tbo_app/para_data/para_data_name.dart';

int maxDisplayMonth;
int minDisplayMonth;

GlobalKey keyButton1 = GlobalKey();
GlobalKey keyButton2 = GlobalKey();
GlobalKey keyButton3 = GlobalKey();
GlobalKey keyButton4 = GlobalKey();

class OverviewPage extends StatefulWidget with ParaDataName {
  final GlobalKey addKeyButton;
  final ValueChanged<DateTime> dateSelected;

  const OverviewPage(this.addKeyButton, {@required this.dateSelected});

  @override
  State<StatefulWidget> createState() {
    return _OverviewPageState();
  }
}

class _OverviewPageState extends State<OverviewPage> {
  double distance;
  double initial;
  int _monthIndex;

  int _calendarMonth;
  int _calendarYear;
  String _displayUser = 'Tom';
  final bool _informationPopup = false;

  @override
  void initState() {
    super.initState();
    keyButton4 = widget.addKeyButton;
    Synchronise.synchronise();
    _monthIndex = 0;
    _calendarYear = DateTime.now().year;
    _calendarMonth = DateTime.now().month;
    minDisplayMonth = getPreviousMonthInt(_calendarMonth);
    maxDisplayMonth = getNextMonthInt(_calendarMonth);

    Future.delayed(const Duration(milliseconds: 1000), () {
      showInitialTutorial(context);
    });
  }

  int getPreviousMonthInt(int currentMonth) {
    return currentMonth == 1 ? 12 : currentMonth - 1;
  }

  int getNextMonthInt(int currentMonth) {
    return currentMonth == 12 ? 1 : currentMonth + 1;
  }

  Future<void> showInitialTutorial(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();
    final status = prefs.getString('mainOverviewTutorial');
    if (status == null) {
      prefs.setString('mainOverviewTutorial', 'shown');
      showTutorial(context);
    }
  }

  void setUser(String user) {
    setState(() {
      _displayUser = user;
    });
  }

  void currentCalendarMonth() {
    if (!_informationPopup) {
      setState(() {
        _calendarYear = DateTime.now().year;
        _calendarMonth = DateTime.now().month;
      });
    }
  }

  void _nextCalendarMonth() {
    if (_monthIndex < 1) {
      setState(() {
        _calendarYear =
            _calendarMonth == 12 ? _calendarYear + 1 : _calendarYear;
        _calendarMonth = _calendarMonth == 12 ? 1 : _calendarMonth + 1;
      });
    }
  }

  void _previousCalendarMonth() {
    if (_monthIndex > -1) {
      setState(() {
        _calendarYear = _calendarMonth == 1 ? _calendarYear - 1 : _calendarYear;
        _calendarMonth = _calendarMonth == 1 ? 12 : _calendarMonth - 1;
      });
    }
  }

  String _getMonthString() {
    final String monthString = DateFormat.MMMM(translate.languagePreference)
        .format(DateTime(_calendarYear, _calendarMonth));
    return monthString[0].toUpperCase() + monthString.substring(1);
  }

  @override
  Widget build(BuildContext context) {
    initTargets(keyButton1, keyButton2, keyButton3, keyButton4, context);
    return ScopedModelDescendant<AgendasScopedModel>(
      builder: (context, child, _) => Center(
        child: Column(
          children: <Widget>[
            TopBarWidget(
              _nextCalendarMonth,
              _previousCalendarMonth,
              _getMonthString(),
              setUser,
              _displayUser,
              currentCalendarMonth,
              _calendarMonth,
              _calendarYear,
              informationPopup: _informationPopup,
            ),
            MidSectionWidget(
              _nextCalendarMonth,
              _previousCalendarMonth,
              _calendarMonth,
              _calendarYear,
              dateSelected: widget.dateSelected,
            ),
          ],
        ),
      ),
    );
  }
}
