import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:tbo_app/core/color_pallet.dart';
import 'package:tbo_app/core/database/user_progress_database.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/core/translate.dart';
import 'package:tuple/tuple.dart';
import 'package:url_launcher/url_launcher.dart';

class InformationPopup extends StatelessWidget {
  const InformationPopup(this.hideInformationPopup);

  final void Function() hideInformationPopup;

  Future<void> _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 400.0 * y,
        margin: EdgeInsets.symmetric(horizontal: 7.0 * x),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: ColorPallet.veryLightGray,
                offset: Offset(5.0 * x, 1.0 * y),
                blurRadius: 2.0 * x,
                spreadRadius: 3.0 * x)
          ],
        ),
        child: Column(
          children: <Widget>[
            SizedBox(height: 10.0 * y),
            Container(
              margin: const EdgeInsets.all(10.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                      flex: 8,
                      child: Center(
                        child: Text(translate.infoTitle,
                            style: TextStyle(
                                color: ColorPallet.darkTextColor,
                                fontSize: 18.0 * f,
                                fontWeight: FontWeight.w700)),
                      )),
                  Expanded(
                    child: Center(
                      child: InkWell(
                        onTap: hideInformationPopup,
                        child: Icon(
                          Icons.close,
                          color: ColorPallet.darkTextColor,
                          size: 27.0 * x,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              flex: 10,
              child: Scrollbar(
                child: Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: 20.0 * x, vertical: 5.0 * y),
                  child: SingleChildScrollView(
                    child: translate.languagePreference == 'nl'
                        ? RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(children: <TextSpan>[
                              TextSpan(
                                text: 'Heeft u vragen?',
                                style: TextStyle(
                                    color: ColorPallet.darkTextColor,
                                    fontWeight: FontWeight.w700,
                                    fontSize: 16.0 * f),
                              ),
                              const TextSpan(text: '\n'),
                              const TextSpan(text: '\n'),
                              TextSpan(
                                text: '''
                                    Zie de schriftelijke handleiding voor meer informatie over de app en het onderzoek. Kijk of u een antwoord kunt vinden in de rubriek ‘veel gestelde vragen’ (FAQ).

Ook zijn op youtube filmpjes te vinden over het invullen van de app, te vinden via ''',
                                style: TextStyle(
                                    color: ColorPallet.darkTextColor,
                                    fontSize: 16.0 * f),
                              ),
                              TextSpan(
                                text: 'deze link.',
                                style: TextStyle(
                                    color: ColorPallet.primaryColor,
                                    fontSize: 16.0 * f),
                                recognizer: TapGestureRecognizer()
                                  ..onTap = () {
                                    _launchURL(
                                        'https://www.youtube.com/channel/UCHReugknWrrno18qj8YQqfg');
                                  },
                              ),
                              const TextSpan(text: '\n'),
                              const TextSpan(text: '\n'),
                              TextSpan(
                                text:
                                    '''Staat uw antwoord er niet bij? Bel ons gerust op (045) 570 7388. U kunt ook mailen naar WINhelpdesk@cbs.nl. Wij zijn bereikbaar van maandag tot en met vrijdag tussen 9.00 en 17.00 uur.''',
                                style: TextStyle(
                                    color: ColorPallet.darkTextColor,
                                    fontSize: 16.0 * f),
                              ),
                            ]),
                          )
                        : translate.languagePreference == 'en'
                            ? RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(children: <TextSpan>[
                                  TextSpan(
                                    text: translate.infoBody,
                                    style: TextStyle(
                                        color: ColorPallet.darkTextColor,
                                        fontSize: 16.0 * f),
                                  ),
                                  TextSpan(
                                    text: 'youtube.',
                                    style: TextStyle(
                                        color: ColorPallet.primaryColor,
                                        fontSize: 16.0 * f),
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () {
                                        _launchURL(
                                            'https://www.youtube.com/playlist?list=PL3c7jGlkxpEo65jnJ_8F3-lKMjDrR6wg_');
                                      },
                                  ),
                                  TextSpan(
                                      text: '\n\n${translate.infoForMoreInfo}',
                                      style: TextStyle(
                                          color: ColorPallet.darkTextColor,
                                          fontSize: 16.0 * f)),
                                  const TextSpan(text: '\n'),
                                ]),
                              )
                            : FutureBuilder<Tuple2<String, String>>(
                                future: UserProgressDatabase()
                                    .getStartAndEndDateExperiment(),
                                builder: (context, snapshot) {
                                  final String startDate = snapshot.data == null
                                      ? ' '
                                      : snapshot.data.item1;
                                  final String endDate = snapshot.data == null
                                      ? ' '
                                      : snapshot.data.item2;

                                  return RichText(
                                    textAlign: TextAlign.center,
                                    text: TextSpan(children: <TextSpan>[
                                      TextSpan(
                                        text: translate.infoBody,
                                        style: TextStyle(
                                            color: ColorPallet.darkTextColor,
                                            fontSize: 16.0 * f),
                                      ),
                                      TextSpan(
                                        text: translate.languagePreference ==
                                                    'fi' ||
                                                translate.languagePreference ==
                                                    'sl'
                                            ? ''
                                            : '${' ${translate.between} '}$startDate${' ${translate.and} '}$endDate. ',
                                        style: TextStyle(
                                            color: ColorPallet.darkTextColor,
                                            fontSize: 16.0 * f),
                                      ),
                                      TextSpan(
                                          text:
                                              '\n\n${translate.infoForMoreInfo}',
                                          style: TextStyle(
                                              color: ColorPallet.darkTextColor,
                                              fontSize: 16.0 * f)),
                                      TextSpan(
                                        text: translate.infoWebsiteLink,
                                        style: TextStyle(
                                            color: ColorPallet.primaryColor,
                                            fontSize: 16.0 * f),
                                        recognizer: TapGestureRecognizer()
                                          ..onTap = () {
                                            _launchURL(translate.url);
                                          },
                                      ),
                                      const TextSpan(text: '\n'),
                                    ]),
                                  );
                                },
                              ),
                  ),
                ),
              ),
            )
          ],
        ));
  }
}
