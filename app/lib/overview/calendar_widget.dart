import 'package:flutter/material.dart';
import 'package:tbo_app/core/database/user_progress_database.dart';
import 'package:tbo_app/overview/calendar.dart';

class CalendarWidget extends StatelessWidget {
  const CalendarWidget(this._nextCalendarMonth, this._previousCalendarMonth,
      this._calendarYear, this._calendarMonth,
      {@required this.dateSelected});

  final int _calendarMonth;
  final int _calendarYear;
  final ValueChanged<DateTime> dateSelected;

  final Function() _nextCalendarMonth;

  final Function() _previousCalendarMonth;

  @override
  Widget build(BuildContext context) {
    double initialSwipe;
    double distanceSwiped;
    return FutureBuilder(
      future: UserProgressDatabase().getCalendarFormatting(),
      builder: (context, snapshot) {
        if (snapshot.data == null) {
          return CalanderWidget(
            _calendarYear,
            _calendarMonth,
            [DateTime.now()],
            const [],
            const [],
            dateSelected: dateSelected,
          );
        } else {
          return GestureDetector(
            onPanStart: (DragStartDetails details) {
              initialSwipe = details.globalPosition.dx;
            },
            onPanUpdate: (DragUpdateDetails details) {
              distanceSwiped = details.globalPosition.dx - initialSwipe;
            },
            onPanEnd: (DragEndDetails details) {
              initialSwipe = 0.0;
              if (distanceSwiped < 50) {
                _nextCalendarMonth();
              }
              if (distanceSwiped > 50) {
                _previousCalendarMonth();
              }
            },
            child: CalanderWidget(
              _calendarYear,
              _calendarMonth,
              snapshot.data['daysOfExperiment'] as List<DateTime>,
              snapshot.data['daysCompleted'] as List<DateTime>,
              snapshot.data['daysMissing'] as List<DateTime>,
              dateSelected: dateSelected,
            ),
          );
        }
      },
    );
  }
}
