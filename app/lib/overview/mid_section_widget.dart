import 'package:flutter/material.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/overview/calendar_widget.dart';
import 'package:tbo_app/overview/information_popup.dart';
import 'package:tbo_app/overview/progress_widget.dart';

class MidSectionWidget extends StatefulWidget {
  const MidSectionWidget(this._nextCalendarMonth, this._previousCalendarMonth,
      this._calendarMonth, this._calendarYear,
      {@required this.dateSelected});

  final int _calendarMonth;
  final int _calendarYear;
  final ValueChanged<DateTime> dateSelected;

  @override
  State<StatefulWidget> createState() {
    return _MidSectionWidgetState();
  }

  final Function() _nextCalendarMonth;

  final Function() _previousCalendarMonth;
}

class _MidSectionWidgetState extends State<MidSectionWidget> {
  bool _informationPopup = false;

  void showInformationPopup() {
    setState(() {
      _informationPopup = true;
    });
  }

  void hideInformationPopup() {
    setState(() {
      _informationPopup = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        margin: EdgeInsets.only(bottom: 6.0 * y),
        child: _informationPopup
            ? InformationPopup(hideInformationPopup)
            : Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  SizedBox(height: 4 * y),
                  Expanded(
                    flex: 21,
//TODO:                    key: keyButton2,
                    child: CalendarWidget(
                      widget._nextCalendarMonth,
                      widget._previousCalendarMonth,
                      widget._calendarYear,
                      widget._calendarMonth,
                      dateSelected: widget.dateSelected,
                    ),
                  ),
                  SizedBox(height: 8 * y),
                  Expanded(
                      flex: 9, child: ProgressWidget(showInformationPopup)),
                  SizedBox(height: 4 * y),
                ],
              ),
      ),
    );
  }
}
