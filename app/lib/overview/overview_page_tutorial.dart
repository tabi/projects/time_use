import 'package:flutter/material.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/core/spotlight_tutorial/animated_focus_light.dart';
import 'package:tbo_app/core/spotlight_tutorial/content_target.dart';
import 'package:tbo_app/core/spotlight_tutorial/target_focus.dart';
import 'package:tbo_app/core/translate.dart';

List<TargetFocus> targets = <TargetFocus>[];

TextStyle titleStyle = TextStyle(
  fontWeight: FontWeight.w700,
  color: Colors.white,
  fontSize: 22.0 * f,
  height: 1.4,
);
TextStyle bodyStyle = TextStyle(
  fontWeight: FontWeight.w400,
  color: Colors.white,
  fontSize: 15.0 * f,
  height: 1.4,
);

void initTargets(GlobalKey keyButton1, GlobalKey keyButton2,
    GlobalKey keyButton3, GlobalKey keyButton4, BuildContext context) {
  if (targets.isEmpty) {
    targets.add(
      TargetFocus(
        identify: 'blue',
        keyTarget: keyButton1,
        contents: [
          ContentTarget(
              child: SizedBox(
            height: MediaQuery.of(context).size.height - 150 * y,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(children: <Widget>[
                  Text(
                    translate.mainOverviewTutorial1,
                    style: titleStyle,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(height: 7 * y),
                  Text(
                    translate.mainOverviewTutorial2,
                    style: bodyStyle,
                    textAlign: TextAlign.center,
                  ),
                ]),
                Column(
                  children: <Widget>[
                    Text(
                      translate.mainOverviewTutorial3,
                      style: TextStyle(
                          fontWeight: FontWeight.w700,
                          color: Colors.white,
                          fontSize: 22.0 * f,
                          height: 1.4,
                          shadows: const [Shadow(blurRadius: 1)]),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 7 * y),
                    Text(
                      translate.mainOverviewTutorial4,
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          color: Colors.white,
                          fontSize: 15.0 * f,
                          height: 1.4,
                          shadows: const [Shadow(blurRadius: 1)]),
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
                Column(
                  children: <Widget>[
                    SizedBox(height: 30 * y),
                    Image.asset(
                      'images/tab_symbol.png',
                      height: 100 * y,
                    ),
                  ],
                ),
              ],
            ),
          ))
        ],
        shape: ShapeLightFocus.rrect,
      ),
    );

    targets.add(
      TargetFocus(
        identify: 'normal',
        keyTarget: keyButton2,
        contents: [
          ContentTarget(
            child: Column(
              children: <Widget>[
                Text(
                  translate.mainOverviewTutorial5,
                  style: titleStyle,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 7 * y),
                Text(
                  translate.mainOverviewTutorial6,
                  style: bodyStyle,
                  textAlign: TextAlign.center,
                ),
              ],
            ),
          )
        ],
        shape: ShapeLightFocus.rrect,
      ),
    );

    if (translate.languagePreference == 'nl' ||
        translate.languagePreference == 'en') {
      targets.add(
        TargetFocus(
          identify: 'normal',
          keyTarget: keyButton3,
          contents: [
            ContentTarget(
                align: AlignContent.top,
                child: Column(
                  children: <Widget>[
                    Text(
                      translate.mainOverviewTutorial7,
                      style: titleStyle,
                      textAlign: TextAlign.center,
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10.0 * y),
                      child: Text(
                        translate.mainOverviewTutorial8,
                        style: bodyStyle,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    SizedBox(height: 15 * y),
                  ],
                ))
          ],
          shape: ShapeLightFocus.rrect,
        ),
      );
    }

    targets.add(
      TargetFocus(
        identify: 'normal',
        keyTarget: keyButton4,
        contents: [
          ContentTarget(
              align: AlignContent.top,
              child: Column(
                children: <Widget>[
                  Text(
                    translate.mainOverviewTutorial9,
                    style: titleStyle,
                    textAlign: TextAlign.center,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10.0 * y),
                    child: Text(
                      translate.mainOverviewTutorial10,
                      style: bodyStyle,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 30 * y),
                ],
              ))
        ],
        shape: ShapeLightFocus.circle,
      ),
    );
  }
}

void showTutorial(BuildContext context) {
  // SystemChrome.setSystemUIOverlayStyle(
  //   SystemUiOverlayStyle(statusBarColor: ColorPallet.darkTextColor),
  // );
  // showGestureLogo = true;
  // TutorialCoachMark(
  //   context,
  //   targets: targets,
  //   colorShadow: ColorPallet.darkTextColor,
  //   textSkip: translate.skip,
  //   paddingFocus: 10,
  //   textStyleSkip: TextStyle(color: Colors.white, fontSize: 16 * f),
  //   alignSkip: Alignment.topLeft,
  //   opacityShadow: .98,
  //   clickSkip: () {
  //     showGestureLogo = false;
  //     SystemChrome.setSystemUIOverlayStyle(
  //       SystemUiOverlayStyle(statusBarColor: ColorPallet.primaryColor),
  //     );
  //   },
  //   finish: () {
  //     SystemChrome.setSystemUIOverlayStyle(
  //       SystemUiOverlayStyle(statusBarColor: ColorPallet.primaryColor),
  //     );
  //   },
  //   clickTarget: (TargetFocus target) {
  //     showGestureLogo = false;
  //   },
  // )..show();
}
