import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/core/para_data_scoped_model.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/core/translate.dart';

class TopBarWidget extends StatefulWidget {
  const TopBarWidget(
      this.nextCalendarMonth,
      this.previousCalendarMonth,
      this.displayMonth,
      this.setUser,
      this.displayUser,
      this.currentCalendarMonth,
      this._calendarMonth,
      this._calendarYear,
      {this.informationPopup});

  final String displayMonth;
  final String displayUser;
  final bool informationPopup;

  final int _calendarMonth;
  final int _calendarYear;

  @override
  _TopBarWidgetState createState() => _TopBarWidgetState();

  final void Function() nextCalendarMonth;

  final void Function() previousCalendarMonth;

  final void Function(String user) setUser;

  final void Function() currentCalendarMonth;
}

class _TopBarWidgetState extends State<TopBarWidget> {
  int monthIndex;

  @override
  void initState() {
    super.initState();
    monthIndex = 0;
  }

  bool isCurrentMonth() {
    if (widget._calendarYear == DateTime.now().year &&
        widget._calendarMonth == DateTime.now().month) {
      return true;
    }
    return false;
  }

  bool canGoBackAMonth() {
    if (monthIndex > -1) {
      return true;
    } else {
      return false;
    }
  }

  bool canGoForwardAMonth() {
    if (monthIndex < 1) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    final String image = translate.languagePreference == 'sl'
        ? 'topbar_sl.png'
        : translate.languagePreference == 'fi'
            ? 'topbar_fin.png'
            : 'topbar.png';
    return ScopedModelDescendant<ParaDataScopedModel>(
        builder: (context, child, paraDataScopedModel) {
      return Stack(
        children: <Widget>[
          InkWell(
            child: Image(
                image: AssetImage('assets/images/$image'),
                fit: BoxFit.fitWidth),
          ),
//         Positioned(
//           right: 16.0 * x,
//           top: 14.0 * y,
//           child: InkWell(
//             onTap: () {
//               showTutorial(context);
//             },
// // TODO:            key: keyButton1,
//             child: Icon(Icons.info, color: Colors.white, size: 28.0 * x),
//           ),
//         ),
          Positioned(
            bottom: 17 * y,
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      InkWell(
                        onTap: () {
                          if (canGoBackAMonth()) {
                            monthIndex--;
                            widget.previousCalendarMonth();
                          }
                        },
                        child: Icon(Icons.keyboard_arrow_left,
                            color: canGoBackAMonth()
                                ? Colors.white
                                : Colors.white.withOpacity(0.39),
                            size: 29.0 * x),
                      ),
                      SizedBox(width: 10.0 * x),
                      Container(
                        width: 120.0 * x,
                        padding: const EdgeInsets.only(bottom: 1),
                        child: Center(
                          child: Text(
                            widget.displayMonth,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20.0 * f,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                      ),
                      SizedBox(width: 10.0 * x),
                      InkWell(
                        onTap: () {
                          if (canGoForwardAMonth()) {
                            paraDataScopedModel.onTap(
                                'InkWell', 'nextCalendarMonth');
                            monthIndex++;
                            widget.nextCalendarMonth();
                          }
                        },
                        child: Icon(Icons.keyboard_arrow_right,
                            color: canGoForwardAMonth()
                                ? Colors.white
                                : Colors.white.withOpacity(0.39),
                            size: 29.0 * x),
                      ),
                    ],
                  ),
                  SizedBox(width: 62.0 * x),
                  Container(width: 26.5 * x),
                  SizedBox(width: 20.0 * x),
                ],
              ),
            ),
          )
        ],
      );
    });
  }
}
