import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/activities/agendas_scoped_model.dart';
import 'package:tbo_app/core/color_pallet.dart';
import 'package:tbo_app/core/database/datamodels/progress_statistics.dart';
import 'package:tbo_app/core/database/user_progress_database.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/core/translate.dart';

class ProgressWidget extends StatefulWidget {
  const ProgressWidget(this.showInformationPopup);

  @override
  _ProgressWidgetState createState() => _ProgressWidgetState();

  final void Function() showInformationPopup;
}

class _ProgressWidgetState extends State<ProgressWidget> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 9.0 * x),
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  color: ColorPallet.veryLightGray,
                  offset: Offset(1.0 * x, 1.0 * x),
                  blurRadius: 2.0 * x,
                  spreadRadius: 3.0 * x)
            ],
          ),
          child: ScopedModelDescendant<AgendasScopedModel>(
            builder: (context, child, _) {
              return SizedBox(
                width: (MediaQuery.of(context).size.width - (27 * x)) / 2,
                height: 132.0 * y,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    FutureBuilder<ProgressStatistics>(
                        future: UserProgressDatabase().getProgressStats(),
                        builder: (context, snapshot) {
                          final String completed = snapshot.data == null
                              ? ' '
                              : snapshot.data.completed.toString();
                          final String missing = snapshot.data == null
                              ? ' '
                              : snapshot.data.missing.toString();
                          final String remaining = snapshot.data == null
                              ? ' '
                              : snapshot.data.remaining.toString();
                          return Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(
                                width: 150 * x,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      translate.progress,
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          fontSize: 14.0 * f,
                                          color: ColorPallet.darkTextColor,
                                          fontWeight: FontWeight.w700),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(height: 12.0 * y),
                              Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Container(
                                    height: 15.0 * y,
                                    width: 15.0 * x,
                                    decoration: BoxDecoration(
                                      color: ColorPallet.lightGreen,
                                      shape: BoxShape.circle,
                                      boxShadow: [
                                        BoxShadow(
                                            color: ColorPallet.veryLightGray,
                                            offset: Offset(1.0 * x, 1.0 * y),
                                            blurRadius: 1.0 * x,
                                            spreadRadius: 1.0 * x)
                                      ],
                                    ),
                                  ),
                                  SizedBox(width: 10.0 * x),
                                  Text(
                                    '$completed ${translate.daysCompleted}',
                                    style: TextStyle(
                                        fontSize: 14.0 * f,
                                        color: ColorPallet.darkTextColor,
                                        fontWeight: FontWeight.w700),
                                  )
                                ],
                              ),
                              SizedBox(height: 8.0 * y),
                              Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Container(
                                    height: 15.0 * y,
                                    width: 15.0 * x,
                                    decoration: BoxDecoration(
                                      color: ColorPallet.orange,
                                      shape: BoxShape.circle,
                                      boxShadow: [
                                        BoxShadow(
                                            color: ColorPallet.veryLightGray,
                                            offset: Offset(1.0 * x, 1.0 * y),
                                            blurRadius: 1.0 * x,
                                            spreadRadius: 1.0 * x)
                                      ],
                                    ),
                                  ),
                                  SizedBox(width: 10.0 * x),
                                  Text(
                                    '$missing ${translate.daysMissing}',
                                    style: TextStyle(
                                        fontSize: 14.0 * f,
                                        color: ColorPallet.darkTextColor,
                                        fontWeight: FontWeight.w700),
                                  )
                                ],
                              ),
                              SizedBox(height: 8.0 * y),
                              Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Container(
                                    height: 15.0 * y,
                                    width: 15.0 * x,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: ColorPallet.veryLightBlue,
                                      boxShadow: [
                                        BoxShadow(
                                            color: ColorPallet.veryLightGray,
                                            offset: Offset(1.0 * x, 1.0 * y),
                                            blurRadius: 1.0 * x,
                                            spreadRadius: 1.0 * x)
                                      ],
                                    ),
                                  ),
                                  SizedBox(width: 10.0 * x),
                                  Text(
                                    '$remaining ${translate.daysRemaining}',
                                    style: TextStyle(
                                        fontSize: 14.0 * f,
                                        color: ColorPallet.darkTextColor,
                                        fontWeight: FontWeight.w700),
                                  )
                                ],
                              )
                            ],
                          );
                        }),
                  ],
                ),
              );
            },
          ),
        ),
        InkWell(
          // onTap: widget.showInformationPopup,
          child: Container(
            margin: EdgeInsets.only(left: 9.0 * x),
            decoration: BoxDecoration(
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: ColorPallet.veryLightGray,
                    offset: Offset(1.0 * x, 1.0 * x),
                    blurRadius: 2.0 * x,
                    spreadRadius: 3.0 * x)
              ],
            ),
            child: SizedBox(
//TODO:              key: keyButton3,
              width: (MediaQuery.of(context).size.width - (27 * x)) / 2,
              height: 132.0 * y,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      SizedBox(width: 128 * x, height: 15 * y),
                      Text(
                          translate.languagePreference == 'en' ||
                                  translate.languagePreference == 'nl'
                              ? '${translate.earnedReward}:'
                              : '',
                          style: TextStyle(
                              color: ColorPallet.darkTextColor,
                              fontWeight: FontWeight.w700,
                              fontSize: 13 * f)),
                      SizedBox(height: 22 * y),
                      FutureBuilder(
                        future: UserProgressDatabase().getDaysCompleted(),
                        builder: (context, snapshot) {
                          if (snapshot.data == null) {
                            return Text(
                              '\u20AC0.00',
                              style: TextStyle(
                                  color: ColorPallet.darkTextColor,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 20 * f),
                            );
                          } else {
                            return Text(
                              translate.languagePreference == 'en' ||
                                      translate.languagePreference == 'nl'
                                  ? '\u20AC${snapshot.data.length.toString()}.00'
                                  : '',
                              style: TextStyle(
                                  color: ColorPallet.darkTextColor,
                                  fontWeight: FontWeight.w500,
                                  fontSize: 30 * f),
                            );
                          }
                        },
                      ),
                      SizedBox(height: 5 * y),
                      SizedBox(height: 5 * y),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
