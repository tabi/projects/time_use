import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/activities/agendas_scoped_model.dart';
import 'package:tbo_app/core/color_pallet.dart';
import 'package:tbo_app/core/configuration.dart';
import 'package:tbo_app/core/input_type.dart';
import 'package:tbo_app/core/para_data_scoped_model.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/core/translate.dart';

String year, month;

class CalanderWidget extends StatefulWidget {
  CalanderWidget(this._displayYear, this._displayMonth, this._datesOfExperiment,
      this._daysCompleted, this._daysMissing,
      {@required this.dateSelected});

  final List<DateTime> _datesOfExperiment;
  final List<DateTime> _daysCompleted;
  final List<DateTime> _daysMissing;
  final int _displayMonth;
  final int _displayYear;
  final List<DateTime> _today = [DateTime.now()];
  final ValueChanged<DateTime> dateSelected;

  @override
  State<StatefulWidget> createState() {
    return _CalanderWidgetState();
  }
}

class _CalanderWidgetState extends State<CalanderWidget> {
  List<Map> calendarValues;

  void computeCalendarValues() {
    calendarValues = <Map>[];

    final int _firstWeekDayDisplayMonth =
        DateTime(widget._displayYear, widget._displayMonth).weekday;
    final int _numberOfDaysDisplayMonth =
        DateTime(widget._displayYear, widget._displayMonth + 1, 0).day;
    int _numberOfDaysPreviousMonth =
        DateTime(widget._displayYear, widget._displayMonth, 0).day;

    //Makes sure to check for next year
    final int _previousMonth =
        widget._displayMonth == 1 ? 12 : widget._displayMonth - 1;
    final int _nextMonth =
        widget._displayMonth == 12 ? 1 : widget._displayMonth + 1;

    //Makes sure to check for previous year
    final int _previousMonthsYear = widget._displayMonth == 1
        ? widget._displayYear - 1
        : widget._displayYear;
    final int _nextMonthsYear = widget._displayMonth == 12
        ? widget._displayYear + 1
        : widget._displayYear;

    //Adds the days to be displayed from the previous month (and formatting value) to the calendarValues list
    for (int i = 0; i < _firstWeekDayDisplayMonth - 1; i++) {
      final calendarValue = {};
      calendarValue['textValue'] = _numberOfDaysPreviousMonth.toString();
      calendarValue['isTitle'] = false;
      calendarValue['greyValue'] = true;
      addFomatting(_previousMonthsYear, _previousMonth,
          _numberOfDaysPreviousMonth, calendarValue);
      calendarValues.add(calendarValue);
      _numberOfDaysPreviousMonth--;
    }
    calendarValues = calendarValues.reversed.toList();

    //Adds the days to be displayed from the this month (and formatting value) to the calendarValues list
    for (int i = 1; i <= _numberOfDaysDisplayMonth; i++) {
      final calendarValue = {};
      calendarValue['textValue'] = i.toString();
      calendarValue['isTitle'] = false;
      calendarValue['greyValue'] = false;
      addFomatting(widget._displayYear, widget._displayMonth, i, calendarValue);
      calendarValues.add(calendarValue);
    }

    //Adds the days to be displayed for the next month (and formatting value) to the calanderValues list
    int i = 1;
    while (calendarValues.length < 42) {
      final calendarValue = {};
      calendarValue['textValue'] = i.toString();
      calendarValue['isTitle'] = false;
      calendarValue['greyValue'] = true;
      addFomatting(_nextMonthsYear, _nextMonth, i, calendarValue);
      calendarValues.add(calendarValue);
      i++;
    }
  }

  void addFomatting(int year, int month, int day, Map calendarValue) {
    final DateTime _firstDayOfExperiment = widget._datesOfExperiment[0];
    final DateTime _lastDayOfExperiment =
        widget._datesOfExperiment[widget._datesOfExperiment.length - 1];
    calendarValue['firstDayOfExperiment'] = false;
    calendarValue['lastDayOfExperiment'] = false;
    calendarValue['year'] = year;
    calendarValue['month'] = month;
    calendarValue['day'] = day;

    if (_firstDayOfExperiment.year == year &&
        _firstDayOfExperiment.month == month &&
        _firstDayOfExperiment.day == day) {
      calendarValue['firstDayOfExperiment'] = true;
    }
    if (_lastDayOfExperiment.year == year &&
        _lastDayOfExperiment.month == month &&
        _lastDayOfExperiment.day == day) {
      calendarValue['lastDayOfExperiment'] = true;
    }

    calendarValue['inExperiment'] = false;
    for (final DateTime specialDate in widget._datesOfExperiment) {
      if (specialDate.year == year &&
          specialDate.month == month &&
          specialDate.day == day) {
        calendarValue['inExperiment'] = true;
      }
    }

    calendarValue['isCompleted'] = false;
    for (final DateTime specialDate in widget._daysCompleted) {
      if (specialDate.year == year &&
          specialDate.month == month &&
          specialDate.day == day) {
        calendarValue['isCompleted'] = true;
      }
    }

    calendarValue['isMissing'] = false;
    for (final DateTime specialDate in widget._daysMissing) {
      if (specialDate.year == year &&
          specialDate.month == month &&
          specialDate.day == day) {
        calendarValue['isMissing'] = true;
      }
    }

    calendarValue['isToday'] = false;
    for (final DateTime specialDate in widget._today) {
      if (specialDate.year == year &&
          specialDate.month == month &&
          specialDate.day == day) {
        calendarValue['isToday'] = true;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    computeCalendarValues();

    year = widget._displayYear.toString();
    month = widget._displayMonth.toString();
    return ScopedModelDescendant<AgendasScopedModel>(
        builder: (context, child, activityModel) {
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 7.0 * x),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: ColorPallet.veryLightGray,
                offset: Offset(5.0 * x, 1.0 * x),
                blurRadius: 2.0 * x,
                spreadRadius: 3.0 * x)
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                _CalenderColumn(
                  [
                    {
                      'textValue': translate.monday,
                      'firstDayOfExperiment': false,
                      'lastDayOfExperiment': false,
                      'isTitle': true,
                      'greyValue': false,
                      'inExperiment': false,
                      'isCompleted': false,
                      'isMissing': false,
                      'isToday': false
                    },
                    calendarValues[0],
                    calendarValues[7],
                    calendarValues[14],
                    calendarValues[21],
                    calendarValues[28],
                    calendarValues[35]
                  ],
                  activityScopedModel: activityModel,
                  dateSelected: widget.dateSelected,
                ),
                _CalenderColumn(
                  [
                    {
                      'textValue': translate.tuesday,
                      'firstDayOfExperiment': false,
                      'lastDayOfExperiment': false,
                      'isTitle': true,
                      'greyValue': false,
                      'inExperiment': false,
                      'isCompleted': false,
                      'isMissing': false,
                      'isToday': false
                    },
                    calendarValues[1],
                    calendarValues[8],
                    calendarValues[15],
                    calendarValues[22],
                    calendarValues[29],
                    calendarValues[36]
                  ],
                  activityScopedModel: activityModel,
                  dateSelected: widget.dateSelected,
                ),
                _CalenderColumn(
                  [
                    {
                      'textValue': translate.wednesday,
                      'firstDayOfExperiment': false,
                      'lastDayOfExperiment': false,
                      'isTitle': true,
                      'greyValue': false,
                      'inExperiment': false,
                      'isCompleted': false,
                      'isMissing': false,
                      'isToday': false
                    },
                    calendarValues[2],
                    calendarValues[9],
                    calendarValues[16],
                    calendarValues[23],
                    calendarValues[30],
                    calendarValues[37]
                  ],
                  activityScopedModel: activityModel,
                  dateSelected: widget.dateSelected,
                ),
                _CalenderColumn(
                  [
                    {
                      'textValue': translate.thursday,
                      'firstDayOfExperiment': false,
                      'lastDayOfExperiment': false,
                      'isTitle': true,
                      'greyValue': false,
                      'inExperiment': false,
                      'isCompleted': false,
                      'isMissing': false,
                      'isToday': false
                    },
                    calendarValues[3],
                    calendarValues[10],
                    calendarValues[17],
                    calendarValues[24],
                    calendarValues[31],
                    calendarValues[38]
                  ],
                  activityScopedModel: activityModel,
                  dateSelected: widget.dateSelected,
                ),
                _CalenderColumn(
                  [
                    {
                      'textValue': translate.friday,
                      'firstDayOfExperiment': false,
                      'lastDayOfExperiment': false,
                      'isTitle': true,
                      'greyValue': false,
                      'inExperiment': false,
                      'isCompleted': false,
                      'isMissing': false,
                      'isToday': false
                    },
                    calendarValues[4],
                    calendarValues[11],
                    calendarValues[18],
                    calendarValues[25],
                    calendarValues[32],
                    calendarValues[39]
                  ],
                  activityScopedModel: activityModel,
                  dateSelected: widget.dateSelected,
                ),
                _CalenderColumn(
                  [
                    {
                      'textValue': translate.saturday,
                      'firstDayOfExperiment': false,
                      'lastDayOfExperiment': false,
                      'isTitle': true,
                      'greyValue': false,
                      'inExperiment': false,
                      'isCompleted': false,
                      'isMissing': false,
                      'isToday': false
                    },
                    calendarValues[5],
                    calendarValues[12],
                    calendarValues[19],
                    calendarValues[26],
                    calendarValues[33],
                    calendarValues[40]
                  ],
                  activityScopedModel: activityModel,
                  dateSelected: widget.dateSelected,
                ),
                _CalenderColumn(
                  [
                    {
                      'textValue': translate.sunday,
                      'firstDayOfExperiment': false,
                      'lastDayOfExperiment': false,
                      'isTitle': true,
                      'greyValue': false,
                      'inExperiment': false,
                      'isCompleted': false,
                      'isMissing': false,
                      'isToday': false
                    },
                    calendarValues[6],
                    calendarValues[13],
                    calendarValues[20],
                    calendarValues[27],
                    calendarValues[34],
                    calendarValues[41]
                  ],
                  activityScopedModel: activityModel,
                  dateSelected: widget.dateSelected,
                ),
              ],
            )
          ],
        ),
      );
    });
  }
}

//This widget generates a single column of text widgets, to be used in the calander widget.
//This widget requires a list of maps: textValue, greyValue, experimentValue, pastValue, completedValue, todayValue
//The text value determines the text, the bool value determines whether the text should be displayed in bold.
class _CalenderColumn extends StatelessWidget {
  const _CalenderColumn(this.columnValues,
      {@required this.activityScopedModel, @required this.dateSelected});

  final List<Map> columnValues;
  final AgendasScopedModel activityScopedModel;
  final ValueChanged<DateTime> dateSelected;

  List<Color> getBackgroundColor(Map value) {
    if (value['firstDayOfExperiment'] == true) {
      return [ColorPallet.veryLightBlue, Colors.transparent];
    } else if (value['lastDayOfExperiment'] == true) {
      return [Colors.transparent, ColorPallet.veryLightBlue];
    } else if (value['inExperiment'] == true) {
      return [ColorPallet.veryLightBlue, ColorPallet.veryLightBlue];
    } else {
      return [Colors.transparent, Colors.transparent];
    }
  }

  Color getCircleColor(Map value) {
    if (value['isCompleted'] == true) {
      return ColorPallet.lightGreen;
    } else if (value['isToday'] == true) {
      return ColorPallet.primaryColor;
    } else if (value['isMissing'] == true) {
      return ColorPallet.orange;
    } else if (value['inExperiment'] == true) {
      return ColorPallet.veryLightBlue;
    } else {
      return Colors.transparent;
    }
  }

  Color getTextColor(Map value) {
    if (value['isCompleted'] == true && value['isToday'] == true) {
      return Colors.white;
    }
    if (value['greyValue'] == true &&
        (value['isCompleted'] == true || value['isMissing'] == true)) {
      return ColorPallet.veryLightGray;
    } else if (value['isToday'] == true) {
      return ColorPallet.darkTextColor;
    } else if (value['greyValue'] == true) {
      return ColorPallet.midGray;
    } else {
      return ColorPallet.darkTextColor;
    }
  }

  @override
  Widget build(BuildContext context) {
    final textWidgets = columnValues.map((Map value) {
      return Container(
        width: 56.0 * x,
        decoration: value['isTitle'] == true
            ? const BoxDecoration(
                border: Border(
                  bottom: BorderSide(color: ColorPallet.lightGray),
                ),
              )
            : const BoxDecoration(),
        child: Container(
          width: 30.0 * x,
          height: 38.0 * y,
          decoration: BoxDecoration(
            gradient: LinearGradient(
                colors: getBackgroundColor(value),
                begin: Alignment.centerRight,
                end: Alignment.centerLeft,
                stops: const [0.5, 0.5]),
          ),
          child: InkWell(
            onTap: () async {
              if (value['isTitle'] == false) {
                final picked = DateTime(value['year'] as int,
                    value['month'] as int, value['day'] as int);

                final InputType inputType = Configuration.of(context).inputType;

                await activityScopedModel.load(
                    Configuration.of(context).mainActivityTree.name,
                    Configuration.of(context).sideActivityTree.name,
                    picked,
                    inputType);
                ParaDataScopedModel.of(context)
                    .onTap('InkWell', 'open day $picked');
                dateSelected(picked);
              }
            },
            child: Container(
              height: 30.0 * y,
              width: 30.0 * x,
              margin: EdgeInsets.symmetric(vertical: 1.0 * y),
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: getCircleColor(value),
              ),
              child: Center(
                child: Text(
                  value['textValue'] as String,
                  style: TextStyle(
                      fontWeight: value['isTitle'] == true
                          ? FontWeight.w800
                          : FontWeight.w700,
                      fontSize: 15.0 * f,
                      color: getTextColor(value)),
                ),
              ),
            ),
          ),
        ),
      );
    }).toList();

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: textWidgets,
    );
  }
}
