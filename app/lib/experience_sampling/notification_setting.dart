import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:tbo_app/core/database/table/experience_sampling_table.dart';
import 'package:tbo_app/experience_sampling/notification.dart';
import 'package:tbo_app/experience_sampling/schedular.dart';

Future<void> initializeES() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  final bool isInitialized = prefs.getBool('isESInitialized') ?? false;
  if (isInitialized == false) {
    scheduleESNotifcations();
    prefs.setBool('isESInitialized', true);
    prefs.setBool('experienceSamplingSetting', true);
  }
}

void scheduleESNotifcations() {
  if (Platform.isIOS) {
    requestIOSPermissions();
  }
  createNotificationSchedule();
}

void removeESNotifications() {
  flutterLocalNotificationsPlugin.cancelAll();
  ExperienceSamplingDb.removeEsTimings();
}
