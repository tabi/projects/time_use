import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tbo_app/core/app_colors.dart';
import 'package:tbo_app/core/color_pallet.dart';
import 'package:tbo_app/core/database/table/experience_sampling_table.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:toast/toast.dart';

class QuestionairAnswers extends Model {
  int happy, energetic, relaxed, cheerful;
  bool socialMedia, texting, games, newsOnline, readingPaper;

  bool feelingQuestionairIsCompleted() {
    if (happy != null &&
        energetic != null &&
        relaxed != null &&
        cheerful != null) {
      return true;
    }
    return false;
  }

  void setFeelAnswer({String id, int value}) {
    if (id == 'happy') {
      happy = value;
    }
    if (id == 'energetic') {
      energetic = value;
    }
    if (id == 'relaxed') {
      relaxed = value;
    }
    if (id == 'cheerful') {
      cheerful = value;
    }
    notifyListeners();
  }

  void setActivityAnswer({String id, bool value}) {
    if (id == 'socialMedia') {
      socialMedia = value;
    }
    if (id == 'texting') {
      texting = value;
    }
    if (id == 'games') {
      games = value;
    }
    if (id == 'newsOnline') {
      newsOnline = value;
    }
    if (id == 'readingPaper') {
      readingPaper = value;
    }
    notifyListeners();
  }

  static QuestionairAnswers of(BuildContext context) =>
      ScopedModel.of<QuestionairAnswers>(context);
}

class QuestionnairePage extends StatelessWidget {
  const QuestionnairePage(this.notificationParameters);

  final String notificationParameters;

  Future<void> setQuestionairFilled(String id) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(id, true);
  }

  @override
  Widget build(BuildContext context) {
    final parameters = json.decode(notificationParameters);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: ScopedModel<QuestionairAnswers>(
        model: QuestionairAnswers(),
        child: Scaffold(
          body: Container(
            height: MediaQuery.of(context).size.height,
            color: AppColors.appColor(enumColor.COL_BLUE_MIDDLE),
            child: SafeArea(
              child: Stack(
                children: <Widget>[
                  SizedBox(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    child: Image.asset(
                      'assets/images/questionnaire_background.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                  Positioned(
                    top: 52 * y,
                    child: Column(
                      children: <Widget>[
                        if (parameters['questionairType'] == 'feelings')
                          QuestionnaireBoxFeelings()
                        else
                          QuestionnaireBoxActivities(),
                        SizedBox(height: 12 * y),
                        ScopedModelDescendant<QuestionairAnswers>(
                          builder: (_, child, model) {
                            return ButtonTheme(
                              minWidth: 150 * x,
                              child: RaisedButton(
                                onPressed: () {
                                  if (parameters['questionairType'] ==
                                      'feelings') {
                                    if (model.feelingQuestionairIsCompleted()) {
                                      ExperienceSamplingDb.addQuestionairResult(
                                        sendTime: parameters['date'] as String,
                                        readTime: DateTime.now().toString(),
                                        isFirstAttempt:
                                            parameters['notificationType'] ==
                                                    'secondAttempt'
                                                ? 1
                                                : 0,
                                        isFeelingQuestionair: 1,
                                        happy: model.happy,
                                        energetic: model.energetic,
                                        relaxed: model.relaxed,
                                        cheerful: model.cheerful,
                                      );
                                      setQuestionairFilled(
                                          parameters['date'] as String);
                                      Navigator.of(context).pop();
                                    } else {
                                      Toast.show(
                                          'Beantwoord alstublieft eerst alle vragen.',
                                          context,
                                          duration: Toast.LENGTH_LONG,
                                          gravity: Toast.BOTTOM);
                                    }
                                  } else {
                                    ExperienceSamplingDb.addQuestionairResult(
                                      sendTime: parameters['date'] as String,
                                      readTime: DateTime.now().toString(),
                                      isFirstAttempt:
                                          parameters['notificationType'] ==
                                                  'secondAttempt'
                                              ? 1
                                              : 0,
                                      isFeelingQuestionair: 0,
                                      socialMedia:
                                          model.socialMedia == true ? 1 : 0,
                                      texting: model.texting == true ? 1 : 0,
                                      games: model.games == true ? 1 : 0,
                                      newsOnline:
                                          model.newsOnline == true ? 1 : 0,
                                      readingPaper:
                                          model.readingPaper == true ? 1 : 0,
                                    );
                                    Navigator.of(context).pop();
                                  }
                                },
                                color: (model.feelingQuestionairIsCompleted() ||
                                        parameters['questionairType'] ==
                                            'activities')
                                    ? ColorPallet.lightGreen
                                    : ColorPallet.midGray,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                ),
                                child: Text(
                                  'Verzenden',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 15 * f),
                                ),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    top: 10 * y,
                    left:
                        (MediaQuery.of(context).size.width / 2) - (80 * x / 2),
                    child: Image.asset(
                      'assets/images/cbs_logo_circle.png',
                      width: 80 * x,
                    ),
                  ),
                  Positioned(
                    right: 19 * x,
                    top: 15 * y,
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Text(
                        'Afsluiten',
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          fontSize: 15 * f,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class QuestionnaireBoxFeelings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 373 * x,
            height: MediaQuery.of(context).size.height * 0.75,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 25.0 * x),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 37 * y,
                  ),
                  Text(
                    'Hoe voelt u zich nu?',
                    style: TextStyle(
                      color: ColorPallet.darkTextColor,
                      fontWeight: FontWeight.w800,
                      fontSize: 21 * f,
                    ),
                  ),
                  SizedBox(
                    height: 5 * y,
                  ),
                  Text(
                    'Let bij het beantwoorden van de vraag erop dat u de emotie beoordeelt aan de hand van hoe u zich nú voelt.',
                    style: TextStyle(
                      color: ColorPallet.darkTextColor,
                      fontWeight: FontWeight.w400,
                      fontSize: 15 * f,
                      fontStyle: FontStyle.italic,
                    ),
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: const <SliderOption>[
                        SliderOption('Ongelukkig', 'Gelukkig', 'happy'),
                        SliderOption('Vermoeid', 'Energiek', 'energetic'),
                        SliderOption('Gestrest', 'Ontspannen', 'relaxed'),
                        SliderOption('Somber', 'Opgewekt', 'cheerful'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class SliderOption extends StatefulWidget {
  const SliderOption(this.lowText, this.highText, this.answerId);

  final String answerId;
  final String highText;
  final String lowText;

  @override
  _SliderOptionState createState() => _SliderOptionState();
}

class _SliderOptionState extends State<SliderOption> {
  bool hasBeenTouched = false;

  double _value = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: 25 * y,
          decoration: BoxDecoration(
              border: Border.all(color: ColorPallet.greySliderBorder),
              borderRadius: BorderRadius.circular(30)),
          child: Stack(
            children: <Widget>[
              SliderTheme(
                data: SliderTheme.of(context).copyWith(
                    activeTrackColor: Colors.transparent,
                    inactiveTrackColor: Colors.transparent,
                    trackShape: const RoundedRectSliderTrackShape(),
                    trackHeight: 6.5 * y,
                    thumbShape:
                        RoundSliderThumbShape(enabledThumbRadius: 8.0 * f),
                    thumbColor: hasBeenTouched
                        ? ColorPallet.primaryColor
                        : Colors.transparent,
                    overlayColor: hasBeenTouched
                        ? ColorPallet.blueSliderPickerHalo
                        : Colors.transparent,
                    overlayShape:
                        RoundSliderOverlayShape(overlayRadius: 13.5 * f),
                    tickMarkShape: const RoundSliderTickMarkShape(),
                    activeTickMarkColor: ColorPallet.greySliderBorder,
                    inactiveTickMarkColor: ColorPallet.greySliderBorder,
                    showValueIndicator: ShowValueIndicator.never),
                child: Slider(
                  value: _value,
                  max: 4,
                  divisions: 4,
                  label: '$_value',
                  onChanged: (value) async {
                    setState(
                      () {
                        _value = value;
                        QuestionairAnswers.of(context).setFeelAnswer(
                            id: widget.answerId, value: value.round());
                      },
                    );
                    if (hasBeenTouched == false) {
                      await Future.delayed(
                          const Duration(milliseconds: 75), () {});
                      setState(() {
                        hasBeenTouched = true;
                      });
                    }
                  },
                ),
              ),
              Positioned(
                child: hasBeenTouched
                    ? Container()
                    : GestureDetector(
                        onTap: () {
                          QuestionairAnswers.of(context)
                              .setFeelAnswer(id: widget.answerId, value: 0);
                          setState(() {
                            hasBeenTouched = true;
                          });
                        },
                        child: Container(
                          width: 50 * x,
                          height: 25 * y,
                          color: Colors.transparent,
                        ),
                      ),
              ),
            ],
          ),
        ),
        Row(
          children: <Widget>[
            SizedBox(width: 21 * x),
            Container(
                width: 1.5 * x,
                height: 18 * y,
                color: ColorPallet.greySliderBorder),
            Expanded(child: Container()),
            Container(
                width: 1.5 * x,
                height: 18 * y,
                color: ColorPallet.greySliderBorder),
            SizedBox(width: 19 * x),
          ],
        ),
        Row(
          children: <Widget>[
            SizedBox(width: 5 * x),
            Text(
              widget.lowText,
              style: TextStyle(
                color: ColorPallet.darkTextColor,
                fontWeight: FontWeight.w500,
                fontSize: 15 * f,
              ),
            ),
            Expanded(child: Container()),
            Text(
              widget.highText,
              style: TextStyle(
                color: ColorPallet.darkTextColor,
                fontWeight: FontWeight.w500,
                fontSize: 15 * f,
              ),
            ),
            SizedBox(
              width: 3 * x,
            ),
          ],
        ),
      ],
    );
  }
}

class QuestionnaireBoxActivities extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            width: 373 * x,
            height: 430 * y,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 25.0 * x),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 37 * y,
                  ),
                  Text(
                    'Heeft u het afgelopen uur de volgende activiteiten gedaan:',
                    style: TextStyle(
                      color: ColorPallet.darkTextColor,
                      fontWeight: FontWeight.w800,
                      fontSize: 21 * f,
                    ),
                  ),
                  SizedBox(
                    height: 5 * y,
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: const <ActivityOption>[
                        ActivityOption('Social media bekeken', 'socialMedia'),
                        ActivityOption(
                            'Appen, sms-en of andere berichten verstuurd',
                            'texting'),
                        ActivityOption('Spelletje op de telefoon', 'games'),
                        ActivityOption(
                            'Lezen van een nieuwssite', 'newsOnline'),
                        ActivityOption(
                            'Krant of tijdschrift lezen', 'readingPaper'),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class ActivityOption extends StatefulWidget {
  const ActivityOption(this.activityText, this.activityId);

  final String activityId;
  final String activityText;

  @override
  _ActivityOptionState createState() => _ActivityOptionState();
}

class _ActivityOptionState extends State<ActivityOption> {
  bool activityValue = false;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Theme(
          data: ThemeData(unselectedWidgetColor: ColorPallet.greySliderBorder),
          child: Checkbox(
            activeColor: ColorPallet.primaryColor,
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            onChanged: (bool value) {
              setState(() {
                activityValue = value;
                QuestionairAnswers.of(context)
                    .setActivityAnswer(id: widget.activityId, value: value);
              });
            },
            value: activityValue,
          ),
        ),
        SizedBox(
          width: 270 * x,
          child: Text(
            widget.activityText,
            style: TextStyle(
              color: ColorPallet.darkTextColor,
              fontSize: 15 * f,
              fontWeight: FontWeight.w600,
            ),
          ),
        )
      ],
    );
  }
}
