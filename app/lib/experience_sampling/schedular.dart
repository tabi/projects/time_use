import 'dart:math';

import 'package:tbo_app/core/database/table/experience_sampling_table.dart';
import 'package:tbo_app/experience_sampling/notification.dart';
import 'package:timezone/timezone.dart' as tz;

const int testPeriodInDays = 14;

void createNotificationSchedule() {
  final DateTime now = DateTime.now();
  for (var day = 0; day <= testPeriodInDays; day++) {
    _createNotificationSchedule('morning', 8, 4, now, day);
    _createNotificationSchedule('afternoon', 12, 6, now, day);
    _createNotificationSchedule('evening', 18, 3, now, day);
  }
  sendNotifications();
}

void _createNotificationSchedule(String timeSlot, int startHour,
    int timeSlotDuration, DateTime now, int day) {
  final Random random = Random();
  final int timeMinute = startHour * 60 + random.nextInt(timeSlotDuration * 60);

  final DateTime notificationTime = DateTime(now.year, now.month, now.day + day,
      (timeMinute / 60).floor(), timeMinute % 60);
  // notificationTime = DateTime(now.year, now.month, now.day + day, 23, 52); // For quick debugging specify time here
  final DateTime warningTime =
      notificationTime.add(const Duration(minutes: 15));
  final DateTime secondAttemptTime =
      notificationTime.add(const Duration(minutes: 60));
  final DateTime maxSeconAttemptTime = DateTime(
    now.year,
    now.month,
    now.day + day,
    startHour + timeSlotDuration,
  );

  if (now.isBefore(notificationTime)) {
    final bool isFeelingsQuestionnaire = random.nextBool();
    ExperienceSamplingDb.addEsTiming(
        notificationTime.toString(),
        notificationTime.millisecondsSinceEpoch,
        isFeelingsQuestionnaire ? 'feelings' : 'activities',
        'firstAttempt');

    ExperienceSamplingDb.addEsTiming(
        warningTime.toString(),
        warningTime.millisecondsSinceEpoch,
        isFeelingsQuestionnaire ? 'feelings' : 'activities',
        secondAttemptTime.isBefore(maxSeconAttemptTime) ? 'warning' : 'ignore');

    ExperienceSamplingDb.addEsTiming(
        secondAttemptTime.toString(),
        secondAttemptTime.millisecondsSinceEpoch,
        isFeelingsQuestionnaire ? 'feelings' : 'activities',
        secondAttemptTime.isBefore(maxSeconAttemptTime)
            ? 'secondAttempt'
            : 'ignore');
  }
}

Future<void> sendNotifications() async {
  final List<Map<String, dynamic>> esTimings =
      await ExperienceSamplingDb.getEsTimings();

  for (final Map<String, dynamic> notification in esTimings) {
    final int notificationMilisecondTime = notification['unixTime'] as int;
    final int nowMilisecondTime = DateTime.now().millisecondsSinceEpoch;
    final int timeTillNotificationInMiliseconds =
        notificationMilisecondTime - nowMilisecondTime;

    if (notification['notificationType'] == 'firstAttempt' ||
        notification['notificationType'] == 'secondAttempt') {
      String notificationTitle;
      String notificationBody;

      if (notification['questionairType'] == 'feelings') {
        notificationTitle = 'Hoe voelt u zich?';
        notificationBody =
            'Wij stellen u 5 vragen over hoe u zich nu voelt. Het invullen duurt heel kort.';
      } else {
        notificationTitle = 'Welke activiteiten heeft u gedaan?';
        notificationBody =
            'Wij vragen u welke activiteiten u het afgelopen uur heeft gedaan. Het invullen duurt heel kort.';
      }

      zonedScheduleNotification(
        notification['id'] as int,
        tz.TZDateTime.now(tz.local)
            .add(Duration(milliseconds: timeTillNotificationInMiliseconds)),
        notificationTitle,
        notificationBody,
        notification,
      );
    }
    if (notification['notificationType'] == 'warning') {
      zonedScheduleNotification(
        notification['id'] as int,
        tz.TZDateTime.now(tz.local)
            .add(Duration(milliseconds: timeTillNotificationInMiliseconds)),
        'Vragenlijst verlopen',
        'Wij proberen het later opnieuw.',
        notification,
      );
    }
  }
}
