import 'package:flutter/widgets.dart';
import 'package:tbo_app/core/para_data_scoped_model.dart';
import 'package:tbo_app/para_data/para_data_name.dart';

class ParaDataScreen extends StatefulWidget {
  final ParaDataScopedModel model;
  final Widget child;

  const ParaDataScreen({Key key, @required this.model, @required this.child})
      : super(key: key);

  @override
  _ParaDataScreenState createState() => _ParaDataScreenState();
}

class _ParaDataScreenState extends State<ParaDataScreen> {
  @override
  void initState() {
    super.initState();
    if (widget.child is ParaDataName) {
      widget.model.openScreen((widget.child as ParaDataName).name);
    } else {
      widget.model.openScreen(widget.child.runtimeType.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }

  @override
  void dispose() {
    if (widget.child is ParaDataName) {
      widget.model.closeScreen((widget.child as ParaDataName).name);
    } else {
      widget.model.closeScreen(widget.child.runtimeType.toString());
    }
    super.dispose();
  }
}
