// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'para_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ParaData> _$paraDataSerializer = new _$ParaDataSerializer();

class _$ParaDataSerializer implements StructuredSerializer<ParaData> {
  @override
  final Iterable<Type> types = const [ParaData, _$ParaData];
  @override
  final String wireName = 'ParaData';

  @override
  Iterable<Object> serialize(Serializers serializers, ParaData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'timestamp',
      serializers.serialize(object.timestamp,
          specifiedType: const FullType(DateTime)),
      'objectName',
      serializers.serialize(object.objectName,
          specifiedType: const FullType(String)),
      'action',
      serializers.serialize(object.action,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  ParaData deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ParaDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'timestamp':
          result.timestamp = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'objectName':
          result.objectName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'action':
          result.action = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ParaData extends ParaData {
  @override
  final DateTime timestamp;
  @override
  final String objectName;
  @override
  final String action;

  factory _$ParaData([void Function(ParaDataBuilder) updates]) =>
      (new ParaDataBuilder()..update(updates)).build();

  _$ParaData._({this.timestamp, this.objectName, this.action}) : super._() {
    if (timestamp == null) {
      throw new BuiltValueNullFieldError('ParaData', 'timestamp');
    }
    if (objectName == null) {
      throw new BuiltValueNullFieldError('ParaData', 'objectName');
    }
    if (action == null) {
      throw new BuiltValueNullFieldError('ParaData', 'action');
    }
  }

  @override
  ParaData rebuild(void Function(ParaDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ParaDataBuilder toBuilder() => new ParaDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ParaData &&
        timestamp == other.timestamp &&
        objectName == other.objectName &&
        action == other.action;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, timestamp.hashCode), objectName.hashCode), action.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ParaData')
          ..add('timestamp', timestamp)
          ..add('objectName', objectName)
          ..add('action', action))
        .toString();
  }
}

class ParaDataBuilder implements Builder<ParaData, ParaDataBuilder> {
  _$ParaData _$v;

  DateTime _timestamp;
  DateTime get timestamp => _$this._timestamp;
  set timestamp(DateTime timestamp) => _$this._timestamp = timestamp;

  String _objectName;
  String get objectName => _$this._objectName;
  set objectName(String objectName) => _$this._objectName = objectName;

  String _action;
  String get action => _$this._action;
  set action(String action) => _$this._action = action;

  ParaDataBuilder();

  ParaDataBuilder get _$this {
    if (_$v != null) {
      _timestamp = _$v.timestamp;
      _objectName = _$v.objectName;
      _action = _$v.action;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ParaData other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ParaData;
  }

  @override
  void update(void Function(ParaDataBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ParaData build() {
    final _$result = _$v ??
        new _$ParaData._(
            timestamp: timestamp, objectName: objectName, action: action);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
