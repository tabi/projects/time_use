import 'package:flutter/widgets.dart';
import 'package:tbo_app/core/para_data_scoped_model.dart';

class ParaDataNavigatorObserver extends NavigatorObserver {
  final ParaDataScopedModel model;

  ParaDataNavigatorObserver(this.model);

  @override
  void didPop(Route<dynamic> route, Route<dynamic> previousRoute) {
    model.closeScreen(route.settings.name);
  }

  @override
  void didPush(Route<dynamic> route, Route<dynamic> previousRoute) {
    model.openScreen(route.settings.name);
  }

  @override
  void didRemove(Route<dynamic> route, Route<dynamic> previousRoute) {
    model.closeScreen(route.settings.name);
  }

  @override
  void didReplace({Route<dynamic> newRoute, Route<dynamic> oldRoute}) {
    model.closeScreen(oldRoute.settings.name);
    model.openScreen(newRoute.settings.name);
  }

  @override
  void didStartUserGesture(Route<dynamic> route, Route<dynamic> previousRoute) {
    // NOOP
  }

  @override
  void didStopUserGesture() {
    // NOOP
  }
}
