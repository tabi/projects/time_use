import 'dart:io' show Platform;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tbo_app/core/app_colors.dart';
import 'package:tbo_app/core/responsive_ui.dart';

class IntroSlider extends StatefulWidget {
  @override
  _IntroSliderState createState() => _IntroSliderState();
}

class _IntroSliderState extends State<IntroSlider> {
  final int _animationMilliseconds = 100;
  int _pageIndex = 0;
  bool _slideForward = true;
  double _initialSwipePosition;
  double _distanceSwiped;

  List<Widget> pages = [
    const IntroSliderPage1(key: ValueKey(0)),
    const IntroSliderPage2(key: ValueKey(1)),
    const IntroSliderPage3(key: ValueKey(2)),
  ];

  @override
  void initState() {
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        statusBarColor: AppColors.appColor(enumColor.COL_OFF_WHITE),
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    if (Platform.isAndroid) {
      SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(
          statusBarColor: AppColors.appColor(enumColor.COL_BLUE_MIDDLE),
          statusBarBrightness: Brightness.light,
          statusBarIconBrightness: Brightness.light,
        ),
      );
    } else {
      SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(
          statusBarColor: AppColors.appColor(enumColor.COL_BLUE_MIDDLE),
        ),
      );
    }
  }

  void _clickRadiobutton(int radioIndex) {
    if (radioIndex > _pageIndex) {
      _slideForward = true;
    } else {
      _slideForward = false;
    }
    setState(() {
      _pageIndex = radioIndex;
    });
  }

  void _nextPage() {
    if (_pageIndex != pages.length - 1) {
      setState(() {
        _slideForward = true;
        _pageIndex += 1;
      });
    }
  }

  void _previousPage() {
    if (_pageIndex != 0) {
      setState(() {
        _slideForward = false;
        _pageIndex -= 1;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: GestureDetector(
          onPanStart: (DragStartDetails details) {
            _initialSwipePosition = details.globalPosition.dx;
          },
          onPanUpdate: (DragUpdateDetails details) {
            _distanceSwiped = details.globalPosition.dx - _initialSwipePosition;
          },
          onPanEnd: (DragEndDetails details) {
            _initialSwipePosition = 0.0;
            if (_distanceSwiped > 50) {
              _previousPage();
            }
            if (_distanceSwiped < 50) {
              _nextPage();
            }
          },
          child: Column(
            children: [
              SizedBox(
                height: 500 * y,
                child: AnimatedSwitcher(
                  duration: Duration(milliseconds: _animationMilliseconds),
                  transitionBuilder:
                      (Widget child, Animation<double> animation) {
                    final inAnimation = Tween<Offset>(
                            begin: Offset(_slideForward ? 1.0 : -1.0, 0.0),
                            end: const Offset(0.0, 0.0))
                        .animate(animation);
                    final outAnimation = Tween<Offset>(
                            begin: Offset(_slideForward ? -1.0 : 1.0, 0.0),
                            end: const Offset(0.0, 0.0))
                        .animate(animation);

                    if (child.key == ValueKey(_pageIndex)) {
                      return ClipRect(
                        child: SlideTransition(
                          position: inAnimation,
                          child: child,
                        ),
                      );
                    } else {
                      return ClipRect(
                        child: SlideTransition(
                          position: outAnimation,
                          child: child,
                        ),
                      );
                    }
                  },
                  child: pages[_pageIndex],
                ),
              ),
              Expanded(
                child: Container(),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  InkWell(
                    onTap: () {
                      if (_pageIndex == 0) {
                        Navigator.of(context).pop();
                      } else {
                        _previousPage();
                      }
                    },
                    child: SizedBox(
                      width: 100 * x,
                      child: Center(
                        child: Text(
                          _pageIndex == 0 ? 'OVERSLAAN' : 'VORIGE',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 14 * f,
                            color:
                                AppColors.appColor(enumColor.COL_BLUE_DARKER),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Theme(
                    data: Theme.of(context).copyWith(
                      unselectedWidgetColor:
                          AppColors.appColor(enumColor.COL_BLUE_DARKER)
                              .withOpacity(0.85),
                      disabledColor:
                          AppColors.appColor(enumColor.COL_BLUE_DARKER),
                    ),
                    child: Row(children: [
                      for (MapEntry entry in pages.asMap().entries)
                        Radio(
                          activeColor:
                              AppColors.appColor(enumColor.COL_BLUE_MIDDLE),
                          value: entry.key as int,
                          groupValue: _pageIndex,
                          onChanged: _clickRadiobutton,
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                        )
                    ]),
                  ),
                  InkWell(
                    onTap: () {
                      if (_pageIndex == pages.length - 1) {
                        Navigator.of(context).pop();
                      } else {
                        _nextPage();
                      }
                    },
                    child: SizedBox(
                      width: 100 * x,
                      child: Center(
                        child: Text(
                          _pageIndex == pages.length - 1
                              ? 'AFSLUITEN'
                              : 'VOLGENDE',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 14 * f,
                            color:
                                AppColors.appColor(enumColor.COL_BLUE_DARKER),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 6 * y,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class IntroSliderPage1 extends StatelessWidget {
  const IntroSliderPage1({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 20 * y),
        Row(
          children: [
            SizedBox(width: 20 * x),
            Text(
              'Introductie',
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 24 * f,
                color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
              ),
            ),
          ],
        ),
        SizedBox(height: 90 * y),
        Image.asset(
          'assets/images/tbo_slider_1.png',
          width: 220 * x,
        ),
        SizedBox(height: 20 * y),
        SizedBox(
          width: 300 * x,
          child: Text(
            'Het noteren van uw tijdbesteding',
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 20 * f,
              color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
            ),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: 6 * y),
        SizedBox(
          width: 300 * x,
          child: Text(
            'Let bij het beantwoorden van de vraag erop dat u de emotie beoordeeld aan de van hun hoe u zich nu voelt.',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 15 * f,
              color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
            ),
            textAlign: TextAlign.center,
          ),
        )
      ],
    );
  }
}

class IntroSliderPage2 extends StatelessWidget {
  const IntroSliderPage2({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 135 * y,
          width: MediaQuery.of(context).size.width,
        ),
        Image.asset(
          'assets/images/tbo_slider_2.png',
          width: 220 * x,
        ),
        SizedBox(height: 20 * y),
        SizedBox(
          width: 300 * x,
          child: Text(
            'Hoe voelde u zich tijdens specifieke activiteiten',
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 20 * f,
              color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
            ),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: 6 * y),
        SizedBox(
          width: 300 * x,
          child: Text(
            'Het gaat er niet om dat u samen iets deed, maar welke bekenden er bij u in de buurt waren (in dezelfde ruimte, op dezelfde plek).',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 15 * f,
              color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
            ),
            textAlign: TextAlign.center,
          ),
        )
      ],
    );
  }
}

class IntroSliderPage3 extends StatelessWidget {
  const IntroSliderPage3({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 135 * y,
          width: MediaQuery.of(context).size.width,
        ),
        Image.asset(
          'assets/images/tbo_slider_3.png',
          width: 220 * x,
        ),
        SizedBox(height: 20 * y),
        SizedBox(
          width: 300 * x,
          child: Text(
            'Krijg inzichten in uw tijdsbesteding',
            style: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: 20 * f,
              color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
            ),
            textAlign: TextAlign.center,
          ),
        ),
        SizedBox(height: 6 * y),
        SizedBox(
          width: 300 * x,
          child: Text(
            'Als tijdens een activiteit mensen bij komen of weg gaan, geef dan aan wie er het grootste deel van deze tijd bij was',
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 15 * f,
              color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
            ),
            textAlign: TextAlign.center,
          ),
        )
      ],
    );
  }
}
