import 'package:flutter/material.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/core/spotlight_tutorial/animated_focus_light.dart';
import 'package:tbo_app/core/spotlight_tutorial/content_target.dart';
import 'package:tbo_app/core/spotlight_tutorial/target_focus.dart';
import 'package:tbo_app/core/translate.dart';

List<TargetFocus> targets = <TargetFocus>[];

TextStyle titleStyle = TextStyle(
  fontWeight: FontWeight.w700,
  color: Colors.white,
  fontSize: 22.0 * f,
  height: 1.4,
);
TextStyle bodyStyle = TextStyle(
  fontWeight: FontWeight.w400,
  color: Colors.white,
  fontSize: 15.0 * f,
  height: 1.4,
);

void initTargets(GlobalKey keyButton1, GlobalKey keyButton2,
    GlobalKey keyButton3, GlobalKey keyButton4, GlobalKey keyButton5) {
  if (targets.isEmpty) {
    targets.add(
      TargetFocus(
        identify: 'normal',
        keyTarget: keyButton1,
        contents: [
          ContentTarget(
              child: Column(
            children: <Widget>[
              SizedBox(height: 10 * y),
              Text(
                translate.mainInsights1,
                style: titleStyle,
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0 * y),
                child: Text(
                  translate.mainInsights2,
                  style: bodyStyle,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 290 * y),
              Column(
                children: <Widget>[
                  Image.asset(
                    'images/tab_symbol.png',
                    height: 100 * y,
                  ),
                  SizedBox(height: 20 * y),
                  Text(
                    translate.swipeToNavigate,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w500,
                        fontSize: 20 * f),
                  ),
                ],
              ),
            ],
          ))
        ],
        shape: ShapeLightFocus.rrect,
      ),
    );

    targets.add(
      TargetFocus(
        identify: 'normal',
        keyTarget: keyButton2,
        contents: [
          ContentTarget(
              child: Column(
            children: <Widget>[
              SizedBox(height: 10 * y),
              Text(
                translate.mainInsights3,
                style: titleStyle,
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0 * y),
                child: Text(
                  translate.mainInsights4,
                  style: bodyStyle,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 30 * y),
            ],
          ))
        ],
        shape: ShapeLightFocus.rrect,
      ),
    );

    targets.add(
      TargetFocus(
        identify: 'normal',
        keyTarget: keyButton3,
        contents: [
          ContentTarget(
              child: Column(
            children: <Widget>[
              SizedBox(height: 10 * y),
              Text(
                translate.mainInsights5,
                style: titleStyle,
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0 * y),
                child: Text(
                  translate.mainInsights6,
                  style: bodyStyle,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 30 * y),
            ],
          ))
        ],
        shape: ShapeLightFocus.rrect,
      ),
    );

    targets.add(
      TargetFocus(
        identify: 'blue',
        keyTarget: keyButton4,
        contents: [
          ContentTarget(
              child: Column(
            children: <Widget>[
              SizedBox(height: 10 * y),
              Text(
                translate.mainInsights7,
                style: titleStyle,
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: EdgeInsets.only(top: 10.0 * y),
                child: Text(
                  translate.mainInsights8,
                  style: bodyStyle,
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 30 * y),
            ],
          ))
        ],
        shape: ShapeLightFocus.rrect,
      ),
    );

    targets.add(
      TargetFocus(
        identify: 'normal',
        keyTarget: keyButton5,
        contents: [
          ContentTarget(
              align: AlignContent.top,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 10 * y),
                  Text(
                    translate.mainInsights9,
                    style: titleStyle,
                    textAlign: TextAlign.center,
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 10.0 * y),
                    child: Text(
                      translate.mainInsights10,
                      style: bodyStyle,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 30 * y),
                ],
              ))
        ],
        shape: ShapeLightFocus.rrect,
      ),
    );
  }
}

void showTutorial(BuildContext context) {
  // SystemChrome.setSystemUIOverlayStyle(
  //   SystemUiOverlayStyle(statusBarColor: ColorPallet.darkTextColor),
  // );
  // showGestureLogo = true;
  // TutorialCoachMark(
  //   context,
  //   targets: targets,
  //   colorShadow: ColorPallet.darkTextColor,
  //   textSkip: translate.skip,
  //   paddingFocus: 10,
  //   textStyleSkip: TextStyle(color: Colors.white, fontSize: 16 * f),
  //   alignSkip: Alignment.topLeft,
  //   opacityShadow: .98,
  //   clickSkip: () {
  //     showGestureLogo = false;
  //     SystemChrome.setSystemUIOverlayStyle(
  //       SystemUiOverlayStyle(statusBarColor: ColorPallet.primaryColor),
  //     );
  //   },
  //   finish: () {
  //     SystemChrome.setSystemUIOverlayStyle(
  //       SystemUiOverlayStyle(statusBarColor: ColorPallet.primaryColor),
  //     );
  //   },
  //   clickTarget: (TargetFocus target) {
  //     showGestureLogo = false;
  //   },
  // )..show();
}
