import 'package:flutter/material.dart';
import 'package:tbo_app/activities/translate_page_activity_agenda.dart';

class IncompleteInsightsDialog extends StatelessWidget {
  const IncompleteInsightsDialog({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _translate = TranslatePageActivityAgenda.instance;
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 20,
                ),
                const Text(
                  'Het overzicht is nog niet beschikbaar.',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18.0,
                    height: 1.25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                const Text(
                  'Maak eerst alle dagen compleet om het overzicht te kunnen bekijken.',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18.0,
                    height: 1.25,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: <Widget>[
                    const Spacer(),
                    FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop(false);
                      },
                      child: Text(
                        _translate.general.ok,
                        style: const TextStyle(
                          color: Colors.blue,
                          fontSize: 18.0,
                          height: 1.25,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
