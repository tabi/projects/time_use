import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tbo_app/activities/activity_node.dart';
import 'package:tbo_app/core/color_pallet.dart';

class BarChart extends StatefulWidget {
  final Map<ActivityNode, Map<DateTime, Duration>> dailyDurationPerActivityNode;
  final DateTime startDate;
  final DateTime endDate;

  const BarChart(
      {this.startDate, this.endDate, this.dailyDurationPerActivityNode});

  @override
  _BarChartState createState() => _BarChartState();
}

class _BarChartState extends State<BarChart> {
  List<charts.Series<ActivityDuration, String>> data = [];
  final _background = Colors.blueGrey[100];

  @override
  void initState() {
    super.initState();
    final _dateFormat = DateFormat('dd-MM');
    data = widget.dailyDurationPerActivityNode.keys.map((activityNode) {
      final _activityDurations =
          widget.dailyDurationPerActivityNode[activityNode].keys.map((date) {
        final _duration =
            widget.dailyDurationPerActivityNode[activityNode][date];
        return ActivityDuration(
            _dateFormat.format(date), _duration, activityNode);
      }).toList();
      return charts.Series<ActivityDuration, String>(
        id: activityNode.activity,
        domainFn: (ActivityDuration activityDuration, _) =>
            activityDuration.date,
        measureFn: (ActivityDuration activityDuration, _) =>
            activityDuration.hours,
        data: _activityDurations,
        colorFn: (_, __) => _colorForActivityNode(activityNode),
        fillColorFn: (_, __) => _colorForActivityNode(activityNode),
      );
    }).toList();
  }

  charts.Color _colorForActivityNode(ActivityNode node) {
    if (node.parentId == -100) {
      return charts.Color(
          r: _background.red, g: _background.green, b: _background.blue);
    } else {
      return charts.Color(
          r: node.colorActivity().red,
          g: node.colorActivity().green,
          b: node.colorActivity().blue);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.only(top: 5),
          child: charts.BarChart(
            data,
            animate: true,
            barGroupingType: charts.BarGroupingType.stacked,
            primaryMeasureAxis: charts.NumericAxisSpec(
              viewport: const charts.NumericExtents(0, 100),
              tickFormatterSpec: charts.BasicNumericTickFormatterSpec(
                  (num value) => '${(24 / 100 * value).toInt()}hh'),
              tickProviderSpec: const charts.BasicNumericTickProviderSpec(
                  desiredMinTickCount: 5),
            ),
            domainAxis: charts.OrdinalAxisSpec(
              renderSpec: charts.SmallTickRendererSpec(
                labelStyle: charts.TextStyleSpec(
                    fontFamily: 'Source Sans Pro Bold',
                    color: charts.Color(
                        r: ColorPallet.darkTextColor.red,
                        g: ColorPallet.darkTextColor.green,
                        b: ColorPallet.darkTextColor.blue,
                        a: ColorPallet.darkTextColor.alpha),
                    fontSize: 14),
              ),
            ),
            selectionModels: [
              charts.SelectionModelConfig(
                changedListener: (_) {},
              )
            ],
          ),
        ),
      ],
    );
  }
}

class ActivityDuration {
  static const _dayDuration = Duration(days: 1);
  final ActivityNode activityNode;
  final Duration duration;
  final String date;
  int get hours {
    return (100 / _dayDuration.inSeconds * duration.inSeconds).round().toInt();
  }

  ActivityDuration(this.date, this.duration, this.activityNode);
}
