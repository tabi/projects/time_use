import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tbo_app/activities/activity_node.dart';
import 'package:tbo_app/activities/activity_tree_scoped_model.dart';
import 'package:tbo_app/core/app_icons.dart';
import 'package:tbo_app/core/color_pallet.dart';
import 'package:tbo_app/core/configuration.dart';
import 'package:tbo_app/core/database/user_progress_database.dart';
import 'package:tbo_app/core/para_data_scoped_model.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/core/translate.dart';
import 'package:tbo_app/insights/bar_chart.dart';
import 'package:tbo_app/insights/donut_chart.dart';
import 'package:tbo_app/insights/insights_page_tutorial.dart';
import 'package:tbo_app/insights/insights_scoped_model.dart';
import 'package:tbo_app/para_data/para_data_name.dart';

GlobalKey keyButton1 = GlobalKey();
GlobalKey keyButton2 = GlobalKey();
GlobalKey keyButton3 = GlobalKey();
GlobalKey keyButton4 = GlobalKey();
GlobalKey keyButton5 = GlobalKey();

enum ChartType { category, time }

class InsightsPage extends StatefulWidget with ParaDataName {
  @override
  State<StatefulWidget> createState() {
    return _InsightsPageState();
  }
}

class _InsightsPageState extends State<InsightsPage>
    with SingleTickerProviderStateMixin {
      
  TabController _controller;
  ChartType _chartType = ChartType.category;
  int _selectedDay = 0;
  final _tabs = {0: ChartType.category, 1: ChartType.time};

  @override
  void initState() {
    _controller = TabController(length: 2, vsync: this);
    _controller.addListener(() {
      setState(() {
        if (_tabs[_controller.index] != _chartType) {
          _chartType = _tabs[_controller.index];
          ParaDataScopedModel.of(context).onTap(
              _controller.index == 0 ? 'Pie chart' : 'Balkdiagram', 'openTab');
        }
      });
    });
    initTargets(keyButton1, keyButton2, keyButton3, keyButton4, keyButton5);
    super.initState();

    Future.delayed(const Duration(milliseconds: 400), () {
      showInitialTutorial(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(
          translate.insights,
          style: const TextStyle(
              color: Colors.white, fontSize: 22.0, fontWeight: FontWeight.w600),
        ),
        bottom: TabBar(
          controller: _controller,
          tabs: [
            Tab(
              child: Row(
                children: [
                  Icon(
                    AppIcons.appIconData(enumIcon.ICO_PIE_CHART),
                  ),
                  const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'Pie chart',
                      style: TextStyle(fontWeight: FontWeight.w600),
                    ),
                  )
                ],
              ),
            ),
            Tab(
              child: Row(
                children: [
                  Icon(
                    AppIcons.appIconData(enumIcon.ICO_BAR_CHART),
                  ),
                  const Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Text(
                      'Balkdiagram',
                      style: TextStyle(fontWeight: FontWeight.w600),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      resizeToAvoidBottomInset: false,
      body: Column(
        children: <Widget>[
          const SizedBox(height: 4),
          Column(
            key: keyButton4,
            children: <Widget>[
              const Align(
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text('Hoofdcategorieën',
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: ColorPallet.darkTextColor,
                          fontWeight: FontWeight.w700,
                          fontSize: 17.0)),
                ),
              ),
              if (_chartType == ChartType.category)
                _buildDonutChartCategoryList(context)
              else
                _buildBarChartCategoryList(context),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 32.0),
            child: _daysLabel(context),
          ),
          Container(
            key: keyButton5,
            child: _chartType == ChartType.category
                ? _buildDonutChartWidget(context)
                : _buildBarChartWidget(context),
          ),
        ],
      ),
    );
  }

  Widget _buildDonutChartCategoryList(BuildContext context) {
    final _treeCode = Configuration.of(context).mainActivityTree.name;
    final ScrollController _arrowsController = ScrollController();
    final topLevelNodes = ActivityTreeScopedModel.of(context)
        .mainActivityTree
        .activityTree()
        .where((element) => element.level == 0)
        .toList();
    return FutureBuilder<Map<ActivityNode, Duration>>(
        future: InsightsScopedModel.of(context).durationsForActivities(
            _treeCode,
            topLevelNodes,
            UserProgressDatabase().getDaysOfExperiment(),
            _selectedDay),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final durationPerNode = snapshot.data;
            return ScopedModelDescendant<InsightsScopedModel>(
                builder: (context, child, insightsModel) {
              return SizedBox(
                height: 63,
                width: MediaQuery.of(context).size.width,
                child: ListView.builder(
                  controller: _arrowsController,
                  scrollDirection: Axis.horizontal,
                  itemCount: topLevelNodes.length,
                  itemBuilder: (BuildContext context, int index) {
                    final node = durationPerNode.keys.toList()[index];
                    return Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        _buildCategoryWidgetVertical(
                            context, node, durationPerNode[node], onTap: () {
                          // _selectedNode = node; //TODO: can this be removed?
                        }),
                      ],
                    );
                  },
                ),
              );
            });
          } else {
            return Container(
              height: 63,
            );
          }
        });
  }

  Widget _buildBarChartCategoryList(BuildContext context) {
    final _treeCode = Configuration.of(context).mainActivityTree.name;
    final ScrollController _arrowsController = ScrollController();
    final topLevelNodes = ActivityTreeScopedModel.of(context)
        .mainActivityTree
        .activityTree()
        .where((element) => element.level == 0)
        .toList();
    return FutureBuilder<Map<ActivityNode, Duration>>(
        future: InsightsScopedModel.of(context).totalDurationsForActivities(
            _treeCode,
            topLevelNodes,
            UserProgressDatabase().getDaysOfExperiment()),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final durationPerNode = snapshot.data;
            return ScopedModelDescendant<InsightsScopedModel>(
                builder: (context, child, insightsModel) {
              return Container(
                height: 63,
                width: MediaQuery.of(context).size.width,
                child: ListView.builder(
                  controller: _arrowsController,
                  scrollDirection: Axis.horizontal,
                  itemCount: topLevelNodes.length,
                  itemBuilder: (BuildContext context, int index) {
                    final node = durationPerNode.keys.toList()[index];
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        _buildCategoryWidgetVertical(
                            context, node, durationPerNode[node], onTap: () {
                          // _selectedNode = node; //TODO: can this be removed?
                        }),
                      ],
                    );
                  },
                ),
              );
            });
          } else {
            return Container(
              height: 63,
            );
          }
        });
  }

  Widget _buildCategoryWidgetVertical(
      BuildContext context, ActivityNode node, Duration duration,
      {VoidCallback onTap}) {
    final _numberFormat = NumberFormat.decimalPattern();
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 4),
      height: 55,
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
        ),
        color: Colors.white,
        onPressed: onTap,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Icon(
              node.iconData(),
              color: node.colorActivity(),
              size: 25,
            ),
            SizedBox(width: 15 * x),
            Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(node.activity,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                        color: ColorPallet.darkTextColor,
                        fontWeight: FontWeight.w600,
                        fontSize: 16.0 * f)),
                Text(
                    '${_numberFormat.format(duration.inHours)}u ${_numberFormat.format(duration.inMinutes % 60)}m',
                    style: TextStyle(
                        color: ColorPallet.midGray,
                        fontWeight: FontWeight.w500,
                        fontSize: 16.0 * f))
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _daysLabel(BuildContext context) {
    if (_chartType == ChartType.category) {
      final _controller = PageController(viewportFraction: 0.4);

      return FutureBuilder<List<DateTime>>(
          future: UserProgressDatabase().getDaysOfExperiment(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              final List<DateTime> _dates = snapshot.data;
              return SizedBox(
                height: 63,
                child: PageView.builder(
                  onPageChanged: (page) {
                    setState(() {
                      _selectedDay = page;
                    });
                  },
                  controller: _controller,
                  itemCount: _dates.length,
                  itemBuilder: (BuildContext context, int index) {
                    final _date = _dates[index];
                    final _dateText =
                        DateFormat('EEEE d/MM', 'nl').format(_date);
                    final bool _isSelected = index == _selectedDay;
                    return Text(_dateText,
                        textAlign: TextAlign.center,
                        style: _isSelected
                            ? const TextStyle(
                                color: ColorPallet.darkTextColor,
                                fontWeight: FontWeight.w700,
                                fontSize: 17.0)
                            : const TextStyle(
                                fontWeight: FontWeight.w500, fontSize: 16));
                  },
                ),
              );
            } else {
              return const SizedBox();
            }
          });
    } else {
      return FutureBuilder<List<DateTime>>(
          future: UserProgressDatabase().getDaysOfExperiment(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              final List<DateTime> _dates = snapshot.data;
              final _startDate = _dates.first;
              final _endDate = _dates.last;
              final _startDateText =
                  DateFormat('EEEE d/MM', 'nl').format(_startDate);
              final _endDateText =
                  DateFormat('EEEE d/MM', 'nl').format(_endDate);
              return SizedBox(
                height: 63,
                child: Text(
                  '$_startDateText - $_endDateText',
                  style: const TextStyle(
                      fontWeight: FontWeight.w700, fontSize: 18),
                ),
              );
            } else {
              return const SizedBox();
            }
          });
    }
  }

  Widget _buildDonutChartWidget(BuildContext context) {
    return ScopedModelDescendant<InsightsScopedModel>(
        builder: (context, child, insightsScopedModel) {
      return FutureBuilder<List<DateTime>>(
          future: UserProgressDatabase().getDaysOfExperiment(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              final List<DateTime> _dates = snapshot.data;
              final _startDate =
                  _dates[_selectedDay].add(const Duration(hours: 4)).toUtc();
              final _endDate = _startDate.add(const Duration(days: 1));
              return FutureBuilder<Map<ActivityNode, Duration>>(
                  future: insightsScopedModel.durationPerActivityNodesForDate(
                      Configuration.of(context).mainActivityTree.name,
                      _startDate,
                      _endDate),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      final durationPerActivity = snapshot.data;
                      return Stack(
                        children: <Widget>[
                          SizedBox(
                              width: MediaQuery.of(context).size.width,
                              height: 274 * f,
                              child: DonutChart(
                                  durationPerActivity, _selectedActivityNode,
                                  animate: true)),
                          Center(
                            child: SizedBox(
                              height: 274 * f,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text('${translate.total}:',
                                      style: TextStyle(
                                          color: ColorPallet.darkTextColor,
                                          fontWeight: FontWeight.w800,
                                          fontSize: 17.0 * f)),
                                  SizedBox(height: 10.0 * y),
                                  Container(
                                      child: _buildDonutCenter(
                                          context, durationPerActivity)),
                                ],
                              ),
                            ),
                          )
                        ],
                      );
                    } else {
                      return const SizedBox();
                    }
                  });
            } else {
              return const SizedBox();
            }
          });
    });
  }

  Widget _buildDonutCenter(
      BuildContext context, Map<ActivityNode, Duration> durationPerActivity) {
    final _duration = durationPerActivity.isEmpty
        ? const Duration()
        : durationPerActivity.values
            .reduce((value, element) => value + element);
    return Text('${_duration.inHours}u ${_duration.inMinutes % 60}m',
        style: TextStyle(
            color: ColorPallet.darkTextColor,
            fontWeight: FontWeight.w800,
            fontSize: 19.0 * f));
  }

  Widget _buildBarChartWidget(BuildContext context) {
    return ScopedModelDescendant<InsightsScopedModel>(
        builder: (context, child, insightsScopedModel) {
      return ScopedModelDescendant<ActivityTreeScopedModel>(
          builder: (context, child, activityTreeScopedModel) {
        return FutureBuilder<List<DateTime>>(
            future: UserProgressDatabase().getDaysOfExperiment(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                final topLevelNodes = activityTreeScopedModel.mainActivityTree
                    .activityTree()
                    .where((element) => element.level == 0)
                    .toList();
                final List<DateTime> _dates = snapshot.data;
                return FutureBuilder<
                        Map<ActivityNode, Map<DateTime, Duration>>>(
                    future: insightsScopedModel
                        .dailyDurationPerActivityNodesForDate(
                            Configuration.of(context).mainActivityTree.name,
                            topLevelNodes,
                            _dates.first,
                            _dates.last),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        final dailyDurationPerActivity = snapshot.data;
                        return Container(
                          height: 223.0,
                          width: MediaQuery.of(context).size.width * 0.9,
                          margin: const EdgeInsets.symmetric(horizontal: 7.0),
                          child: BarChart(
                            startDate: _dates.first,
                            endDate: _dates.last,
                            dailyDurationPerActivityNode:
                                dailyDurationPerActivity,
                          ),
                        );
                      } else {
                        return const SizedBox();
                      }
                    });
              } else {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
            });
      });
    });
  }

  void _selectedActivityNode(ActivityNode node) {}

  Future<void> showInitialTutorial(BuildContext context) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String status = prefs.getString('mainInsightsTutorial') ?? '';
    if (status == '') {
      prefs.setString('mainInsightsTutorial', 'shown');
      showTutorial(context);
    }
  }
}
