import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'insights_visibility.g.dart';

class InsightsVisibility extends EnumClass {
  static Serializer<InsightsVisibility> get serializer =>
      _$insightsVisibilitySerializer;

  static const InsightsVisibility none = _$none;
  static const InsightsVisibility directly = _$directly;
  static const InsightsVisibility afterExperiment = _$afterExperiment;

  const InsightsVisibility._(String name) : super(name);

  static BuiltSet<InsightsVisibility> get values => _$values;

  static InsightsVisibility valueOf(String name) => _$valueOf(name);
}
