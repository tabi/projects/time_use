import 'package:built_collection/built_collection.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:tbo_app/activities/activity_node.dart';
import 'package:tbo_app/core/responsive_ui.dart';

class DonutChart extends StatefulWidget {
  const DonutChart(this.durationPerActivity, this.changeCategory,
      {this.animate});

  final bool animate;
  final Map<ActivityNode, Duration> durationPerActivity;

  final Function(ActivityNode node) changeCategory;

  @override
  _DonutChartState createState() => _DonutChartState();
}

class _DonutChartState extends State<DonutChart> {
  List<charts.Series> _seriesList;

  @override
  void initState() {
    super.initState();
    _createSeriesFromDuration();
  }

  @override
  void didUpdateWidget(DonutChart oldWidget) {
    super.didUpdateWidget(oldWidget);
    _createSeriesFromDuration();
  }

  void _createSeriesFromDuration() {
    final _durationPerActivity = widget.durationPerActivity.build().toMap();
    final remainingNode = ActivityNode.remainingActivity();
    final _duration = widget.durationPerActivity.values.isEmpty
        ? const Duration()
        : widget.durationPerActivity.values
            .reduce((value, element) => value + element);
    final _dayDuration = _duration > const Duration(days: 1)
        ? _duration
        : const Duration(days: 1);
    final _remainingDuration = _dayDuration - _duration;
    if (_remainingDuration.inSeconds > 0) {
      _durationPerActivity[remainingNode] = _remainingDuration.abs();
    }
    final _background = Colors.blueGrey[100];
    final _items = _durationPerActivity.keys
        .map(
          (node) => DonutChartItem(
              (100 /
                      _dayDuration.inSeconds *
                      _durationPerActivity[node].inSeconds)
                  .roundToDouble(),
              node.parentId == -100
                  ? charts.Color(
                      r: _background.red,
                      g: _background.green,
                      b: _background.blue)
                  : charts.Color(
                      r: node.colorActivity().red,
                      g: node.colorActivity().green,
                      b: node.colorActivity().blue),
              node),
        )
        .toList();

    _seriesList = <charts.Series<DonutChartItem, int>>[
      charts.Series<DonutChartItem, int>(
          id: 'node',
          overlaySeries: true,
          data: _items,
          domainFn: (DonutChartItem item, _) => _items.indexOf(item),
          measureFn: (DonutChartItem item, _) => item.value,
          labelAccessorFn: (DonutChartItem item, _) =>
              '${item.value.toStringAsFixed(0)}%',
          colorFn: (DonutChartItem item, _) => item.color)
    ];
  }

  void _onSelectionChanged(charts.SelectionModel model) {
    if (model.selectedDatum.isNotEmpty) {
      final _index = model.selectedDatum.first.index;
      final _node = _seriesList.first.data[_index].node as ActivityNode;
      if (-100 != _node?.parentId) {
        widget.changeCategory(_node);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return charts.PieChart(
      _seriesList ?? [],
      animate: widget.animate,
      defaultRenderer: charts.ArcRendererConfig(
        arcWidth: (30 * x).floor(),
        arcRendererDecorators: [
          charts.ArcLabelDecorator(
              labelPosition: charts.ArcLabelPosition.outside),
        ],
      ),
      selectionModels: [
        charts.SelectionModelConfig(
            type: charts.SelectionModelType.info,
            changedListener: _onSelectionChanged)
      ],
    );
  }
}

class DonutChartItem {
  final ActivityNode node;
  final double value;
  final charts.Color color;

  DonutChartItem(this.value, this.color, this.node);

  @override
  String toString() {
    return {'value': value, 'color': color, 'node': node}.toString();
  }
}
