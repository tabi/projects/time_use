import 'package:flutter/widgets.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/activities/activity.dart';
import 'package:tbo_app/activities/activity_node.dart';
import 'package:tbo_app/activities/activity_tree.dart';
import 'package:tbo_app/activities/activity_type.dart';
import 'package:tbo_app/core/database/table/activity_info_table.dart';
import 'package:tbo_app/core/extensions.dart';

class InsightsScopedModel extends Model {
  ActivityTree activityTree;

  static InsightsScopedModel of(BuildContext context) =>
      ScopedModel.of<InsightsScopedModel>(context);

  Future<Duration> _durationForDate(
      String treeCode, DateTime startDate, DateTime endDate) async {
    final List<Activity> _allActivities = await _selectMainActivities(treeCode);
    Duration activityDuration = const Duration();
    try {
      activityDuration = _allActivities
          .where((activity) =>
              (startDate.isBefore(activity.startDate) ||
                  startDate.isAtSameMomentAs(activity.startDate)) &&
              endDate.isAfter(activity.endDate))
          .map((activity) {
        return activity.endDate.difference(activity.startDate);
      }).reduce((value, element) => value + element);
    } catch (e) {
      //NOOP
    }
    return activityDuration;
  }

  Future<Duration> _remainingDurationForDate(
      String treeCode, DateTime startDate, DateTime endDate) async {
    const _dayDuration = Duration(days: 1);
    final _duration = await _durationForDate(treeCode, startDate, endDate);
    return _dayDuration - _duration;
  }

  Future<Map<ActivityNode, Map<DateTime, Duration>>>
      dailyDurationPerActivityNodesForDate(
          String treeCode,
          List<ActivityNode> activityNodes,
          DateTime startDate,
          DateTime endDate) async {
    try {
      final _dailyDurationPerActivityNode =
          await activityNodes.asyncMap((activityNode) async {
        final _durationPerDate = <DateTime, Duration>{};
        DateTime _currentDate = startDate;
        while (_currentDate.isSameOrBefore(endDate)) {
          final _currentEndDate = _currentDate.add(const Duration(days: 1));
          final _duration = await durationForActivityNode(
              treeCode, activityNode, _currentDate, _currentEndDate);
          _durationPerDate[_currentDate] = _duration;
          _currentDate = _currentEndDate;
        }
        return MapEntry(activityNode, _durationPerDate);
      });

      final _remainingDurationPerDate = <DateTime, Duration>{};
      DateTime _currentDate = startDate;
      while (_currentDate.isSameOrBefore(endDate)) {
        final _currentEndDate = _currentDate.add(const Duration(days: 1));
        final _duration = await _remainingDurationForDate(
            treeCode, _currentDate, _currentEndDate);
        _remainingDurationPerDate[_currentDate] = _duration;
        _currentDate = _currentEndDate;
      }
      final _allDurationPerActivityNode =
          _dailyDurationPerActivityNode.toList();
      _allDurationPerActivityNode.insert(
          0,
          MapEntry(
              ActivityNode.remainingActivity(), _remainingDurationPerDate));
      return Map<ActivityNode, Map<DateTime, Duration>>.fromEntries(
          _allDurationPerActivityNode);
    } catch (e) {
      //NOOP
    }
    return {};
  }

  Future<Map<ActivityNode, Duration>> durationPerActivityNodesForDate(
      String treeCode, DateTime startDate, DateTime endDate) async {
    final List<Activity> _allActivities = await _selectMainActivities(treeCode);
    try {
      final _activityNodes = _allActivities
          .where((activity) =>
              startDate.isSameOrBefore(activity.startDate) &&
              endDate.isSameOrAfter(activity.endDate))
          .map((activity) {
        return activity.activityNode;
      }).toList();
      final _singleActivityNodes = <ActivityNode>[];
      for (final _activityNode in _activityNodes) {
        if (_singleActivityNodes
            .where((element) => element.rootCode == _activityNode.rootCode)
            .isEmpty) {
          _singleActivityNodes.add(_activityNode);
        }
      }
      final _durationPerActivityNode =
          await _singleActivityNodes.asyncMap((activityNode) async {
        final _duration = await durationForActivityNode(
            treeCode, activityNode, startDate, endDate);
        return MapEntry(activityNode, _duration);
      });
      return Map<ActivityNode, Duration>.fromEntries(_durationPerActivityNode);
    } catch (e) {
      //NOOP
    }
    return {};
  }

  Future<Map<ActivityNode, Duration>> durationsForActivities(
      String treeCode,
      List<ActivityNode> activityNodes,
      Future<List<DateTime>> daysOfExperiment,
      int selectedDay) async {
    final _daysOfExperiment = await daysOfExperiment;
    final _startDate = _daysOfExperiment[selectedDay];
    final _endDate = _startDate.add(const Duration(days: 1));
    final _durationPerActivityNode = (await activityNodes.asyncMap(
            (activityNode) async => MapEntry(
                activityNode,
                await durationForActivityNode(
                    treeCode, activityNode, _startDate, _endDate))))
        .toList();
    _durationPerActivityNode.sort((lhs, rhs) {
      return lhs.value.compareTo(rhs.value) * -1;
    });
    return Map.fromEntries(_durationPerActivityNode);
  }

  Future<Duration> durationForActivityNode(String treeCode,
      ActivityNode activityNode, DateTime startDate, DateTime endDate) async {
    print(treeCode);
    final List<Activity> _allActivities = await _selectMainActivities(treeCode);
    try {
      return _allActivities
          .where((activity) =>
              activityNode.rootCode == activity.activityNode?.rootCode)
          .where((activity) =>
              startDate.isSameOrBefore(activity.startDate) &&
              endDate.isSameOrAfter(activity.endDate))
          .map((activity) {
        return activity.endDate.difference(activity.startDate);
      }).reduce((value, element) => value + element);
    } catch (e) {
      return const Duration();
    }
  }

  Future<Map<ActivityNode, Duration>> totalDurationsForActivities(
      String treeCode,
      List<ActivityNode> activityNodes,
      Future<List<DateTime>> daysOfExperiment) async {
    final _daysOfExperiment = await daysOfExperiment;
    final _startDate = _daysOfExperiment.first;
    final _endDate = _daysOfExperiment.last;
    final _durationPerActivityNode = (await activityNodes.asyncMap(
            (activityNode) async => MapEntry(
                activityNode,
                await durationForActivityNode(
                    treeCode, activityNode, _startDate, _endDate))))
        .toList();
    _durationPerActivityNode.sort((lhs, rhs) {
      return lhs.value.compareTo(rhs.value) * -1;
    });
    return Map.fromEntries(_durationPerActivityNode);
  }

  Future<List<Activity>> _selectMainActivities(String treeCode) async {
    final _allActivityRows = await ActivityInfoTable.selectAll(treeCode);
    final _activities = _allActivityRows
        .map((activityTableRow) {
          return activityTableRow.createActivity(
              _findActivityNode(activityTableRow, activityTree));
        })
        .where((activity) => activity.type == ActivityType.main)
        .toList();
    return _activities;
  }

  ActivityNode _findActivityNode(
      ActivityInfoTable _activityInfo, ActivityTree activityTree) {
    return activityTree.activityTree().firstWhere((activityNode) =>
        activityNode.nodeCode == _activityInfo.nodeCode &&
        activityNode.activity == _activityInfo.description);
  }
}

extension MatchDateTime on DateTime {
  bool isSameOrBefore(DateTime date) {
    return isAtSameMomentAs(date) || isBefore(date);
  }

  bool isSameOrAfter(DateTime date) {
    return isAtSameMomentAs(date) || isAfter(date);
  }
}
