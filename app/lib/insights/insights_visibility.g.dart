// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'insights_visibility.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const InsightsVisibility _$none = const InsightsVisibility._('none');
const InsightsVisibility _$directly = const InsightsVisibility._('directly');
const InsightsVisibility _$afterExperiment =
    const InsightsVisibility._('afterExperiment');

InsightsVisibility _$valueOf(String name) {
  switch (name) {
    case 'none':
      return _$none;
    case 'directly':
      return _$directly;
    case 'afterExperiment':
      return _$afterExperiment;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<InsightsVisibility> _$values =
    new BuiltSet<InsightsVisibility>(const <InsightsVisibility>[
  _$none,
  _$directly,
  _$afterExperiment,
]);

Serializer<InsightsVisibility> _$insightsVisibilitySerializer =
    new _$InsightsVisibilitySerializer();

class _$InsightsVisibilitySerializer
    implements PrimitiveSerializer<InsightsVisibility> {
  @override
  final Iterable<Type> types = const <Type>[InsightsVisibility];
  @override
  final String wireName = 'InsightsVisibility';

  @override
  Object serialize(Serializers serializers, InsightsVisibility object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  InsightsVisibility deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      InsightsVisibility.valueOf(serialized as String);
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
