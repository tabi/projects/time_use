import 'package:flutter/widgets.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tbo_app/activities/activity_tree_scoped_model.dart';
import 'package:tbo_app/core/activity_tree_code.dart';
import 'package:tbo_app/core/configuration.dart';
import 'package:tbo_app/core/configuration_state.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_group_info.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_id.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_phone_info.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_register_data.dart';
import 'package:tbo_app/core/database/synchronise.dart';
import 'package:tbo_app/core/database/table/sync_database.dart';
import 'package:tbo_app/core/database/user_progress_database.dart';
import 'package:tbo_app/core/experience_sampling_severity.dart';
import 'package:tbo_app/core/input_type.dart';
import 'package:tbo_app/core/plausibility_check_type.dart';
import 'package:tbo_app/core/translate.dart';
import 'package:tbo_app/insights/insights_visibility.dart';

enum LoginResult { success, invalidUsername, invalidPassword, error }

class LoginScopedModel extends Model {
  static LoginScopedModel of(BuildContext context) =>
      ScopedModel.of<LoginScopedModel>(context);

  Future<LoginResult> login(
      BuildContext context, String username, String password) async {
    final _syncDatabase = SyncDatabase();
    SyncId syncId = await _syncDatabase.getSyncId();

    if (Synchronise.isBackendActive()) {
      try {
        //determine an unique phoneName for one user
        final String phoneName =
            DateTime.now().millisecondsSinceEpoch.toString();

        //add a list op phone properties
        final List<SyncPhoneInfo> phoneInfos = <SyncPhoneInfo>[];
        phoneInfos.add(SyncPhoneInfo.newInstance('key1', 'val1'));
        phoneInfos.add(SyncPhoneInfo.newInstance('key2', 'val2'));
        phoneInfos.add(SyncPhoneInfo.newInstance('key3', 'val3'));
        final _appFlavor = Configuration.of(context).appFlavor;
        final SyncRegisterData syncRegisterData =
            await Synchronise.registerNewPhone(
                username, password, phoneName, phoneInfos);
        final _configuration = Configuration.of(context);
        InsightsVisibility _insightsVisibility =
            InsightsVisibility.afterExperiment;
        PlausibilityCheckType _plausibilityCheckType =
            PlausibilityCheckType.soft;
        ExperienceSamplingSeverity _experienceSamplingSeverity =
            ExperienceSamplingSeverity.mild;
        InputType _inputType = InputType.halfOpen;
        ActivityTreeCode _mainActivityTree = _appFlavor == AppFlavor.time
            ? ActivityTreeCode.T20200908
            : ActivityTreeCode.MEDIA20201102;
        ActivityTreeCode _sideActivityTree = _appFlavor == AppFlavor.time
            ? ActivityTreeCode.T20200908
            : ActivityTreeCode.MEDIA20201102N;
        if (syncRegisterData.user.id != -1) {
          syncId = SyncId((b) => b
            ..userName = syncRegisterData.user.name
            ..userPassword = password
            ..phoneName = syncRegisterData.phone.name);
          await _syncDatabase.updateSyncId(syncId);

          //the groupInfos determine the app functionality
          for (final SyncGroupInfo gi in syncRegisterData.groupInfos) {
            // ignore: avoid_print
            print('app setting: ${gi.key} = ${gi.value}');
            if (gi.key == 'feedback') {
              _insightsVisibility = InsightsVisibility.valueOf(gi.value);
            }
            if (gi.key == 'plausibility') {
              _plausibilityCheckType = PlausibilityCheckType.valueOf(gi.value);
            }
            if (gi.key == 'experience') {
              _experienceSamplingSeverity =
                  ExperienceSamplingSeverity.valueOf(gi.value);
            }
            if (gi.key == 'input') {
              _inputType = InputType.valueOf(gi.value);
            }
            switch (gi.key) {
              case 'feedback':
                _insightsVisibility = InsightsVisibility.valueOf(gi.value);
                break;
              case 'plausibility':
                _plausibilityCheckType =
                    PlausibilityCheckType.valueOf(gi.value);
                break;
              case 'experience':
                _experienceSamplingSeverity =
                    ExperienceSamplingSeverity.valueOf(gi.value);
                break;
              case 'main_tree':
                _mainActivityTree = ActivityTreeCode.valueOf(gi.value);
                break;
              case 'side_tree':
                _sideActivityTree = ActivityTreeCode.valueOf(gi.value);
                break;
            }
          }
        }
        _configuration.insightsVisibility = _insightsVisibility;
        _configuration.plausibilityCheckType = _plausibilityCheckType;
        _configuration.experienceSamplingSeverity = _experienceSamplingSeverity;
        _configuration.mainActivityTree = _appFlavor == AppFlavor.time
            ? _mainActivityTree
            : ActivityTreeCode.MEDIA20201102;
        _configuration.sideActivityTree = _appFlavor == AppFlavor.time
            ? _sideActivityTree
            : ActivityTreeCode.MEDIA20201102N;
        _configuration.inputType = _inputType;
        await _configuration.save();
        await ActivityTreeScopedModel.of(context).load(context);
      } catch (e) {
        return LoginResult.error;
      }
    }
    final _password = '$username###***###$password';
    if ((_password.toLowerCase() == 'hbs###***###2020') ||
        (syncId.userName != '' && syncId.phoneName != '')) {
      final _userProgressDatabase = UserProgressDatabase();
      _userProgressDatabase.initializeData();
      _saveLanguagePreference();
      _saveTablePreference();
      return LoginResult.success;
    } else {
      if (username.toLowerCase() != 'hbs') {
        return LoginResult.invalidUsername;
      } else {
        return LoginResult.invalidPassword;
      }
    }
  }

  Future<void> _saveLanguagePreference() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('languagePreference', translate.languagePreference);
  }

  Future<void> _saveTablePreference() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('tablePreference', translate.tablePreference);
  }
}
