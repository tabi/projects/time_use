import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/core/color_pallet.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/core/translate.dart';
import 'package:tbo_app/core/user_input.dart';
import 'package:tbo_app/experience_sampling/notification_setting.dart';
import 'package:tbo_app/home/home_page.dart';
import 'package:tbo_app/register/login_scoped_model.dart';
import 'package:toast/toast.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  void initState() {
    initializeES();
    super.initState();
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
          statusBarColor: ColorPallet.lightBlueWithOpacity),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              _QRLoginWidget(),
              _LoginManualWidget(),
            ],
          ),
        ),
      ),
    );
  }
}

class _QRLoginWidget extends StatefulWidget {
  @override
  __QRLoginWidgetState createState() => __QRLoginWidgetState();
}

class __QRLoginWidgetState extends State<_QRLoginWidget> {
  String barcode = '';

  Future<void> scan() async {
    //ToDo: re-implement QR code scanner
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 5.5 / 10,
      color: ColorPallet.lightBlueWithOpacity,
      child: Padding(
        padding: EdgeInsets.only(left: 35.0 * x, right: 10 * x),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: 68 * y,
            ),
            Text(
              '${translate.welcome},',
              style: TextStyle(
                color: Colors.white,
                fontSize: 42 * f,
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(height: 5 * y),
            SizedBox(
              width: 360 * x,
              child: Text(
                translate.toLogin,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 22 * f,
                  fontWeight: FontWeight.w700,
                  height: 1.1,
                ),
              ),
            ),
            SizedBox(
              height: 150 * y,
              child: Center(
                child: Row(children: <Widget>[
                  ButtonTheme(
                    minWidth: 180.0 * x,
                    height: 37.0 * y,
                    child: RaisedButton(
                      onPressed: () {
                        scan();
                      },
                      color: ColorPallet.pink,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(11.0),
                      ),
                      child: Text(
                        translate.scanQRcode,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 17 * f,
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 60 * x),
                  Image.asset(
                    'assets/images/qr_code_scanning.png',
                    height: 110 * y,
                  )
                ]),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _LoginManualWidget extends StatefulWidget {
  @override
  __LoginManualWidgetState createState() => __LoginManualWidgetState();
}

class __LoginManualWidgetState extends State<_LoginManualWidget> {
  String password = '';
  bool passwordInvisible = true;
  String username = '';

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 4.5 / 10,

      /// 2,
      color: ColorPallet.primaryColor,
      child: Column(
        children: <Widget>[
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 40 * y,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 40 * x),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: ColorPallet.lightBlueWithOpacity,
            ),
            child: Padding(
              padding:
                  EdgeInsets.symmetric(horizontal: 12.0 * x, vertical: 2 * y),
              child: TextField(
                cursorColor: ColorPallet.darkTextColor,
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 19 * f,
                    color: Colors.white),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.symmetric(vertical: 7 * y),
                  border: InputBorder.none,
                  icon:
                      Icon(Icons.person, color: Colors.white.withOpacity(0.7)),
                  hintText: translate.user,
                  hintStyle: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 17 * f,
                      color: Colors.white.withOpacity(0.7)),
                ),
                onChanged: (value) {
                  value = UserInput.textSanitizer(value);
                  setState(() {
                    username = value;
                  });
                },
              ),
            ),
          ),
          SizedBox(height: 18 * y),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 40 * x),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: ColorPallet.lightBlueWithOpacity,
            ),
            child: Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 12.0, vertical: 2),
              child: Stack(
                children: <Widget>[
                  TextField(
                    obscureText: passwordInvisible,
                    cursorColor: ColorPallet.darkTextColor,
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 19 * f,
                        color: Colors.white),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.symmetric(vertical: 7 * y),
                      border: InputBorder.none,
                      icon: Icon(Icons.vpn_key,
                          color: Colors.white.withOpacity(0.7)),
                      hintText: translate.password,
                      hintStyle: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 17 * f,
                          color: Colors.white.withOpacity(0.7)),
                    ),
                    onChanged: (value) {
                      value = UserInput.textSanitizer(value);
                      setState(() {
                        password = value;
                      });
                    },
                  ),
                  if (password == '')
                    Container()
                  else
                    Positioned(
                      top: 6 * y,
                      right: 10 * x,
                      child: InkWell(
                        onTap: () {
                          setState(() {
                            passwordInvisible = !passwordInvisible;
                          });
                        },
                        child: !passwordInvisible
                            ? Icon(Icons.visibility,
                                color: Colors.white.withOpacity(0.7))
                            : Icon(
                                Icons.visibility_off,
                                color: Colors.white.withOpacity(0.7),
                              ),
                      ),
                    )
                ],
              ),
            ),
          ),
          SizedBox(
            height: 120 * y,
            child: Center(
              child: ButtonTheme(
                minWidth: 330.0 * x,
                height: 37.0 * y,
                child: ScopedModelDescendant<LoginScopedModel>(
                    builder: (context, child, loginScopedModel) {
                  return RaisedButton(
                    onPressed: () async {
                      await _performLogin(context, username, password);
                    },
                    color: ColorPallet.lightGreen,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(11.0),
                    ),
                    child: Text(
                      translate.loginManually,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 17 * f,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  );
                }),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Future<void> _performLogin(
    BuildContext context, String username, String password) async {
  final loginScopedModel = LoginScopedModel.of(context);
  final _loginResult =
      await loginScopedModel.login(context, username, password);
  switch (_loginResult) {
    case LoginResult.success:
      _changeStatusBarColor();
      _navigateToHomePage(context);
      break;
    case LoginResult.invalidUsername:
      Toast.show(translate.incorrectUserName, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      break;
    case LoginResult.invalidPassword:
      Toast.show(translate.incorrectPassword, context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      break;
    case LoginResult.error:
      //TODO: translate default something went wrong message
      Toast.show('Something went wrong', context,
          duration: Toast.LENGTH_LONG, gravity: Toast.CENTER);
      break;
  }
}

void _navigateToHomePage(BuildContext context) {
  Navigator.push(
    context,
    MaterialPageRoute(
      settings: const RouteSettings(name: 'HomePage'),
      builder: (context) => HomePage(),
    ),
  );
}

void _changeStatusBarColor() {
  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(
        statusBarColor: ColorPallet.primaryColor,
        systemNavigationBarColor: Colors.black,
        systemNavigationBarIconBrightness: Brightness.light),
  );
}
