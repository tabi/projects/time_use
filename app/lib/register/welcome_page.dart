import 'dart:ui' as ui;

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tbo_app/core/color_pallet.dart';
import 'package:tbo_app/core/configuration.dart';
import 'package:tbo_app/core/configuration_state.dart';
import 'package:tbo_app/core/database/database_helper.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/core/translate.dart';
import 'package:tbo_app/register/login_page.dart';
import 'package:url_launcher/url_launcher.dart';

class WelcomePage extends StatefulWidget {
  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  String dropdownValue;

  final List<String> _countries = [
    'Nederland' , 'United Kingdom'
  ]; 

  @override
  void initState() {
    super.initState();
    initDatabase();
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
          statusBarColor: ColorPallet.primaryColor,
          systemNavigationBarColor: ColorPallet.primaryColor),
    );
    if (ui.window.locale.languageCode == 'nl') {
      dropdownValue = 'Nederland';
      translate.languagePreference = 'nl';
      translate.tablePreference = 'nl';
    } else {
      dropdownValue = 'United Kingdom';
      translate.languagePreference = 'en';
      translate.tablePreference = 'en';
    }
  }

  Future<void> _launchURL() async {
    final String url = translate.url;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void changeCountry(String country) {
    setState(() {
      dropdownValue = country;
    });
  }

  Future<void> initDatabase() async {
    await DatabaseHelper.instance.database;
  }

  Widget _countryIcon(String country) {
    String png = '';
    if (country == 'Nederland') {
      png = 'assets/images/nl.png';
    } else if (country == 'United Kingdom') {
      png = 'assets/images/gb.png';
    }
    if (png == '') {
      return Row(
        children: <Widget>[
          SizedBox(width: 30 * x, height: 110 * y),
          Text(
            country,
            style: TextStyle(
              color: Colors.white,
              fontSize: 18 * f,
              fontWeight: FontWeight.w700,
            ),
          ),
        ],
      );
    }
    return Row(
      children: <Widget>[
        SizedBox(width: 10 * x, height: 110 * y),
        Container(
            width: 26 * x,
            height: 26 * x,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    fit: BoxFit.fill, image: ExactAssetImage(png)))),
        SizedBox(
          width: 10 * x,
        ),
        Text(
          country,
          style: TextStyle(
            color: Colors.white,
            fontSize: 18 * f,
            fontWeight: FontWeight.w700,
          ),
        ),
      ],
    );
  }

  String _languageValue(String country) {
    String languageValue = 'en';
    if (country == 'Suomi') {
      languageValue = 'fi';
    } else if (country == 'Slovenija') {
      languageValue = 'sl';
    } else if (country == 'Nederland') {
      languageValue = 'nl';
    } else if (country == 'United Kingdom') {
      languageValue = 'en';
    }
    return languageValue;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: ColorPallet.primaryColor,
        child: Column(
          children: <Widget>[
            SizedBox(height: 130 * y),
            SizedBox(
              height: 150 * y,
              child: Center(
                child: Text(
                  Configuration.of(context).appFlavor == AppFlavor.time
                      ? translate.timeSurveyTitle
                      : translate.mediaSurveyTitle,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 50 * f,
                    fontWeight: FontWeight.w700,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            SizedBox(height: 120 * y),
            Container(
              width: 310 * x,
              color: ColorPallet.primaryColor,
              child: Theme(
                data: Theme.of(context).copyWith(
                  canvasColor: ColorPallet.primaryColor,
                ),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    iconEnabledColor: Colors.white.withOpacity(0.8),
                    value: dropdownValue,
                    elevation: 0,
                    onChanged: (String newValue) {
                      setState(() {
                        //dropdownValue = newValue;
                        changeCountry(newValue);
                        translate.languagePreference = _languageValue(newValue);
                        translate.tablePreference = _languageValue(newValue);
                      });
                    },
                    items: _countries
                        .map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                          value: value,
                          child: Container(
                            height: 45 * y,
                            decoration: const BoxDecoration(
                                color: ColorPallet.lightBlueWithOpacity,
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10.0),
                                    topRight: Radius.circular(10.0),
                                    bottomLeft: Radius.circular(10.0),
                                    bottomRight: Radius.circular(10.0))),
                            //color: ColorPallet.lightBlueWithOpacity,
                            width: 260 * x,
                            margin: const EdgeInsets.all(5.0),
                            child: _countryIcon(value),
                          ));
                    }).toList(),
                  ),
                ),
              ),
            ),
            SizedBox(height: 67 * y),
            ButtonTheme(
              minWidth: 300.0 * x,
              height: 40.0 * y,
              child: RaisedButton(
                onPressed: () {
                  if (dropdownValue != 'Please choose a country') {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        settings: const RouteSettings(name: 'LoginPage'),
                        builder: (context) => LoginPage(),
                      ),
                    );
                  }
                },
                color: dropdownValue != 'Please choose a country'
                    ? ColorPallet.lightGreen
                    : ColorPallet.lightGray,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(11.0),
                ),
                child: Text(
                  translate.start,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 18 * f,
                    fontWeight: FontWeight.w600,
                  ),
                ),
              ),
            ),
            Expanded(child: Container()),
            RichText(
              textAlign: TextAlign.center,
              text: TextSpan(children: <TextSpan>[
                TextSpan(
                  text: translate.www,
                  style: TextStyle(
                      color: Colors.white.withOpacity(0.8),
                      fontSize: 17.0 * f,
                      decoration: TextDecoration.underline),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      _launchURL();
                    },
                ),
                const TextSpan(text: '\n'),
              ]),
            ),
            SizedBox(height: 20 * y),
          ],
        ),
      ),
    );
  }
}
