class UserInput {
  //Remove all characters except for numbers
  static String valueSanitizer(String str) {
    var _str = str.replaceAll(RegExp(','), '.');
    _str = _str.replaceAll(RegExp('[^0-9.,]+'), '');

    try {
      if (_str == '') {
        return '';
      }
      double.parse(_str).toString();
      return _str;

      // if (double.parse(str) > 1000) {
      //   return double.parse(str).toStringAsFixed(0);
      // } else {
      //   return double.parse(str).toStringAsFixed(2);
      // }
    } catch (e) {
      return null;
    }
  }

  //Remove everything except letters and spaces
  //Makes sure that the string is formatted to have the first letter upper case and the rest lower case.
  static String textSanitizer(String str) {
    //str = str.replaceAll(RegExp('[^a-zA-Z 0-9]+'), '');
    //str = str.replaceAll(RegExp('[^a-zA-Z 0-9ČčŠšŽžÅåÄäÖö]+'), '');
    final _str = str.replaceAll(RegExp('[^a-zA-Z 0-9\x80-\xFF]+'), '');
    if (_str.length > 1) {
      return '${_str[0].toUpperCase()}${_str.substring(1).toLowerCase()}';
    } else {
      return _str;
    }
  }

  //Needs to match a CoiCop classification.
  static bool textValidator(String str) {
    if (str is String) {
      return true;
    }
    return false;
  }

  static bool intValidator(String str) {
    if (str == null) {
      return false;
    }

    if (str.replaceAll(RegExp('[^0-9]+'), '').length == str.length) {
      if (int.parse(str) >= 0) {
        return true;
      }
    }
    return false;
  }

  //Make sure that the value is a positive double
  static bool valueValidator(String str) {
    var _str = str.replaceAll(',', '.');
    if (_str == null) {
      return false;
    }

    if (_str.substring(0, 1) == '-') {
      _str = _str.substring(1, _str.length);
    }

    if (_str.length == 1) {
      if (_str[0] == '.') {
        return true;
      }
    }

    if (_str.length >= 2) {
      if (_str[0] == '0' && _str[1] != '.') {
        return false;
      }
    }

    if (_str.isEmpty) {
      return true;
    }

    if (_str.contains(' ')) {
      return false;
    }

    try {
      final double value = double.parse(_str);
      if (value >= -10000000 && value <= 10000000) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }

  static bool percentageValidator(String str) {
    if (str == null) {
      return false;
    }

    final double value = double.parse(str);
    try {
      if (value >= 0 && value <= 100) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      return false;
    }
  }
}
