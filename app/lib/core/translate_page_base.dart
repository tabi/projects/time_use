import 'package:tbo_app/core/constant.dart';
import 'package:tbo_app/core/translate_page_general.dart';

abstract class TranslatePageBase {
  String pageKey;
  List<String> keys;
  Map<String, String> defaultTranslation;
  Map<String, String> chosenTranslation;

  TranslatePageBase() {
    pageKey = Constant.emptyString;
    keys = <String>[];
    defaultTranslation = {};
    chosenTranslation = {};
    initPage();
    initKeys();
  }

  TranslatePageGeneral get general {
    return TranslatePageGeneral.instance;
  }

  void initPage();
  void initKeys();

  String translation(String key) {
    if (!keys.contains(key)) {
      return '!!! {$key}';
    } else if (chosenTranslation.containsKey(key)) {
      return chosenTranslation[key];
    } else if (chosenTranslation.isEmpty &&
        defaultTranslation.containsKey(key)) {
      return defaultTranslation[key];
    } else if (defaultTranslation.containsKey(key)) {
      return '[${defaultTranslation[key]}]';
    } else {
      return '{$key}';
    }
  }

  String sqlDefault(String key) {
    if (defaultTranslation.containsKey(key)) {
      return defaultTranslation[key];
    } else {
      return '{$key}';
    }
  }

  String sqlChosen(String key) {
    if (chosenTranslation.containsKey(key)) {
      return chosenTranslation[key];
    } else if (defaultTranslation.containsKey(key)) {
      return '[${defaultTranslation[key]}]';
    } else {
      return '{$key}';
    }
  }

  void editDefaultTranslation(String key, String value) {
    defaultTranslation[key] = value;
  }

  void editChosenTranslation(String key, String value) {
    chosenTranslation[key] = value;
  }
}
