import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'input_type.g.dart';

class InputType extends EnumClass {
  static Serializer<InputType> get serializer => _$inputTypeSerializer;

  static const InputType closed = _$closed;
  static const InputType halfOpen = _$halfOpen;
  // ignore: constant_identifier_names
  static const InputType half_open = _$half_open;

  const InputType._(String name) : super(name);

  static BuiltSet<InputType> get values => _$values;

  static InputType valueOf(String name) => _$valueOf(name);
}
