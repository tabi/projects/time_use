// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializers.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(Activity.serializer)
      ..add(ActivityCompanion.serializer)
      ..add(ActivityLocation.serializer)
      ..add(ActivityNode.serializer)
      ..add(ActivityTreeCode.serializer)
      ..add(ActivityType.serializer)
      ..add(Gender.serializer)
      ..add(LivingSituation.serializer)
      ..add(Medium.serializer)
      ..add(ParaData.serializer)
      ..add(StartQuestionnaireResult.serializer)
      ..add(SyncGroupInfo.serializer)
      ..add(SyncId.serializer)
      ..add(SyncImage.serializer)
      ..add(SyncPhone.serializer)
      ..add(SyncPhoneInfo.serializer)
      ..add(SyncProduct.serializer)
      ..add(SyncPullBody.serializer)
      ..add(SyncPushReceiptBody.serializer)
      ..add(SyncPushSearchProductBody.serializer)
      ..add(SyncPushSearchStoreBody.serializer)
      ..add(SyncReceiptData.serializer)
      ..add(SyncRegisterBody.serializer)
      ..add(SyncRegisterData.serializer)
      ..add(SyncSearchProduct.serializer)
      ..add(SyncSearchProductData.serializer)
      ..add(SyncSearchStore.serializer)
      ..add(SyncSearchStoreData.serializer)
      ..add(SyncSync.serializer)
      ..add(SyncTransaction.serializer)
      ..add(SyncUser.serializer)
      ..add(UserProgress.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(ActivityCompanion)]),
          () => new ListBuilder<ActivityCompanion>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SyncGroupInfo)]),
          () => new ListBuilder<SyncGroupInfo>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SyncPhoneInfo)]),
          () => new ListBuilder<SyncPhoneInfo>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SyncProduct)]),
          () => new ListBuilder<SyncProduct>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(SyncProduct)]),
          () => new ListBuilder<SyncProduct>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(int)]),
          () => new ListBuilder<int>()))
    .build();

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
