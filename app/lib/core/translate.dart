library translate;

final Translate translate = Translate._private();

class Translate {
  Translate._private();

  String treePreference;
  String languagePreference;
  String tablePreference;

  final _abroad = <String, String>{
    'nl': 'Buitenland',
    'en': 'Abroad',
    'sl': 'V tujini',
    'fi': 'Ulkomailta'
  };

  final _add = <String, String>{
    'nl': 'Toevoegen',
    'en': 'Add',
    'sl': 'Dodaj',
    'fi': 'Lisää'
  };

  final _addBeforeCompletion = <String, String>{
    'nl': 'Toevoegen voordat u de bestelling afrond?',
    'en': 'Add before you complete the entry?',
    'sl': 'Dodaj?',
    'fi': 'Lisää ja vahvista ostos?'
  };

  final _addInfo = <String, String>{
    'nl': 'Bon toevoegen',
    'en': 'Add receipt',
    'sl': 'Dodaj račun',
    'fi': 'Lisää tietoja'
  };

  final _addingNewItems = <String, String>{
    'nl': 'Nieuw item toevoegen',
    'en': 'New item',
    'sl': 'Dodaj',
    'fi': 'Lisää uusi tuote'
  };

  final _additional2 = <String, String>{
    'nl': 'Overige',
    'en': 'Additional',
    'sl': 'Drugo',
    'fi': 'Muu'
  };

  final _addNewServiceProductOrDiscount = <String, String>{
    'nl': 'Product, dienst, of korting toevoegen',
    'en': 'Add new expense or return',
    'sl': 'Dodaj nov izdelek, storitev ali popust',
    'fi': 'Lisää uusi tuote, palvelu tai alennus'
  };

  final _addOneProduct = <String, String>{
    'nl': 'Voeg eerst minstens één product toe.',
    'en': 'First add at least one product.',
    'sl': 'Najprej dodajte vsaj en izdelek.',
    'fi': 'Lisää ensin vähintään yksi tuote.'
  };

  final _addReceipt = <String, String>{
    'nl': 'Bon invoeren',
    'en': 'Add receipt',
    'sl': 'Nov nakup',
    'fi': 'Lisää ostokerta',
  };

  final _addYourself = <String, String>{
    'nl': 'Zelf toevoegen',
    'en': 'Add it yourself',
    'sl': 'Dodaj',
    'fi': 'Lisää'
  };

  final _alcoholAndTobacco = <String, String>{
    'nl': 'Alcohol en tabak',
    'en': 'Alcohol and tobacco',
    'sl': 'Alkohol in tobak',
    'fi': 'Alkoholi ja tupakka'
  };

  final _and = <String, String>{
    'nl': 'en',
    'en': 'and',
    'sl': 'in',
    'fi': 'ja'
  };

  final _beginDate = <String, String>{
    'nl': 'Start datum',
    'en': 'Start date',
    'sl': 'Začetni datum',
    'fi': 'Aloituspäivä'
  };

  final _belongsTo = <String, String>{
    'nl': 'Behoort tot',
    'en': 'Type of product',
    'sl': 'Pripada',
    'fi': 'Kuuluu ryhmään'
  };

  final _belongsTo2 = <String, String>{
    'nl': 'Behoort tot',
    'en': 'Type of shop',
    'sl': 'Pripada',
    'fi': 'Kuuluu ryhmään'
  };

  final _between = <String, String>{
    'nl': 'tussen',
    'en': 'between',
    'sl': 'med',
    'fi': 'Välillä'
  };

  final _byCategory = <String, String>{
    'nl': 'Per categorie',
    'en': 'By category',
    'sl': 'Po kategorijah',
    'fi': 'Tuoteryhmittäin'
  };

  final _cameraHint1 = <String, String>{
    'nl':
        'Vermijd een witte achtergrond, ook als dit maar een klein stukje is.',
    'en':
        'Avoid something white in the background, even if this is only a small piece.',
    'sl': 'Ne fotografirajte računa na  belem ozadju.',
    'fi': 'Vältä valkoista taustaa.',
  };

  final _cameraHint2 = <String, String>{
    'nl':
        'Als een bon kreukels of vouwen heeft probeer hem dan zo plat mogelijk te maken.',
    'en':
        'If your receipt has wrinkles or creases, try to make it as flat as possible.',
    'sl': 'Če je račun zguban ali prepognjen, ga poskusite čim bolj poravnati.',
    'fi':
        'Jos kuitti on rypistynyt, yritä suoristaa se mahdollisimman suoraksi.',
  };

  final _cameraHint3 = <String, String>{
    'nl': 'Scan bij voorkeur overdag bij daglicht.',
    'en': 'Preferably scan during the day in daylight.',
    'sl': 'Ko slikate račun, poskrbite za dobro osvetljenost.',
    'fi': 'Pyri ottamaan kuvat päivänvalossa.',
  };

  final _cameraHint4 = <String, String>{
    'nl': 'Vermijd schaduwwerking in de scan.',
    'en': 'Avoid shadowing in the photo.',
    'sl': 'Pazite, da ne naredite senc na fotografiji.',
    'fi': 'Vältä varjoja valokuvassa.',
  };

  final _cameraHint5 = <String, String>{
    'nl': 'Zorg dat de hoeken van de bon recht en vrij zijn.',
    'en': 'Make sure the corners of the receipt are straight and free.',
    'sl': 'Prepričajte se, da so koti računa poravnani in vidni.',
    'fi': 'Varmista, että kuitti on suorassa ja kokonaan näkyvissä.',
  };

  final _cameraHint6 = <String, String>{
    'nl': 'Probeer de bon zo plat mogelijk te houden.',
    'en': 'Try to keep the surface of the receipt completely flat.',
    'sl': 'Potrudite se, da bo površina prejema popolnoma ravna.',
    'fi': 'Yritä pitää kuitin pinta täysin tasaisena.',
  };

  final _cancel = <String, String>{
    'nl': 'Annuleren',
    'en': 'Cancel',
    'sl': 'Prekliči',
    'fi': 'Peruuta'
  };

  final _categories = <String, String>{
    'nl': 'Categorieën',
    'en': 'Categories',
    'sl': 'Kategorije',
    'fi': 'Tuoteryhmät'
  };

  final _category = <String, String>{
    'nl': 'Categorie',
    'en': 'Category',
    'sl': 'Kategorija',
    'fi': 'Tuoteryhmä'
  };

  final _close = <String, String>{
    'nl': 'Sluiten',
    'en': 'Close',
    'sl': 'Zapri',
    'fi': 'Sulje'
  };

  final _clothing = <String, String>{
    'nl': 'Kleding',
    'en': 'Clothing',
    'sl': 'Oblačila in obutev',
    'fi': 'Vaatetus'
  };

  final _communication = <String, String>{
    'nl': 'Communcatie',
    'en': 'Communication',
    'sl': 'Komunikacije',
    'fi': 'Viestintä'
  };

  final _complete = <String, String>{
    'nl': 'Gereed',
    'en': 'Done',
    'sl': 'Zaključi',
    'fi': 'Valmis'
  };

  final _completeDay = <String, String>{
    'nl': 'Dag afsluiten',
    'en': 'Complete day',
    'sl': 'Zaključi dan',
    'fi': 'Kuittaa päivä valmiiksi'
  };

  final _completeShopProduct = <String, String>{
    'nl':
        'U moet eerst de winkel informatie invullen en minimaal één product toevoegen.',
    'en':
        'You will first need to complete the required store information and add at least one product.',
    'sl':
        'Najprej morate izpolniti zahtevane podatke o trgovini in dodati vsaj en izdelek.',
    'fi': 'Lisää kauppa ja vähintään yksi tuote.'
  };

  final _completeStoreInfo = <String, String>{
    'nl': 'Vul eerst de winkel informatie in.',
    'en': 'First complete the required store information.',
    'sl': 'Najprej dokončajte zahtevane podatke o trgovini.',
    'fi': 'Täytä ensin tarvittavat tallennustiedot.'
  };

  final _contact = <String, String>{
    'nl': 'Contact',
    'en': 'Contact',
    'sl': 'Kontakt za pomoč',
    'fi': 'Yhteystiedot'
  };

  final _contentDay = <String, String>{
    'nl': 'inhoud dag',
    'en': 'content day',
    'sl': 'dan za izpolnjevanje',
    'fi': 'Ostopäivä'
  };

  final _couldNotLaunch = <String, String>{
    'nl': 'Kan niet starten',
    'en': 'Could not launch ',
    'sl': 'Zagon ni možen',
    'fi': 'Ei voitu käynnistää'
  };

  final _currency = <String, String>{
    'nl': 'Munteenheid',
    'en': 'Currency',
    'sl': 'Valuta',
    'fi': 'Valuutta'
  };

  final _dailyReminders = <String, String>{
    'nl': 'Dagelijkse herinnering',
    'en': 'Daily reminder',
    'sl': 'Dnevni opomnik',
    'fi': 'Päivittäinen muistutus'
  };

  final _dailyReminderText = <String, String>{
    'nl': 'Vergeet niet je uitgaven in te vullen!',
    'en': "Don't forget to enter your expenses!",
    'sl': 'Ne pozabite vpisati vseh izdatkov.',
    'fi': 'Muista kirjata ostoksesi!'
  };

  final _date = <String, String>{
    'nl': 'Datum',
    'en': 'Date',
    'sl': 'Datum',
    'fi': 'Päivämäärä'
  };

  final _dayAlreadyCompleted = <String, String>{
    'nl': 'Dag is al afgerond',
    'en': 'Day already completed',
    'sl': 'Dan je že zaključen',
    'fi': 'Päivä kuitattu valmiiksi',
  };

  final _dayCompleted = <String, String>{
    'nl': 'Dag afgesloten',
    'en': 'Day completed',
    'sl': 'Dan je zaključen!',
    'fi': 'Päivän ostokset kirjattu'
  };

  final _daysCompleted = <String, String>{
    'nl': 'dagen afgesloten',
    'en': 'days completed',
    'sl': 'izpolnjeni',
    'fi': 'päivää täytetty'
  };

  final _daysMissing = <String, String>{
    'nl': 'dagen ontbreken',
    'en': 'days missing',
    'sl': 'manjkajoči',
    'fi': 'päivää puuttuu'
  };

  final _daysRemaining = <String, String>{
    'nl': 'dagen resterend',
    'en': 'days remaining',
    'sl': 'preostali',
    'fi': 'päivää jäljellä'
  };

  final _delete = <String, String>{
    'nl': 'Verwijderen',
    'en': 'Delete',
    'sl': 'Briši',
    'fi': 'Poista'
  };

  final _deleteItem = <String, String>{
    'nl': 'Verwijderen',
    'en': 'Delete',
    'sl': 'Izbriši',
    'fi': 'Poista'
  };

  final _discount = <String, String>{
    'nl': 'Korting op totaal',
    'en': 'Discount on total',
    'sl': 'Popust',
    'fi': 'Alennus'
  };

  final _discountEntireReceipt = <String, String>{
    'nl': 'Korting op gehele bon',
    'en': 'Discount on entire receipt',
    'sl': 'Popust na celoten znesek',
    'fi': 'Koko ostosta koskeva alennus'
  };

  final _discountExpensesDiscount = <String, String>{
    'nl': 'Korting',
    'en': 'Discount',
    'sl': 'Popust',
    'fi': 'Alennus'
  };

  final _discountReturnEtc = <String, String>{
    'nl': 'Korting/retour/etc',
    'en': 'Discount/return/etc',
    'sl': 'Popust',
    'fi': 'Alennus/palautus/tms'
  };

  final _duplicate = <String, String>{
    'nl': 'Dupliceren',
    'en': 'Duplicate',
    'sl': 'Podvoji',
    'fi': 'Kopioi'
  };

  final _earnedReward = <String, String>{
    'nl': 'Beloning verdiend',
    'en': 'Reward earned',
    'sl': 'Nagrada',
    'fi': 'Kertynyt palkkio'
  };

  final _edit = <String, String>{
    'nl': 'Aanpassen',
    'en': 'Edit',
    'sl': 'Uredi',
    'fi': 'Muokkaa'
  };

  final _education = <String, String>{
    'nl': 'Educatie',
    'en': 'Education',
    'sl': 'Izobraževanje',
    'fi': 'Opiskelu'
  };

  final _email = <String, String>{
    'nl': 'E-mail',
    'en': 'E-mail',
    'sl': 'E-Pošta',
    'fi': 'Sähköposti'
  };

  final _emailAdress = <String, String>{
    'nl': 'WINHelpdesk@cbs.nl',
    'en': 'WINHelpdesk@cbs.nl',
    'sl': 'vprasalnik.surs@gov.si',
    'fi': 'signaali@tilastokeskus.fi'
  };

  final _endDate = <String, String>{
    'nl': 'Eind datum',
    'en': 'End date',
    'sl': 'Končni datum',
    'fi': 'Lopetuspäivä'
  };

  final _enterCatagory = <String, String>{
    'nl': 'Voer categorie in',
    'en': 'Enter catagory',
    'sl': 'Vnesi kategorijo',
    'fi': 'Syötä tuoteryhmä'
  };

  final _enterDiscount = <String, String>{
    'nl': 'Korting invoeren',
    'en': 'Enter discount',
    'sl': 'Vnesi popust',
    'fi': 'Syötä alennus '
  };

  final _enterDiscountReceipt = <String, String>{
    'nl': 'Voer korting op bon in',
    'en': 'Enter discount on receipt',
    'sl': 'Vnesi popust na končni znesek',
    'fi': 'Syötä alennus'
  };

  final _enterPrice = <String, String>{
    'nl': 'Voer prijs in',
    'en': 'Enter price',
    'sl': 'Vnesi ceno',
    'fi': 'Syötä hinta'
  };

  final _enterProductService = <String, String>{
    'nl': 'Voer product/dienst in',
    'en': 'Enter Product / Service',
    'sl': 'Vnesi izdelek / storitev',
    'fi': 'Syötä tuote/palvelu'
  };

  final _enterShop = <String, String>{
    'nl': 'Voer winkel in',
    'en': 'Enter shop',
    'sl': 'Vnesi trgovino',
    'fi': 'Syötä kauppa'
  };

  final _enterShopType = <String, String>{
    'nl': 'Voer winkeltype in',
    'en': 'Enter shop type',
    'sl': 'Vnesi vrsto trgovine',
    'fi': 'Syötä kauppatyyppi'
  };

  final _enterTotalPrice = <String, String>{
    'nl': 'Voer totaal prijs in',
    'en': 'Enter total price',
    'sl': 'Vnesi skupno ceno',
    'fi': 'Syötä loppusumma'
  };

  final _enterTypeOfProducts = <String, String>{
    'nl': 'Voer het type producten in ..',
    'en': 'Enter the type of products ..',
    'sl': 'Vnesite vrsto izdelkov ..',
    'fi': 'Valitse tuoteryhmä ..'
  };

  final _everything = <String, String>{
    'nl': 'Alles',
    'en': 'Everything',
    'sl': 'Vse',
    'fi': 'Kaikki'
  };

  final _expensesPage = <String, String>{
    'nl': 'Uitgaven',
    'en': 'Spending',
    'sl': 'Izdatki',
    'fi': 'Ostokset'
  };

  final _family = <String, String>{
    'nl': 'Familie',
    'en': 'Family',
    'sl': 'Gospodinjstvo',
    'fi': 'Kotitalous'
  };

  final _familyCode = <String, String>{
    'nl': 'Familie code',
    'en': 'Family code',
    'sl': 'Oznaka gospodinjstva',
    'fi': 'Kotitalouden tunnus'
  };

  final _familyMembers = <String, String>{
    'nl': 'Familie leden',
    'en': 'Family code',
    'sl': 'Člani gospodinjstva',
    'fi': 'Kotitalouden tunnus'
  };

  final _filterAmount = <String, String>{
    'nl': 'Bedrag filteren',
    'en': 'Filter amount',
    'sl': 'Filter',
    'fi': 'Rajaa summa'
  };

  final _filterExpenses = <String, String>{
    'nl': 'Uitgaven filteren',
    'en': 'Filter expenses',
    'sl': 'Filter za izdatke',
    'fi': 'Rajaa ostoja'
  };

  final _findProductService = <String, String>{
    'nl': 'Zoek product/dienst',
    'en': 'Find product/service',
    'sl': 'Poišči izdelek / storitev',
    'fi': 'Etsi tuote/palvelu'
  };

  final _findShop = <String, String>{
    'nl': 'Zoek winkel',
    'en': 'Find shop',
    'sl': 'Poišči trgovino',
    'fi': 'Etsi kauppa'
  };

  final _findTransactions = <String, String>{
    'nl': 'Zoek transacties',
    'en': 'Find transactions',
    'sl': 'Poišči izdatek',
    'fi': 'Etsi'
  };

  final _fixedExpenses = <String, String>{
    'nl': 'Vaste lasten',
    'en': 'Fixed expenses',
    'sl': 'Fiksni izdatki',
    'fi': 'Kiinteät kulut'
  };

  final _fixedExpensesFilled = <String, String>{
    'nl': 'Vaste uitgaven ingevuld',
    'en': 'Fixed expenses filled out',
    'sl': 'Fiksni izdatki so izpolnjeni',
    'fi': 'Kiinteät kulut täytetty'
  };

  final _fixedOrVariable = <String, String>{
    'nl': 'Vast en variabel',
    'en': 'Fixed or variable',
    'sl': 'Fiksni ali spremenljivi',
    'fi': 'Kiinteä tai vaihteleva'
  };

  final _foodAndDrink = <String, String>{
    'nl': 'Voeding en drank',
    'en': 'Food and drink',
    'sl': 'Hrana in pijače',
    'fi': 'Ruoka ja juoma'
  };

  final _frequentItems = <String, String>{
    'nl': 'Frequente items',
    'en': 'Frequent items',
    'sl': 'Najpogostejši vnos',
    'fi': 'Useimmin käytetyt',
  };

  final _friday = <String, String>{
    'nl': 'Vr',
    'en': 'Fr',
    'sl': 'Pe',
    'fi': 'Pe'
  };

  final _hbsLong = <String, String>{
    'nl': 'Budget Onderzoek',
    'en': 'Household Budget Survey',
    'sl': 'Moji izdatki ',
    'fi': 'Kulutustutkimus'
  };

  final _hbsLong2 = <String, String>{
    'nl': 'Budget Onderzoek',
    'en': 'Household Budget Survey',
    'sl': 'Moji izdatki ',
    'fi': 'Kulutus-\ntutkimus'
  };

  final _hbsShort = <String, String>{
    'nl': 'HBS',
    'en': 'HBS',
    'sl': 'HBS',
    'fi': 'HBS'
  };

  final _health = <String, String>{
    'nl': 'Gezondheid',
    'en': 'Health',
    'sl': 'Zdravstvo',
    'fi': 'Terveys'
  };

  final _householdExpenses = <String, String>{
    'nl': 'Huishoudelijke uitgaven',
    'en': 'Household expenses',
    'sl': 'Gospodinjska oprema',
    'fi': 'Kodintavarat'
  };

  final _housingCost = <String, String>{
    'nl': 'Woonkosten',
    'en': 'Housing costs',
    'sl': 'Stanovanjski stroški',
    'fi': 'Asumiskustannukset'
  };

  final _ifApplicable = <String, String>{
    'nl': 'Indien van toepassing',
    'en': 'If applicable',
    'sl': 'Če ustreza',
    'fi': 'Tarvittaessa'
  };

  final _incorrectPassword = <String, String>{
    'nl': 'Wachtwoord onjuist',
    'en': 'Incorrect password',
    'sl': 'Napačno geslo',
    'fi': 'Väärä salasana'
  };

  final _incorrectUserName = <String, String>{
    'nl': 'Gebruikersnaam onjuist',
    'en': 'Incorrect username',
    'sl': 'Napačno uporabniško ime',
    'fi': 'Väärä käyttäjänimi'
  };

  final _info = <String, String>{
    'nl': 'Info',
    'en': 'Info',
    'sl': 'Info',
    'fi': 'Info'
  };

  final _infoBody = <String, String>{
    'nl': '''
        Door het gebruik van deze app helpt u mee aan het budget onderzoek van het centraal bureau statistiek.

Als dank voor uw help mogen wij u aan het einde van het onderzoek een VVV bon ter waarde van 30 euro aanbieden. Om in aanraking te komen voor deze bon gelden de volgende regels:

1) U heeft een uitnodiging ontvangen om deel te nemen aan het Eurostat onderzoek.
2) Gedurende 30 dagen houdt u iedere dag al uw uitgaven bij in deze app. Zowel uw vaste lasten, als ook variabele uitgaven.
3) Aan het einde van iedere dag verifieert u dat u alle uitgaven heeft bijgewerkt. Ook als u die dag niets heeft uitgegeven.
4) U doet mee aan dit onderzoek binnen de onderzoeks periode ''',
    'en': '''
    Welcome to the Household Budget Survey App!

This app is under development until the Summer of 2020 and intended for future use for the European Household Budget Survey.

You can use this app to keep track of your expenditure.

All data entered in this version of the app remains solely on the user’s device.  Also, in this stage of development of the app, the incentive of 1 euro for each completed day as shown on the overview screen is fictional.

For tutorials on how to use the app see ''',
    'fi': '''
    Tämä on sovelluksen testiversio. 
      
Tarkemmat tiedot Kulutustutkimuksesta päivitetään lopulliseen versioon.

''',
    'sl': '''
        V raziskovanju Poraba v gospodinjstvih zbiramo podatke o izdatkih gospodinjstev za življenjske potrebščine, na primer za hrano in pijače, oblačila in obutev, transport, stanovanje itd.  Te podatke zbiramo s pomočjo vprašalnika in dnevnika izdatkov. Za vodenje dnevnika izdatkov smo oblikovali aplikacijo APG,  ki je namenjena zbiranju podatkov o vsakodnevnih nakupih.

Prosimo vas, da naslednjih 14 dni vanjo vpisujete vse vaše izdatke. Na koncu vsakega dne preverite ali ste vnesli vse izdatke tega dne.

Vaš termin za vodenje dnevnika je med 26. avgustom in 19. septembrom. 

'''
  };

  final _infoForMoreInfo = <String, String>{
    'nl': 'Voor meer informatie kunt u terecht ',
    'en': 'We greatly appreciate your feedback at WINHelpdesk@cbs.nl',
    'sl': 'Za več informacij obiščite našo spletno stran ',
    'fi': 'Lisätietoja saat '
  };

  final _information = <String, String>{
    'nl': 'Informatie',
    'en': 'Information',
    'sl': 'Informacije',
    'fi': 'Tiedotus',
  };

  final _informationIncomplete = <String, String>{
    'nl': 'Informatie incompleet',
    'en': 'Information incomplete',
    'sl': 'Informacije so nepopolne',
    'fi': 'Tietoja puuttuu'
  };

  final _infoTitle = <String, String>{
    'nl': 'Welkom in de CBS uitgaven app!',
    'en': 'Budget research information',
    'sl': 'Raziskovanje Poraba v gospodinjstvih',
    'fi': 'Tietoa Kulutustutkimuksesta'
  };

  final _infoWebsiteLink = <String, String>{
    'nl': 'op de website van het CBS.',
    'en': 'our website.',
    'sl': 'www.stat.si',
    'fi': 'internet-sivuiltamme.'
  };

  final _insights = <String, String>{
    'nl': 'Inzichten',
    'en': 'Overview',
    'sl': 'Povzetek',
    'fi': 'Kooste'
  };

  final _insightsPage = <String, String>{
    'nl': 'Overzicht',
    'en': 'Overview',
    'sl': 'Pregled',
    'fi': 'Kooste'
  };

  final _invalidAmount = <String, String>{
    'nl': 'Ongeldig bedrag',
    'en': 'Invalid amount',
    'sl': 'Neveljaven znesek',
    'fi': 'Virheellinen määrä'
  };

  final _invalidInput = <String, String>{
    'nl': 'Ongeldige invoer',
    'en': 'Invalid input',
    'sl': 'Neveljaven vnos',
    'fi': 'Virheellinen syöte'
  };

  final _key = <String, String>{'nl': 'nl', 'en': 'en', 'sl': 'sl', 'fi': 'fi'};

  final _language = <String, String>{
    'nl': 'Taal',
    'en': 'Language',
    'sl': 'Jezik',
    'fi': 'Kieli'
  };

  final _languageValue = <String, String>{
    'nl': 'Nederlands',
    'en': 'English',
    'sl': 'Slovenščina',
    'fi': 'Englanti'
  };

  final _loginManually = <String, String>{
    'nl': 'Login handmatig',
    'en': 'Login manually',
    'sl': 'Prijava',
    'fi': 'Kirjaudu '
  };

  final _mainCategories = <String, String>{
    'nl': 'Hoofdcategorieën',
    'en': 'Main categories',
    'sl': 'Glavne kategorije',
    'fi': 'Tuoteryhmät'
  };

  final _mainExpenselistTutorial1 = <String, String>{
    'nl': 'Uw uitgaven',
    'en': 'Your expenses',
    'sl': 'Vaši izdatki',
    'fi': 'Ostoksesi',
  };

  final _mainExpenselistTutorial2 = <String, String>{
    'nl':
        'Het uitgaven scherm toont een lijst van alle uitgaven die u heeft ingevoerd. Hier kunt u uitgaven verwijderen, aanpassen of dupliceren.',
    'en':
        'The spending screen provides a list of all reported expenses. You can delete, edit or duplicate your expenses.',
    'sl':
        'Zaslon vsebuje vse vaše vnesene izdatke, ki jih lahko tudi odstranite, urejate ali podvajate.',
    'fi':
        'Tältä sivulta löydät listan kaikista kirjaamistasi ostoksista.\nVoit muokata tai poistaa niitä.',
  };

  final _mainExpenselistTutorial3 = <String, String>{
    'nl': 'Zoeken',
    'en': 'Search',
    'sl': 'Iskanje',
    'fi': 'Haku',
  };

  final _mainExpenselistTutorial4 = <String, String>{
    'nl':
        'Hier kunt u zoeken op woorden uit de beschrijving van de ingevoerde uitgaven.',
    'en': 'Here you can search your reported expenses.',
    'sl': 'Tu lahko poiščete kupljene izdelke ali storitve.',
    'fi': 'Tästä voit hakea kirjaamaasi ostosta.',
  };

  final _mainExpenselistTutorial5 = <String, String>{
    'nl': 'Filteren',
    'en': 'Filter',
    'sl': 'Filter',
    'fi': 'Suodatus',
  };

  final _mainExpenselistTutorial6 = <String, String>{
    'nl':
        'Met de filter optie kunt u een selectie van de ingevoerde uitgaven tonen. U kunt filteren op datum en op prijs.',
    'en':
        'With the filter option you can show a selection of the reported expenses. You can filter by date and by price.',
    'sl': 'Z uporabo filtra lahko izdatke izberete glede na datum in  znesek.',
    'fi':
        'Suodattimilla voit rajata sivulla näkyviä ostoksia.\nVoit rajata tietoja päivämäärän tai hinnan mukaan.',
  };

  final _mainInsights1 = <String, String>{
    'nl': 'Inzichten',
    'en': 'Overview',
    'sl': 'Povzetki',
    'fi': 'Kooste',
  };

  final _mainInsights10 = <String, String>{
    'nl':
        'Hier ziet u uw uitgaven naar categorie of tijd. U kunt tikken in de figuur voor meer details.',
    'en':
        'Here you can see your expenses by category or over time. Tap on the graph to see more details.',
    'sl':
        'Tu je na voljo slikovni prikaz izdatkov po kategorijah ali po dnevih. Lahko se dotaknete dela grafa ali kategorije, če želite videti več podrobnosti.',
    'fi':
        'Tästä näet ostoksiin käytetyn summan tuoteryhmittäin. Valitsemalla kaaviosta lohkon voit tarkastella ostoksiasi yksityiskohtaisemmin.',
  };

  final _mainInsights2 = <String, String>{
    'nl':
        'Bij inzichten ziet u een samenvatting van het totaal van uw ingevoerde uitgaven. U kunt hier zien waar u uw geld aan uitgeeft en wanneer.',
    'en':
        'This overview provides summaries of all reported expenses. Here you can find insights on when and what you spend your money on.',
    'sl':
        'Ta pregled vsebuje različne povzetke vseh prijavljenih stroškov. Tu lahko najdete vpogled v to, za kaj porabite svoj denar in kdaj to počnete.',
    'fi':
        'Koosteesta löydät erilaisia yhteenvetoja ostoksistasi.\nVoit tarkastella millaisia ostoksia olet tehnyt ja milloin.',
  };

  final _mainInsights3 = <String, String>{
    'nl': 'Filteren',
    'en': 'Filter',
    'sl': 'Filter',
    'fi': 'Suodatus',
  };

  final _mainInsights4 = <String, String>{
    'nl':
        'Hier kunt u filteren welke uitgaven u wilt opnemen in het overzicht. U kunt filteren naar datum en naar prijs.',
    'en':
        'Tap here to filter the expenses you want to include in the overview. You can filter by date and by price.',
    'sl': 'Z uporabo filtra lahko izdatke izberete glede na datum in  znesek.',
    'fi':
        'Suodattimilla voit rajata sivulla näkyvää koostetta.\nVoit rajata tietoja päivämäärän tai hinnan mukaan.',
  };

  final _mainInsights5 = <String, String>{
    'nl': 'Verschillende inzichten',
    'en': 'Overviews',
    'sl': 'Različni povzetki',
    'fi': 'Koosteet',
  };

  final _mainInsights6 = <String, String>{
    'nl':
        'U kunt een overzicht van uw uitgaven zien per categorie of over de tijd.',
    'en':
        'You can choose to see an overview of your expenses by category or over time.',
    'sl':
        'Tu si lahko ogledate pregled  izdatkov po kategorijah (skupinah izdelkov) ali  dnevih.',
    'fi': 'Voit tarkastella koostetta tuoteryhmittäin tai aikajanalla.',
  };

  final _mainInsights7 = <String, String>{
    'nl': 'Categorieën',
    'en': 'Categories',
    'sl': 'Kategorije',
    'fi': 'Tuoteryhmät',
  };

  final _mainInsights8 = <String, String>{
    'nl':
        'De categorieën zijn gebaseerd op hoe de uitgaven zijn ingevoerd. Tik op een categorie om meer details te zien.',
    'en':
        'Categories are based on the different types of expenses entered. Tap on a category to see more details.',
    'sl':
        'Kategorije predstavljajo skupine izdelkov. Za podrobnejši pregled znotraj kategorij, izberite posamezno kategorijo, ki vas zanima.',
    'fi':
        'Tuoteryhmät perustuvat kirjaamiisi ostoksiin.\nValitsemalla tuoteryhmän näet siitä tarkempia tietoja.',
  };

  final _mainInsights9 = <String, String>{
    'nl': 'Grafieken',
    'en': 'Graphs',
    'sl': 'Grafikon',
    'fi': 'Kaavio',
  };

  final _mainOverviewTutorial1 = <String, String>{
    'nl': 'Hoe werkt deze app?',
    'en': 'How does this app work?',
    'sl': 'Kako deluje aplikacija?',
    'fi': 'Miten sovellus toimii?',
  };

  final _mainOverviewTutorial10 = <String, String>{
    'nl':
        'Hier kunt u beginnen met het invoeren van een uitgave. Na tikken op het plus-teken kunt u kiezen om een foto te maken van een bon of om de details van de uitgave handmatig in te voeren.',
    'en':
        'Here you can start reporting an expense. Tap the plus sign to start reporting an expense. You can either take a picture of a receipt or enter the expense details manually.',
    'sl':
        'Tukaj začnete z vnosom izdatkov. Po izboru ikone plus (+), lahko račun fotografirate ali pa vsebino računa vnesite ročno.',
    'fi':
        'Tästä voit aloittaa ostosten tallentamisen. Plus-painikkeesta pääset ottamaan kuvan kuitista tai kirjaamaan ostoksen tiedot manuaalisesti.',
  };

  final _mainOverviewTutorial2 = <String, String>{
    'nl':
        'Noteer a.u.b. voor de hele opgaveweek dagelijks al uw uitgaven en die van eventuele andere leden van uw huishouden.',
    'en':
        'Please enter all your expenses and those of any other members of your household every day for the duration of the survey.',
    'sl':
        'Prosimo vnesite vsak dan vse svoje izdatke in se izdatke vseh članov gospodinjstva v 14 dnevnem obdobju vodenja dnevnika izdatkov. Ta zaslon bo prikazal, kako napredujete.',
    'fi':
        'Kirjaa päivittäin kaikki tekemäsi ostokset sekä kotitaloutesi muiden jäsenten ostokset koko tutkimusjakson ajan. Tältä sivulta näet ostosten kirjaamisen tilanteen.',
  };

  final _mainOverviewTutorial3 = <String, String>{
    'nl': 'Hoe werkt deze informatie pagina?',
    'en': 'How does this tutorial work?',
    'sl': 'Kako deluje vodič po aplikaciji?',
    'fi': 'Mistä löydät ohjeet?',
  };

  final _mainOverviewTutorial4 = <String, String>{
    'nl':
        "U kunt op ieder scherm op de 'i' tikken voor een korte rondleiding. Druk ergens op het scherm om naar de volgende pagina te gaan.",
    'en':
        "On each screen you can tap the  'i' for a short tour of the screen. Press anywhere on the screen to go to the next page.",
    'sl':
        '''Vedno lahko izberite ikono 'i' za kratka pojasnila za zaslon. Pritisnite kjer koli na zaslonu, da odprete naslednjo stran.''',
    'fi':
        '''Avaa ohjeet 'i'-painikkeesta. Siirry seuraavalle sivulle painamalla mitä tahansa näytön kohtaa.''',
  };

  final _mainOverviewTutorial5 = <String, String>{
    'nl': 'Uw voortgang',
    'en': 'Your progress',
    'sl': 'Vaš napredek',
    'fi': 'Tilanne',
  };

  final _mainOverviewTutorial6 = <String, String>{
    'nl':
        'De kalender toont de opgaveweek voor het onderzoek, de huidige dag en voor welke dagen u uw uitgaven al heeft ingevuld.',
    'en':
        'The calendar shows the duration of this survey, the current day, and for which days you have already entered your expenses.',
    'sl':
        'Koledar prikazuje obdobje trajanja vodenja dnevnika izdatkov, trenutni dan, in za katere dneve ste že vnesli svoje izdatke.',
    'fi':
        'Kalenterista näet tutkimusjakson keston, nykyisen päivän sekä miltä päiviltä olet kirjannut ostokset.',
  };

  final _mainOverviewTutorial7 = <String, String>{
    'nl': 'Uw beloning',
    'en': 'Your reward',
    'sl': '',
    'fi': '',
  };

  final _mainOverviewTutorial8 = <String, String>{
    'nl':
        'Als u de uitgaven van uw huishouden voor de hele opgaveweek heeft gerapporteerd, ontvangt u een cadeaukaart van 30 euro als teken van waardering voor uw bijdrage aan dit onderzoek.',
    'en':
        'If you have reported the expenses of your household for the entire duration of they survey, you will receive a gift card of 30 euros as a token of appreciation for your contribution to this study.',
    'sl': '',
    'fi': '',
  };

  final _mainOverviewTutorial9 = <String, String>{
    'nl': 'Uitgaven toevoegen',
    'en': 'Adding expenditures',
    'sl': 'Vnos izdatkov',
    'fi': 'Ostosten kirjaaminen',
  };

  final _mainSettings1 = <String, String>{
    'nl': 'Dagelijkse herinnering',
    'en': 'Daily reminder',
    'sl': 'Dnevni opomnik',
    'fi': 'Ilmoitukset',
  };

  final _mainSettings2 = <String, String>{
    'nl':
        'Hier kunt u een dagelijkse herinnering aan het onderzoek aanzetten op uw telefoon. U krijgt dan een notificatie om u eraan ter herinneren uw dagelijkse uitgaven in te vullen. Het tijdstip voor de notificatie kunt u zelf instellen. Standaard staat dit op 20.00.',
    'en':
        'Here you can set a daily reminder. This will send you a notification to remind you to report your daily expenses.',
    'sl':
        'Tu lahko nastavite dnevni opomnik za izpolnjevanje dnevnika. To pomeni, da boste prejeli obvestilo, ki vas bo spomnilo na vnašanje dnevnih izdatkov. Imate možnost, da si sami izberete čas prejemanja opomnika. Sicer je privzeti čas nastavljen ob 20.00.',
    'fi':
        'Voit asettaa itsellesi päivittäisen muistutuksen.\nIlmoitus muistuttaa sinua kirjaamaan päivittäiset ostoksesi.\nVoit asettaa muistutukseen haluamasi kellonajan.\nOletusasetuksena on klo 20.00.',
  };

  final _mainSettings3 = <String, String>{
    'nl': 'Contact informatie',
    'en': 'Contact information',
    'sl': 'Kontaktni podatki',
    'fi': 'Käyttäjätuki',
  };

  final _mainSettings4 = <String, String>{
    'nl':
        'Voor meer informatie over dit onderzoek of hulp bij het gebruik van de app kunt u ons bellen of mailen. Onze helpdesk is van maandag tot en met vrijdag bereikbaar van 8.30 tot 17.00.',
    'en':
        'If you want more information about the survey or need help using the app, please contact us by phone or email. Our helpdesk is open Monday to Friday, 8.30 am to 5.30 pm.',
    'sl': '',
    'fi':
        'Jos haluat lisätietoja tutkimuksesta tai tarvitset apua sovelluksen käytössä, ota yhteyttä puhelimitse tai sähköpostilla. Käyttäjätukemme on avoinna maanantaista perjantaisin klo 9-15.',
  };

  final _mainSettings5 = <String, String>{
    'nl': 'Taal kiezen',
    'en': 'Choose a language',
    'sl': 'Izberite jezik',
    'fi': 'Valitse kieli',
  };

  final _mainSettings6 = <String, String>{
    'nl':
        'U kunt hier de taal van de app aanpassen. Momenteel is de app beschikbaar in het Nederlands, Engels, Fins en Sloveens.',
    'en':
        'Select the language you want to use the app in. The app is currently available in English, Dutch, Finnish and Slovenian.',
    'sl':
        'Lahko si izberete jezik, v katerem boste uporabljali aplikacijo. Aplikacija je dostopna v slovenskem, angleškem, nizozemskem in finskem jeziku.',
    'fi':
        'Voit valita sovelluksen kielen valikosta.\nSovellus on saatavilla suomeksi, englanniksi, hollanniksi ja sloveniaksi.',
  };

  final _maximum = <String, String>{
    'nl': 'Maximum',
    'en': 'Maximum',
    'sl': 'Največ',
    'fi': 'Maksimi'
  };

  final _minimum = <String, String>{
    'nl': 'Minimum',
    'en': 'Minimum',
    'sl': 'Najmanj',
    'fi': 'Minimi'
  };

  final _modify = <String, String>{
    'nl': 'Aanpassen',
    'en': 'Modify',
    'sl': 'Spremeni',
    'fi': 'Muokkaa'
  };

  final _monday = <String, String>{
    'nl': 'Ma',
    'en': 'Mo',
    'sl': 'Po',
    'fi': 'Ma'
  };

  final _moreInfo = <String, String>{
    'nl': 'Meer informatie',
    'en': 'More information',
    'sl': 'Več informacij',
    'fi': 'Lisätiedot'
  };

  final _newEntryTutorial1 = <String, String>{
    'nl': 'Bon invoeren',
    'en': 'Adding expenses',
    'sl': 'Vnos izdatkov',
    'fi': 'Ostosten lisääminen',
  };

  final _newEntryTutorial10 = <String, String>{
    'nl':
        'Wat heeft u gekocht? Noteer hier alle producten en diensten afzonderlijk.\nIdentieke items kunt u makkelijk kopiëren.\nNoteer per item de prijs die u daadwerkelijk betaald heeft. Voor items met korting kunt u ook de originele prijs noteren en de korting als een apart item toevoegen. Korting over een hele bon met meerdere items a.u.b. invoeren via de knop Korting op totaal.',
    'en':
        'Via this field you can enter your expenses. Enter all items seperately.\nYou can easily copy identical items.\nPlease enter the actual price paid per item. For discount received on an entire receipt with multiple items, please use the Discount on total button below.',
    'sl':
        'Kaj ste kupili? Tu vnesite vsak posamezen izdelek ali storitev.\nČe ste prejeli popust na posamezen izdelek ali storitev, vnesite ceno z upoštevanim popustom (končno ceno).',
    'fi':
        'Kirjoita kaikki ostokseen kuuluneet tuotteet ja palvelut yksitellen tähän.\nJos ostit useamman kappaleen samaa tuotetta, voit kopioida ne kun merkitset tuotteen hintaa.\nVoit myös ilmoittaa tuotteen hinnasta saadun alennuksen tai tuotepalautukseen liittyvän hyvityksen.',
  };

  final _newEntryTutorial11 = <String, String>{
    'nl': 'Korting op totaal',
    'en': 'Discount on entire receipt',
    'sl': 'Popust',
    'fi': 'Alennus',
  };

  final _newEntryTutorial12 = <String, String>{
    'nl':
        'Korting op het totaal van een bon met meerdere items noteert u hier. Kies of u de totale korting opgeeft als percentage of als totaal bedrag.',
    'en':
        'Here you can enter a discount for the entire receipt if applicable. Choose either a percentage discount or a total amount discount.',
    'sl':
        'Tu vnesite popust na celotni račun. Izberite, ali želite navesti skupni popust v odstotkih ali v znesku.',
    'fi':
        'Kirjaa koko ostotapahtumaa koskeva alennus tähän. Valitse kirjaatko alennusprosentin vai euromäärän.',
  };

  final _newEntryTutorial13 = <String, String>{
    'nl': 'Totaal bedrag',
    'en': 'Total amount',
    'sl': 'Skupni znesek',
    'fi': 'Loppusumma',
  };

  final _newEntryTutorial14 = <String, String>{
    'nl':
        'Hier ziet u het totale bedrag van alle ingevoerde items en kortingen.',
    'en':
        'Here you can see the total amount of all entered items, including expenses, discounts and refunds.',
    'sl':
        'Tu si lahko ogledate skupni znesek posameznega nakupa skupaj z morebitnim vnesenim popustom.',
    'fi': 'Täältä näet kirjaamiesi tuotteiden loppusumman.',
  };

  final _newEntryTutorial15 = <String, String>{
    'nl': 'Gereed',
    'en': 'Done',
    'sl': 'Zaključevanje nakupa',
    'fi': 'Valmis',
  };

  final _newEntryTutorial16 = <String, String>{
    'nl': 'Kies gereed als u alle items van een winkel heeft ingevoerd.',
    'en':
        '''Tap the 'done' button when you have finished entering all the information from an expense.''',
    'sl':
        '''Ko ste končali z dodajanjem vseh informacij, pritisnite na gumb 'Zaključi'.''',
    'fi':
        '''Kuittaa kirjaamasi ostotapahtuma valmiiksi, kun olet lisännut siihen kaikki tiedot.''',
  };

  final _newEntryTutorial2 = <String, String>{
    'nl': 'In dit scherm kunt u uw uitgaven handmatig invoeren.',
    'en': 'Here you can enter your expenses manually.',
    'sl': 'Tu lahko izdatke vnesete ročno.',
    'fi': 'Tällä sivulla pääset lisäämään ostoksesi manuaalisesti.',
  };

  final _newEntryTutorial3 = <String, String>{
    'nl': 'Winkel invoeren',
    'en': 'Select a shop',
    'sl': 'Izbor trgovine',
    'fi': 'Kaupan valmitseminen',
  };

  final _newEntryTutorial4 = <String, String>{
    'nl':
        'Waar of aan wie heeft u de uitgave heeft gedaan? Type de naam van de winkelketen of een omschrijving, bijvoorbeeld: C&A, Lidl, kapper, tandarts, particuliere verkoper via marktplaats. Winkels die u niet in de lijst vindt kunt zelf toevoegen met de knop ‘niet gevonden’. Eerder ingevoerde winkels vindt u makkelijk terug als u begint met invoeren.',
    'en':
        '''In this field you can enter the name or description of the shop, for example: Lidl, hairdresser, dentist, private seller via eBay. Shops that you can't find in the list can be added manually with the 'not found' button. You can also use the 'recent items' and 'frequent items' list to find previously selected shops.''',
    'sl':
        '''Kje ste nakupovali? Vnesite tip trgovine ali njen opis, na primer: zobozdravnik, živilska trgovina, eBay. Trgovin, ki jih ne najdete na seznamu trgovin, lahko sami dodate s pritiskom na gumb  'Ni najdeno'. Že vnesene trgovine lahko najdete na seznamu zadnjih vnosov ali na seznamu najpogostejših vnosov trgovin.''',
    'fi':
        '''Kirjoita kauppaketjun tai palveluntarjoajan nimi, esimerkiksi:\nPrisma, K-supermarket, kampaamo, hammaslääkäri, yksityishenkilö Tori.fi:ssä.\nJos kauppaa ei löydy listasta, voit lisätä sen itse 'Ei löytynyt?' -painikkeesta.\nLöydät aiemmin kirjaamasi kaupat myös viimeisimmäksi.\nkirjattujen tai usein kirjattujen kauppojen listasta.''',
  };

  final _newEntryTutorial5 = <String, String>{
    'nl': 'Datum invoeren',
    'en': 'Select a date',
    'sl': 'Izbor datuma',
    'fi': 'Päivämäärän valinta',
  };

  final _newEntryTutorial6 = <String, String>{
    'nl': 'Wanneer heeft u de uitgave gedaan?',
    'en': 'In this field you can enter de date of the expense.',
    'sl': 'Kdaj ste nakupovali?',
    'fi': 'Minä päivänä teit ostoksesi?',
  };

  final _newEntryTutorial7 = <String, String>{
    'nl': 'Overige',
    'en': 'Other',
    'sl': 'Drugo',
    'fi': 'Lisätiedot',
  };

  final _newEntryTutorial8 = <String, String>{
    'nl':
        'Geef aan of de aankoop in het buitenland of online is gedaan. Bij een online aankoop wordt besteld via een computer, tablet, smartphone e.d.',
    'en':
        'With these switches you can indicate whether the expense was made abroad or online. An online expense is ordered via a computer, tablet, smartphone, etc.',
    'sl':
        'Navedite, ali je bil nakup opravljen v tujini ali prek spleta. Spletni nakup je naročen prek računalnika, tabličnega računalnika, pametnega telefona itd.',
    'fi':
        'Merkitse, jos teit ostoksen ulkomailla tai verkossa. Verkko-ostos tarkoittaa tietokoneella, tabletilla tai puhelimella tehtyä ostosta.',
  };

  final _newEntryTutorial9 = <String, String>{
    'nl': 'Producten, diensten en teruggaven',
    'en': 'Expenses and returns',
    'sl': 'Izdelek, storitev in popust',
    'fi': 'Tuotteet, palvelut ja palautukset',
  };

  final _newTransaction = <String, String>{
    'nl': 'Nieuwe transactie',
    'en': 'New transaction',
    'sl': 'Nov nakup',
    'fi': 'Uusi osto'
  };

  final _no = <String, String>{'nl': 'Nee', 'en': 'No', 'sl': 'NE', 'fi': 'Ei'};

  final _noDiscount = <String, String>{
    'nl': 'Geen korting',
    'en': 'No discount',
    'sl': 'Brez popusta',
    'fi': 'Ei alennusta'
  };

  final _noEarlierExpenses = <String, String>{
    'nl': 'Geen eerdere uitgaven gevonden. Controleer de filter waarden.',
    'en': 'No earlier expenses found. Check filter settings.',
    'sl':
        'Predhodni stroški niso bili ugotovljeni. Preverite nastavitve filtra.',
    'fi': 'Aikaisempia kuluja ei löytynyt. Tarkista suodattimen asetukset.'
  };

  final _noExpensesFound = <String, String>{
    'nl': 'Geen uitgaven gevonden',
    'en': 'No expenses found',
    'sl': 'Ni zadetkov',
    'fi': 'Ei ostoksia'
  };

  final _noLaterExpenses = <String, String>{
    'nl': 'Geen latere uitgaven gevonden. Controleer de filter waarden.',
    'en': 'No later expenses found. Check filter settings.',
    'sl': 'Pozneje ni bilo ugotovljenih stroškov. Preverite nastavitve filtra.',
    'fi': 'Myöhempiä kuluja ei löytynyt. Tarkista suodattimen asetukset.'
  };

  final _none = <String, String>{
    'nl': 'Geen',
    'en': 'None',
    'sl': 'Noben',
    'fi': 'Ei yhtään'
  };

  final _notFound = <String, String>{
    'nl': 'Niet gevonden',
    'en': 'Not found',
    'sl': 'Ni najdeno',
    'fi': 'Ei löytynyt'
  };

  final _notificationMessage = <String, String>{
    'nl': 'Vergeet niet je uitgaven in te vullen!',
    'en': "Don't forget to enter your expenses!",
    'sl': 'Ne pozabite vpisati vseh izdatkov.',
    'fi': 'Muista kirjata ostoksesi!'
  };

  final _notifications = <String, String>{
    'nl': 'Notificaties',
    'en': 'Notifications',
    'sl': 'Obvestila',
    'fi': 'Ilmoitukset'
  };

  final _notificationsOff = <String, String>{
    'nl': 'Notificaties staan nu uit',
    'en': 'Reminder turned off',
    'sl': 'Obvestila so izklopljena',
    'fi': 'Muistutus ei käytössä'
  };

  final _notificationsTurnedOnAt = <String, String>{
    'nl': 'Notificaties aangezet om ',
    'en': 'Reminder set for ',
    'sl': 'Obvestilo bo prikazano ob ',
    'fi': 'Muistutus asetettu klo '
  };

  final _offValue = <String, String>{
    'nl': 'Uit',
    'en': 'Off',
    'sl': 'Vklop',
    'fi': 'Ei käytössä'
  };

  final _ok = <String, String>{
    'nl': 'Ok',
    'en': 'Ok',
    'sl': 'V redu',
    'fi': 'Ok'
  };

  final _online = <String, String>{
    'nl': 'Online',
    'en': 'Online',
    'sl': 'Preko spleta',
    'fi': 'Verkkokaupasta'
  };

  final _onValue = <String, String>{
    'nl': 'Aan',
    'en': 'On',
    'sl': 'Izklop',
    'fi': 'Käytössä'
  };

  final _operatingExpense = <String, String>{
    'nl': 'Huishoudelijke uitgaven',
    'en': 'Operating expenses',
    'sl': 'Operativni stroÅ¡ki',
    'fi': 'Kodintavarat'
  };

  final _or = <String, String>{
    'nl': 'of',
    'en': 'or',
    'sl': 'Ali',
    'fi': 'tai'
  };

  final _otherCategory = <String, String>{
    'nl': 'Overige',
    'en': 'Other',
    'sl': 'Drugo',
    'fi': 'Muut ostokset'
  };

  final _others = <String, String>{
    'nl': 'Overige',
    'en': 'Additional',
    'sl': 'Dodatno',
    'fi': 'Lisätiedot'
  };

  final _others2 = <String, String>{
    'nl': 'Overige',
    'en': 'Where did you buy this?',
    'sl': 'Dodatno',
    'fi': 'Lisätiedot: Oliko osto...'
  };

  final _overTime = <String, String>{
    'nl': 'Over tijd',
    'en': 'Over time',
    'sl': 'Po dnevih',
    'fi': 'Aikajana'
  };

  final _overviewPage = <String, String>{
    'nl': 'Kalender',
    'en': 'Calendar',
    'sl': 'Koledar',
    'fi': 'Etusivu'
  };

  final _password = <String, String>{
    'nl': 'Wachwoord',
    'en': 'Password',
    'sl': 'Geslo',
    'fi': 'Salasana'
  };

  final _people = <String, String>{
    'nl': 'Personen',
    'en': 'People',
    'sl': 'Osebe',
    'fi': 'Henkilöt'
  };

  final _phoneNumber = <String, String>{
    'nl': 'Telefoonnummer',
    'en': 'Phone number',
    'sl': 'Telefonska številka',
    'fi': 'Puhelinnumero'
  };

  final _phoneNumberDigits = <String, String>{
    'nl': '06 36 305 201',
    'en': '+31 70 337 3800',
    'sl': '030 721 572',
    'fi': '029  551 3047'
  };

  final _photographReceipt = <String, String>{
    'nl': 'Bon fotograferen',
    'en': 'Photograph of receipt',
    'sl': 'Slikanje računa',
    'fi': 'Ota kuva kuitista'
  };

  final _precentage = <String, String>{
    'nl': 'Percentage',
    'en': 'Percentage',
    'sl': 'Odstotek',
    'fi': 'Prosenttia'
  };

  final _preferences = <String, String>{
    'nl': 'Voorkeuren',
    'en': 'Preferences',
    'sl': 'Osnovno',
    'fi': 'Valinnat'
  };

  final _price = <String, String>{
    'nl': 'Prijs',
    'en': 'Price',
    'sl': 'Cena',
    'fi': 'Hinta'
  };

  final _priceRange = <String, String>{
    'nl': 'Bedragen',
    'en': 'Price range',
    'sl': 'Cenovni razpon',
    'fi': 'Hinta'
  };

  final _productCategory = <String, String>{
    'nl': 'Producten categorie',
    'en': 'Product type',
    'sl': 'Kategorija izdelkov',
    'fi': 'Tuoteryhmä'
  };

  final _productDiscount = <String, String>{
    'nl': 'Korting op totaal',
    'en': 'Discount',
    'sl': 'Popust',
    'fi': 'Alennus'
  };

  final _products = <String, String>{
    'nl': 'Producten',
    'en': 'Products',
    'sl': 'Izdelki',
    'fi': 'Tuotteet'
  };

  final _productService = <String, String>{
    'nl': 'Product / Dienst',
    'en': 'Product / Service',
    'sl': 'Izdelek / storitev',
    'fi': 'Tuote/palvelu'
  };

  final _progress = <String, String>{
    'nl': 'Voortgang:',
    'en': 'Progress:',
    'sl': 'Napredek',
    'fi': 'Tilanne'
  };

  final _purchasedfrom = <String, String>{
    'nl': 'Gekocht bij',
    'en': 'Purchased from',
    'sl': 'Kupljeno v ',
    'fi': 'Ostopaikka'
  };

  final _receipt = <String, String>{
    'nl': 'Bon',
    'en': 'Receipt',
    'sl': 'Prejemu',
    'fi': 'Kuitti'
  };

  final _receiptItems = <String, String>{
    'nl': 'Items',
    'en': 'Receipt items',
    'sl': 'Nakup',
    'fi': 'Tuotteet'
  };

  final _recentItems = <String, String>{
    'nl': 'Recente items',
    'en': 'Recent items',
    'sl': 'Zadnji vnos',
    'fi': 'Viimeisimmät',
  };

  final _recreationAndCultur = <String, String>{
    'nl': 'Recreatie en cultuur',
    'en': 'Recreation, entertainment',
    'sl': 'Rekreacija in kultura',
    'fi': 'Vapaa-aika ja kulttuuri'
  };

  final _removalWarningText = <String, String>{
    'nl':
        'U staat op het punt de transactie af te sluiten en de ingevoerde data wordt hierbij verwijderd.',
    'en':
        'You are about to close this transaction. If you press delete, the transaction will not be saved.',
    'sl':
        '''Vaši vnos ni bil shranjen:\nČe želite nadaljevati z vnosom, izberite gumb 'Prekliči'.\nČe želite zaključiti nakup, izberite gumb 'Prekliči' in nato gumb 'Zaključi'.\nČe želite izbrisati nakup, izberite 'Briši'.''',
    'fi':
        'Olet sulkemassa ostotapahtumaa. Jos jatkat, tämä ostotapahtuma poistetaan.'
  };

  final _removeFilters = <String, String>{
    'nl': 'Filters wissen',
    'en': 'Remove filters',
    'sl': 'Odstrani filtre',
    'fi': 'Poista rajaukset'
  };

  final _required = <String, String>{
    'nl': 'Verplicht',
    'en': 'Required',
    'sl': 'Obvezno',
    'fi': 'Pakollinen'
  };

  final _restaurantAndHotel = <String, String>{
    'nl': 'Restaurants en hotel',
    'en': 'Hotels and restaurants',
    'sl': 'Restavracije in hoteli',
    'fi': 'Ravintolat ja hotellit'
  };

  final _saturday = <String, String>{
    'nl': 'Za',
    'en': 'Sa',
    'sl': 'So',
    'fi': 'La'
  };

  final _scanQRcode = <String, String>{
    'nl': 'Scan QR code',
    'en': 'Scan QR code',
    'sl': 'Skeniraj QR kodo',
    'fi': 'Skannaa QR-koodi'
  };

  final _selectFilter = <String, String>{
    'nl': 'Filter selecteren',
    'en': 'Select filter',
    'sl': 'Izberi filter',
    'fi': 'Valitse rajaus'
  };

  final _selectShop = <String, String>{
    'nl': 'Winkel selecteren',
    'en': 'Select Shop',
    'sl': 'Izberi trgovino',
    'fi': 'Valitse kauppa'
  };

  final _sendMessage = <String, String>{
    'nl': 'Bericht versturen',
    'en': 'Send message',
    'sl': 'Pošlji sporočilo',
    'fi': 'Lähetä viesti'
  };

  final _settings = <String, String>{
    'nl': 'Instellingen',
    'en': 'Settings',
    'sl': 'Pomoč',
    'fi': 'Asetukset'
  };

  final _shareWithFamily = <String, String>{
    'nl': 'Uitgaven delen',
    'en': 'Share expenses',
    'sl': 'Deli s člani gospodinjstva',
    'fi': 'Jaa kustannukset'
  };

  final _shop = <String, String>{
    'nl': 'Winkel',
    'en': 'Shop',
    'sl': 'Trgovina',
    'fi': 'Kauppa'
  };

  final _shopType = <String, String>{
    'nl': 'Winkeltype',
    'en': 'Shop type',
    'sl': 'Vrsta trgovine',
    'fi': 'Kauppatyyppi'
  };

  final _skip = <String, String>{
    'nl': 'OVERSLAAN',
    'en': 'SKIP',
    'sl': 'Preskoči',
    'fi': 'OHITA',
  };

  final _start = <String, String>{
    'nl': 'Start',
    'en': 'Start',
    'sl': 'Začni',
    'fi': 'Aloita'
  };

  final _sunday = <String, String>{
    'nl': 'Zo',
    'en': 'Su',
    'sl': 'Ne',
    'fi': 'Su'
  };

  final _swipeToNavigate = <String, String>{
    'nl': 'Tik om door te gaan',
    'en': 'Tap to continue',
    'sl': 'Tapnite za nadaljevanje',
    'fi': 'Napauta jatkaaksesi'
  };

  final _thursday = <String, String>{
    'nl': 'Do',
    'en': 'Th',
    'sl': 'Če',
    'fi': 'To'
  };

  final _time = <String, String>{
    'nl': 'Tijdstip',
    'en': 'Time',
    'sl': 'Čas',
    'fi': 'Aika '
  };

  final _tips = <String, String>{
    'nl': 'Tips',
    'en': 'Tips',
    'sl': 'Tips',
    'fi': 'Vinkit',
  };

  final _title = <String, String>{
    'nl': 'titel',
    'en': 'title',
    'sl': 'Naslov',
    'fi': 'otsikko'
  };

  final _today = <String, String>{
    'nl': 'Vandaag',
    'en': 'Today',
    'sl': 'Danes',
    'fi': 'Tänään'
  };

  final _toLogin = <String, String>{
    'nl':
        'Om in te loggen, kunt u de verstrekte QR-code scannen of handmatig inloggen',
    'en': 'To login, scan the QR code provided or login manually',
    'sl':
        'Za prijavo lahko skenirate QR kodo ali ročno vnesete uporabniško ime in geslo',
    'fi': 'Voit kirjautua QR-koodilla tai antamalla tunnuksen ja salasanan'
  };

  final _tomorrow = <String, String>{
    'nl': 'Morgen',
    'en': 'Tomorrow',
    'sl': 'Jutri',
    'fi': 'Huomenna'
  };

  final _total = <String, String>{
    'nl': 'Totaal',
    'en': 'Total',
    'sl': 'Skupaj',
    'fi': 'Euroa '
  };

  final _total2 = <String, String>{
    'nl': 'Totaal',
    'en': 'Total',
    'sl': 'Znesek',
    'fi': 'Euroa '
  };

  final _totalAmount = <String, String>{
    'nl': 'Totaal bedrag',
    'en': 'Total amount',
    'sl': 'Skupni znesek',
    'fi': 'Loppusumma'
  };

  final _totalPrice = <String, String>{
    'nl': 'Totaal prijs',
    'en': 'Total price',
    'sl': 'Skupna cena',
    'fi': 'Loppusumma'
  };

  final _transactionDetails = <String, String>{
    'nl': 'Transactie details',
    'en': 'Transaction details',
    'sl': 'Podrobnosti transakcije',
    'fi': 'Ostotapahtuman tiedot'
  };

  final _transactionRemoved = <String, String>{
    'nl': 'Transactie verwijderd',
    'en': 'Transaction removed',
    'sl': 'Nakup je bil izbrisan.',
    'fi': 'Osto poistettu'
  };

  final _transactions = <String, String>{
    'nl': 'Transacties',
    'en': 'Transactions',
    'sl': 'Transakcije',
    'fi': 'Ostotapahtumat'
  };

  final _transport = <String, String>{
    'nl': 'Vervoer',
    'en': 'Transport',
    'sl': 'Prevoz',
    'fi': 'Liikkuminen'
  };

  final _tuesday = <String, String>{
    'nl': 'Di',
    'en': 'Tu',
    'sl': 'To',
    'fi': 'Ti'
  };

  final _understood = <String, String>{
    'nl': 'Begrepen',
    'en': 'Understood',
    'sl': 'V redu',
    'fi': 'OK'
  };

  final _undo = <String, String>{
    'nl': 'Ongedaan maken',
    'en': 'Undo',
    'sl': 'Razveljavi',
    'fi': 'Peruuta'
  };

  final _url = <String, String>{
    'nl': 'https://www.cbs.nl',
    'en': 'https://www.cbs.nl',
    'sl': 'https://www.stat.si',
    'fi': 'http://stat.fi'
  };

  final _user = <String, String>{
    'nl': 'Gebruiker',
    'en': 'User',
    'sl': 'Uporabniško ime',
    'fi': 'Tunnus'
  };

  final _variableExpenses = <String, String>{
    'nl': 'Variabele uitgaven',
    'en': 'Variable expenses',
    'sl': 'Spremenljivi izdatki',
    'fi': 'Vaihtelevat kulut'
  };

  final _variablexExpensesFilled = <String, String>{
    'nl': 'Variabele uitgaven',
    'en': 'Variable expenses',
    'sl': 'Spremenljivi izdatki',
    'fi': 'Vaihtelevat kulut'
  };

  final _versionNumber = <String, String>{
    'nl': 'Versienummer',
    'en': 'Version number',
    'sl': 'Številka različice',
    'fi': 'Versionumero',
  };

  final _warning = <String, String>{
    'nl': 'Waarschuwing',
    'en': 'Cancel transaction',
    'sl': 'Opozorilo',
    'fi': 'Huomio'
  };

  final _warningIncomplete = <String, String>{
    'nl':
        'U moet eerst de winkel informatie invullen en minimaal één product toevoegen.',
    'en': 'Please enter a shop, date of transaction and item to continue.',
    'sl':
        'Najprej morate izpolniti zahtevane podatke o trgovini in dodati vsaj en izdelek.',
    'fi': 'Lisää kauppa ja vähintään yksi tuote.'
  };

  final _warningNoProduct = <String, String>{
    'nl': 'Voeg eerst minstens één product toe.',
    'en': 'First add at least one product.',
    'sl': 'Najprej dodajte vsaj en izdelek.',
    'fi': 'Lisää vähintään yksi tuote.'
  };

  final _warningNoShopInfo = <String, String>{
    'nl': 'Vul eerst de winkel informatie in.',
    'en': 'First complete the required store information.',
    'sl': 'Najprej dokončajte zahtevane podatke o trgovini.',
    'fi': 'Lisää kauppa.'
  };

  final _wednesday = <String, String>{
    'nl': 'Wo',
    'en': 'We',
    'sl': 'Sr',
    'fi': 'Ke'
  };

  final _welcome = <String, String>{
    'nl': 'Welkom',
    'en': 'Welcome',
    'sl': 'Pozdravljeni',
    'fi': 'Tervetuloa'
  };

  final _www = <String, String>{
    'nl': 'www.cbs.nl',
    'en': 'www.cbs.nl',
    'sl': 'www.stat.si',
    'fi': 'www.stat.fi'
  };

  final _yes = <String, String>{
    'nl': 'Ja',
    'en': 'Yes',
    'sl': 'DA',
    'fi': 'Kyllä'
  };

  final _yesterday = <String, String>{
    'nl': 'Gisteren',
    'en': 'Yesterday',
    'sl': 'Včeraj',
    'fi': 'Eilen'
  };

  final _youCanNotCompleteDays = <String, String>{
    'nl':
        'U kunt geen dagen in de toekomst of buiten de experiment periode afsluiten.',
    'en':
        "You can't complete days which are in the future or outside of the experimental period.",
    'sl':
        'Dnevov v prihodnosti ali izven opazovanega obdobja ne morete zaključiti.',
    'fi':
        'Et voi kuitata tulevaisuudessa tai tutkimusjakson ulkopuolella olevia päiviä.'
  };

  final _youHaveNotAdded = <String, String>{
    'nl': 'U heeft nog niet toegevoegd',
    'en': "You haven't added",
    'sl': 'Niste še dodali',
    'fi': 'Et ole vielä lisännyt tuotetta'
  };

  final _pictureInfoIncomplete = <String, String>{
    'nl': 'U heeft nog niet alle benodigde informatie ingevuld.',
    'en': 'You have not yet filled in all the necessary information.',
    'sl': 'Še niste izpolnili vseh potrebnih podatkov.',
    'fi': 'Et ole vielä täyttänyt kaikkia tarvittavia tietoja.'
  };

  final _startingQuestionnaire = <String, String>{
    'nl': 'Startvragenlijst',
    'en': 'Starting questionnaire',
    'sl': 'Startvragenlijst',
    'fi': 'Startvragenlijst',
  };

  final _save = <String, String>{
    'nl': 'Opslaan',
    'en': 'Save',
    'sl': 'Opslaan',
    'fi': 'Opslaan',
  };

  final _question = <String, String>{
    'nl': 'Vraag',
    'en': 'Question',
    'sl': 'Vraag',
    'fi': 'Vraag',
  };

  final _ageQuestion = <String, String>{
    'nl': 'Wat is uw leeftijd?',
    'en': 'What is your age?',
    'sl': 'Wat is uw leeftijd?',
    'fi': 'Wat is uw leeftijd?',
  };

  final _enterAge = <String, String>{
    'nl': 'Vul uw leeftijd in',
    'en': 'Fill in your age',
    'sl': 'Vul uw leeftijd in',
    'fi': 'Vul uw leeftijd in',
  };

  final _next = <String, String>{
    'nl': 'Volgende',
    'en': 'Next',
    'sl': 'Volgende',
    'fi': 'Volgende',
  };

  final _genderQuestion = <String, String>{
    'nl': 'Wat is uw geslacht?',
    'en': 'What is your gender?',
    'sl': 'Wat is uw geslacht?',
    'fi': 'Wat is uw geslacht?',
  };

  final _male = <String, String>{
    'nl': 'Man',
    'en': 'Man',
    'sl': 'Man',
    'fi': 'Man',
  };

  final _female = <String, String>{
    'nl': 'Vrouw',
    'en': 'Woman',
    'sl': 'Vrouw',
    'fi': 'Vrouw',
  };

  final _livingSituationQuestion = <String, String>{
    'nl': 'Wat is uw huidige woonsituatie?',
    'en': 'What is your current living situation?',
    'sl': 'Wat is uw huidige woonsituatie?',
    'fi': 'Wat is uw huidige woonsituatie?',
  };

  final _aloneWithoutChildren = <String, String>{
    'nl': 'Alleenstaand, zonder (deels) thuiswonend(e) kind(eren)',
    'en': 'Alleenstaand, zonder (deels) thuiswonend(e) kind(eren)',
    'sl': 'Alleenstaand, zonder (deels) thuiswonend(e) kind(eren)',
    'fi': 'Alleenstaand, zonder (deels) thuiswonend(e) kind(eren)',
  };

  final _aloneWithChildren = <String, String>{
    'nl': 'Alleenstaand, met (deels) thuiswonend(e) kind(eren)',
    'en': 'Alleenstaand, met (deels) thuiswonend(e) kind(eren)',
    'sl': 'Alleenstaand, met (deels) thuiswonend(e) kind(eren)',
    'fi': 'Alleenstaand, met (deels) thuiswonend(e) kind(eren)',
  };

  final _livingTogetherWithoutChildren = <String, String>{
    'nl':
        'Samenwonend met partner/echtgeno(o)t(e), zonder (deels) thuiswonend(e) kind(eren)',
    'en':
        'Samenwonend met partner/echtgeno(o)t(e), zonder (deels) thuiswonend(e) kind(eren)',
    'sl':
        'Samenwonend met partner/echtgeno(o)t(e), zonder (deels) thuiswonend(e) kind(eren)',
    'fi':
        'Samenwonend met partner/echtgeno(o)t(e), zonder (deels) thuiswonend(e) kind(eren)',
  };

  final _livingTogetherWithChildren = <String, String>{
    'nl':
        'Samenwonend met partner/echtgeno(o)t(e), met (deels) thuiswonend(e) kind(eren)',
    'en':
        'Samenwonend met partner/echtgeno(o)t(e), met (deels) thuiswonend(e) kind(eren)',
    'sl':
        'Samenwonend met partner/echtgeno(o)t(e), met (deels) thuiswonend(e) kind(eren)',
    'fi':
        'Samenwonend met partner/echtgeno(o)t(e), met (deels) thuiswonend(e) kind(eren)',
  };

  final _other = <String, String>{
    'nl': 'Een andere samenstelling',
    'en': 'Een andere samenstelling',
    'sl': 'Een andere samenstelling',
    'fi': 'Een andere samenstelling',
  };

  final _partnerQuestion = <String, String>{
    'nl': 'Heeft u een vaste partner?',
    'en': 'Heeft u een vaste partner?',
    'sl': 'Heeft u een vaste partner?',
    'fi': 'Heeft u een vaste partner?',
  };

  final _paidWorkQuestion = <String, String>{
    'nl': 'Heeft u op dit moment betaald werk?',
    'en': 'Heeft u op dit moment betaald werk?',
    'sl': 'Heeft u op dit moment betaald werk?',
    'fi': 'Heeft u op dit moment betaald werk?',
  };

  final _paidWorkExplanation = <String, String>{
    'nl':
        'Ook 1 uur per week of een korte periode telt al mee. Evenals freelance werk.',
    'en':
        'Ook 1 uur per week of een korte periode telt al mee. Evenals freelance werk.',
    'sl':
        'Ook 1 uur per week of een korte periode telt al mee. Evenals freelance werk.',
    'fi':
        'Ook 1 uur per week of een korte periode telt al mee. Evenals freelance werk.',
  };

  String get paidWorkExplanation => _paidWorkExplanation[languagePreference];
  String get paidWorkQuestion => _paidWorkQuestion[languagePreference];
  String get partnerQuestion => _partnerQuestion[languagePreference];
  String get other => _other[languagePreference];
  String get livingTogetherWithChildren =>
      _livingTogetherWithChildren[languagePreference];
  String get livingTogetherWithoutChildren =>
      _livingTogetherWithoutChildren[languagePreference];
  String get aloneWithChildren => _aloneWithChildren[languagePreference];
  String get aloneWithoutChildren => _aloneWithoutChildren[languagePreference];
  String get livingSituationQuestion =>
      _livingSituationQuestion[languagePreference];
  String get female => _female[languagePreference];
  String get male => _male[languagePreference];
  String get genderQuestion => _genderQuestion[languagePreference];
  String get next => _next[languagePreference];
  String get enterAge => _enterAge[languagePreference];
  String get ageQuestion => _ageQuestion[languagePreference];
  String get question => _question[languagePreference];
  String get save => _save[languagePreference];
  String get startingQuestionnaire =>
      _startingQuestionnaire[languagePreference];

  String get pictureInfoIncomplete =>
      _pictureInfoIncomplete[languagePreference];

  String get infoBody => _infoBody[languagePreference];

  String get key => _key[tablePreference];

  String get fixedExpensesFilled => _fixedExpensesFilled[languagePreference];

  String get variablexExpensesFilled =>
      _variablexExpensesFilled[languagePreference];

  String get info => _info[languagePreference];

  String get infoTitle => _infoTitle[languagePreference];

  String get infoForMoreInfo => _infoForMoreInfo[languagePreference];

  String get infoWebsiteLink => _infoWebsiteLink[languagePreference];

  String get variableExpenses => _variableExpenses[languagePreference];

  String get fixedExpenses => _fixedExpenses[languagePreference];

  String get selectFilter => _selectFilter[languagePreference];

  String get filterExpenses => _filterExpenses[languagePreference];

  String get people => _people[languagePreference];

  String get fixedOrVariable => _fixedOrVariable[languagePreference];

  String get removeFilters => _removeFilters[languagePreference];

  String get ifApplicable => _ifApplicable[languagePreference];

  String get abroad => _abroad[languagePreference];

  String get online => _online[languagePreference];

  String get purchasedfrom => _purchasedfrom[languagePreference];

  String get youHaveNotAdded => _youHaveNotAdded[languagePreference];

  String get addBeforeCompletion => _addBeforeCompletion[languagePreference];

  String get photographReceipt => _photographReceipt[languagePreference];

  String get transactions => _transactions[languagePreference];

  String get findTransactions => _findTransactions[languagePreference];

  String get transactionDetails => _transactionDetails[languagePreference];

  String get newTransaction => _newTransaction[languagePreference];

  String get receiptItems => _receiptItems[languagePreference];

  String get earnedReward => _earnedReward[languagePreference];

  String get moreInfo => _moreInfo[languagePreference];

  String get belongsTo => _belongsTo[languagePreference];

  String get belongsTo2 => _belongsTo2[languagePreference];

  String get addYourself => _addYourself[languagePreference];

  String get addInfo => _addInfo[languagePreference];

  String get title => _title[languagePreference];

  String get contentDay => _contentDay[languagePreference];

  String get shop => _shop[languagePreference];

  String get enterShop => _enterShop[languagePreference];

  String get findShop => _findShop[languagePreference];

  String get selectShop => _selectShop[languagePreference];

  String get shopType => _shopType[languagePreference];

  String get enterShopType => _enterShopType[languagePreference];

  String get products => _products[languagePreference];

  String get productService => _productService[languagePreference];

  String get enterProductService => _enterProductService[languagePreference];

  String get findProductService => _findProductService[languagePreference];

  String get category => _category[languagePreference];

  String get categories => _categories[languagePreference];

  String get enterCatagory => _enterCatagory[languagePreference];

  String get mainCategories => _mainCategories[languagePreference];

  String get byCategory => _byCategory[languagePreference];

  String get time => _time[languagePreference];

  String get monday => _monday[languagePreference];

  String get tuesday => _tuesday[languagePreference];

  String get wednesday => _wednesday[languagePreference];

  String get thursday => _thursday[languagePreference];

  String get friday => _friday[languagePreference];

  String get saturday => _saturday[languagePreference];

  String get sunday => _sunday[languagePreference];

  String get today => _today[languagePreference];

  String get yesterday => _yesterday[languagePreference];

  String get tomorrow => _tomorrow[languagePreference];

  String get daysCompleted => _daysCompleted[languagePreference];

  String get daysMissing => _daysMissing[languagePreference];

  String get daysRemaining => _daysRemaining[languagePreference];

  String get date => _date[languagePreference];

  String get beginDate => _beginDate[languagePreference];

  String get endDate => _endDate[languagePreference];

  String get dailyReminders => _dailyReminders[languagePreference];

  String get overTime => _overTime[languagePreference];

  String get discount => _discount[languagePreference];

  String get enterTotalPrice => _enterTotalPrice[languagePreference];

  String get totalPrice => _totalPrice[languagePreference];

  String get enterPrice => _enterPrice[languagePreference];

  String get price => _price[languagePreference];

  String get priceRange => _priceRange[languagePreference];

  String get enterDiscountReceipt => _enterDiscountReceipt[languagePreference];

  String get productDiscount => _productDiscount[languagePreference];

  String get noDiscount => _noDiscount[languagePreference];

  String get discountEntireReceipt =>
      _discountEntireReceipt[languagePreference];

  String get enterDiscount => _enterDiscount[languagePreference];

  String get precentage => _precentage[languagePreference];

  String get invalidAmount => _invalidAmount[languagePreference];

  String get total => _total[languagePreference];

  String get total2 => _total2[languagePreference];

  String get noExpensesFound => _noExpensesFound[languagePreference];

  String get discountExpensesDiscount =>
      _discountExpensesDiscount[languagePreference];

  String get filterAmount => _filterAmount[languagePreference];

  String get no => _no[languagePreference];

  String get yes => _yes[languagePreference];

  String get delete => _delete[languagePreference];

  String get deleteItem => _deleteItem[languagePreference];

  String get edit => _edit[languagePreference];

  String get complete => _complete[languagePreference];

  String get warning => _warning[languagePreference];

  String get required => _required[languagePreference];

  String get or => _or[languagePreference];

  String get add => _add[languagePreference];

  String get cancel => _cancel[languagePreference];

  String get onValue => _onValue[languagePreference];

  String get minimum => _minimum[languagePreference];

  String get maximum => _maximum[languagePreference];

  String get none => _none[languagePreference];

  String get everything => _everything[languagePreference];

  String get invalidInput => _invalidInput[languagePreference];

  String get notFound => _notFound[languagePreference];

  String get close => _close[languagePreference];

  String get ok => _ok[languagePreference];

  String get settings => _settings[languagePreference];

  String get preferences => _preferences[languagePreference];

  String get language => _language[languagePreference];

  String get currency => _currency[languagePreference];

  String get family => _family[languagePreference];

  String get familyCode => _familyCode[languagePreference];

  String get shareWithFamily => _shareWithFamily[languagePreference];

  String get familyMembers => _familyMembers[languagePreference];

  String get notifications => _notifications[languagePreference];

  String get contact => _contact[languagePreference];

  String get phoneNumber => _phoneNumber[languagePreference];

  String get email => _email[languagePreference];

  String get sendMessage => _sendMessage[languagePreference];

  String get languageValue => _languageValue[languagePreference];

  String get overviewPage => _overviewPage[languagePreference];

  String get expensesPage => _expensesPage[languagePreference];

  String get insightsPage => _insightsPage[languagePreference];

  String get insights => _insights[languagePreference];

  String get hbsShort => _hbsShort[languagePreference];

  String get url => _url[languagePreference];

  String get www => _www[languagePreference];

  String get couldNotLaunch => _couldNotLaunch[languagePreference];

  String get start => _start[languagePreference];

  String get welcome => _welcome[languagePreference];

  String get toLogin => _toLogin[languagePreference];

  String get scanQRcode => _scanQRcode[languagePreference];

  String get user => _user[languagePreference];

  String get password => _password[languagePreference];

  String get loginManually => _loginManually[languagePreference];

  String get informationIncomplete =>
      _informationIncomplete[languagePreference];

  String get completeShopProduct => _completeShopProduct[languagePreference];

  String get addOneProduct => _addOneProduct[languagePreference];

  String get completeStoreInfo => _completeStoreInfo[languagePreference];

  String get receipt => _receipt[languagePreference];

  String get foodAndDrink => _foodAndDrink[languagePreference];

  String get alcoholAndTobacco => _alcoholAndTobacco[languagePreference];

  String get clothing => _clothing[languagePreference];

  String get housingCost => _housingCost[languagePreference];

  String get health => _health[languagePreference];

  String get transport => _transport[languagePreference];

  String get communication => _communication[languagePreference];

  String get recreationAndCultur => _recreationAndCultur[languagePreference];

  String get education => _education[languagePreference];

  String get restaurantAndHotel => _restaurantAndHotel[languagePreference];

  String get otherCategory => _otherCategory[languagePreference];

  String get warningIncomplete => _warningIncomplete[languagePreference];

  String get warningNoProduct => _warningNoProduct[languagePreference];

  String get warningNoShopInfo => _warningNoShopInfo[languagePreference];

  String get progress => _progress[languagePreference];

  String get between => _between[languagePreference];

  String get and => _and[languagePreference];

  String get noEarlierExpenses => _noEarlierExpenses[languagePreference];

  String get noLaterExpenses => _noLaterExpenses[languagePreference];

  String get offValue => _offValue[languagePreference];

  String get notificationsTurnedOnAt =>
      _notificationsTurnedOnAt[languagePreference];

  String get notificationsOff => _notificationsOff[languagePreference];

  String get incorrectPassword => _incorrectPassword[languagePreference];

  String get incorrectUserName => _incorrectUserName[languagePreference];

  String get transactionRemoved => _transactionRemoved[languagePreference];

  String get undo => _undo[languagePreference];

  String get totalAmount => _totalAmount[languagePreference];

  String get additional2 => _additional2[languagePreference];

  String get others2 => _others2[languagePreference];

  String get others => _others[languagePreference];

  String get enterTypeOfProducts => _enterTypeOfProducts[languagePreference];

  String get householdExpenses => _householdExpenses[languagePreference];

  String get operatingExpense => _operatingExpense[languagePreference];

  String get phoneNumberDigits => _phoneNumberDigits[languagePreference];

  String get emailAdress => _emailAdress[languagePreference];

  String get completeDay => _completeDay[languagePreference];

  String get dayCompleted => _dayCompleted[languagePreference];

  String get dailyReminderText => _dailyReminderText[languagePreference];

  String get notificationMessage => _notificationMessage[languagePreference];

//new translations

  String get productCategory => _productCategory[languagePreference];

//andere text

  String get youCanNotCompleteDays =>
      _youCanNotCompleteDays[languagePreference];

  String get hbsLong2 => _hbsLong2[languagePreference];

  String get hbsLong => _hbsLong[languagePreference];

  //New text for the camera hints
  String get cameraHint1 => _cameraHint1[languagePreference];

  String get cameraHint2 => _cameraHint2[languagePreference];

  String get cameraHint3 => _cameraHint3[languagePreference];

  String get cameraHint4 => _cameraHint4[languagePreference];

  String get cameraHint5 => _cameraHint5[languagePreference];

  String get cameraHint6 => _cameraHint6[languagePreference];

  String get understood => _understood[languagePreference];

  //New text strings for test round 2

  String get duplicate => _duplicate[languagePreference];

  String get modify => _modify[languagePreference];

  String get addingNewItems => _addingNewItems[languagePreference];

  String get discountReturnEtc => _discountReturnEtc[languagePreference];

  String get removalWarningText => _removalWarningText[languagePreference];

  String get swipeToNavigate => _swipeToNavigate[languagePreference];

  String get addNewServiceProductOrDiscount =>
      _addNewServiceProductOrDiscount[languagePreference];

  String get addReceipt => _addReceipt[languagePreference];

  String get recentItems => _recentItems[languagePreference];

  String get frequentItems => _frequentItems[languagePreference];

  String get dayAlreadyCompleted => _dayAlreadyCompleted[languagePreference];

  //New translations for the spotlight tutorials

  String get skip => _skip[languagePreference];

  String get newEntryTutorial1 => _newEntryTutorial1[languagePreference];

  String get newEntryTutorial2 => _newEntryTutorial2[languagePreference];

  String get newEntryTutorial3 => _newEntryTutorial3[languagePreference];

  String get newEntryTutorial4 => _newEntryTutorial4[languagePreference];

  String get newEntryTutorial5 => _newEntryTutorial5[languagePreference];

  String get newEntryTutorial6 => _newEntryTutorial6[languagePreference];

  String get newEntryTutorial7 => _newEntryTutorial7[languagePreference];

  String get newEntryTutorial8 => _newEntryTutorial8[languagePreference];

  String get newEntryTutorial9 => _newEntryTutorial9[languagePreference];

  String get newEntryTutorial10 => _newEntryTutorial10[languagePreference];

// String get newEntryTutorial10 => _newEntryTutorial10[languagePreference];
//   final _newEntryTutorial10 = <String, String>{
//     'nl':
//         '''Wat heeft u gekocht? Noteer hier alle producten en diensten afzonderlijk.
// \nIdentieke items kunt u makkelijk kopiëren.
// \nNoteer voor items met korting de prijs in die u daadwerkelijk betaald heeft. U kunt ook producten/diensten invoeren met de originele prijs en de korting als een apart item invoeren.  Dit is soms makkelijker als u gegevens overneemt van een bon.
// \nKorting over de gehele bon a.u.b. invoeren via Korting op totaal (zie volgend onderwerp). ''',
//     'en': '''What did you buy? Enter all products/services separately.
// \nYou can easily copy identical items.
// \nFor items with a discount enter the actual price paid. If your prefer, you may also enter the product/service with the original price and the amount of discount as separate items. This may be easier when copying information from a receipt.
// \nFor discount received on an entire receipt with multiple items, please use the Discount button below (see next tutorial topic).''',
//     'sl': '''What did you buy? Enter all products/services separately.
// \nYou can easily copy identical items.
// \nFor items with a discount enter the actual price paid. If your prefer, you may also enter the product/service with the original price and the amount of discount as separate items. This may be easier when copying information from a receipt.
// \nFor discount received on an entire receipt with multiple items, please use the Discount button below (see next tutorial topic).''',
//     'fi': '''What did you buy? Enter all products/services separately.
// \nYou can easily copy identical items.
// \nFor items with a discount enter the actual price paid. If your prefer, you may also enter the product/service with the original price and the amount of discount as separate items. This may be easier when copying information from a receipt.
// \nFor discount received on an entire receipt with multiple items, please use the Discount button below (see next tutorial topic).''',
//   };

  String get newEntryTutorial11 => _newEntryTutorial11[languagePreference];

  String get newEntryTutorial12 => _newEntryTutorial12[languagePreference];

  String get newEntryTutorial13 => _newEntryTutorial13[languagePreference];

  String get newEntryTutorial14 => _newEntryTutorial14[languagePreference];

  String get newEntryTutorial15 => _newEntryTutorial15[languagePreference];

  String get newEntryTutorial16 => _newEntryTutorial16[languagePreference];

  String get mainOverviewTutorial1 =>
      _mainOverviewTutorial1[languagePreference];

  String get mainOverviewTutorial2 =>
      _mainOverviewTutorial2[languagePreference];

  String get mainOverviewTutorial3 =>
      _mainOverviewTutorial3[languagePreference];

  String get mainOverviewTutorial4 =>
      _mainOverviewTutorial4[languagePreference];

  String get mainOverviewTutorial5 =>
      _mainOverviewTutorial5[languagePreference];

  String get mainOverviewTutorial6 =>
      _mainOverviewTutorial6[languagePreference];

  String get mainOverviewTutorial7 =>
      _mainOverviewTutorial7[languagePreference];

  String get mainOverviewTutorial8 =>
      _mainOverviewTutorial8[languagePreference];

  String get mainOverviewTutorial9 =>
      _mainOverviewTutorial9[languagePreference];

  String get mainOverviewTutorial10 =>
      _mainOverviewTutorial10[languagePreference];

  String get mainExpenselistTutorial1 =>
      _mainExpenselistTutorial1[languagePreference];

  String get mainExpenselistTutorial2 =>
      _mainExpenselistTutorial2[languagePreference];

  String get mainExpenselistTutorial3 =>
      _mainExpenselistTutorial3[languagePreference];

  String get mainExpenselistTutorial4 =>
      _mainExpenselistTutorial4[languagePreference];

  String get mainExpenselistTutorial5 =>
      _mainExpenselistTutorial5[languagePreference];

  String get mainExpenselistTutorial6 =>
      _mainExpenselistTutorial6[languagePreference];

  String get mainInsights1 => _mainInsights1[languagePreference];

  String get mainInsights2 => _mainInsights2[languagePreference];

  String get mainInsights3 => _mainInsights3[languagePreference];

  String get mainInsights4 => _mainInsights4[languagePreference];

  String get mainInsights5 => _mainInsights5[languagePreference];

  String get mainInsights6 => _mainInsights6[languagePreference];

  String get mainInsights7 => _mainInsights7[languagePreference];

  String get mainInsights8 => _mainInsights8[languagePreference];

  String get mainInsights9 => _mainInsights9[languagePreference];

  String get mainInsights10 => _mainInsights10[languagePreference];

  String get mainSettings1 => _mainSettings1[languagePreference];

  String get mainSettings2 => _mainSettings2[languagePreference];

  String get mainSettings3 => _mainSettings3[languagePreference];

  String get mainSettings4 => _mainSettings4[languagePreference];

  String get mainSettings5 => _mainSettings5[languagePreference];

  String get mainSettings6 => _mainSettings6[languagePreference];

  String get tips => _tips[languagePreference];

  String get information => _information[languagePreference];

  String get versionNumber => _versionNumber[languagePreference];

  String get timeSurveyTitle => _timeSurveyTitle[languagePreference];
  String get mediaSurveyTitle => _mediaSurveyTitle[languagePreference];

  final Map<String, String> _timeSurveyTitle = <String, String>{
    'nl': 'Tijdgebruik Onderzoek',
    'en': 'Time Survey Research',
  };

  final Map<String, String> _mediaSurveyTitle = <String, String>{
    'nl': 'Mediagebruik Onderzoek',
    'en': 'Media Survey Research',
  };
}
