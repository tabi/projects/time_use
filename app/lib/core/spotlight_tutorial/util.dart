import 'package:flutter/widgets.dart';
import 'package:tbo_app/core/spotlight_tutorial/target_focus.dart';
import 'package:tbo_app/core/spotlight_tutorial/target_position.dart';

TargetPosition getTargetCurrent(TargetFocus target) {
  if (target.keyTarget != null) {
    final key = target.keyTarget;

    try {
      final RenderBox renderBoxRed =
          key.currentContext.findRenderObject() as RenderBox;
      final size = renderBoxRed.size;
      final offset = renderBoxRed.localToGlobal(Offset.zero);

      return TargetPosition(size, offset);
    } catch (e) {
      return null;
    }
  } else {
    return target.targetPosition;
  }
}
