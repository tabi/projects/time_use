import 'package:flutter/widgets.dart';

enum AlignContent { top, bottom, left, right }

class ContentTarget {
  ContentTarget({this.align = AlignContent.bottom, @required this.child})
      : assert(child != null);

  final AlignContent align;
  final Widget child;

  @override
  String toString() {
    return 'ContentTarget{align: $align, child: $child}';
  }
}
