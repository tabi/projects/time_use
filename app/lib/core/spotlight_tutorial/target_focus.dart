import 'package:flutter/widgets.dart';
import 'package:tbo_app/core/spotlight_tutorial/animated_focus_light.dart';
import 'package:tbo_app/core/spotlight_tutorial/content_target.dart';
import 'package:tbo_app/core/spotlight_tutorial/target_position.dart';

class TargetFocus {
  TargetFocus({
    this.identify,
    this.keyTarget,
    this.targetPosition,
    this.contents,
    this.shape,
  }) : assert(keyTarget != null || targetPosition != null);

  final List<ContentTarget> contents;
  final dynamic identify;
  final GlobalKey keyTarget;
  final ShapeLightFocus shape;
  final TargetPosition targetPosition;

  @override
  String toString() {
    return 'TargetFocus{identify: $identify, keyTarget: $keyTarget, targetPosition: $targetPosition, contents: $contents, shape: $shape}';
  }
}
