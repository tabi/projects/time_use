import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class LightPaint extends CustomPainter {
  LightPaint(this.progress, this.positioned, this.sizeCircle,
      {this.colorShadow = Colors.black, this.opacityShadow = 0.8})
      : assert(opacityShadow >= 0 && opacityShadow <= 1) {
    _paintFocus = Paint()
      ..color = Colors.transparent
      ..blendMode = BlendMode.clear;
  }

  final Color colorShadow;
  final double opacityShadow;
  final Offset positioned;
  final double progress;
  final double sizeCircle;

  Paint _paintFocus;

  @override
  void paint(Canvas canvas, Size size) {
    canvas.saveLayer(Offset.zero & size, Paint());
    canvas.drawColor(colorShadow.withOpacity(opacityShadow), BlendMode.dstATop);

    final maxSize = size.width > size.height ? size.width : size.height;

    final double radius = maxSize * (1 - progress) + sizeCircle;

    canvas.drawCircle(positioned, radius, _paintFocus);
    canvas.restore();
  }

  @override
  bool shouldRepaint(LightPaint oldDelegate) {
    return oldDelegate.progress != progress;
  }
}
