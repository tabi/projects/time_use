import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'plausibility_check_type.g.dart';

class PlausibilityCheckType extends EnumClass {
  static Serializer<PlausibilityCheckType> get serializer =>
      _$plausibilityCheckTypeSerializer;

  static const PlausibilityCheckType soft = _$soft;
  static const PlausibilityCheckType hard = _$hard;

  const PlausibilityCheckType._(String name) : super(name);

  static BuiltSet<PlausibilityCheckType> get values => _$values;

  static PlausibilityCheckType valueOf(String name) => _$valueOf(name);
}
