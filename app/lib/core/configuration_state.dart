import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tbo_app/core/activity_tree_code.dart';
import 'package:tbo_app/core/experience_sampling_severity.dart';
import 'package:tbo_app/core/input_type.dart';
import 'package:tbo_app/core/plausibility_check_type.dart';
import 'package:tbo_app/insights/insights_visibility.dart';

class ConfigurationState {
  final Environment environment;
  final AppFlavor appFlavor;
  InsightsVisibility insightsVisibility;
  PlausibilityCheckType plausibilityCheckType;
  ExperienceSamplingSeverity experienceSamplingSeverity;
  InputType inputType;
  ActivityTreeCode mainActivityTree;
  ActivityTreeCode sideActivityTree;

  ConfigurationState({@required this.environment, @required this.appFlavor})
      : assert(environment != null),
        assert(appFlavor != null);

  Future<void> load() async {
    final SharedPreferences _preferences =
        await SharedPreferences.getInstance();
    final _insightsVisibilityName =
        _preferences.getString('insightsVisibility') ??
            InsightsVisibility.none.name;
    final _plausibilityCheckType =
        _preferences.getString('plausibilityCheckType') ??
            PlausibilityCheckType.soft.name;
    final _experienceSamplingSeverity =
        _preferences.getString('experienceSamplingSeverity') ??
            ExperienceSamplingSeverity.mild.name;
    final _inputType =
        _preferences.getString('inputType') ?? InputType.halfOpen.name;
    final _mainActivityTree = _preferences.getString('mainActivityTree') ??
        (appFlavor == AppFlavor.time
            ? ActivityTreeCode.T20200908.name
            : ActivityTreeCode.MEDIA20201102.name);
    final _sideActivityTree = _preferences.getString('sideActivityTree') ??
        (appFlavor == AppFlavor.time
            ? ActivityTreeCode.T20200908.name
            : ActivityTreeCode.MEDIA20201102N.name);

    insightsVisibility = InsightsVisibility.valueOf(_insightsVisibilityName);
    plausibilityCheckType =
        PlausibilityCheckType.valueOf(_plausibilityCheckType);
    experienceSamplingSeverity =
        ExperienceSamplingSeverity.valueOf(_experienceSamplingSeverity);
    mainActivityTree = ActivityTreeCode.valueOf(_mainActivityTree);
    sideActivityTree = ActivityTreeCode.valueOf(_sideActivityTree);
    inputType = InputType.valueOf(_inputType);
  }

  Future<void> save() async {
    final SharedPreferences _preferences =
        await SharedPreferences.getInstance();
    _preferences.setString('insightsVisibility', insightsVisibility.name);
    _preferences.setString('plausibilityCheckType', plausibilityCheckType.name);
    _preferences.setString(
        'experienceSamplingSeverity', experienceSamplingSeverity.name);
    _preferences.setString('inputType', inputType.name);
    _preferences.setString('mainActivityTree', mainActivityTree.name);
    _preferences.setString('sideActivityTree', sideActivityTree.name);
  }
}

enum Environment { development, qa, acceptance, production }
enum AppFlavor { time, media }
