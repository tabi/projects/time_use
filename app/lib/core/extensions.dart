import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tbo_app/activities/activity.dart';
import 'package:tbo_app/activities/activity_companion.dart';
import 'package:tbo_app/activities/activity_location.dart';
import 'package:tbo_app/activities/activity_type.dart';
import 'package:tbo_app/activities/medium.dart';
import 'package:tbo_app/activities/translate_page_activity_entry.dart';
import 'package:tbo_app/core/app_colors.dart';

extension DateOnActivity on Activity {
  bool matchesCurrentDate(DateTime date) {
    return date.isAtSameMomentAs(startDate) ||
        date.isAtSameMomentAs(endDate) ||
        date.isAfter(startDate) && date.isBefore(endDate);
  }
}

extension SelectActivity on List<Activity> {
  bool isStartOfActivity(DateTime date) {
    if (isEmpty) {
      return true;
    }
    final _matchingActivity = firstWhere(
        (activity) => date.isAtSameMomentAs(activity.startDate),
        orElse: () => null);
    return _matchingActivity != null;
  }

  bool isWithinActivity(DateTime date) {
    if (isEmpty) {
      return true;
    }
    final _matchingActivity = firstWhere(
        (activity) =>
            date.isAfter(activity.startDate) && date.isBefore(activity.endDate),
        orElse: () => null);
    return _matchingActivity != null;
  }

  Activity selectActivityWithStartDate(DateTime date, ActivityType type) {
    if (isEmpty) {
      return _activityForDate(date, type);
    }
    return firstWhere((activity) => date.isAtSameMomentAs(activity.startDate),
        orElse: () => _activityForDate(date, type));
  }

  Activity selectActivityForDate(DateTime date, ActivityType type) {
    if (isEmpty) {
      return _activityForDate(date, type);
    }
    return firstWhere(
        (activity) =>
            date.isAtSameMomentAs(activity.startDate) ||
            (date.isAfter(activity.startDate) &&
                date.isBefore(activity.endDate)),
        orElse: () => _activityForDate(date, type));
  }

  List<Activity> replace(Activity from, Activity to) {
    final _index = indexOf(from);
    remove(from);
    if (_index == -1) {
      add(to);
    } else {
      insert(_index, to);
    }
    return this;
  }

  Activity _activityForDate(DateTime date, ActivityType type) {
    return _activityWithType(date, type);
  }

  Activity _activityWithType(DateTime date, ActivityType type) {
    return Activity((b) => b
      ..type = type
      ..startDate = date.toUtc()
      ..endDate = date.add(const Duration(minutes: 10)).toUtc());
  }

  void reset() {
    removeWhere((activity) {
      // final _difference =
      //     activity.endDate.difference(activity.startDate).inMinutes;
      // return _difference > 10 && activity.activityNode == null;
      return activity.activityNode == null;
    });
  }

  bool areMissing(DateTime endDate) {
    if (isEmpty) {
      return true;
    }
    return !every((activity) {
      if (last != activity) {
        final _index = indexOf(activity);
        final _nextActivity = elementAt(_index + 1);
        return activity.endDate.isAtSameMomentAs(_nextActivity.startDate);
      } else {
        return activity.endDate.isAtSameMomentAs(endDate);
      }
    });
  }

  Activity before(Activity activity) {
    if (isEmpty) {
      return null;
    }
    sort((lhs, rhs) => lhs.startDate.compareTo(rhs.startDate));
    final _index = indexOf(activity);
    if (first == activity || _index == -1) {
      return null;
    }

    return elementAt(_index - 1);
  }

  Activity after(Activity activity) {
    if (isEmpty) {
      return null;
    }
    sort((lhs, rhs) => lhs.startDate.compareTo(rhs.startDate));
    final _index = indexOf(activity);
    if (last == activity || _index == -1) {
      return null;
    }
    return elementAt(_index + 1);
  }
}

extension IterableUtils<T> on Iterable<T> {
  Future<void> asyncForEach(Future<void> Function(T element) f) async {
    for (final element in this) {
      await f(element);
    }
  }

  Future<Iterable<R>> asyncMap<R>(FutureOr<R> Function(T event) convert) async {
    final result = <R>[];
    await asyncForEach((element) async {
      result.add(await convert(element));
    });
    return result;
  }
}

extension CompanionAdditions on ActivityCompanion {
  String get label {
    return {
      ActivityCompanion.none: TranslatePageActivityEntry.instance.alone,
      ActivityCompanion.partner: TranslatePageActivityEntry.instance.partner,
      ActivityCompanion.parents: TranslatePageActivityEntry.instance.parent,
      ActivityCompanion.children: TranslatePageActivityEntry.instance.children,
      ActivityCompanion.houseMates:
          TranslatePageActivityEntry.instance.housemates,
      ActivityCompanion.other: TranslatePageActivityEntry.instance.others
    }[this];
  }

  ImageIcon imageIcon() {
    final _image = {
      ActivityCompanion.none: 'assets/images/icon_alleen.png',
      ActivityCompanion.partner: 'assets/images/icon_partner.png',
      ActivityCompanion.parents: 'assets/images/icon_ouder.png',
      ActivityCompanion.children: 'assets/images/icon_kinderen.png',
      ActivityCompanion.houseMates: 'assets/images/icon_overig.png',
      ActivityCompanion.other: 'assets/images/icon_andere.png'
    }[this];
    return ImageIcon(AssetImage(_image),
        size: 40, color: AppColors.appColor(enumColor.COL_BLUE_DARKER));
  }
}

extension LocationAdditions on ActivityLocation {
  String get label {
    return {
      ActivityLocation.home: 'Thuis',
      ActivityLocation.work: 'Werk',
      ActivityLocation.schoolStudy: 'School/studie',
      ActivityLocation.other: 'Overig'
    }[this];
  }

  ImageIcon imageIcon() {
    switch (this) {
      case ActivityLocation.home:
        return const ImageIcon(AssetImage('assets/images/icon_thuis.png'));
      case ActivityLocation.work:
        return const ImageIcon(AssetImage('assets/images/icon_werk.png'));
      case ActivityLocation.schoolStudy:
        return const ImageIcon(
            AssetImage('assets/images/icon_school_studie.png'));
      case ActivityLocation.other:
        return const ImageIcon(
            AssetImage('assets/images/icon_overig_media.png'));
      default:
        return const ImageIcon(
            AssetImage('assets/images/icon_overig_media.png'));
    }
  }
}

extension MediumAdditions on Medium {
  String get label {
    return {
      Medium.desktop: '(thuis) computer (PC, Desktop)',
      Medium.laptop: 'Portable PC (Laptop, Notebook, Netbook)',
      Medium.smartphone: 'Mobiel/smartphone',
      Medium.television: 'Vast televisietoestel of beamer ',
      Medium.console: 'Spelcomputer (consoles) ',
      Medium.tablet: 'Tablet ',
      Medium.carRadio: 'Autoradio/cd-speler ',
      Medium.paper: 'Papier',
      Medium.eReader: 'E-reader ',
      Medium.handheld:
          'draagbare spelcomputer/ spelletjes apparaat (handheld) ',
      Medium.radio: 'Vaste radio/cd of stereo installatie ',
      Medium.phone: 'Vaste (huis)telefoon (aansluiting) ',
      Medium.portableRadio: 'Draagbare radio of muziekspeler ',
      Medium.none: 'Op geen apparaat'
    }[this]
        ?.trim();
  }

  ImageIcon imageIcon() {
    switch (this) {
      case Medium.desktop:
        return const ImageIcon(AssetImage('assets/images/icon_computer.png'));
      case Medium.laptop:
        return const ImageIcon(
            AssetImage('assets/images/icon_portable_pc.png'));
      case Medium.smartphone:
        return const ImageIcon(
            AssetImage('assets/images/icon_mobiel_smartphone.png'));
      case Medium.television:
        return const ImageIcon(
            AssetImage('assets/images/icon_vast_televisietoestel.png'));
      case Medium.console:
        return const ImageIcon(
            AssetImage('assets/images/icon_draagbare_spelcomputer.png'));
      case Medium.tablet:
        return const ImageIcon(AssetImage('assets/images/icon_tablet.png'));
      case Medium.carRadio:
        return const ImageIcon(
            AssetImage('assets/images/icon_auto_radio_cd_speler.png'));
      case Medium.paper:
        return const ImageIcon(AssetImage('assets/images/icon_papier.png'));
      case Medium.eReader:
        return const ImageIcon(AssetImage('assets/images/icon_e-reader.png'));
      case Medium.handheld:
        return const ImageIcon(
            AssetImage('assets/images/icon_draagbare_spelcomputer.png'));
      case Medium.radio:
        return const ImageIcon(
            AssetImage('assets/images/icon_vaste_radio_stereo.png'));
      case Medium.phone:
        return const ImageIcon(
            AssetImage('assets/images/icon_vaste_telefoon.png'));
      case Medium.portableRadio:
        return const ImageIcon(
            AssetImage('assets/images/icon_draagbare_radio_muziekspeler.png'));
      case Medium.none:
        return const ImageIcon(
            AssetImage('assets/images/icon_overig_media.png'));
      default:
        return const ImageIcon(
            AssetImage('assets/images/icon_overig_media.png'));
    }
  }
}
