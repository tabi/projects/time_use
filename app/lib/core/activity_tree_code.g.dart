// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activity_tree_code.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const ActivityTreeCode _$MEDIA20201102 =
    const ActivityTreeCode._('MEDIA20201102');
const ActivityTreeCode _$MEDIA20201102N =
    const ActivityTreeCode._('MEDIA20201102N');
const ActivityTreeCode _$T20200908 = const ActivityTreeCode._('T20200908');
const ActivityTreeCode _$T20201004 = const ActivityTreeCode._('T20201004');

ActivityTreeCode _$valueOf(String name) {
  switch (name) {
    case 'MEDIA20201102':
      return _$MEDIA20201102;
    case 'MEDIA20201102N':
      return _$MEDIA20201102N;
    case 'T20200908':
      return _$T20200908;
    case 'T20201004':
      return _$T20201004;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<ActivityTreeCode> _$values =
    new BuiltSet<ActivityTreeCode>(const <ActivityTreeCode>[
  _$MEDIA20201102,
  _$MEDIA20201102N,
  _$T20200908,
  _$T20201004,
]);

Serializer<ActivityTreeCode> _$activityTreeCodeSerializer =
    new _$ActivityTreeCodeSerializer();

class _$ActivityTreeCodeSerializer
    implements PrimitiveSerializer<ActivityTreeCode> {
  @override
  final Iterable<Type> types = const <Type>[ActivityTreeCode];
  @override
  final String wireName = 'ActivityTreeCode';

  @override
  Object serialize(Serializers serializers, ActivityTreeCode object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  ActivityTreeCode deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ActivityTreeCode.valueOf(serialized as String);
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
