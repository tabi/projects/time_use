// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'input_type.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const InputType _$closed = const InputType._('closed');
const InputType _$halfOpen = const InputType._('halfOpen');
// ignore: constant_identifier_names
const InputType _$half_open = const InputType._('half_open');

InputType _$valueOf(String name) {
  switch (name) {
    case 'closed':
      return _$closed;
    case 'halfOpen':
      return _$halfOpen;
    case 'half_open':
      return _$half_open;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<InputType> _$values = new BuiltSet<InputType>(const <InputType>[
  _$closed,
  _$halfOpen,
  _$half_open,
]);

Serializer<InputType> _$inputTypeSerializer = new _$InputTypeSerializer();

class _$InputTypeSerializer implements PrimitiveSerializer<InputType> {
  @override
  final Iterable<Type> types = const <Type>[InputType];
  @override
  final String wireName = 'InputType';

  @override
  Object serialize(Serializers serializers, InputType object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  InputType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      InputType.valueOf(serialized as String);
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
