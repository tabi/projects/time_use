// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'plausibility_check_type.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const PlausibilityCheckType _$soft = const PlausibilityCheckType._('soft');
const PlausibilityCheckType _$hard = const PlausibilityCheckType._('hard');

PlausibilityCheckType _$valueOf(String name) {
  switch (name) {
    case 'soft':
      return _$soft;
    case 'hard':
      return _$hard;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<PlausibilityCheckType> _$values =
    new BuiltSet<PlausibilityCheckType>(const <PlausibilityCheckType>[
  _$soft,
  _$hard,
]);

Serializer<PlausibilityCheckType> _$plausibilityCheckTypeSerializer =
    new _$PlausibilityCheckTypeSerializer();

class _$PlausibilityCheckTypeSerializer
    implements PrimitiveSerializer<PlausibilityCheckType> {
  @override
  final Iterable<Type> types = const <Type>[PlausibilityCheckType];
  @override
  final String wireName = 'PlausibilityCheckType';

  @override
  Object serialize(Serializers serializers, PlausibilityCheckType object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  PlausibilityCheckType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      PlausibilityCheckType.valueOf(serialized as String);
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
