import 'package:flutter/widgets.dart';
import 'package:tbo_app/core/configuration_state.dart';

class Configuration extends InheritedWidget {
  final ConfigurationState state;

  const Configuration({Key key, Widget child, @required this.state})
      : super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static ConfigurationState of(BuildContext context) =>
      (context.dependOnInheritedWidgetOfExactType<Configuration>())?.state;
}
