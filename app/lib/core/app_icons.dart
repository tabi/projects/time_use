import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

enum enumIcon {
  ICO_PAW,
  ICO_PRAYING_HANDS,
  ICO_TOOLS,
  ICO_USER_FRIENDS,
  ICO_HAND_HOLDINHG_HEART,
  ICO_HOUSE_USER,
  ICO_NOT_EXISTS,
  ICO_SUITCASE,
  ICO_BROOM,
  ICO_SHOPPINGCART,
  ICO_HANDSHELPING,
  ICO_BATH,
  ICO_BED,
  ICO_UTENSILS,
  ICO_SEARCH,
  ICO_USERFRIENDS,
  ICO_DUMBBELL,
  ICO_DICETHREE,
  ICO_BOOKREADER,
  ICO_BUS,
  ICO_GRADUATIONCAP,
  ICO_TRAIN,
  ICO_THEATHERMASKS,
  ICO_WALKING,
  ICO_CHEVRONCIRCLEDOWN,
  ICO_CHEVRONCIRCLEUP,
  ICO_CHEVRONLEFT,
  ICO_CHEVRONRIGHT,
  ICO_ANGLELEFT,
  ICO_ANGLERIGHT,
  ICO_CARETDOWN,
  ICO_CARETUP,
  ICO_CHILD,
  ICO_HEART,
  ICO_MALE,
  ICO_RUNNING,
  ICO_OLDMAN,
  ICO_PLUSSQUARE,
  ICO_CLOCK,
  ICO_ARROWLEFT,
  ICO_ARROWRIGHT,
  ICO_ADDBOX,
  ICO_BRIGHTNESS_1,
  ICO_CHECKSQUARE,
  ICO_SQUARE,
  ICO_CIRCLE,
  ICO_DOTCIRCLE,
  ICO_DEFAULT,
  ICO_INFO,
  ICO_CHECK,
  ICO_PIE_CHART,
  ICO_BAR_CHART,
  ICO_SOLID_CLOCK
}

// enum enumWho {
//   WHO_ALONE,
//   WHO_PARTNER,
//   WHO_PARENT,
//   WHO_CHILDREN,
//   WHO_HOUSEMATES,
//   WHO_OTHERS
// }

class AppIcons {
  static enumIcon icon(String code) {
    for (final enumIcon icon in enumIcon.values) {
      if (iconCode(icon) == code) {
        return icon;
      }
    }
    return enumIcon.ICO_NOT_EXISTS;
  }

  static String iconCode(enumIcon icon) {
    final String code = icon.toString();
    return code.substring(code.indexOf('.') + 1);
  }

  static bool isValidIconCode(String code) {
    return icon(code) != enumIcon.ICO_NOT_EXISTS;
  }

  static IconData appIconDataFromIconCode(String code) {
    return appIconData(icon(code));
  }

  static IconData appIconData(enumIcon icon) {
    switch (icon) {
      case enumIcon.ICO_PAW:
        return FontAwesomeIcons.paw;
      case enumIcon.ICO_PRAYING_HANDS:
        return FontAwesomeIcons.prayingHands;
      case enumIcon.ICO_TOOLS:
        return FontAwesomeIcons.tools;
      case enumIcon.ICO_USER_FRIENDS:
        return FontAwesomeIcons.userFriends;
      case enumIcon.ICO_HAND_HOLDINHG_HEART:
        return FontAwesomeIcons.handHoldingHeart;
      case enumIcon.ICO_HOUSE_USER:
        return FontAwesomeIcons.houseUser;
      case enumIcon.ICO_SUITCASE:
        return FontAwesomeIcons.suitcase;
      case enumIcon.ICO_BROOM:
        return FontAwesomeIcons.broom;
      case enumIcon.ICO_SHOPPINGCART:
        return FontAwesomeIcons.shoppingCart;
      case enumIcon.ICO_HANDSHELPING:
        return FontAwesomeIcons.handsHelping;
      case enumIcon.ICO_BATH:
        return FontAwesomeIcons.bath;
      case enumIcon.ICO_BED:
        return FontAwesomeIcons.bed;
      case enumIcon.ICO_UTENSILS:
        return FontAwesomeIcons.utensils;
      case enumIcon.ICO_SEARCH:
        return FontAwesomeIcons.search;
      case enumIcon.ICO_DICETHREE:
        return FontAwesomeIcons.diceThree;
      case enumIcon.ICO_DUMBBELL:
        return FontAwesomeIcons.dumbbell;
      case enumIcon.ICO_BOOKREADER:
        return FontAwesomeIcons.bookReader;
      case enumIcon.ICO_BUS:
        return FontAwesomeIcons.bus;
      case enumIcon.ICO_GRADUATIONCAP:
        return FontAwesomeIcons.graduationCap;
      case enumIcon.ICO_TRAIN:
        return Icons.train;
      case enumIcon.ICO_THEATHERMASKS:
        return FontAwesomeIcons.theaterMasks;
      case enumIcon.ICO_WALKING:
        return FontAwesomeIcons.walking;
      case enumIcon.ICO_CHEVRONCIRCLEDOWN:
        return FontAwesomeIcons.chevronCircleDown;
      case enumIcon.ICO_CHEVRONCIRCLEUP:
        return FontAwesomeIcons.chevronCircleUp;
      case enumIcon.ICO_ANGLELEFT:
        return FontAwesomeIcons.angleLeft;
      case enumIcon.ICO_ANGLERIGHT:
        return FontAwesomeIcons.angleRight;
      case enumIcon.ICO_CARETDOWN:
        return FontAwesomeIcons.caretDown;
      case enumIcon.ICO_CARETUP:
        return FontAwesomeIcons.caretUp;
      case enumIcon.ICO_CHILD:
        return FontAwesomeIcons.child;
      case enumIcon.ICO_HEART:
        return FontAwesomeIcons.solidHeart; //.heart;
      case enumIcon.ICO_MALE:
        return FontAwesomeIcons.male;
      case enumIcon.ICO_RUNNING:
        return FontAwesomeIcons.running;
      case enumIcon.ICO_OLDMAN:
        return FontAwesomeIcons.oldRepublic;
      case enumIcon.ICO_PLUSSQUARE:
        return FontAwesomeIcons.plusSquare;
      case enumIcon.ICO_CLOCK:
        return FontAwesomeIcons.clock;
      case enumIcon.ICO_ARROWLEFT:
        return FontAwesomeIcons.arrowLeft; //return Icons.arrow_back;
      case enumIcon.ICO_ARROWRIGHT:
        return FontAwesomeIcons.arrowRight;
      case enumIcon.ICO_ADDBOX:
        return Icons.add_box;
      case enumIcon.ICO_BRIGHTNESS_1:
        return Icons.brightness_1;
      case enumIcon.ICO_CHECKSQUARE:
        return FontAwesomeIcons.checkSquare;
      case enumIcon.ICO_SQUARE:
        return FontAwesomeIcons.square;
      case enumIcon.ICO_DOTCIRCLE:
        return FontAwesomeIcons.dotCircle;
      case enumIcon.ICO_CIRCLE:
        return FontAwesomeIcons.circle;
      case enumIcon.ICO_INFO:
        return Icons.info;
      case enumIcon.ICO_CHECK:
        return FontAwesomeIcons.check;
      case enumIcon.ICO_NOT_EXISTS:
        return FontAwesomeIcons.stop;
      case enumIcon.ICO_PIE_CHART:
        return FontAwesomeIcons.chartPie;
      case enumIcon.ICO_BAR_CHART:
        return FontAwesomeIcons.chartBar;
      case enumIcon.ICO_SOLID_CLOCK:
          return FontAwesomeIcons.solidClock;
      case enumIcon.ICO_CHEVRONLEFT:
          return FontAwesomeIcons.chevronLeft;
      case enumIcon.ICO_CHEVRONRIGHT:
          return FontAwesomeIcons.chevronRight;
      default:
        return FontAwesomeIcons.square;
    }
  }

  static Icon appIconFromIconCode(String code, Color color, double size) {
    return appIcon(icon(code), color, size);
  }

  static Icon appIcon(enumIcon icon, Color color, double size) {
    return Icon(appIconData(icon), color: color, size: size);
  }
}

// ignore_for_file: constant_identifier_names
