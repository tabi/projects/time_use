// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'experience_sampling_severity.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const ExperienceSamplingSeverity _$none =
    const ExperienceSamplingSeverity._('none');
const ExperienceSamplingSeverity _$mild =
    const ExperienceSamplingSeverity._('mild');
const ExperienceSamplingSeverity _$heavy =
    const ExperienceSamplingSeverity._('heavy');

ExperienceSamplingSeverity _$valueOf(String name) {
  switch (name) {
    case 'none':
      return _$none;
    case 'mild':
      return _$mild;
    case 'heavy':
      return _$heavy;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<ExperienceSamplingSeverity> _$values =
    new BuiltSet<ExperienceSamplingSeverity>(const <ExperienceSamplingSeverity>[
  _$none,
  _$mild,
  _$heavy,
]);

Serializer<ExperienceSamplingSeverity> _$experienceSamplingSeveritySerializer =
    new _$ExperienceSamplingSeveritySerializer();

class _$ExperienceSamplingSeveritySerializer
    implements PrimitiveSerializer<ExperienceSamplingSeverity> {
  @override
  final Iterable<Type> types = const <Type>[ExperienceSamplingSeverity];
  @override
  final String wireName = 'ExperienceSamplingSeverity';

  @override
  Object serialize(Serializers serializers, ExperienceSamplingSeverity object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  ExperienceSamplingSeverity deserialize(
          Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ExperienceSamplingSeverity.valueOf(serialized as String);
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
