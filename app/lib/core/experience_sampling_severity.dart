import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'experience_sampling_severity.g.dart';

class ExperienceSamplingSeverity extends EnumClass {
  static Serializer<ExperienceSamplingSeverity> get serializer =>
      _$experienceSamplingSeveritySerializer;

  static const ExperienceSamplingSeverity none = _$none;
  static const ExperienceSamplingSeverity mild = _$mild;
  static const ExperienceSamplingSeverity heavy = _$heavy;

  const ExperienceSamplingSeverity._(String name) : super(name);

  static BuiltSet<ExperienceSamplingSeverity> get values => _$values;

  static ExperienceSamplingSeverity valueOf(String name) => _$valueOf(name);
}
