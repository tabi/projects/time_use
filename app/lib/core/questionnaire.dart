import 'dart:math';

import 'package:flutter/material.dart';
import 'package:tbo_app/core/color_pallet.dart';
import 'package:tbo_app/core/translate.dart';

class Questionnaire extends StatefulWidget {
  final int itemCount;
  final IndexedWidgetBuilder itemBuilder;
  final VoidCallback onComplete;

  const Questionnaire(
      {Key key,
      @required this.itemBuilder,
      @required this.itemCount,
      @required this.onComplete})
      : assert(itemCount != null && itemCount > 0),
        assert(itemBuilder != null),
        assert(onComplete != null),
        super(key: key);
  @override
  QuestionnaireState createState() => QuestionnaireState();
}

class QuestionnaireState extends State<Questionnaire>
    with TickerProviderStateMixin {
  AnimationController _animationController;
  AnimationController _progressAnimationController;
  Animation<double> _opacityAnimation;
  Animation<double> _progressAnimation;
  final _scrollController = ScrollController();
  double _progress;
  int _index = 0;
  int _highestIndex = 0;
  bool ignore = false;
  double distance = 0;

  Map<int, int> _thresholds = <int, int>{};
  List<GlobalKey> _keys;
  @override
  void initState() {
    super.initState();
    _progress = 1 / widget.itemCount;
    _keys = List.generate(widget.itemCount, (index) => GlobalKey());
    _animationController = AnimationController(
        duration: const Duration(milliseconds: 330), vsync: this);
    _progressAnimationController = AnimationController(
        duration: const Duration(milliseconds: 1650), vsync: this);
    _opacityAnimation =
        Tween<double>(begin: 0.0, end: 1.0).animate(_animationController);
    _progressAnimation = Tween<double>(begin: 0.0, end: 1.0)
        .animate(_progressAnimationController);
    WidgetsBinding.instance.addPostFrameCallback(_afterLayout);
  }

  void _afterLayout(Duration duration) {
    final _sizes = _thresholds;
    if (_sizes[_index + 1] == null && _index < _keys.length) {
      final _key = _keys[_index];
      final renderBox = _key.currentContext.findRenderObject() as RenderBox;
      final _totalHeight = renderBox.size.height.toInt() +
          (_sizes.isNotEmpty ? _sizes.values.last : 0);
      _sizes.putIfAbsent(_index + 1, () => _totalHeight);
      setState(() {
        _thresholds = _sizes;
      });
    }
  }

  void nextQuestion(int index, {int forcedIndex}) {
    setState(() {
      _highestIndex = max(index, _highestIndex);
      _highestIndex = min(widget.itemCount, _highestIndex);
      _index = forcedIndex ?? _highestIndex;
      ignore = true;
      distance = 0;
    });
    _progressAnimationController.animateTo(_highestIndex * _progress);
    if (_index < widget.itemCount) {
      _scrollController.animateTo(_thresholds[_index].toDouble(),
          duration: const Duration(milliseconds: 330),
          curve: Curves.easeInCubic);
    } else {
      _animationController.forward();
    }
  }

  void _previousQuestion(int index) {
    if (index >= 0) {
      setState(() {
        _index = index;
        ignore = true;
        distance = 0;
      });
      _progressAnimationController.animateTo(_highestIndex * _progress);
      if (_index < widget.itemCount) {
        _scrollController.animateTo(_thresholds[_index]?.toDouble() ?? 0,
            duration: const Duration(milliseconds: 330),
            curve: Curves.easeInCubic);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    DragStartDetails startVerticalDragDetails;
    DragUpdateDetails updateVerticalDragDetails;

    return GestureDetector(
      onVerticalDragStart: (dragDetails) {
        startVerticalDragDetails = dragDetails;
      },
      onVerticalDragEnd: (details) {
        double dx = updateVerticalDragDetails.globalPosition.dx -
            startVerticalDragDetails.globalPosition.dx;
        double dy = updateVerticalDragDetails.globalPosition.dy -
            startVerticalDragDetails.globalPosition.dy;
        final velocity = details.primaryVelocity;

        if (dx < 0) dx = -dx;
        if (dy < 0) dy = -dy;
        final positiveVelocity = velocity < 0 ? -velocity : velocity;
        if (dy < 200) return;
        if (positiveVelocity < 500) return;

        if (velocity < 0) {
          //Swipe Up
          if (_index < _highestIndex) {
            nextQuestion(0, forcedIndex: _index + 1);
          }
        } else {
          //Swipe Down
          _previousQuestion(_index - 1);
        }
      },
      onVerticalDragUpdate: (details) {
        updateVerticalDragDetails = details;
      },
      child: Theme(
        data: Theme.of(context).copyWith(
          textTheme: Theme.of(context).textTheme.copyWith(
                caption: const TextStyle(
                  color: ColorPallet.lightGray,
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                ),
                button: const TextStyle(
                  color: ColorPallet.lightBlueWithOpacity,
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
                headline6: const TextStyle(
                  color: ColorPallet.darkTextColor,
                  fontSize: 24,
                  fontWeight: FontWeight.w600,
                ),
              ),
        ),
        child: Builder(
          builder: (context) {
            return Scaffold(
              backgroundColor: ColorPallet.primaryColor,
              appBar: AppBar(
                automaticallyImplyLeading: false,
                elevation: 0,
                title: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24.0),
                  child: Text(translate.startingQuestionnaire),
                ),
                bottom: PreferredSize(
                  preferredSize: const Size.fromHeight(18.0),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 40.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: AnimatedBuilder(
                          animation: _progressAnimation,
                          builder: (context, child) {
                            return LinearProgressIndicator(
                              backgroundColor: Colors.white,
                              valueColor: const AlwaysStoppedAnimation<Color>(
                                  ColorPallet.lightGreen),
                              value: _progressAnimation.value,
                            );
                          }),
                    ),
                  ),
                ),
              ),
              body: Padding(
                padding: const EdgeInsets.only(left: 17.0, top: 37, right: 13),
                child: ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    controller: _scrollController,
                    itemCount: widget.itemCount + 1,
                    itemBuilder: (context, index) {
                      WidgetsBinding.instance
                          .addPostFrameCallback(_afterLayout);
                      if (index == widget.itemCount) {
                        return Align(
                          child: AnimatedBuilder(
                            animation: _opacityAnimation,
                            builder: (context, child) {
                              return Opacity(
                                  opacity: _opacityAnimation.value,
                                  child: child);
                            },
                            child: Container(
                              margin:
                                  const EdgeInsets.only(top: 18, bottom: 400),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5)),
                              width: 147,
                              height: 41,
                              child: Material(
                                borderRadius: BorderRadius.circular(5),
                                color: ColorPallet.lightGreen,
                                child: InkWell(
                                  borderRadius: BorderRadius.circular(5),
                                  onTap: widget.onComplete,
                                  child: Center(
                                    child: Text(
                                      translate.save,
                                      style: Theme.of(context)
                                          .textTheme
                                          .button
                                          .copyWith(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                      } else {
                        return Question(
                          key: index < _keys.length ? _keys[index] : null,
                          active: index <= _highestIndex,
                          child: widget.itemBuilder(context, index),
                        );
                      }
                    }),
              ),
            );
          },
        ),
      ),
    );
  }
}

class Question extends StatelessWidget {
  final bool active;
  final Widget child;

  const Question({@required Key key, @required this.active, this.child})
      : assert(key != null),
        assert(active != null),
        super(key: key);
  @override
  Widget build(BuildContext context) {
    return ColorFiltered(
      colorFilter: ColorFilter.mode(const Color.fromRGBO(0, 161, 205, 0.5),
          active ? BlendMode.dst : BlendMode.srcOver),
      child: Container(
        margin: const EdgeInsets.only(bottom: 12),
        padding: const EdgeInsets.symmetric(vertical: 17),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(5)),
        child: child,
      ),
    );
  }
}
//ignore_for_file: prefer_is_empty
