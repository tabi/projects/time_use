import 'package:charts_flutter/flutter.dart' as charts;
import 'package:intl/intl.dart';
import 'package:tbo_app/core/translate.dart';

class DateString {
  static String dateTimeToDateString(DateTime dt) {
    return dt.toString().substring(0, 10);
  }

  static int dateTimeToDateInt(DateTime dt) {
    return dateStringToDateInt(dateTimeToDateString(dt));
  }

  static int dateStringToDateInt(String date) {
    return int.parse(date.replaceAll('-', ''));
  }

  static String dateIntToDateString(int date) {
    final String dateString = date.toString();
    final String result =
        '${dateString.substring(0, 4)}-${dateString.substring(4, 6)}-${dateString.substring(6, 8)}';
    return result;
  }

  static DateTime dateStringToDateTime(String date) {
    final int y = int.parse(date.substring(0, 4));
    final int m = int.parse(date.substring(5, 7));
    final int d = int.parse(date.substring(8, 10));
    return DateTime(y, m, d);
  }

  static DateTime dateIntToDateTime(int date) {
    return dateStringToDateTime(dateIntToDateString(date));
  }

  static String dayOfWeek(int weekday) {
    String result = '';
    switch (weekday) {
      case 1:
        result = translate.monday;
        break;
      case 2:
        result = translate.tuesday;
        break;
      case 3:
        result = translate.wednesday;
        break;
      case 4:
        result = translate.thursday;
        break;
      case 5:
        result = translate.friday;
        break;
      case 6:
        result = translate.saturday;
        break;
      case 7:
        result = translate.sunday;
        break;
      default:
        result = translate.monday;
    }
    return result;
  }

  static String firstDateOfMonth(String date) {
    DateTime dt;
    dt = DateTime.parse(date);
    dt = dt.add(Duration(days: 1 - dt.day));
    return dateTimeToDateString(dt);
  }

  static String lastDateOfMonth(String date) {
    DateTime dt;
    dt = DateTime.parse(firstDateOfMonth(date));
    dt = dt.add(const Duration(days: 35));
    dt = DateTime.parse(firstDateOfMonth(dateTimeToDateString(dt)));
    dt = dt.add(const Duration(hours: -1));
    return dateTimeToDateString(dt);
  }

  static String firstDateOfWeek(String date) {
    DateTime dt;
    dt = DateTime.parse(date);
    dt = dt.add(Duration(days: 1 - dt.weekday));
    return dateTimeToDateString(dt);
  }

  static String lastDateOfWeek(String date) {
    DateTime dt;
    dt = DateTime.parse(date);
    dt = dt.add(Duration(days: 7 - dt.weekday));
    return dateTimeToDateString(dt);
  }

  static String monthString(DateTime dt) {
    final String result = DateFormat.MMMM(translate.languagePreference)
        .format(DateTime(dt.year, dt.month));
    return result[0].toUpperCase() + result.substring(1);
  }

  static bool nextPeriodExists(DateTime nextDate, int absMinDate,
      int absMaxDate, String period, int swipe) {
    final String dateString = dateTimeToDateString(nextDate);
    DateTime firstDay;
    DateTime lastDay;

    if (period == 'week') {
      firstDay = DateTime.parse(firstDateOfWeek(dateString));
      lastDay = DateTime.parse(lastDateOfWeek(dateString));
    } else {
      firstDay = DateTime.parse(firstDateOfMonth(dateString));
      lastDay = DateTime.parse(lastDateOfMonth(dateString));
    }

    if (absMinDate > absMaxDate) {
      return false;
    }
    if (swipe == -1 &&
        absMinDate > dateStringToDateInt(dateTimeToDateString(lastDay))) {
      return false;
    }
    if (swipe == 1 &&
        absMaxDate < dateStringToDateInt(dateTimeToDateString(firstDay))) {
      return false;
    }
    return true;
  }

  static String barChartTitle(DateTime dayInPeriod, String period) {
    String result;

    final String dateString = dateTimeToDateString(dayInPeriod);
    DateTime firstDay;
    DateTime lastDay;

    if (period == 'week') {
      firstDay = DateTime.parse(firstDateOfWeek(dateString));
      lastDay = DateTime.parse(lastDateOfWeek(dateString));
    } else {
      firstDay = DateTime.parse(firstDateOfMonth(dateString));
      lastDay = DateTime.parse(lastDateOfMonth(dateString));
    }

    if (period == 'week') {
      result =
          '${firstDay.day}/${firstDay.month} - ${lastDay.day}/${lastDay.month}';
    } else {
      result = '${monthString(lastDay)} ${lastDay.year}';
    }

    return result;
  }

  static String barChartWeekday(String date) {
    final DateTime _date = DateTime.parse(date);
    return '${DateString.dayOfWeek(_date.weekday)}-${_date.day}';
  }

  static String barChartMonthday(String date) {
    final int dayNumber = DateTime.parse(date).day;
    return dayNumber.toString();
  }

  static List<charts.TickSpec<String>> barChartTickSpec(
      DateTime dayInPeriod, String period) {
    final List<charts.TickSpec<String>> result = [];

    final Map<String, String> labels = barChartLabels(dayInPeriod, period);

    for (final String label in labels.keys) {
      result.add(charts.TickSpec(label, label: labels[label]));
    }

    return result;
  }

  static Map<String, String> barChartLabels(
      DateTime dayInPeriod, String period) {
    final Map<String, String> labels = {};

    final String dateString = dateTimeToDateString(dayInPeriod);
    DateTime firstDay;
    DateTime lastDay;

    if (period == 'week') {
      firstDay = DateTime.parse(firstDateOfWeek(dateString));
      lastDay = DateTime.parse(lastDateOfWeek(dateString));
    } else {
      firstDay = DateTime.parse(firstDateOfMonth(dateString));
      lastDay = DateTime.parse(lastDateOfMonth(dateString));
    }

    DateTime dt = firstDay;
    int tick = 1;
    while (dt.compareTo(lastDay) <= 0) {
      if (period == 'week') {
        final String lab = barChartWeekday(dateTimeToDateString(dt));
        labels[lab] =
            barChartWeekday(dateTimeToDateString(dt)); //dayOfWeek(tick);
      } else {
        final String lab = barChartMonthday(dateTimeToDateString(dt));
        labels[lab] = tick == 1 || tick % 5 == 0 ? tick.toString() : '';
      }
      tick++;
      dt = dt.add(const Duration(days: 1));
    }

    return labels;
  }
}
