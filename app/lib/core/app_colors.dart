import 'package:flutter/material.dart';

enum enumColor {
  COL_ACT_01,
  COL_ACT_02,
  COL_ACT_03,
  COL_ACT_04,
  COL_ACT_05,
  COL_ACT_06,
  COL_ACT_07,
  COL_ACT_08,
  COL_ACT_09,
  COL_ACT_10,
  COL_ACT_11,
  COL_ACT_12,
  COL_ACT_13,
  COL_ACT_14,
  COL_ACT_15,
  COL_ACT_16,
  COL_ACT_17,
  COL_ACT_18,
  COL_ACT_19,
  COL_ACT_20,
  COL_NOT_EXISTS,
  COL_YELOW,
  COL_ORANGE_DARK,
  COL_ORANGE_LIGHT,
  COL_RED,
  COL_PINK,
  COL_PURPLE,
  COL_BLUE_DARKER,
  COL_BLUE_DARK,
  COL_BLUE_MIDDLE,
  COL_BLUE_LIGHT,
  COL_BLUE_LIGHTER,
  COL_GRAY_LIGHT,
  COL_GRAY_MIDDLE,
  COL_GRAY_DARKER,
  COL_GRAY_BORDER,
  COL_GREEN_DARK,
  COL_GREEN_MIDDLE,
  COL_GREEN_LIGHT,
  COL_BLACK,
  COL_WHITE,
  COL_OFF_WHITE,
  COL_DEFAULT
}

class AppColors {
  static enumColor color(String code) =>
      enumColor.values.firstWhere((color) => colorCode(color) == code,
          orElse: () => enumColor.COL_NOT_EXISTS);

  static String colorCode(enumColor color) {
    final code = color.toString();
    return code.substring(code.indexOf('.') + 1);
  }

  static bool isValidColorCode(String code) {
    return color(code) != enumColor.COL_NOT_EXISTS;
  }

  static Color appColorFromColorCode(String code) {
    return appColor(color(code));
  }

  static Color appColor(enumColor color) {
    switch (color) {
      case enumColor.COL_ACT_01:
        return const Color(0xFFF4CD31);
      case enumColor.COL_ACT_02:
        return const Color(0xFFF3AE46);
      case enumColor.COL_ACT_03:
        return const Color(0xFFC4D45E);
      case enumColor.COL_ACT_04:
        return const Color(0xFFDE84C4);
      case enumColor.COL_ACT_05:
        return const Color(0xFF5590D1);
      case enumColor.COL_ACT_06:
        return const Color(0xFF72C0D6);
      case enumColor.COL_ACT_07:
        return const Color(0xFF8BC166);
      case enumColor.COL_ACT_08:
        return const Color(0xFFC0B71C);
      case enumColor.COL_ACT_09:
        return const Color(0xFFD3AE92);
      case enumColor.COL_ACT_10:
        return const Color(0xFFFF985D);
      case enumColor.COL_ACT_11:
        return const Color(0xFFFF9EBC);
      case enumColor.COL_ACT_12:
        return const Color(0xFFBA6AD7);
      case enumColor.COL_ACT_13:
        return const Color(0xFFEF824E);
      case enumColor.COL_ACT_14:
        return const Color(0xFF67BCBC);
      case enumColor.COL_ACT_15:
        return const Color(0xFF799F7A);
      case enumColor.COL_ACT_16:
        return const Color(0xFFC2C38F);
      case enumColor.COL_ACT_17:
        return const Color(0xFF81DBDB);
      case enumColor.COL_ACT_18:
        return const Color(0xFF5575AA);
      case enumColor.COL_ACT_19:
        return const Color(0xFF9E8585);
      case enumColor.COL_ACT_20:
        return const Color(0xFF9E8585);
      case enumColor.COL_YELOW:
        return const Color(0xFFF4CD31);
      case enumColor.COL_ORANGE_DARK:
        return const Color(0xFFF39200);
      case enumColor.COL_ORANGE_LIGHT:
        return const Color(0xFFF3AE46);
      case enumColor.COL_RED:
        return const Color(0xFFE06E37);
      case enumColor.COL_PINK:
        return const Color(0xFFDE84C4);
      case enumColor.COL_PURPLE:
        return const Color(0xFFBA6AD7);
      case enumColor.COL_BLUE_DARK:
        return const Color(0xFF5590D1);
      case enumColor.COL_BLUE_DARKER:
        return const Color(0xFF33425B);
      case enumColor.COL_BLUE_MIDDLE:
        return const Color(0xFF00A1CD);
      case enumColor.COL_BLUE_LIGHT:
        return const Color(0xFF72C0D6);
      case enumColor.COL_BLUE_LIGHTER:
        return const Color(0xFF72C0D6);
      case enumColor.COL_GRAY_LIGHT:
        return const Color(0xFFDDDDDD);
      case enumColor.COL_GRAY_MIDDLE:
        return const Color(0xFFAAAAAA);
      case enumColor.COL_GRAY_DARKER:
        return const Color(0xFF9A9A9A);
      case enumColor.COL_GRAY_BORDER:
        return const Color(0xFFB1B9AC);
      case enumColor.COL_GREEN_DARK:
        return const Color(0xFF8BC166);
      case enumColor.COL_GREEN_MIDDLE:
        return const Color(0xFFAFCB05);
      case enumColor.COL_GREEN_LIGHT:
        return const Color(0xFFC4D55E);
      case enumColor.COL_BLACK:
        return const Color(0xFF000000);
      case enumColor.COL_WHITE:
        return const Color(0xFFFFFFFF);
      case enumColor.COL_OFF_WHITE:
        return const Color(0xFFF8F8F8);
      case enumColor.COL_DEFAULT:
        return appColor(enumColor.COL_BLUE_DARK);
      case enumColor.COL_NOT_EXISTS:
        return appColor(enumColor.COL_BLUE_DARK);
      default:
        return appColor(enumColor.COL_BLUE_DARK);
    }
  }
}
// ignore_for_file: constant_identifier_names
