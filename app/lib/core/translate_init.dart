import 'package:tbo_app/activities/translate_page_activity_agenda.dart';
import 'package:tbo_app/activities/translate_page_activity_entry.dart';
import 'package:tbo_app/activities/translate_page_select_activity.dart';
import 'package:tbo_app/core/constant.dart';
import 'package:tbo_app/core/database/table/translation_table.dart';
import 'package:tbo_app/core/translate_page_base.dart';
import 'package:tbo_app/core/translate_page_general.dart';

class TranslateInit {
  TranslateInit._privateConstructor();
  static final TranslateInit instance = TranslateInit._privateConstructor();

  List<TranslatePageBase> _pages;

  Future init(String language) async {
    _initPages();
    await _initTranslations(language);
  }

  void _initPages() {
    _pages = <TranslatePageBase>[];
    _pages.add(TranslatePageGeneral.instance);
    _pages.add(TranslatePageActivityEntry.instance);
    _pages.add(TranslatePageSelectActivity.instance);
    _pages.add(TranslatePageActivityAgenda.instance);
  }

  Future _initTranslations(String language) async {
    final List<TranslationTable> rows = await TranslationTable.select();

    for (final TranslationTable row in rows) {
      if (row.itemKey != Constant.emptyString) {
        for (final TranslatePageBase page in _pages) {
          if (row.pageKey == page.pageKey) {
            if (row.language == 'default') {
              page.editDefaultTranslation(row.itemKey, row.translation);
            }
            if (row.language == language) {
              page.editChosenTranslation(row.itemKey, row.translation);
            }
          }
        }
      }
    }
  }
}
