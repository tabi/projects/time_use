class ProgressStatistics {
  final int completed;
  final int missing;
  final int remaining;

  ProgressStatistics({this.completed, this.missing, this.remaining});
}
