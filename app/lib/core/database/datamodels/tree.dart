import 'dart:async';

import 'package:tbo_app/core/database/database_helper.dart';
import 'package:tbo_app/core/date_string.dart';
import 'package:tbo_app/core/translate.dart';

class Item {
  Item(this.id, this.description, this.value, this.parentId, this.date);

  String date;
  String description;
  String id;
  String parentId;
  double value;

  int get dateInt {
    return DateString.dateStringToDateInt(date);
  }
}

class Node {
  Node(this.id, this.description, this.type);

  List<String> children = [];
  String description;
  String id;
  List<Item> items = [];
  int type;
  double value = 0.0;
}

class Tree {
  Tree._();

  static final Tree instance = Tree._();

  String marker = ' #_# ';
  int maxProductDate;
  int minProductDate;
  Map<String, Node> nodes = {};
  String selectedId = '.';
  String separator = '.';

  String get rootId {
    return separator;
  }

  Node get selectedNode {
    return nodes[selectedId];
  }

  Future<void> initCoicop() async {
    final db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> tblCoicop =
        await db.query('tblCoicop' + '_' + translate.key);

    nodes.clear();
    addNode(rootId, translate.mainCategories);
    for (final coicop in tblCoicop) {
      addNode(coicop['code'].toString(), coicop['coicop'].toString());
    }
  }

  Future<void> initProducts(double minFilterPrice, double maxFilterPrice,
      int minFilterDate, int maxFilterDate) async {
    final db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> tblProducts = await db.query('products');

    deleteAllItems();
    minProductDate = 25000101;
    maxProductDate = 19000101;

    int nn = 0;
    for (final product in tblProducts) {
      final int prodDate = int.parse(product['productDate'].toString());
      final double price = double.parse(product['price'].toString());

      nn = nn + 1;
      if (minFilterPrice <= price &&
          price <= maxFilterPrice &&
          minFilterDate <= prodDate &&
          prodDate <= maxFilterDate &&
          price > 0.0) {
        addItem(
            nn,
            product['productCode'].toString(),
            product['product'].toString(),
            double.parse(product['price'].toString()),
            DateString.dateIntToDateString(prodDate));

        minProductDate = minProductDate < prodDate ? minProductDate : prodDate;
        maxProductDate = maxProductDate > prodDate ? maxProductDate : prodDate;
      }
    }

    calculateAllValues();
  }

  void calculateAllValues() {
    _calculateValues(rootId);
  }

  void _calculateValues(String id) {
    double sum = 0;
    final Node node = nodes[id];
    for (final String child in node.children) {
      _calculateValues(child);
      sum += nodes[child].value;
    }
    for (final Item item in node.items) {
      sum += item.value;
    }
    node.value = sum;
  }

  void deleteAllItems() {
    _deleteItems(rootId);
    _deleteItemNodes();
  }

  void _deleteItems(String id) {
    final Node node = nodes[id];
    for (final String child in node.children) {
      _deleteItems(child);
    }
    node.items.clear();
    node.value = 0.0;
  }

  void _deleteItemNodes() {
    nodes.forEach((k, v) => v.children.removeWhere((c) => nodes[c].type == 2));
    nodes.removeWhere((k, v) => v.type == 2);
  }

  String parentId(String id) {
    if (id == rootId) {
      return id;
    } else if (!id.contains(separator)) {
      return rootId;
    }
    return id.substring(0, id.lastIndexOf(separator));
  }

  void addNode(String id, String description) {
    if (!nodes.keys.contains(id)) {
      nodes[id] = Node(id, description, 1);
      _addNodeToParent(id);
    } else {
      nodes[id].description = description;
    }
  }

  void _addParentNode(String id) {
    if (!nodes.keys.contains(id)) {
      nodes[id] = Node(id, '?', 1);
      _addNodeToParent(id);
    }
  }

  void _addNodeToParent(String id) {
    if (id != rootId) {
      final String parent = parentId(id);
      _addParentNode(parent);
      if (!nodes[parent].children.contains(id)) {
        nodes[parent].children.add(id);
      }
    }
  }

  void addItem(
      int nn, String id, String description, double value, String date) {
    _addParentNode(id);
    final String itemId = '$id${separator}_$nn';
    final String itemDescription = '$description$marker$nn';
    nodes[itemId] = Node(itemId, itemDescription, 2);
    nodes[id].children.add(itemId);
    nodes[itemId].items.add(Item(itemId, itemDescription, value, id, date));
  }

  void addItemOKE(String id, String description, double value, String date) {
    _addParentNode(id);
    nodes[id].items.add(Item(id, description, value, parentId(id), date));
  }

  Item item(String id) {
    if (!nodes.keys.contains(id)) {
      return Item(
          id, 'Id ' ' + id + ' ' does not exist!', 0.0, parentId(id), '');
    }
    return Item(id, nodes[id].description, nodes[id].value, parentId(id), '');
  }

  List<Item> items(String id) {
    final List<Item> result = [];

    if (!nodes.keys.contains(id)) {
      result.add(
          Item(id, 'Id ' ' + id + ' ' does not exist!', 0.0, parentId(id), ''));
      return result;
    }

    final Map<String, Item> sumItems = {};
    for (final Item item in nodes[id].items) {
      if (!sumItems.keys.contains(item.description)) {
        sumItems[item.description] = Item(
            item.id, item.description, item.value, item.parentId, item.date);
      } else {
        sumItems[item.description].value += item.value;
      }
    }
    for (final Item item in sumItems.values) {
      result.add(item);
    }

    return result;
  }

  List<Item> allItemsDated(String id) {
    final List<Item> result = [];

    for (final Item item in nodes[id].items) {
      result.add(Item(id, item.description, item.value, id, item.date));
    }

    for (final String child in nodes[id].children) {
      for (final Item item in allItemsDated(child)) {
        result.add(Item(
            child, nodes[child].description, item.value, child, item.date));
      }
    }

    return result;
  }

  List<Item> periodItemsDated(String id, String fromDate, String toDate) {
    final List<Item> result = [];
    for (final Item item in allItemsDated(id)) {
      if (fromDate.compareTo(item.date) <= 0 &&
          toDate.compareTo(item.date) >= 0) {
        result.add(item);
      }
    }
    return result;
  }

  List<Item> children(String id) {
    final List<Item> result = [];

    if (!nodes.keys.contains(id)) {
      result.add(
          Item(id, 'Id ' ' + id + ' ' does not exist!', 0.0, parentId(id), ''));
      return result;
    }

    for (final String child in nodes[id].children) {
      if (item(child).value >= 0) {
        result.add(item(child));
      }
    }

    return result;
  }

  List<Item> ancestorsInclusive(String id) {
    final List<Item> result = [];

    if (!nodes.keys.contains(id)) {
      result.add(
          Item(id, 'Id ' ' + id + ' ' does not exist!', 0, parentId(id), ''));
      return result;
    }

    if (id != rootId) {
      for (final Item item in ancestorsInclusive(parentId(id))) {
        result.add(item);
      }
    }

    result.add(item(id));

    return result;
  }

  List<Item> ancestors(String id) {
    final List<Item> result = ancestorsInclusive(id);
    result.removeLast();
    return result;
  }
}
