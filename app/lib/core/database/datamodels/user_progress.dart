import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:tbo_app/core/serializers.dart';

part 'user_progress.g.dart';

abstract class UserProgress
    implements Built<UserProgress, UserProgressBuilder> {
  static Serializer<UserProgress> get serializer => _$userProgressSerializer;

  @nullable
  int get daysOfExperiment;
  @nullable
  int get daysCompleted;
  @nullable
  int get daysMissing;

  factory UserProgress([Function(UserProgressBuilder b) updates]) =
      _$UserProgress;

  UserProgress._();

  factory UserProgress.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map toJson() {
    return serializers.serializeWith(serializer, this) as Map;
  }
}
