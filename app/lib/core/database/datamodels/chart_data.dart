import 'dart:async';
import 'dart:math';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:tbo_app/core/color_pallet.dart';
import 'package:tbo_app/core/database/datamodels/tree.dart';
import 'package:tbo_app/core/date_string.dart';

class ChartData {
  Tree coicopTree;
  bool refreshdata = true;

  double minFilterPrice;
  double maxFilterPrice;
  int minFilterDate;
  int maxFilterDate;
  int minFoundDate;
  int maxFoundDate;

  void reset() {
    refreshdata = true;
  }

  Future<void> loadChartTables() async {
    if (coicopTree == null) {
      refreshdata = false;
      coicopTree = Tree.instance;
      await coicopTree.initCoicop();
      refreshdata = true;
    }
    if (true) {
      refreshdata = false;
      await coicopTree.initProducts(
          minFilterPrice, maxFilterPrice, minFilterDate, maxFilterDate);
    }
  }

  Map<int, Color> categoryColors = {
    0: ColorPallet.lightGreen,
    1: ColorPallet.orange,
    2: ColorPallet.primaryColor,
    3: ColorPallet.pink,
    4: ColorPallet.darkGreen,
    5: ColorPallet.midblue,
    6: ColorPallet.lightGreen.withOpacity(0.5),
    7: ColorPallet.orange.withOpacity(0.5),
    8: ColorPallet.primaryColor.withOpacity(0.5),
    9: ColorPallet.pink.withOpacity(0.5),
    10: ColorPallet.darkGreen.withOpacity(0.5),
    11: ColorPallet.midblue.withOpacity(0.5),
  };

  void deterimineFilterValues(
      DateTime dayInPeriod,
      String period,
      String minimumPrice,
      String maximumPrice,
      String startDate,
      String endDate,
      {bool timeChartPage}) {
    if (minimumPrice == '') {
      minFilterPrice = 0.0;
    } else {
      minFilterPrice = double.parse(minimumPrice);
    }

    if (maximumPrice == '') {
      maxFilterPrice = 1000000000.0;
    } else {
      maxFilterPrice = double.parse(maximumPrice);
    }

    if (startDate == '') {
      minFilterDate = 19000101;
    } else {
      minFilterDate = DateString.dateStringToDateInt(startDate);
    }

    if (endDate == '') {
      maxFilterDate = 22220101;
    } else {
      maxFilterDate = DateString.dateStringToDateInt(endDate);
    }

    if (timeChartPage) {
      final day = DateString.dateTimeToDateString(dayInPeriod);
      String firstDay;
      String lastDay;
      if (period == 'week') {
        firstDay = DateString.firstDateOfWeek(day);
        lastDay = DateString.lastDateOfWeek(day);
      } else {
        firstDay = DateString.firstDateOfMonth(day);
        lastDay = DateString.lastDateOfMonth(day);
      }
      minFilterDate =
          max(minFilterDate, DateString.dateStringToDateInt(firstDay));
      maxFilterDate =
          min(maxFilterDate, DateString.dateStringToDateInt(lastDay));
    }
  }

  dynamic determineFoundDateBoundaries(String coicopId) async {
    minFoundDate = 22220101;
    maxFoundDate = 19000101;
    for (final Item item in coicopTree.allItemsDated(coicopId)) {
      final int _dateString = DateString.dateStringToDateInt(item.date);
      minFoundDate = min(minFoundDate, _dateString);
      maxFoundDate = max(maxFoundDate, _dateString);
    }
  }

  Future<void> getFilteredData(
      DateTime dayInPeriod,
      String period,
      String minimumPrice,
      String maximumPrice,
      String startDate,
      String endDate,
      {bool timeChartPage}) async {
    deterimineFilterValues(
        dayInPeriod, period, minimumPrice, maximumPrice, startDate, endDate,
        timeChartPage: timeChartPage);

    await loadChartTables();
  }

  Future<List<dynamic>> getData(
      String coicopId,
      DateTime dayInPeriod,
      String period,
      String minimumPrice,
      String maximumPrice,
      String startDate,
      String endDate) async {
    final List<dynamic> data = [];

    await getFilteredData(
        dayInPeriod, period, minimumPrice, maximumPrice, startDate, endDate,
        timeChartPage: false);
    {
      final List<Map<String, dynamic>> _ancestors =
          await getAncestors(coicopId);
      while (_ancestors.length > 1) {
        _ancestors.removeAt(0);
      }
      data.add(_ancestors);
      data.add(await getChildren(coicopId));
      data.add(await getDonutChartData(coicopId));
      data.add(coicopTree.nodes[coicopId].value.toString());
    }

    determineFoundDateBoundaries(coicopId);

    await getFilteredData(
        dayInPeriod, period, minimumPrice, maximumPrice, startDate, endDate,
        timeChartPage: true);
    {
      data.add(await getChildren(coicopId));
      data.add(await getBarChartData(coicopId, dayInPeriod, period));
      data.add(minFoundDate);
      data.add(maxFoundDate);
    }

    return data;
  }

  Map<String, dynamic> insight(
      Item item, String type, double totalSpend, Color color) {
    final double allowed = item.value.abs();
    final double percentage =
        totalSpend == 0.0 ? 0.0 : (allowed / totalSpend) * 100.0;
    return {
      'code': item.id,
      'name': item.description,
      'parent': item.parentId,
      'spendTotal': double.parse(item.value.toString()),
      'percentage': percentage.toStringAsFixed(0),
      'type': type,
      'color': color
    };
  }

  Future<List<Map<String, dynamic>>> getAncestors(String coicopId) async {
    final double totalSpend = coicopTree.nodes[coicopId].value;

    final List<Map<String, dynamic>> category = [];

    for (final Item item in coicopTree.ancestors(coicopId)) {
      category
          .add(insight(item, 'ancestor', totalSpend, const Color(0xFFEFEFEF)));
    }

    final Item item = coicopTree.item(coicopId);
    category
        .add(insight(item, 'ancestor', totalSpend, const Color(0xFF000000)));

    category.sort((a, b) {
      return (b['spendTotal'] as double).compareTo(a['spendTotal'] as double);
    });

    return category;
  }

  Future<List<Map<String, dynamic>>> getChildren(String coicopId) async {
    double totalSpend = coicopTree.nodes[coicopId].value;

    final List<Map<String, dynamic>> _categories = [];

    totalSpend = 0.0;
    for (final Item item in coicopTree.children(coicopId)) {
      if (item.value > 0.0) {
        totalSpend = totalSpend + item.value;
      }
    }
    for (final Item item in coicopTree.items(coicopId)) {
      if (item.value > 0.0) {
        totalSpend = totalSpend + item.value;
      }
    }

    for (final Item item in coicopTree.children(coicopId)) {
      _categories
          .add(insight(item, 'child', totalSpend, const Color(0xFFEFEFEF)));
    }

    for (final Item item in coicopTree.items(coicopId)) {
      _categories
          .add(insight(item, 'item', totalSpend, const Color(0xFFEFEFEF)));
    }

    _categories.sort((a, b) {
      return (b['spendTotal'] as double).compareTo(a['spendTotal'] as double);
    });

    int colorCounter = 0;
    for (final category in _categories) {
      if (category['type'] == 'child' || category['type'] == 'item') {
        category['color'] = categoryColors[colorCounter];
        colorCounter++;
        if (colorCounter == 12) colorCounter = 0;
      }
    }

    return _categories;
  }

  dynamic getDonutChartData(String coicopId) async {
    final List<Map<String, dynamic>> category1Donut =
        await getChildren(coicopId);

    category1Donut.removeWhere((category) {
      return category['spendTotal'] == 0;
    });

    if (category1Donut.isEmpty) {
      return null;
    }

    if (category1Donut.length == 1) {
      return 1;
    }

    final List<ExpenseCategory> data = [];
    for (final category in category1Donut) {
      final int value = int.parse(
          double.parse(category['spendTotal'].toString()).toStringAsFixed(0));
      data.add(ExpenseCategory(
          category['name'].toString(),
          value.abs(),
          getChartColor(category['color'] as Color),
          category['code'] as String,
          double.parse(category['spendTotal'].toString()).toStringAsFixed(2)));
    }

    return [
      charts.Series<ExpenseCategory, String>(
        id: 'Category',
        domainFn: (ExpenseCategory category, _) => category.categoryName,
        measureFn: (ExpenseCategory category, _) => category.moneySpent,
        data: data,
        colorFn: (ExpenseCategory category, _) => category.color,
        labelAccessorFn: (ExpenseCategory category, _) => '€${category.label}',
        outsideLabelStyleAccessorFn: (ExpenseCategory category, _) =>
            charts.TextStyleSpec(
                fontFamily: 'Source Sans Pro Bold',
                fontSize: 16, // size in Pts.
                color: getChartColor(ColorPallet.darkTextColor)),
      )
    ];
  }

  dynamic getBarChartData(
      String coicopId, DateTime dayInPeriod, String period) async {
    final List<Map<String, dynamic>> childColors = await getChildren(coicopId);

    final String day = DateString.dateTimeToDateString(dayInPeriod);
    String firstDay;
    String lastDay;
    if (period == 'week') {
      firstDay = DateString.firstDateOfWeek(day);
      lastDay = DateString.lastDateOfWeek(day);
    } else {
      firstDay = DateString.firstDateOfMonth(day);
      lastDay = DateString.lastDateOfMonth(day);
    }
    final DateTime minDate = DateTime.parse(firstDay);
    final DateTime maxDate = DateTime.parse(lastDay);

    Map<String, Map<String, double>> allItemsDated = {};
    for (final Item item in coicopTree.allItemsDated(coicopId)) {
      DateString.dateStringToDateInt(item.date);
    }

    bool itemsFound = false;
    allItemsDated = {};
    for (final Item item
        in coicopTree.periodItemsDated(coicopId, firstDay, lastDay)) {
      itemsFound = true;
      if (!allItemsDated.containsKey(item.description)) {
        allItemsDated[item.description] = {item.date: item.value};
      } else {
        if (!allItemsDated[item.description].containsKey(item.date)) {
          allItemsDated[item.description][item.date] = item.value;
        } else {
          allItemsDated[item.description][item.date] += item.value;
        }
      }
    }

    if (!itemsFound) {
      allItemsDated[''] = {};
    }

    final Map<String, Map<String, double>> allDates = {};
    for (final String id in allItemsDated.keys) {
      allDates[id] = {};
      DateTime dt = minDate;
      while (dt.compareTo(maxDate) <= 0) {
        final String _dateTimeString = DateString.dateTimeToDateString(dt);
        allDates[id][_dateTimeString] = 0.0;
        dt = dt.add(const Duration(days: 1));
      }
      for (final String date in allItemsDated[id].keys) {
        allDates[id][date] = allItemsDated[id][date];
      }
    }

    final Map<String, List<ExpenseDate>> barData = {};
    for (final String id in allDates.keys) {
      barData[id] = [];
      for (final String date in allDates[id].keys) {
        barData[id].add(ExpenseDate(date, allDates[id][date]));
      }
    }

    final List<charts.Series<ExpenseDate, String>> barChartData = [];
    for (final String id in barData.keys) {
      Color barColor = Colors.blue;
      for (final Map<String, dynamic> col in childColors) {
        if (col['name'].toString() == id) {
          barColor = col['color'] as Color;
        }
      }
      barChartData.add(charts.Series<ExpenseDate, String>(
        id: id,
        domainFn: (ExpenseDate sales, _) => period == 'week'
            ? DateString.barChartWeekday(sales.date)
            : DateString.barChartMonthday(sales.date),
        measureFn: (ExpenseDate sales, _) => sales.expense,
        data: barData[id],
        colorFn: (ExpenseDate sales, _) => getChartColor(barColor),
      ));
    }

    return barChartData;
  }

  charts.Color getChartColor(Color color) {
    return charts.Color(
        r: color.red, g: color.green, b: color.blue, a: color.alpha);
  }
}

class ExpenseCategory {
  ExpenseCategory(this.categoryName, this.moneySpent, this.color,
      this.snapshotCode, this.label);

  final String categoryName;
  final charts.Color color;
  final String label;
  final int moneySpent;
  final String snapshotCode;
}

class ExpenseDate {
  ExpenseDate(this.date, this.expense);

  final String date;
  final double expense;
}
