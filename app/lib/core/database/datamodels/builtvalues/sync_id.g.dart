// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_id.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncId> _$syncIdSerializer = new _$SyncIdSerializer();

class _$SyncIdSerializer implements StructuredSerializer<SyncId> {
  @override
  final Iterable<Type> types = const [SyncId, _$SyncId];
  @override
  final String wireName = 'SyncId';

  @override
  Iterable<Object> serialize(Serializers serializers, SyncId object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.userName != null) {
      result
        ..add('userName')
        ..add(serializers.serialize(object.userName,
            specifiedType: const FullType(String)));
    }
    if (object.userPassword != null) {
      result
        ..add('userPassword')
        ..add(serializers.serialize(object.userPassword,
            specifiedType: const FullType(String)));
    }
    if (object.phoneName != null) {
      result
        ..add('phoneName')
        ..add(serializers.serialize(object.phoneName,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  SyncId deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncIdBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'userName':
          result.userName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'userPassword':
          result.userPassword = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'phoneName':
          result.phoneName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$SyncId extends SyncId {
  @override
  final String userName;
  @override
  final String userPassword;
  @override
  final String phoneName;

  factory _$SyncId([void Function(SyncIdBuilder) updates]) =>
      (new SyncIdBuilder()..update(updates)).build();

  _$SyncId._({this.userName, this.userPassword, this.phoneName}) : super._();

  @override
  SyncId rebuild(void Function(SyncIdBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncIdBuilder toBuilder() => new SyncIdBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncId &&
        userName == other.userName &&
        userPassword == other.userPassword &&
        phoneName == other.phoneName;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, userName.hashCode), userPassword.hashCode),
        phoneName.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncId')
          ..add('userName', userName)
          ..add('userPassword', userPassword)
          ..add('phoneName', phoneName))
        .toString();
  }
}

class SyncIdBuilder implements Builder<SyncId, SyncIdBuilder> {
  _$SyncId _$v;

  String _userName;
  String get userName => _$this._userName;
  set userName(String userName) => _$this._userName = userName;

  String _userPassword;
  String get userPassword => _$this._userPassword;
  set userPassword(String userPassword) => _$this._userPassword = userPassword;

  String _phoneName;
  String get phoneName => _$this._phoneName;
  set phoneName(String phoneName) => _$this._phoneName = phoneName;

  SyncIdBuilder();

  SyncIdBuilder get _$this {
    if (_$v != null) {
      _userName = _$v.userName;
      _userPassword = _$v.userPassword;
      _phoneName = _$v.phoneName;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncId other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SyncId;
  }

  @override
  void update(void Function(SyncIdBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncId build() {
    final _$result = _$v ??
        new _$SyncId._(
            userName: userName,
            userPassword: userPassword,
            phoneName: phoneName);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
