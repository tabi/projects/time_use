import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:tbo_app/core/serializers.dart';
import 'package:built_collection/built_collection.dart';

import 'package:tbo_app/core/database/datamodels/builtvalues/sync_user.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_phone.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_phone_info.dart';

part 'sync_register_body.g.dart';

abstract class SyncRegisterBody implements Built<SyncRegisterBody, SyncRegisterBodyBuilder> {
  static Serializer<SyncRegisterBody> get serializer => _$syncRegisterBodySerializer;

  SyncUser get user;
  SyncPhone get phone;
  BuiltList<SyncPhoneInfo> get phoneInfos;

  factory SyncRegisterBody([Function(SyncRegisterBodyBuilder b) updates]) = _$SyncRegisterBody;

  SyncRegisterBody._();

  factory SyncRegisterBody.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
