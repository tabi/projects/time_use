// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_search_store_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncSearchStoreData> _$syncSearchStoreDataSerializer =
    new _$SyncSearchStoreDataSerializer();

class _$SyncSearchStoreDataSerializer
    implements StructuredSerializer<SyncSearchStoreData> {
  @override
  final Iterable<Type> types = const [
    SyncSearchStoreData,
    _$SyncSearchStoreData
  ];
  @override
  final String wireName = 'SyncSearchStoreData';

  @override
  Iterable<Object> serialize(
      Serializers serializers, SyncSearchStoreData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'synchronisation',
      serializers.serialize(object.synchronisation,
          specifiedType: const FullType(SyncSync)),
      'searchStore',
      serializers.serialize(object.searchStore,
          specifiedType: const FullType(SyncSearchStore)),
    ];

    return result;
  }

  @override
  SyncSearchStoreData deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncSearchStoreDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'synchronisation':
          result.synchronisation.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncSync)) as SyncSync);
          break;
        case 'searchStore':
          result.searchStore.replace(serializers.deserialize(value,
                  specifiedType: const FullType(SyncSearchStore))
              as SyncSearchStore);
          break;
      }
    }

    return result.build();
  }
}

class _$SyncSearchStoreData extends SyncSearchStoreData {
  @override
  final SyncSync synchronisation;
  @override
  final SyncSearchStore searchStore;

  factory _$SyncSearchStoreData(
          [void Function(SyncSearchStoreDataBuilder) updates]) =>
      (new SyncSearchStoreDataBuilder()..update(updates)).build();

  _$SyncSearchStoreData._({this.synchronisation, this.searchStore})
      : super._() {
    if (synchronisation == null) {
      throw new BuiltValueNullFieldError(
          'SyncSearchStoreData', 'synchronisation');
    }
    if (searchStore == null) {
      throw new BuiltValueNullFieldError('SyncSearchStoreData', 'searchStore');
    }
  }

  @override
  SyncSearchStoreData rebuild(
          void Function(SyncSearchStoreDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncSearchStoreDataBuilder toBuilder() =>
      new SyncSearchStoreDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncSearchStoreData &&
        synchronisation == other.synchronisation &&
        searchStore == other.searchStore;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, synchronisation.hashCode), searchStore.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncSearchStoreData')
          ..add('synchronisation', synchronisation)
          ..add('searchStore', searchStore))
        .toString();
  }
}

class SyncSearchStoreDataBuilder
    implements Builder<SyncSearchStoreData, SyncSearchStoreDataBuilder> {
  _$SyncSearchStoreData _$v;

  SyncSyncBuilder _synchronisation;
  SyncSyncBuilder get synchronisation =>
      _$this._synchronisation ??= new SyncSyncBuilder();
  set synchronisation(SyncSyncBuilder synchronisation) =>
      _$this._synchronisation = synchronisation;

  SyncSearchStoreBuilder _searchStore;
  SyncSearchStoreBuilder get searchStore =>
      _$this._searchStore ??= new SyncSearchStoreBuilder();
  set searchStore(SyncSearchStoreBuilder searchStore) =>
      _$this._searchStore = searchStore;

  SyncSearchStoreDataBuilder();

  SyncSearchStoreDataBuilder get _$this {
    if (_$v != null) {
      _synchronisation = _$v.synchronisation?.toBuilder();
      _searchStore = _$v.searchStore?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncSearchStoreData other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SyncSearchStoreData;
  }

  @override
  void update(void Function(SyncSearchStoreDataBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncSearchStoreData build() {
    _$SyncSearchStoreData _$result;
    try {
      _$result = _$v ??
          new _$SyncSearchStoreData._(
              synchronisation: synchronisation.build(),
              searchStore: searchStore.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'synchronisation';
        synchronisation.build();
        _$failedField = 'searchStore';
        searchStore.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'SyncSearchStoreData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
