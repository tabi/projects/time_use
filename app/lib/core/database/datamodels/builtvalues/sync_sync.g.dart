// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_sync.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncSync> _$syncSyncSerializer = new _$SyncSyncSerializer();

class _$SyncSyncSerializer implements StructuredSerializer<SyncSync> {
  @override
  final Iterable<Type> types = const [SyncSync, _$SyncSync];
  @override
  final String wireName = 'SyncSync';

  @override
  Iterable<Object> serialize(Serializers serializers, SyncSync object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.dataType != null) {
      result
        ..add('dataType')
        ..add(serializers.serialize(object.dataType,
            specifiedType: const FullType(int)));
    }
    if (object.dataIdentifier != null) {
      result
        ..add('dataIdentifier')
        ..add(serializers.serialize(object.dataIdentifier,
            specifiedType: const FullType(String)));
    }
    if (object.syncOrder != null) {
      result
        ..add('syncOrder')
        ..add(serializers.serialize(object.syncOrder,
            specifiedType: const FullType(int)));
    }
    if (object.syncTime != null) {
      result
        ..add('syncTime')
        ..add(serializers.serialize(object.syncTime,
            specifiedType: const FullType(int)));
    }
    if (object.action != null) {
      result
        ..add('action')
        ..add(serializers.serialize(object.action,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  SyncSync deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncSyncBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'dataType':
          result.dataType = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'dataIdentifier':
          result.dataIdentifier = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'syncOrder':
          result.syncOrder = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'syncTime':
          result.syncTime = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'action':
          result.action = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$SyncSync extends SyncSync {
  @override
  final int dataType;
  @override
  final String dataIdentifier;
  @override
  final int syncOrder;
  @override
  final int syncTime;
  @override
  final int action;

  factory _$SyncSync([void Function(SyncSyncBuilder) updates]) =>
      (new SyncSyncBuilder()..update(updates)).build();

  _$SyncSync._(
      {this.dataType,
      this.dataIdentifier,
      this.syncOrder,
      this.syncTime,
      this.action})
      : super._();

  @override
  SyncSync rebuild(void Function(SyncSyncBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncSyncBuilder toBuilder() => new SyncSyncBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncSync &&
        dataType == other.dataType &&
        dataIdentifier == other.dataIdentifier &&
        syncOrder == other.syncOrder &&
        syncTime == other.syncTime &&
        action == other.action;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, dataType.hashCode), dataIdentifier.hashCode),
                syncOrder.hashCode),
            syncTime.hashCode),
        action.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncSync')
          ..add('dataType', dataType)
          ..add('dataIdentifier', dataIdentifier)
          ..add('syncOrder', syncOrder)
          ..add('syncTime', syncTime)
          ..add('action', action))
        .toString();
  }
}

class SyncSyncBuilder implements Builder<SyncSync, SyncSyncBuilder> {
  _$SyncSync _$v;

  int _dataType;
  int get dataType => _$this._dataType;
  set dataType(int dataType) => _$this._dataType = dataType;

  String _dataIdentifier;
  String get dataIdentifier => _$this._dataIdentifier;
  set dataIdentifier(String dataIdentifier) =>
      _$this._dataIdentifier = dataIdentifier;

  int _syncOrder;
  int get syncOrder => _$this._syncOrder;
  set syncOrder(int syncOrder) => _$this._syncOrder = syncOrder;

  int _syncTime;
  int get syncTime => _$this._syncTime;
  set syncTime(int syncTime) => _$this._syncTime = syncTime;

  int _action;
  int get action => _$this._action;
  set action(int action) => _$this._action = action;

  SyncSyncBuilder();

  SyncSyncBuilder get _$this {
    if (_$v != null) {
      _dataType = _$v.dataType;
      _dataIdentifier = _$v.dataIdentifier;
      _syncOrder = _$v.syncOrder;
      _syncTime = _$v.syncTime;
      _action = _$v.action;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncSync other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SyncSync;
  }

  @override
  void update(void Function(SyncSyncBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncSync build() {
    final _$result = _$v ??
        new _$SyncSync._(
            dataType: dataType,
            dataIdentifier: dataIdentifier,
            syncOrder: syncOrder,
            syncTime: syncTime,
            action: action);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
