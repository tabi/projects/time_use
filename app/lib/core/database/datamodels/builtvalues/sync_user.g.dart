// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_user.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncUser> _$syncUserSerializer = new _$SyncUserSerializer();

class _$SyncUserSerializer implements StructuredSerializer<SyncUser> {
  @override
  final Iterable<Type> types = const [SyncUser, _$SyncUser];
  @override
  final String wireName = 'SyncUser';

  @override
  Iterable<Object> serialize(Serializers serializers, SyncUser object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'groupId',
      serializers.serialize(object.groupId, specifiedType: const FullType(int)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'password',
      serializers.serialize(object.password,
          specifiedType: const FullType(String)),
      'syncOrder',
      serializers.serialize(object.syncOrder,
          specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  SyncUser deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncUserBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'groupId':
          result.groupId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'password':
          result.password = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'syncOrder':
          result.syncOrder = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$SyncUser extends SyncUser {
  @override
  final int id;
  @override
  final int groupId;
  @override
  final String name;
  @override
  final String password;
  @override
  final int syncOrder;

  factory _$SyncUser([void Function(SyncUserBuilder) updates]) =>
      (new SyncUserBuilder()..update(updates)).build();

  _$SyncUser._(
      {this.id, this.groupId, this.name, this.password, this.syncOrder})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('SyncUser', 'id');
    }
    if (groupId == null) {
      throw new BuiltValueNullFieldError('SyncUser', 'groupId');
    }
    if (name == null) {
      throw new BuiltValueNullFieldError('SyncUser', 'name');
    }
    if (password == null) {
      throw new BuiltValueNullFieldError('SyncUser', 'password');
    }
    if (syncOrder == null) {
      throw new BuiltValueNullFieldError('SyncUser', 'syncOrder');
    }
  }

  @override
  SyncUser rebuild(void Function(SyncUserBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncUserBuilder toBuilder() => new SyncUserBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncUser &&
        id == other.id &&
        groupId == other.groupId &&
        name == other.name &&
        password == other.password &&
        syncOrder == other.syncOrder;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, id.hashCode), groupId.hashCode), name.hashCode),
            password.hashCode),
        syncOrder.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncUser')
          ..add('id', id)
          ..add('groupId', groupId)
          ..add('name', name)
          ..add('password', password)
          ..add('syncOrder', syncOrder))
        .toString();
  }
}

class SyncUserBuilder implements Builder<SyncUser, SyncUserBuilder> {
  _$SyncUser _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  int _groupId;
  int get groupId => _$this._groupId;
  set groupId(int groupId) => _$this._groupId = groupId;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _password;
  String get password => _$this._password;
  set password(String password) => _$this._password = password;

  int _syncOrder;
  int get syncOrder => _$this._syncOrder;
  set syncOrder(int syncOrder) => _$this._syncOrder = syncOrder;

  SyncUserBuilder();

  SyncUserBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _groupId = _$v.groupId;
      _name = _$v.name;
      _password = _$v.password;
      _syncOrder = _$v.syncOrder;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncUser other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SyncUser;
  }

  @override
  void update(void Function(SyncUserBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncUser build() {
    final _$result = _$v ??
        new _$SyncUser._(
            id: id,
            groupId: groupId,
            name: name,
            password: password,
            syncOrder: syncOrder);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
