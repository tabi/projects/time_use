// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_push_receipt_body.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncPushReceiptBody> _$syncPushReceiptBodySerializer =
    new _$SyncPushReceiptBodySerializer();

class _$SyncPushReceiptBodySerializer
    implements StructuredSerializer<SyncPushReceiptBody> {
  @override
  final Iterable<Type> types = const [
    SyncPushReceiptBody,
    _$SyncPushReceiptBody
  ];
  @override
  final String wireName = 'SyncPushReceiptBody';

  @override
  Iterable<Object> serialize(
      Serializers serializers, SyncPushReceiptBody object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'user',
      serializers.serialize(object.user,
          specifiedType: const FullType(SyncUser)),
      'phone',
      serializers.serialize(object.phone,
          specifiedType: const FullType(SyncPhone)),
      'syncOrder',
      serializers.serialize(object.syncOrder,
          specifiedType: const FullType(int)),
      'synchronisation',
      serializers.serialize(object.synchronisation,
          specifiedType: const FullType(SyncSync)),
      'transaction',
      serializers.serialize(object.transaction,
          specifiedType: const FullType(SyncTransaction)),
      'products',
      serializers.serialize(object.products,
          specifiedType:
              const FullType(BuiltList, const [const FullType(SyncProduct)])),
      'image',
      serializers.serialize(object.image,
          specifiedType: const FullType(SyncImage)),
    ];

    return result;
  }

  @override
  SyncPushReceiptBody deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncPushReceiptBodyBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'user':
          result.user.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncUser)) as SyncUser);
          break;
        case 'phone':
          result.phone.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncPhone)) as SyncPhone);
          break;
        case 'syncOrder':
          result.syncOrder = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'synchronisation':
          result.synchronisation.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncSync)) as SyncSync);
          break;
        case 'transaction':
          result.transaction.replace(serializers.deserialize(value,
                  specifiedType: const FullType(SyncTransaction))
              as SyncTransaction);
          break;
        case 'products':
          result.products.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(SyncProduct)]))
              as BuiltList<Object>);
          break;
        case 'image':
          result.image.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncImage)) as SyncImage);
          break;
      }
    }

    return result.build();
  }
}

class _$SyncPushReceiptBody extends SyncPushReceiptBody {
  @override
  final SyncUser user;
  @override
  final SyncPhone phone;
  @override
  final int syncOrder;
  @override
  final SyncSync synchronisation;
  @override
  final SyncTransaction transaction;
  @override
  final BuiltList<SyncProduct> products;
  @override
  final SyncImage image;

  factory _$SyncPushReceiptBody(
          [void Function(SyncPushReceiptBodyBuilder) updates]) =>
      (new SyncPushReceiptBodyBuilder()..update(updates)).build();

  _$SyncPushReceiptBody._(
      {this.user,
      this.phone,
      this.syncOrder,
      this.synchronisation,
      this.transaction,
      this.products,
      this.image})
      : super._() {
    if (user == null) {
      throw new BuiltValueNullFieldError('SyncPushReceiptBody', 'user');
    }
    if (phone == null) {
      throw new BuiltValueNullFieldError('SyncPushReceiptBody', 'phone');
    }
    if (syncOrder == null) {
      throw new BuiltValueNullFieldError('SyncPushReceiptBody', 'syncOrder');
    }
    if (synchronisation == null) {
      throw new BuiltValueNullFieldError(
          'SyncPushReceiptBody', 'synchronisation');
    }
    if (transaction == null) {
      throw new BuiltValueNullFieldError('SyncPushReceiptBody', 'transaction');
    }
    if (products == null) {
      throw new BuiltValueNullFieldError('SyncPushReceiptBody', 'products');
    }
    if (image == null) {
      throw new BuiltValueNullFieldError('SyncPushReceiptBody', 'image');
    }
  }

  @override
  SyncPushReceiptBody rebuild(
          void Function(SyncPushReceiptBodyBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncPushReceiptBodyBuilder toBuilder() =>
      new SyncPushReceiptBodyBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncPushReceiptBody &&
        user == other.user &&
        phone == other.phone &&
        syncOrder == other.syncOrder &&
        synchronisation == other.synchronisation &&
        transaction == other.transaction &&
        products == other.products &&
        image == other.image;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, user.hashCode), phone.hashCode),
                        syncOrder.hashCode),
                    synchronisation.hashCode),
                transaction.hashCode),
            products.hashCode),
        image.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncPushReceiptBody')
          ..add('user', user)
          ..add('phone', phone)
          ..add('syncOrder', syncOrder)
          ..add('synchronisation', synchronisation)
          ..add('transaction', transaction)
          ..add('products', products)
          ..add('image', image))
        .toString();
  }
}

class SyncPushReceiptBodyBuilder
    implements Builder<SyncPushReceiptBody, SyncPushReceiptBodyBuilder> {
  _$SyncPushReceiptBody _$v;

  SyncUserBuilder _user;
  SyncUserBuilder get user => _$this._user ??= new SyncUserBuilder();
  set user(SyncUserBuilder user) => _$this._user = user;

  SyncPhoneBuilder _phone;
  SyncPhoneBuilder get phone => _$this._phone ??= new SyncPhoneBuilder();
  set phone(SyncPhoneBuilder phone) => _$this._phone = phone;

  int _syncOrder;
  int get syncOrder => _$this._syncOrder;
  set syncOrder(int syncOrder) => _$this._syncOrder = syncOrder;

  SyncSyncBuilder _synchronisation;
  SyncSyncBuilder get synchronisation =>
      _$this._synchronisation ??= new SyncSyncBuilder();
  set synchronisation(SyncSyncBuilder synchronisation) =>
      _$this._synchronisation = synchronisation;

  SyncTransactionBuilder _transaction;
  SyncTransactionBuilder get transaction =>
      _$this._transaction ??= new SyncTransactionBuilder();
  set transaction(SyncTransactionBuilder transaction) =>
      _$this._transaction = transaction;

  ListBuilder<SyncProduct> _products;
  ListBuilder<SyncProduct> get products =>
      _$this._products ??= new ListBuilder<SyncProduct>();
  set products(ListBuilder<SyncProduct> products) =>
      _$this._products = products;

  SyncImageBuilder _image;
  SyncImageBuilder get image => _$this._image ??= new SyncImageBuilder();
  set image(SyncImageBuilder image) => _$this._image = image;

  SyncPushReceiptBodyBuilder();

  SyncPushReceiptBodyBuilder get _$this {
    if (_$v != null) {
      _user = _$v.user?.toBuilder();
      _phone = _$v.phone?.toBuilder();
      _syncOrder = _$v.syncOrder;
      _synchronisation = _$v.synchronisation?.toBuilder();
      _transaction = _$v.transaction?.toBuilder();
      _products = _$v.products?.toBuilder();
      _image = _$v.image?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncPushReceiptBody other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SyncPushReceiptBody;
  }

  @override
  void update(void Function(SyncPushReceiptBodyBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncPushReceiptBody build() {
    _$SyncPushReceiptBody _$result;
    try {
      _$result = _$v ??
          new _$SyncPushReceiptBody._(
              user: user.build(),
              phone: phone.build(),
              syncOrder: syncOrder,
              synchronisation: synchronisation.build(),
              transaction: transaction.build(),
              products: products.build(),
              image: image.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'user';
        user.build();
        _$failedField = 'phone';
        phone.build();

        _$failedField = 'synchronisation';
        synchronisation.build();
        _$failedField = 'transaction';
        transaction.build();
        _$failedField = 'products';
        products.build();
        _$failedField = 'image';
        image.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'SyncPushReceiptBody', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
