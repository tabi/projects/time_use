// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_search_product_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncSearchProductData> _$syncSearchProductDataSerializer =
    new _$SyncSearchProductDataSerializer();

class _$SyncSearchProductDataSerializer
    implements StructuredSerializer<SyncSearchProductData> {
  @override
  final Iterable<Type> types = const [
    SyncSearchProductData,
    _$SyncSearchProductData
  ];
  @override
  final String wireName = 'SyncSearchProductData';

  @override
  Iterable<Object> serialize(
      Serializers serializers, SyncSearchProductData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'synchronisation',
      serializers.serialize(object.synchronisation,
          specifiedType: const FullType(SyncSync)),
      'searchProduct',
      serializers.serialize(object.searchProduct,
          specifiedType: const FullType(SyncSearchProduct)),
    ];

    return result;
  }

  @override
  SyncSearchProductData deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncSearchProductDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'synchronisation':
          result.synchronisation.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncSync)) as SyncSync);
          break;
        case 'searchProduct':
          result.searchProduct.replace(serializers.deserialize(value,
                  specifiedType: const FullType(SyncSearchProduct))
              as SyncSearchProduct);
          break;
      }
    }

    return result.build();
  }
}

class _$SyncSearchProductData extends SyncSearchProductData {
  @override
  final SyncSync synchronisation;
  @override
  final SyncSearchProduct searchProduct;

  factory _$SyncSearchProductData(
          [void Function(SyncSearchProductDataBuilder) updates]) =>
      (new SyncSearchProductDataBuilder()..update(updates)).build();

  _$SyncSearchProductData._({this.synchronisation, this.searchProduct})
      : super._() {
    if (synchronisation == null) {
      throw new BuiltValueNullFieldError(
          'SyncSearchProductData', 'synchronisation');
    }
    if (searchProduct == null) {
      throw new BuiltValueNullFieldError(
          'SyncSearchProductData', 'searchProduct');
    }
  }

  @override
  SyncSearchProductData rebuild(
          void Function(SyncSearchProductDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncSearchProductDataBuilder toBuilder() =>
      new SyncSearchProductDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncSearchProductData &&
        synchronisation == other.synchronisation &&
        searchProduct == other.searchProduct;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, synchronisation.hashCode), searchProduct.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncSearchProductData')
          ..add('synchronisation', synchronisation)
          ..add('searchProduct', searchProduct))
        .toString();
  }
}

class SyncSearchProductDataBuilder
    implements Builder<SyncSearchProductData, SyncSearchProductDataBuilder> {
  _$SyncSearchProductData _$v;

  SyncSyncBuilder _synchronisation;
  SyncSyncBuilder get synchronisation =>
      _$this._synchronisation ??= new SyncSyncBuilder();
  set synchronisation(SyncSyncBuilder synchronisation) =>
      _$this._synchronisation = synchronisation;

  SyncSearchProductBuilder _searchProduct;
  SyncSearchProductBuilder get searchProduct =>
      _$this._searchProduct ??= new SyncSearchProductBuilder();
  set searchProduct(SyncSearchProductBuilder searchProduct) =>
      _$this._searchProduct = searchProduct;

  SyncSearchProductDataBuilder();

  SyncSearchProductDataBuilder get _$this {
    if (_$v != null) {
      _synchronisation = _$v.synchronisation?.toBuilder();
      _searchProduct = _$v.searchProduct?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncSearchProductData other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SyncSearchProductData;
  }

  @override
  void update(void Function(SyncSearchProductDataBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncSearchProductData build() {
    _$SyncSearchProductData _$result;
    try {
      _$result = _$v ??
          new _$SyncSearchProductData._(
              synchronisation: synchronisation.build(),
              searchProduct: searchProduct.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'synchronisation';
        synchronisation.build();
        _$failedField = 'searchProduct';
        searchProduct.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'SyncSearchProductData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
