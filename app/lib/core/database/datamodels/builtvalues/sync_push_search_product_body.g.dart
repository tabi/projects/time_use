// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_push_search_product_body.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncPushSearchProductBody> _$syncPushSearchProductBodySerializer =
    new _$SyncPushSearchProductBodySerializer();

class _$SyncPushSearchProductBodySerializer
    implements StructuredSerializer<SyncPushSearchProductBody> {
  @override
  final Iterable<Type> types = const [
    SyncPushSearchProductBody,
    _$SyncPushSearchProductBody
  ];
  @override
  final String wireName = 'SyncPushSearchProductBody';

  @override
  Iterable<Object> serialize(
      Serializers serializers, SyncPushSearchProductBody object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'user',
      serializers.serialize(object.user,
          specifiedType: const FullType(SyncUser)),
      'phone',
      serializers.serialize(object.phone,
          specifiedType: const FullType(SyncPhone)),
      'syncOrder',
      serializers.serialize(object.syncOrder,
          specifiedType: const FullType(int)),
      'synchronisation',
      serializers.serialize(object.synchronisation,
          specifiedType: const FullType(SyncSync)),
      'searchProduct',
      serializers.serialize(object.searchProduct,
          specifiedType: const FullType(SyncSearchProduct)),
    ];

    return result;
  }

  @override
  SyncPushSearchProductBody deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncPushSearchProductBodyBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'user':
          result.user.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncUser)) as SyncUser);
          break;
        case 'phone':
          result.phone.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncPhone)) as SyncPhone);
          break;
        case 'syncOrder':
          result.syncOrder = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'synchronisation':
          result.synchronisation.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncSync)) as SyncSync);
          break;
        case 'searchProduct':
          result.searchProduct.replace(serializers.deserialize(value,
                  specifiedType: const FullType(SyncSearchProduct))
              as SyncSearchProduct);
          break;
      }
    }

    return result.build();
  }
}

class _$SyncPushSearchProductBody extends SyncPushSearchProductBody {
  @override
  final SyncUser user;
  @override
  final SyncPhone phone;
  @override
  final int syncOrder;
  @override
  final SyncSync synchronisation;
  @override
  final SyncSearchProduct searchProduct;

  factory _$SyncPushSearchProductBody(
          [void Function(SyncPushSearchProductBodyBuilder) updates]) =>
      (new SyncPushSearchProductBodyBuilder()..update(updates)).build();

  _$SyncPushSearchProductBody._(
      {this.user,
      this.phone,
      this.syncOrder,
      this.synchronisation,
      this.searchProduct})
      : super._() {
    if (user == null) {
      throw new BuiltValueNullFieldError('SyncPushSearchProductBody', 'user');
    }
    if (phone == null) {
      throw new BuiltValueNullFieldError('SyncPushSearchProductBody', 'phone');
    }
    if (syncOrder == null) {
      throw new BuiltValueNullFieldError(
          'SyncPushSearchProductBody', 'syncOrder');
    }
    if (synchronisation == null) {
      throw new BuiltValueNullFieldError(
          'SyncPushSearchProductBody', 'synchronisation');
    }
    if (searchProduct == null) {
      throw new BuiltValueNullFieldError(
          'SyncPushSearchProductBody', 'searchProduct');
    }
  }

  @override
  SyncPushSearchProductBody rebuild(
          void Function(SyncPushSearchProductBodyBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncPushSearchProductBodyBuilder toBuilder() =>
      new SyncPushSearchProductBodyBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncPushSearchProductBody &&
        user == other.user &&
        phone == other.phone &&
        syncOrder == other.syncOrder &&
        synchronisation == other.synchronisation &&
        searchProduct == other.searchProduct;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, user.hashCode), phone.hashCode), syncOrder.hashCode),
            synchronisation.hashCode),
        searchProduct.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncPushSearchProductBody')
          ..add('user', user)
          ..add('phone', phone)
          ..add('syncOrder', syncOrder)
          ..add('synchronisation', synchronisation)
          ..add('searchProduct', searchProduct))
        .toString();
  }
}

class SyncPushSearchProductBodyBuilder
    implements
        Builder<SyncPushSearchProductBody, SyncPushSearchProductBodyBuilder> {
  _$SyncPushSearchProductBody _$v;

  SyncUserBuilder _user;
  SyncUserBuilder get user => _$this._user ??= new SyncUserBuilder();
  set user(SyncUserBuilder user) => _$this._user = user;

  SyncPhoneBuilder _phone;
  SyncPhoneBuilder get phone => _$this._phone ??= new SyncPhoneBuilder();
  set phone(SyncPhoneBuilder phone) => _$this._phone = phone;

  int _syncOrder;
  int get syncOrder => _$this._syncOrder;
  set syncOrder(int syncOrder) => _$this._syncOrder = syncOrder;

  SyncSyncBuilder _synchronisation;
  SyncSyncBuilder get synchronisation =>
      _$this._synchronisation ??= new SyncSyncBuilder();
  set synchronisation(SyncSyncBuilder synchronisation) =>
      _$this._synchronisation = synchronisation;

  SyncSearchProductBuilder _searchProduct;
  SyncSearchProductBuilder get searchProduct =>
      _$this._searchProduct ??= new SyncSearchProductBuilder();
  set searchProduct(SyncSearchProductBuilder searchProduct) =>
      _$this._searchProduct = searchProduct;

  SyncPushSearchProductBodyBuilder();

  SyncPushSearchProductBodyBuilder get _$this {
    if (_$v != null) {
      _user = _$v.user?.toBuilder();
      _phone = _$v.phone?.toBuilder();
      _syncOrder = _$v.syncOrder;
      _synchronisation = _$v.synchronisation?.toBuilder();
      _searchProduct = _$v.searchProduct?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncPushSearchProductBody other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SyncPushSearchProductBody;
  }

  @override
  void update(void Function(SyncPushSearchProductBodyBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncPushSearchProductBody build() {
    _$SyncPushSearchProductBody _$result;
    try {
      _$result = _$v ??
          new _$SyncPushSearchProductBody._(
              user: user.build(),
              phone: phone.build(),
              syncOrder: syncOrder,
              synchronisation: synchronisation.build(),
              searchProduct: searchProduct.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'user';
        user.build();
        _$failedField = 'phone';
        phone.build();

        _$failedField = 'synchronisation';
        synchronisation.build();
        _$failedField = 'searchProduct';
        searchProduct.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'SyncPushSearchProductBody', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
