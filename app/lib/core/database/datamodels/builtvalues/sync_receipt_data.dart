import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:tbo_app/core/serializers.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_sync.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_transaction.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_product.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_image.dart';
import 'package:built_collection/built_collection.dart';

part 'sync_receipt_data.g.dart';

abstract class SyncReceiptData implements Built<SyncReceiptData, SyncReceiptDataBuilder> {
  static Serializer<SyncReceiptData> get serializer => _$syncReceiptDataSerializer;

  SyncSync get synchronisation;
  SyncTransaction get transaction;
  BuiltList<SyncProduct> get products;
  SyncImage get image;

  factory SyncReceiptData([Function(SyncReceiptDataBuilder b) updates]) = _$SyncReceiptData;

  SyncReceiptData._();

  factory SyncReceiptData.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
