import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:tbo_app/core/serializers.dart';

part 'sync_product.g.dart';

abstract class SyncProduct implements Built<SyncProduct, SyncProductBuilder> {
  static Serializer<SyncProduct> get serializer => _$syncProductSerializer;

  double get price;
  String get product;
  String get productCategory;
  String get productCode;
  String get productDate;
  String get productGroupID;
  String get transactionID;

  factory SyncProduct([Function(SyncProductBuilder b) updates]) = _$SyncProduct;

  SyncProduct._();

  factory SyncProduct.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
