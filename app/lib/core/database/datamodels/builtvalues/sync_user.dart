import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:tbo_app/core/serializers.dart';

part 'sync_user.g.dart';

abstract class SyncUser implements Built<SyncUser, SyncUserBuilder> {
  static Serializer<SyncUser> get serializer => _$syncUserSerializer;

  int get id;
  int get groupId;
  String get name;
  String get password;
  int get syncOrder;

  factory SyncUser([Function(SyncUserBuilder b) updates]) = _$SyncUser;

  SyncUser._();

  factory SyncUser.newInstance(String name, String password) {
    return SyncUser((b) => b
      ..id = -1
      ..groupId = -1
      ..name = name
      ..password = password
      ..syncOrder = -1);
  }

  factory SyncUser.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
