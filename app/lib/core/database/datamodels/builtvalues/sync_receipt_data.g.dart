// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_receipt_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncReceiptData> _$syncReceiptDataSerializer =
    new _$SyncReceiptDataSerializer();

class _$SyncReceiptDataSerializer
    implements StructuredSerializer<SyncReceiptData> {
  @override
  final Iterable<Type> types = const [SyncReceiptData, _$SyncReceiptData];
  @override
  final String wireName = 'SyncReceiptData';

  @override
  Iterable<Object> serialize(Serializers serializers, SyncReceiptData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'synchronisation',
      serializers.serialize(object.synchronisation,
          specifiedType: const FullType(SyncSync)),
      'transaction',
      serializers.serialize(object.transaction,
          specifiedType: const FullType(SyncTransaction)),
      'products',
      serializers.serialize(object.products,
          specifiedType:
              const FullType(BuiltList, const [const FullType(SyncProduct)])),
      'image',
      serializers.serialize(object.image,
          specifiedType: const FullType(SyncImage)),
    ];

    return result;
  }

  @override
  SyncReceiptData deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncReceiptDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'synchronisation':
          result.synchronisation.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncSync)) as SyncSync);
          break;
        case 'transaction':
          result.transaction.replace(serializers.deserialize(value,
                  specifiedType: const FullType(SyncTransaction))
              as SyncTransaction);
          break;
        case 'products':
          result.products.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(SyncProduct)]))
              as BuiltList<Object>);
          break;
        case 'image':
          result.image.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncImage)) as SyncImage);
          break;
      }
    }

    return result.build();
  }
}

class _$SyncReceiptData extends SyncReceiptData {
  @override
  final SyncSync synchronisation;
  @override
  final SyncTransaction transaction;
  @override
  final BuiltList<SyncProduct> products;
  @override
  final SyncImage image;

  factory _$SyncReceiptData([void Function(SyncReceiptDataBuilder) updates]) =>
      (new SyncReceiptDataBuilder()..update(updates)).build();

  _$SyncReceiptData._(
      {this.synchronisation, this.transaction, this.products, this.image})
      : super._() {
    if (synchronisation == null) {
      throw new BuiltValueNullFieldError('SyncReceiptData', 'synchronisation');
    }
    if (transaction == null) {
      throw new BuiltValueNullFieldError('SyncReceiptData', 'transaction');
    }
    if (products == null) {
      throw new BuiltValueNullFieldError('SyncReceiptData', 'products');
    }
    if (image == null) {
      throw new BuiltValueNullFieldError('SyncReceiptData', 'image');
    }
  }

  @override
  SyncReceiptData rebuild(void Function(SyncReceiptDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncReceiptDataBuilder toBuilder() =>
      new SyncReceiptDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncReceiptData &&
        synchronisation == other.synchronisation &&
        transaction == other.transaction &&
        products == other.products &&
        image == other.image;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, synchronisation.hashCode), transaction.hashCode),
            products.hashCode),
        image.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncReceiptData')
          ..add('synchronisation', synchronisation)
          ..add('transaction', transaction)
          ..add('products', products)
          ..add('image', image))
        .toString();
  }
}

class SyncReceiptDataBuilder
    implements Builder<SyncReceiptData, SyncReceiptDataBuilder> {
  _$SyncReceiptData _$v;

  SyncSyncBuilder _synchronisation;
  SyncSyncBuilder get synchronisation =>
      _$this._synchronisation ??= new SyncSyncBuilder();
  set synchronisation(SyncSyncBuilder synchronisation) =>
      _$this._synchronisation = synchronisation;

  SyncTransactionBuilder _transaction;
  SyncTransactionBuilder get transaction =>
      _$this._transaction ??= new SyncTransactionBuilder();
  set transaction(SyncTransactionBuilder transaction) =>
      _$this._transaction = transaction;

  ListBuilder<SyncProduct> _products;
  ListBuilder<SyncProduct> get products =>
      _$this._products ??= new ListBuilder<SyncProduct>();
  set products(ListBuilder<SyncProduct> products) =>
      _$this._products = products;

  SyncImageBuilder _image;
  SyncImageBuilder get image => _$this._image ??= new SyncImageBuilder();
  set image(SyncImageBuilder image) => _$this._image = image;

  SyncReceiptDataBuilder();

  SyncReceiptDataBuilder get _$this {
    if (_$v != null) {
      _synchronisation = _$v.synchronisation?.toBuilder();
      _transaction = _$v.transaction?.toBuilder();
      _products = _$v.products?.toBuilder();
      _image = _$v.image?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncReceiptData other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SyncReceiptData;
  }

  @override
  void update(void Function(SyncReceiptDataBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncReceiptData build() {
    _$SyncReceiptData _$result;
    try {
      _$result = _$v ??
          new _$SyncReceiptData._(
              synchronisation: synchronisation.build(),
              transaction: transaction.build(),
              products: products.build(),
              image: image.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'synchronisation';
        synchronisation.build();
        _$failedField = 'transaction';
        transaction.build();
        _$failedField = 'products';
        products.build();
        _$failedField = 'image';
        image.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'SyncReceiptData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
