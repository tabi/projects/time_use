import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:tbo_app/core/serializers.dart';

part 'sync_search_product.g.dart';

abstract class SyncSearchProduct implements Built<SyncSearchProduct, SyncSearchProductBuilder> {
  static Serializer<SyncSearchProduct> get serializer => _$syncSearchProductSerializer;

  String get product;
  String get productCategory;
  String get productCode;
  int get lastAdded;
  int get count;

  factory SyncSearchProduct([Function(SyncSearchProductBuilder b) updates]) = _$SyncSearchProduct;

  SyncSearchProduct._();

  factory SyncSearchProduct.newInstance(String productCode) {
    return SyncSearchProduct((b) => b
      ..count = 0
      ..lastAdded = 0
      ..product = ''
      ..productCategory = ''
      ..productCode = productCode);
  }
  
  factory SyncSearchProduct.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
