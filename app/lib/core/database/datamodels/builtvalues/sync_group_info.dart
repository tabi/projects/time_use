import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:tbo_app/core/serializers.dart';

part 'sync_group_info.g.dart';

abstract class SyncGroupInfo implements Built<SyncGroupInfo, SyncGroupInfoBuilder> {
  static Serializer<SyncGroupInfo> get serializer => _$syncGroupInfoSerializer;

  int get id;
  int get groupId;
  String get key;
  String get value;

  factory SyncGroupInfo([Function(SyncGroupInfoBuilder b) updates]) = _$SyncGroupInfo;

  SyncGroupInfo._();

  factory SyncGroupInfo.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
