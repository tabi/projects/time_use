import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:tbo_app/core/serializers.dart';

part 'sync_sync.g.dart';

abstract class SyncSync implements Built<SyncSync, SyncSyncBuilder> {
  static Serializer<SyncSync> get serializer => _$syncSyncSerializer;

  
  @nullable
  int get dataType;
  @nullable
  String get dataIdentifier;
  @nullable
  int get syncOrder;
  @nullable
  int get syncTime;
  @nullable
  int get action;

  factory SyncSync([Function(SyncSyncBuilder b) updates]) = _$SyncSync;

  SyncSync._();

  factory SyncSync.newInstance() {
    return SyncSync((b) => b
      ..dataType = 0
      ..dataIdentifier = ''
      ..syncOrder = 0
      ..syncTime = 0
      ..action = 0);
  }

  factory SyncSync.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
