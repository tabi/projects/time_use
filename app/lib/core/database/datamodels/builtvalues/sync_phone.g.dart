// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_phone.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncPhone> _$syncPhoneSerializer = new _$SyncPhoneSerializer();

class _$SyncPhoneSerializer implements StructuredSerializer<SyncPhone> {
  @override
  final Iterable<Type> types = const [SyncPhone, _$SyncPhone];
  @override
  final String wireName = 'SyncPhone';

  @override
  Iterable<Object> serialize(Serializers serializers, SyncPhone object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'userId',
      serializers.serialize(object.userId, specifiedType: const FullType(int)),
      'name',
      serializers.serialize(object.name, specifiedType: const FullType(String)),
      'syncOrder',
      serializers.serialize(object.syncOrder,
          specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  SyncPhone deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncPhoneBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'userId':
          result.userId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'syncOrder':
          result.syncOrder = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$SyncPhone extends SyncPhone {
  @override
  final int id;
  @override
  final int userId;
  @override
  final String name;
  @override
  final int syncOrder;

  factory _$SyncPhone([void Function(SyncPhoneBuilder) updates]) =>
      (new SyncPhoneBuilder()..update(updates)).build();

  _$SyncPhone._({this.id, this.userId, this.name, this.syncOrder}) : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('SyncPhone', 'id');
    }
    if (userId == null) {
      throw new BuiltValueNullFieldError('SyncPhone', 'userId');
    }
    if (name == null) {
      throw new BuiltValueNullFieldError('SyncPhone', 'name');
    }
    if (syncOrder == null) {
      throw new BuiltValueNullFieldError('SyncPhone', 'syncOrder');
    }
  }

  @override
  SyncPhone rebuild(void Function(SyncPhoneBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncPhoneBuilder toBuilder() => new SyncPhoneBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncPhone &&
        id == other.id &&
        userId == other.userId &&
        name == other.name &&
        syncOrder == other.syncOrder;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, id.hashCode), userId.hashCode), name.hashCode),
        syncOrder.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncPhone')
          ..add('id', id)
          ..add('userId', userId)
          ..add('name', name)
          ..add('syncOrder', syncOrder))
        .toString();
  }
}

class SyncPhoneBuilder implements Builder<SyncPhone, SyncPhoneBuilder> {
  _$SyncPhone _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  int _userId;
  int get userId => _$this._userId;
  set userId(int userId) => _$this._userId = userId;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  int _syncOrder;
  int get syncOrder => _$this._syncOrder;
  set syncOrder(int syncOrder) => _$this._syncOrder = syncOrder;

  SyncPhoneBuilder();

  SyncPhoneBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _userId = _$v.userId;
      _name = _$v.name;
      _syncOrder = _$v.syncOrder;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncPhone other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SyncPhone;
  }

  @override
  void update(void Function(SyncPhoneBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncPhone build() {
    final _$result = _$v ??
        new _$SyncPhone._(
            id: id, userId: userId, name: name, syncOrder: syncOrder);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
