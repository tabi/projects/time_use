import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:tbo_app/core/serializers.dart';

part 'sync_search_store.g.dart';

abstract class SyncSearchStore implements Built<SyncSearchStore, SyncSearchStoreBuilder> {
  static Serializer<SyncSearchStore> get serializer => _$syncSearchStoreSerializer;

  String get storeName;
  String get storeType;
  int get lastAdded;
  int get count;

  factory SyncSearchStore([Function(SyncSearchStoreBuilder b) updates]) = _$SyncSearchStore;

  SyncSearchStore._();

  factory SyncSearchStore.newInstance(String storeName) {
    return SyncSearchStore((b) => b
      ..storeName = storeName
      ..storeType = ''
      ..lastAdded = 0
      ..count = 0);
  }

  factory SyncSearchStore.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
