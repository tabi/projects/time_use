import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:tbo_app/core/serializers.dart';

part 'sync_image.g.dart';

abstract class SyncImage implements Built<SyncImage, SyncImageBuilder> {
  static Serializer<SyncImage> get serializer => _$syncImageSerializer;

  String get base64image;
  String get transactionID;

  factory SyncImage([Function(SyncImageBuilder b) updates]) = _$SyncImage;

  SyncImage._();

  factory SyncImage.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
