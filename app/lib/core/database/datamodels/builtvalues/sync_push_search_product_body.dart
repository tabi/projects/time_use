import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:tbo_app/core/serializers.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_user.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_phone.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_sync.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_search_product.dart';

part 'sync_push_search_product_body.g.dart';

abstract class SyncPushSearchProductBody implements Built<SyncPushSearchProductBody, SyncPushSearchProductBodyBuilder> {
  static Serializer<SyncPushSearchProductBody> get serializer => _$syncPushSearchProductBodySerializer;

  SyncUser get user;
  SyncPhone get phone;
  int get syncOrder;
  SyncSync get synchronisation;
  SyncSearchProduct get searchProduct;

  factory SyncPushSearchProductBody([Function(SyncPushSearchProductBodyBuilder b) updates]) = _$SyncPushSearchProductBody;

  SyncPushSearchProductBody._();

  factory SyncPushSearchProductBody.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
