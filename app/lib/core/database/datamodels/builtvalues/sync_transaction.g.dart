// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_transaction.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncTransaction> _$syncTransactionSerializer =
    new _$SyncTransactionSerializer();

class _$SyncTransactionSerializer
    implements StructuredSerializer<SyncTransaction> {
  @override
  final Iterable<Type> types = const [SyncTransaction, _$SyncTransaction];
  @override
  final String wireName = 'SyncTransaction';

  @override
  Iterable<Object> serialize(Serializers serializers, SyncTransaction object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'date',
      serializers.serialize(object.date, specifiedType: const FullType(int)),
      'discountAmount',
      serializers.serialize(object.discountAmount,
          specifiedType: const FullType(String)),
      'discountPercentage',
      serializers.serialize(object.discountPercentage,
          specifiedType: const FullType(String)),
      'discountText',
      serializers.serialize(object.discountText,
          specifiedType: const FullType(String)),
      'expenseAbroad',
      serializers.serialize(object.expenseAbroad,
          specifiedType: const FullType(String)),
      'expenseOnline',
      serializers.serialize(object.expenseOnline,
          specifiedType: const FullType(String)),
      'receiptLocation',
      serializers.serialize(object.receiptLocation,
          specifiedType: const FullType(String)),
      'receiptProductType',
      serializers.serialize(object.receiptProductType,
          specifiedType: const FullType(String)),
      'store',
      serializers.serialize(object.store,
          specifiedType: const FullType(String)),
      'storeType',
      serializers.serialize(object.storeType,
          specifiedType: const FullType(String)),
      'totalPrice',
      serializers.serialize(object.totalPrice,
          specifiedType: const FullType(double)),
      'transactionID',
      serializers.serialize(object.transactionID,
          specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  SyncTransaction deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncTransactionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'date':
          result.date = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'discountAmount':
          result.discountAmount = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'discountPercentage':
          result.discountPercentage = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'discountText':
          result.discountText = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'expenseAbroad':
          result.expenseAbroad = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'expenseOnline':
          result.expenseOnline = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'receiptLocation':
          result.receiptLocation = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'receiptProductType':
          result.receiptProductType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'store':
          result.store = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'storeType':
          result.storeType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'totalPrice':
          result.totalPrice = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'transactionID':
          result.transactionID = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$SyncTransaction extends SyncTransaction {
  @override
  final int date;
  @override
  final String discountAmount;
  @override
  final String discountPercentage;
  @override
  final String discountText;
  @override
  final String expenseAbroad;
  @override
  final String expenseOnline;
  @override
  final String receiptLocation;
  @override
  final String receiptProductType;
  @override
  final String store;
  @override
  final String storeType;
  @override
  final double totalPrice;
  @override
  final String transactionID;

  factory _$SyncTransaction([void Function(SyncTransactionBuilder) updates]) =>
      (new SyncTransactionBuilder()..update(updates)).build();

  _$SyncTransaction._(
      {this.date,
      this.discountAmount,
      this.discountPercentage,
      this.discountText,
      this.expenseAbroad,
      this.expenseOnline,
      this.receiptLocation,
      this.receiptProductType,
      this.store,
      this.storeType,
      this.totalPrice,
      this.transactionID})
      : super._() {
    if (date == null) {
      throw new BuiltValueNullFieldError('SyncTransaction', 'date');
    }
    if (discountAmount == null) {
      throw new BuiltValueNullFieldError('SyncTransaction', 'discountAmount');
    }
    if (discountPercentage == null) {
      throw new BuiltValueNullFieldError(
          'SyncTransaction', 'discountPercentage');
    }
    if (discountText == null) {
      throw new BuiltValueNullFieldError('SyncTransaction', 'discountText');
    }
    if (expenseAbroad == null) {
      throw new BuiltValueNullFieldError('SyncTransaction', 'expenseAbroad');
    }
    if (expenseOnline == null) {
      throw new BuiltValueNullFieldError('SyncTransaction', 'expenseOnline');
    }
    if (receiptLocation == null) {
      throw new BuiltValueNullFieldError('SyncTransaction', 'receiptLocation');
    }
    if (receiptProductType == null) {
      throw new BuiltValueNullFieldError(
          'SyncTransaction', 'receiptProductType');
    }
    if (store == null) {
      throw new BuiltValueNullFieldError('SyncTransaction', 'store');
    }
    if (storeType == null) {
      throw new BuiltValueNullFieldError('SyncTransaction', 'storeType');
    }
    if (totalPrice == null) {
      throw new BuiltValueNullFieldError('SyncTransaction', 'totalPrice');
    }
    if (transactionID == null) {
      throw new BuiltValueNullFieldError('SyncTransaction', 'transactionID');
    }
  }

  @override
  SyncTransaction rebuild(void Function(SyncTransactionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncTransactionBuilder toBuilder() =>
      new SyncTransactionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncTransaction &&
        date == other.date &&
        discountAmount == other.discountAmount &&
        discountPercentage == other.discountPercentage &&
        discountText == other.discountText &&
        expenseAbroad == other.expenseAbroad &&
        expenseOnline == other.expenseOnline &&
        receiptLocation == other.receiptLocation &&
        receiptProductType == other.receiptProductType &&
        store == other.store &&
        storeType == other.storeType &&
        totalPrice == other.totalPrice &&
        transactionID == other.transactionID;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc($jc(0, date.hashCode),
                                                discountAmount.hashCode),
                                            discountPercentage.hashCode),
                                        discountText.hashCode),
                                    expenseAbroad.hashCode),
                                expenseOnline.hashCode),
                            receiptLocation.hashCode),
                        receiptProductType.hashCode),
                    store.hashCode),
                storeType.hashCode),
            totalPrice.hashCode),
        transactionID.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncTransaction')
          ..add('date', date)
          ..add('discountAmount', discountAmount)
          ..add('discountPercentage', discountPercentage)
          ..add('discountText', discountText)
          ..add('expenseAbroad', expenseAbroad)
          ..add('expenseOnline', expenseOnline)
          ..add('receiptLocation', receiptLocation)
          ..add('receiptProductType', receiptProductType)
          ..add('store', store)
          ..add('storeType', storeType)
          ..add('totalPrice', totalPrice)
          ..add('transactionID', transactionID))
        .toString();
  }
}

class SyncTransactionBuilder
    implements Builder<SyncTransaction, SyncTransactionBuilder> {
  _$SyncTransaction _$v;

  int _date;
  int get date => _$this._date;
  set date(int date) => _$this._date = date;

  String _discountAmount;
  String get discountAmount => _$this._discountAmount;
  set discountAmount(String discountAmount) =>
      _$this._discountAmount = discountAmount;

  String _discountPercentage;
  String get discountPercentage => _$this._discountPercentage;
  set discountPercentage(String discountPercentage) =>
      _$this._discountPercentage = discountPercentage;

  String _discountText;
  String get discountText => _$this._discountText;
  set discountText(String discountText) => _$this._discountText = discountText;

  String _expenseAbroad;
  String get expenseAbroad => _$this._expenseAbroad;
  set expenseAbroad(String expenseAbroad) =>
      _$this._expenseAbroad = expenseAbroad;

  String _expenseOnline;
  String get expenseOnline => _$this._expenseOnline;
  set expenseOnline(String expenseOnline) =>
      _$this._expenseOnline = expenseOnline;

  String _receiptLocation;
  String get receiptLocation => _$this._receiptLocation;
  set receiptLocation(String receiptLocation) =>
      _$this._receiptLocation = receiptLocation;

  String _receiptProductType;
  String get receiptProductType => _$this._receiptProductType;
  set receiptProductType(String receiptProductType) =>
      _$this._receiptProductType = receiptProductType;

  String _store;
  String get store => _$this._store;
  set store(String store) => _$this._store = store;

  String _storeType;
  String get storeType => _$this._storeType;
  set storeType(String storeType) => _$this._storeType = storeType;

  double _totalPrice;
  double get totalPrice => _$this._totalPrice;
  set totalPrice(double totalPrice) => _$this._totalPrice = totalPrice;

  String _transactionID;
  String get transactionID => _$this._transactionID;
  set transactionID(String transactionID) =>
      _$this._transactionID = transactionID;

  SyncTransactionBuilder();

  SyncTransactionBuilder get _$this {
    if (_$v != null) {
      _date = _$v.date;
      _discountAmount = _$v.discountAmount;
      _discountPercentage = _$v.discountPercentage;
      _discountText = _$v.discountText;
      _expenseAbroad = _$v.expenseAbroad;
      _expenseOnline = _$v.expenseOnline;
      _receiptLocation = _$v.receiptLocation;
      _receiptProductType = _$v.receiptProductType;
      _store = _$v.store;
      _storeType = _$v.storeType;
      _totalPrice = _$v.totalPrice;
      _transactionID = _$v.transactionID;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncTransaction other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SyncTransaction;
  }

  @override
  void update(void Function(SyncTransactionBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncTransaction build() {
    final _$result = _$v ??
        new _$SyncTransaction._(
            date: date,
            discountAmount: discountAmount,
            discountPercentage: discountPercentage,
            discountText: discountText,
            expenseAbroad: expenseAbroad,
            expenseOnline: expenseOnline,
            receiptLocation: receiptLocation,
            receiptProductType: receiptProductType,
            store: store,
            storeType: storeType,
            totalPrice: totalPrice,
            transactionID: transactionID);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
