import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:tbo_app/core/serializers.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_user.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_phone.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_sync.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_search_store.dart';

part 'sync_push_search_store_body.g.dart';

abstract class SyncPushSearchStoreBody implements Built<SyncPushSearchStoreBody, SyncPushSearchStoreBodyBuilder> {
  static Serializer<SyncPushSearchStoreBody> get serializer => _$syncPushSearchStoreBodySerializer;

  SyncUser get user;
  SyncPhone get phone;
  int get syncOrder;
  SyncSync get synchronisation;
  SyncSearchStore get searchStore;

  factory SyncPushSearchStoreBody([Function(SyncPushSearchStoreBodyBuilder b) updates]) = _$SyncPushSearchStoreBody;

  SyncPushSearchStoreBody._();

  factory SyncPushSearchStoreBody.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
