// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_pull_body.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncPullBody> _$syncPullBodySerializer =
    new _$SyncPullBodySerializer();

class _$SyncPullBodySerializer implements StructuredSerializer<SyncPullBody> {
  @override
  final Iterable<Type> types = const [SyncPullBody, _$SyncPullBody];
  @override
  final String wireName = 'SyncPullBody';

  @override
  Iterable<Object> serialize(Serializers serializers, SyncPullBody object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'user',
      serializers.serialize(object.user,
          specifiedType: const FullType(SyncUser)),
      'phone',
      serializers.serialize(object.phone,
          specifiedType: const FullType(SyncPhone)),
      'syncOrder',
      serializers.serialize(object.syncOrder,
          specifiedType: const FullType(int)),
    ];

    return result;
  }

  @override
  SyncPullBody deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncPullBodyBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'user':
          result.user.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncUser)) as SyncUser);
          break;
        case 'phone':
          result.phone.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncPhone)) as SyncPhone);
          break;
        case 'syncOrder':
          result.syncOrder = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$SyncPullBody extends SyncPullBody {
  @override
  final SyncUser user;
  @override
  final SyncPhone phone;
  @override
  final int syncOrder;

  factory _$SyncPullBody([void Function(SyncPullBodyBuilder) updates]) =>
      (new SyncPullBodyBuilder()..update(updates)).build();

  _$SyncPullBody._({this.user, this.phone, this.syncOrder}) : super._() {
    if (user == null) {
      throw new BuiltValueNullFieldError('SyncPullBody', 'user');
    }
    if (phone == null) {
      throw new BuiltValueNullFieldError('SyncPullBody', 'phone');
    }
    if (syncOrder == null) {
      throw new BuiltValueNullFieldError('SyncPullBody', 'syncOrder');
    }
  }

  @override
  SyncPullBody rebuild(void Function(SyncPullBodyBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncPullBodyBuilder toBuilder() => new SyncPullBodyBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncPullBody &&
        user == other.user &&
        phone == other.phone &&
        syncOrder == other.syncOrder;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, user.hashCode), phone.hashCode), syncOrder.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncPullBody')
          ..add('user', user)
          ..add('phone', phone)
          ..add('syncOrder', syncOrder))
        .toString();
  }
}

class SyncPullBodyBuilder
    implements Builder<SyncPullBody, SyncPullBodyBuilder> {
  _$SyncPullBody _$v;

  SyncUserBuilder _user;
  SyncUserBuilder get user => _$this._user ??= new SyncUserBuilder();
  set user(SyncUserBuilder user) => _$this._user = user;

  SyncPhoneBuilder _phone;
  SyncPhoneBuilder get phone => _$this._phone ??= new SyncPhoneBuilder();
  set phone(SyncPhoneBuilder phone) => _$this._phone = phone;

  int _syncOrder;
  int get syncOrder => _$this._syncOrder;
  set syncOrder(int syncOrder) => _$this._syncOrder = syncOrder;

  SyncPullBodyBuilder();

  SyncPullBodyBuilder get _$this {
    if (_$v != null) {
      _user = _$v.user?.toBuilder();
      _phone = _$v.phone?.toBuilder();
      _syncOrder = _$v.syncOrder;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncPullBody other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SyncPullBody;
  }

  @override
  void update(void Function(SyncPullBodyBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncPullBody build() {
    _$SyncPullBody _$result;
    try {
      _$result = _$v ??
          new _$SyncPullBody._(
              user: user.build(), phone: phone.build(), syncOrder: syncOrder);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'user';
        user.build();
        _$failedField = 'phone';
        phone.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'SyncPullBody', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
