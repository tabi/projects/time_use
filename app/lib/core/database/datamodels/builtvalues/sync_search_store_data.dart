import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:tbo_app/core/serializers.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_sync.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_search_store.dart';

part 'sync_search_store_data.g.dart';

abstract class SyncSearchStoreData implements Built<SyncSearchStoreData, SyncSearchStoreDataBuilder> {
  static Serializer<SyncSearchStoreData> get serializer => _$syncSearchStoreDataSerializer;

  SyncSync get synchronisation;
  SyncSearchStore get searchStore;

  factory SyncSearchStoreData([Function(SyncSearchStoreDataBuilder b) updates]) = _$SyncSearchStoreData;

  SyncSearchStoreData._();

  factory SyncSearchStoreData.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
