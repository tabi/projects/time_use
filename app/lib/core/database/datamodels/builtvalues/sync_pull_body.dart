import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:tbo_app/core/serializers.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_user.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_phone.dart';

part 'sync_pull_body.g.dart';

abstract class SyncPullBody implements Built<SyncPullBody, SyncPullBodyBuilder> {
  static Serializer<SyncPullBody> get serializer => _$syncPullBodySerializer;

  SyncUser get user;
  SyncPhone get phone;
  int get syncOrder;

  factory SyncPullBody([Function(SyncPullBodyBuilder b) updates]) = _$SyncPullBody;

  SyncPullBody._();

  factory SyncPullBody.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
