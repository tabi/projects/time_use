// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sync_register_data.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<SyncRegisterData> _$syncRegisterDataSerializer =
    new _$SyncRegisterDataSerializer();

class _$SyncRegisterDataSerializer
    implements StructuredSerializer<SyncRegisterData> {
  @override
  final Iterable<Type> types = const [SyncRegisterData, _$SyncRegisterData];
  @override
  final String wireName = 'SyncRegisterData';

  @override
  Iterable<Object> serialize(Serializers serializers, SyncRegisterData object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'user',
      serializers.serialize(object.user,
          specifiedType: const FullType(SyncUser)),
      'phone',
      serializers.serialize(object.phone,
          specifiedType: const FullType(SyncPhone)),
      'groupInfos',
      serializers.serialize(object.groupInfos,
          specifiedType:
              const FullType(BuiltList, const [const FullType(SyncGroupInfo)])),
    ];

    return result;
  }

  @override
  SyncRegisterData deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new SyncRegisterDataBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'user':
          result.user.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncUser)) as SyncUser);
          break;
        case 'phone':
          result.phone.replace(serializers.deserialize(value,
              specifiedType: const FullType(SyncPhone)) as SyncPhone);
          break;
        case 'groupInfos':
          result.groupInfos.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(SyncGroupInfo)]))
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$SyncRegisterData extends SyncRegisterData {
  @override
  final SyncUser user;
  @override
  final SyncPhone phone;
  @override
  final BuiltList<SyncGroupInfo> groupInfos;

  factory _$SyncRegisterData(
          [void Function(SyncRegisterDataBuilder) updates]) =>
      (new SyncRegisterDataBuilder()..update(updates)).build();

  _$SyncRegisterData._({this.user, this.phone, this.groupInfos}) : super._() {
    if (user == null) {
      throw new BuiltValueNullFieldError('SyncRegisterData', 'user');
    }
    if (phone == null) {
      throw new BuiltValueNullFieldError('SyncRegisterData', 'phone');
    }
    if (groupInfos == null) {
      throw new BuiltValueNullFieldError('SyncRegisterData', 'groupInfos');
    }
  }

  @override
  SyncRegisterData rebuild(void Function(SyncRegisterDataBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SyncRegisterDataBuilder toBuilder() =>
      new SyncRegisterDataBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SyncRegisterData &&
        user == other.user &&
        phone == other.phone &&
        groupInfos == other.groupInfos;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, user.hashCode), phone.hashCode), groupInfos.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SyncRegisterData')
          ..add('user', user)
          ..add('phone', phone)
          ..add('groupInfos', groupInfos))
        .toString();
  }
}

class SyncRegisterDataBuilder
    implements Builder<SyncRegisterData, SyncRegisterDataBuilder> {
  _$SyncRegisterData _$v;

  SyncUserBuilder _user;
  SyncUserBuilder get user => _$this._user ??= new SyncUserBuilder();
  set user(SyncUserBuilder user) => _$this._user = user;

  SyncPhoneBuilder _phone;
  SyncPhoneBuilder get phone => _$this._phone ??= new SyncPhoneBuilder();
  set phone(SyncPhoneBuilder phone) => _$this._phone = phone;

  ListBuilder<SyncGroupInfo> _groupInfos;
  ListBuilder<SyncGroupInfo> get groupInfos =>
      _$this._groupInfos ??= new ListBuilder<SyncGroupInfo>();
  set groupInfos(ListBuilder<SyncGroupInfo> groupInfos) =>
      _$this._groupInfos = groupInfos;

  SyncRegisterDataBuilder();

  SyncRegisterDataBuilder get _$this {
    if (_$v != null) {
      _user = _$v.user?.toBuilder();
      _phone = _$v.phone?.toBuilder();
      _groupInfos = _$v.groupInfos?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SyncRegisterData other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$SyncRegisterData;
  }

  @override
  void update(void Function(SyncRegisterDataBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SyncRegisterData build() {
    _$SyncRegisterData _$result;
    try {
      _$result = _$v ??
          new _$SyncRegisterData._(
              user: user.build(),
              phone: phone.build(),
              groupInfos: groupInfos.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'user';
        user.build();
        _$failedField = 'phone';
        phone.build();
        _$failedField = 'groupInfos';
        groupInfos.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'SyncRegisterData', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
