import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:tbo_app/core/serializers.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_sync.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_search_product.dart';

part 'sync_search_product_data.g.dart';

abstract class SyncSearchProductData implements Built<SyncSearchProductData, SyncSearchProductDataBuilder> {
  static Serializer<SyncSearchProductData> get serializer => _$syncSearchProductDataSerializer;

  SyncSync get synchronisation;
  SyncSearchProduct get searchProduct;

  factory SyncSearchProductData([Function(SyncSearchProductDataBuilder b) updates]) = _$SyncSearchProductData;

  SyncSearchProductData._();

  factory SyncSearchProductData.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
