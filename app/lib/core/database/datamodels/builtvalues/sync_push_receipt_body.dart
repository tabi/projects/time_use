import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:built_collection/built_collection.dart';
import 'package:tbo_app/core/serializers.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_user.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_phone.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_sync.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_transaction.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_product.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_image.dart';

part 'sync_push_receipt_body.g.dart';

abstract class SyncPushReceiptBody implements Built<SyncPushReceiptBody, SyncPushReceiptBodyBuilder> {
  static Serializer<SyncPushReceiptBody> get serializer => _$syncPushReceiptBodySerializer;

  SyncUser get user;
  SyncPhone get phone;
  int get syncOrder;
  SyncSync get synchronisation;
  SyncTransaction get transaction;
  BuiltList<SyncProduct> get products;
  SyncImage get image;

  factory SyncPushReceiptBody([Function(SyncPushReceiptBodyBuilder b) updates]) = _$SyncPushReceiptBody;

  SyncPushReceiptBody._();

  factory SyncPushReceiptBody.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
