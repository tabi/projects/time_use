// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_progress.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<UserProgress> _$userProgressSerializer =
    new _$UserProgressSerializer();

class _$UserProgressSerializer implements StructuredSerializer<UserProgress> {
  @override
  final Iterable<Type> types = const [UserProgress, _$UserProgress];
  @override
  final String wireName = 'UserProgress';

  @override
  Iterable<Object> serialize(Serializers serializers, UserProgress object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[];
    if (object.daysOfExperiment != null) {
      result
        ..add('daysOfExperiment')
        ..add(serializers.serialize(object.daysOfExperiment,
            specifiedType: const FullType(int)));
    }
    if (object.daysCompleted != null) {
      result
        ..add('daysCompleted')
        ..add(serializers.serialize(object.daysCompleted,
            specifiedType: const FullType(int)));
    }
    if (object.daysMissing != null) {
      result
        ..add('daysMissing')
        ..add(serializers.serialize(object.daysMissing,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  UserProgress deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new UserProgressBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'daysOfExperiment':
          result.daysOfExperiment = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'daysCompleted':
          result.daysCompleted = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'daysMissing':
          result.daysMissing = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$UserProgress extends UserProgress {
  @override
  final int daysOfExperiment;
  @override
  final int daysCompleted;
  @override
  final int daysMissing;

  factory _$UserProgress([void Function(UserProgressBuilder) updates]) =>
      (new UserProgressBuilder()..update(updates)).build();

  _$UserProgress._(
      {this.daysOfExperiment, this.daysCompleted, this.daysMissing})
      : super._();

  @override
  UserProgress rebuild(void Function(UserProgressBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UserProgressBuilder toBuilder() => new UserProgressBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UserProgress &&
        daysOfExperiment == other.daysOfExperiment &&
        daysCompleted == other.daysCompleted &&
        daysMissing == other.daysMissing;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, daysOfExperiment.hashCode), daysCompleted.hashCode),
        daysMissing.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UserProgress')
          ..add('daysOfExperiment', daysOfExperiment)
          ..add('daysCompleted', daysCompleted)
          ..add('daysMissing', daysMissing))
        .toString();
  }
}

class UserProgressBuilder
    implements Builder<UserProgress, UserProgressBuilder> {
  _$UserProgress _$v;

  int _daysOfExperiment;
  int get daysOfExperiment => _$this._daysOfExperiment;
  set daysOfExperiment(int daysOfExperiment) =>
      _$this._daysOfExperiment = daysOfExperiment;

  int _daysCompleted;
  int get daysCompleted => _$this._daysCompleted;
  set daysCompleted(int daysCompleted) => _$this._daysCompleted = daysCompleted;

  int _daysMissing;
  int get daysMissing => _$this._daysMissing;
  set daysMissing(int daysMissing) => _$this._daysMissing = daysMissing;

  UserProgressBuilder();

  UserProgressBuilder get _$this {
    if (_$v != null) {
      _daysOfExperiment = _$v.daysOfExperiment;
      _daysCompleted = _$v.daysCompleted;
      _daysMissing = _$v.daysMissing;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UserProgress other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$UserProgress;
  }

  @override
  void update(void Function(UserProgressBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UserProgress build() {
    final _$result = _$v ??
        new _$UserProgress._(
            daysOfExperiment: daysOfExperiment,
            daysCompleted: daysCompleted,
            daysMissing: daysMissing);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
