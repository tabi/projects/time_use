import 'dart:async';

import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:tbo_app/core/database/database_helper.dart';
import 'package:tbo_app/core/database/datamodels/progress_statistics.dart';
import 'package:tbo_app/core/database/datamodels/user_progress.dart';
import 'package:tbo_app/core/translate.dart';
import 'package:tuple/tuple.dart';

class UserProgressDatabase {
  Map<String, List<DateTime>> dayValues;

  Future<void> initializeData() async {
    final _database = await DatabaseHelper.instance.database;
    final _days = _createDaysOfExperiment();
    for (final _day in _days) {
      _database.insert('user_progress', {'daysOfExperiment': _day.toString()});
    }
    setInitializedToTrue();
  }

  Future<void> setInitializedToTrue() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setBool('initialized', true);
  }

  Future<bool> isInitialized() async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getBool('initialized') != null;
  }

  List<String> _createDaysOfExperiment() {
    final _days = List.generate(
      8,
      (index) => DateFormat('yyyyMMdd').format(
        DateTime.now().add(Duration(days: index)),
      ),
    );
    return _days;
  }

  Future<Map<String, List<DateTime>>> getCalendarFormatting() async {
    final daysOfExperiment = await getDaysOfExperiment();
    final daysCompleted = await getDaysCompleted();
    final daysMissing = await getDaysMissing();
    return {
      'daysOfExperiment': daysOfExperiment,
      'daysCompleted': daysCompleted,
      'daysMissing': daysMissing,
    };
  }

  Future<List<DateTime>> getDaysOfExperiment() async {
    final database = await DatabaseHelper.instance.database;

    final List<Map<String, dynamic>> rawResult =
        await database.rawQuery('SELECT * FROM user_progress');
    final result = rawResult.map((e) => UserProgress.fromJson(e)).toList();
    final List<DateTime> daysOfExperiment = [];
    for (final _userProgress in result) {
      if (_userProgress.daysOfExperiment != null) {
        daysOfExperiment
            .add(DateTime.parse(_userProgress.daysOfExperiment.toString()));
      }
    }
    return daysOfExperiment;
  }

  Future<List<DateTime>> getDaysCompleted() async {
    final database = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> rawResult =
        await database.rawQuery('SELECT * FROM user_progress');
    final result = rawResult.map((e) => UserProgress.fromJson(e));
    final List<DateTime> daysCompleted = [];
    for (final _userProgress in result) {
      if (_userProgress.daysCompleted != null) {
        daysCompleted
            .add(DateTime.parse(_userProgress.daysCompleted.toString()));
      }
    }
    return daysCompleted;
  }

  Future<List<DateTime>> getDaysMissing() async {
    final DateTime today = DateTime.now();
    final List<DateTime> daysOfExperiment = await getDaysOfExperiment();
    final List<DateTime> daysCompleted = await getDaysCompleted();
    final List<DateTime> daysMissing = [];
    for (final date in daysOfExperiment) {
      if (date.difference(today).inDays < 0) {
        if (daysCompleted.contains(date) == false) {
          daysMissing.add(date);
        }
      }
    }
    return daysMissing;
  }

  Future<ProgressStatistics> getProgressStats() async {
    final List<DateTime> daysCompleted = await getDaysCompleted();

    final List<DateTime> daysMissing = await getDaysMissing();

    final List<DateTime> daysOfExperiment = await getDaysOfExperiment();
    final DateTime lastDate = daysOfExperiment[daysOfExperiment.length - 1];
    final int difference = await isDayCompleted(DateTime.now().year.toString(),
            DateTime.now().month.toString(), DateTime.now().day.toString())
        ? 0
        : 1;

    return ProgressStatistics(
        completed: daysCompleted.length,
        missing: daysMissing.length,
        remaining: lastDate.difference(DateTime.now()).inDays + difference);
  }

  Future<Tuple2<String, String>> getStartAndEndDateExperiment() async {
    final List<DateTime> datesOfExperiment = await getDaysOfExperiment();
    final String startDate = DateFormat('d MMMM', translate.languagePreference)
        .format(datesOfExperiment[0]);
    final String endDate = DateFormat('d MMMM', translate.languagePreference)
        .format(datesOfExperiment[datesOfExperiment.length - 1]);

    return Tuple2(startDate, endDate);
  }

  Future<void> markDayComplete(String year, String month, String day) async {
    final Database database = await DatabaseHelper.instance.database;
    final bool isNew = await isDayCompleted(year, month, day) == false;
    if (isNew) {
      database.insert(
          'user_progress',
          UserProgress((b) =>
                  b..daysCompleted = int.parse(getDateString(year, month, day)))
              .toJson() as Map<String, dynamic>);
    }
  }

  Future<void> unmarkDayComplete(String year, String month, String day) async {
    final _database = await DatabaseHelper.instance.database;
    _database.delete('user_progress',
        where: 'daysCompleted = ?',
        whereArgs: [getDateString(year, month, day)]);
  }

  Future<bool> isDayCompleted(String year, String month, String day) async {
    final Database _database = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> dayComplete = await _database.rawQuery(
        'SELECT * FROM user_progress WHERE daysCompleted = ${getDateString(year, month, day)}');
    return dayComplete.isNotEmpty;
  }

  String getDateString(String year, String month, String day) {
    final _month = month.length == 1 ? '0$month' : month;
    final _day = day.length == 1 ? '0$day' : day;
    return '$year$_month$_day';
  }

  Future<String> canCompleteDay(
      String completeDay, String year, String month, String day) async {
    final dayIsCompleted = await isDayCompleted(year, month, day);

    if (dayIsCompleted) {
      return 'alreadyComplete';
    }

    final List<DateTime> daysOfExperiment = await getDaysOfExperiment();
    bool isInExperiment = false;
    for (final date in daysOfExperiment) {
      if (int.parse(DateFormat('yyyyMMdd').format(date)) ==
          int.parse(completeDay)) {
        isInExperiment = true;
      }
    }

    final int today = int.parse(DateFormat('yyyyMMdd').format(DateTime.now()));
    final bool isInPast = int.parse(completeDay) <= today;

    if (isInExperiment && isInPast) {
      return 'canComplete';
    }
    return 'cannotComplete';
  }
}
