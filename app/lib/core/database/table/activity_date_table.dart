import 'package:sqflite/sqflite.dart';
import 'package:tbo_app/core/database/agenda_database.dart';

class ActivityDateTable {
  static const tblActivityDate = 'tbl_ActivityDate';

  static const colId = 'id';
  static const colTreeCode = 'treeCode';
  static const colDate = 'date';
  static const colStatus = 'status';

  static const int statusOpen = 0;
  static const int statusClosed = 1;

  static Future create(Database db, int version) async {
    await db.execute('''
          CREATE TABLE $tblActivityDate (
            $colId INTEGER PRIMARY KEY,
            $colTreeCode TEXT NOT NULL,
            $colDate TEXT NOT NULL,
            $colStatus INTEGER NOT NULL
          )
          ''');
  }

  static Future<int> _insert(Map<String, dynamic> row) async {
    final Database db = await AgendaDatabase.instance.database;
    return db.insert(tblActivityDate, row);
  }

  static Future<List<Map<String, dynamic>>> _select(
      String treeCode, String date) async {
    final Database db = await AgendaDatabase.instance.database;
    return db.query(tblActivityDate,
        where: '$colTreeCode = ? AND $colDate = ?',
        whereArgs: [treeCode, date]);
  }

  static Future update(String treeCode, String date, int status) async {
    final Database db = await AgendaDatabase.instance.database;
    final Map<String, dynamic> data = <String, dynamic>{};
    data[colStatus] = status;
    await db.update(tblActivityDate, data,
        where: '$colTreeCode = ? AND $colDate = ?',
        whereArgs: [treeCode, date]);
  }

  static Future<int> insert(String treeCode, String date) async {
    final Map<String, dynamic> row = {
      colTreeCode: treeCode,
      colDate: date,
      colStatus: statusOpen
    };
    final id = await _insert(row);
    return id;
  }

  static Future<List<ActivityDateTable>> select(
      String treeCode, String date) async {
    final allRows = await _select(treeCode, date);
    final List<ActivityDateTable> actDates = <ActivityDateTable>[];
    for (final Map<String, dynamic> row in allRows) {
      actDates.add(ActivityDateTable.fromRow(row));
    }
    return actDates;
  }

  int id;
  String treeCode;
  String date;
  int status;

  ActivityDateTable(this.id, this.treeCode, this.date, this.status);

  ActivityDateTable.fromRow(Map<String, dynamic> row) {
    id = row[colId] as int;
    treeCode = row[colTreeCode] as String;
    date = row[colDate] as String;
    status = row[colStatus] as int;
  }

  Map<String, dynamic> toRow() {
    return {colId: id, colTreeCode: treeCode, colDate: date, colStatus: status};
  }
}
