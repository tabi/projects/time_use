import 'package:built_collection/built_collection.dart';
import 'package:sqflite/sqflite.dart';
import 'package:tbo_app/activities/activity.dart';
import 'package:tbo_app/activities/activity_companion.dart';
import 'package:tbo_app/activities/activity_location.dart';
import 'package:tbo_app/activities/activity_node.dart';
import 'package:tbo_app/activities/activity_type.dart';
import 'package:tbo_app/activities/medium.dart';
import 'package:tbo_app/core/database/agenda_database.dart';

import '../../../activities/activity_companion.dart';

class ActivityInfoTable {
  //### sql database interface ##########################################################################

  static const int _mainActivityType = 1;

  static const tblActivityInfo = 'tbl_ActivityInfo';

  static const colTreeCode = 'treeCode';
  static const colDateId = 'dateId';
  static const colActivityType = 'activityType';
  static const colNodeCode = 'nodeCode';
  static const colDescription = 'description';
  static const colTick = 'tick';
  static const colHeight = 'height';
  static const colStartHour = 'startHour';
  static const colStartMinute = 'startMinute';
  static const colStopHour = 'stopHour';
  static const colStopMinute = 'stopMinute';
  static const colMedium = 'medium';
  static const colLocation = 'location';

  static Future create(Database db, int version) async {
    await db.execute('''
          CREATE TABLE $tblActivityInfo (
            $colTreeCode TEXT NOT NULL,
            $colDateId INTEGER NOT NULL,
            $colActivityType INTEGER NOT NULL,
            $colNodeCode TEXT NOT NULL,
            $colDescription TEXT NOT NULL,
            $colTick INTEGER NOT NULL,
            $colHeight INTEGER NOT NULL,
            $colStartHour INTEGER NOT NULL,
            $colStartMinute INTEGER NOT NULL,
            $colStopHour INTEGER NOT NULL,
            $colStopMinute INTEGER NOT NULL,
            $colMedium TEXT NOT NULL,
            $colLocation TEXT NOT NULL
          )
          ''');
  }

  static Future<List<Map<String, dynamic>>> _select(
      String treeCode, int dateId, int activityType) async {
    final Database db = await AgendaDatabase.instance.database;
    return db.query(tblActivityInfo,
        where: '$colTreeCode = ? AND $colDateId = ? AND $colActivityType = ?',
        whereArgs: [treeCode, dateId, activityType],
        orderBy: colTick);
  }

  static Future<List<Map<String, dynamic>>> _selectAll(String treeCode) async {
    final Database db = await AgendaDatabase.instance.database;
    return db.query(tblActivityInfo,
        where: '$colTreeCode = ?', whereArgs: [treeCode]);
  }

  static Future<int> _delete(
      String treeCode, int dateId, int activityType) async {
    final db = await AgendaDatabase.instance.database;
    return db.delete(tblActivityInfo,
        where: '$colTreeCode = ? AND $colDateId = ? AND $colActivityType = ?',
        whereArgs: [treeCode, dateId, activityType]);
  }

  static Future<int> _insert(Map<String, dynamic> row) async {
    final db = await AgendaDatabase.instance.database;
    return db.insert(tblActivityInfo, row);
  }

  //### class interface ##########################################################################

  String treeCode;
  int dateId;
  int activityType;
  String nodeCode;
  String description;
  int tick;
  int height;
  int startHour;
  int startMinute;
  int stopHour;
  int stopMinute;
  Medium medium;
  ActivityLocation location;

  ActivityInfoTable(
    this.treeCode,
    this.dateId,
    this.activityType,
    this.nodeCode,
    this.description,
    this.tick,
    this.height,
    this.startHour,
    this.startMinute,
    this.stopHour,
    this.stopMinute, {
    this.medium,
    this.location,
  });

  ActivityInfoTable.fromRow(Map<String, dynamic> row) {
    treeCode = row[colTreeCode] as String;
    dateId = row[colDateId] as int;
    activityType = row[colActivityType] as int;
    nodeCode = row[colNodeCode] as String;
    description = row[colDescription] as String;
    tick = row[colTick] as int;
    height = row[colHeight] as int;
    startHour = row[colStartHour] as int;
    startMinute = row[colStartMinute] as int;
    stopHour = row[colStopHour] as int;
    stopMinute = row[colStopMinute] as int;
    medium = ((row[colMedium] as String)?.isNotEmpty ?? false)
        ? Medium.valueOf(row[colMedium] as String)
        : null;
    location = ((row[colLocation] as String)?.isNotEmpty ?? false)
        ? ActivityLocation.valueOf(row[colLocation] as String)
        : null;
  }

  Map<String, dynamic> toRow() {
    return {
      colTreeCode: treeCode,
      colDateId: dateId,
      colActivityType: activityType,
      colNodeCode: nodeCode,
      colDescription: description,
      colTick: tick,
      colHeight: height,
      colStartHour: startHour,
      colStartMinute: startMinute,
      colStopHour: stopHour,
      colStopMinute: stopMinute,
      colMedium: medium?.name ?? '',
      colLocation: location?.name ?? '',
    };
  }

  static Future<List<ActivityInfoTable>> select(
      String treeCode, int dateId, int activityType) async {
    final allRows = await _select(treeCode, dateId, activityType);
    final actInfos = <ActivityInfoTable>[];
    for (final Map<String, dynamic> row in allRows) {
      actInfos.add(ActivityInfoTable.fromRow(row));
    }
    return actInfos;
  }

  static Future<List<ActivityInfoTable>> selectAll(String treeCode) async {
    final allRows = await _selectAll(treeCode);
    final actInfos = <ActivityInfoTable>[];
    for (final Map<String, dynamic> row in allRows) {
      actInfos.add(ActivityInfoTable.fromRow(row));
    }
    return actInfos;
  }

  static Future<int> delete(
      String treeCode, int dateId, int activityType) async {
    final rowsDeleted = await _delete(treeCode, dateId, activityType);
    return rowsDeleted;
  }

  static Future<int> insert(List<ActivityInfoTable> actInfos) async {
    int count = 0;
    for (final info in actInfos) {
      await _insert(info.toRow());
      count++;
    }
    return count;
  }

  //############################################################################
  Activity createActivity(ActivityNode activityNode,
      {ListBuilder<ActivityCompanion> activityCompanion}) {
    return Activity((b) => b
      ..type = activityType == _mainActivityType
          ? ActivityType.main
          : ActivityType.side
      ..activityNode = activityNode.toBuilder()
      ..activity = description
      ..startDate = DateTime.fromMillisecondsSinceEpoch(startHour, isUtc: true)
      ..endDate = DateTime.fromMillisecondsSinceEpoch(stopHour, isUtc: true)
      ..companion = activityCompanion
      ..medium = medium
      ..location = location);
  }

  Activity activityPlusProperties(ActivityNode activityNode,
      ListBuilder<ActivityCompanion> activityCompanion) {
    return createActivity(activityNode)
        .rebuild((b) => b.companion = activityCompanion);
  }
}
