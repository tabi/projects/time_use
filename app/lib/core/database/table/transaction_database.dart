import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';
import 'package:tbo_app/core/database/database_helper.dart';
import 'package:tbo_app/core/database/datamodels/chart_data.dart';
import 'package:tbo_app/core/translate.dart';

class Transaction {
  String date = '';
  String discountAmount = '';
  String discountPercentage = '';
  String discountText = '';
  String expenseAbroad = '';
  String expenseOnline = '';
  String price = '';
  String product = '';
  String productCategory = '';
  String productCode = '';
  String productDate = '';
  String productGroupID = '';
  String receiptLocation = '';
  String receiptProductType = '';
  String store = '';
  String storeType = '';
  String totalPrice = '';
  String transactionID = '';
}

class TransactionDatabase {
  static const String _tableProduct = 'products';
  static const String _tableProductBackup = 'product_backup';
  static const String _tableTransaction = 'transactions';
  static const String _tableTransactionBackup = 'transactions_backup';
  static const String _tableUserProgress = 'user_progress';

  Future<void> createTables(Database db) async {
    await db.execute('''
          CREATE TABLE $_tableTransaction (
            transactionID TEXT,
            store TEXT,
            storeType TEXT,
            date int,
            discountAmount TEXT,
            discountPercentage TEXT,
            expenseAbroad TEXT,
            expenseOnline TEXT,
            totalPrice float,
            discountText TEXT,
            receiptLocation TEXT,
            receiptProductType TEXT)
          ''');

    await db.execute('''
          CREATE TABLE $_tableTransactionBackup (
            transactionID TEXT,
            store TEXT,
            storeType TEXT,
            date int,
            discountAmount TEXT,
            discountPercentage TEXT,
            expenseAbroad TEXT,
            expenseOnline TEXT,
            totalPrice float,
            discountText TEXT,
            receiptLocation TEXT,
            receiptProductType TEXT)
          ''');

    await db.execute('''
          CREATE TABLE $_tableProduct (
            transactionID TEXT,
            product TEXT,
            productCategory TEXT,
            price float,
            productCode TEXT,
            productDate TEXT,
            productGroupID TEXT)
          ''');

    await db.execute('''
          CREATE TABLE $_tableProductBackup (
            transactionID TEXT,
            product TEXT,
            productCategory TEXT,
            price float,
            productCode TEXT,
            productDate TEXT,
            productGroupID TEXT)
          ''');

    await db.execute('''
          CREATE TABLE $_tableUserProgress (
            daysOfExperiment int,
            daysCompleted int,
            daysMissing int)
          ''');
  }

  Future<void> insertTransaction(Transaction trx) async {
    final _database = await DatabaseHelper.instance.database;
    final _data = <String, dynamic>{};

    _data['transactionID'] = trx.transactionID;
    _data['store'] = trx.store;
    _data['storeType'] = trx.storeType;
    _data['date'] = trx.date.replaceAll('-', '');
    _data['discountAmount'] = trx.discountAmount;
    _data['discountPercentage'] = trx.discountPercentage;
    _data['discountText'] = trx.discountText;
    _data['expenseAbroad'] = trx.expenseAbroad;
    _data['expenseOnline'] = trx.expenseOnline;
    _data['totalPrice'] = trx.totalPrice;
    _data['receiptLocation'] = trx.receiptLocation;
    _data['receiptProductType'] = trx.receiptProductType;

    await _database.insert(_tableTransaction, _data);
    final _chartData = ChartData();
    _chartData.reset();
  }

  Future<void> insertProduct(Transaction trx) async {
    final _database = await DatabaseHelper.instance.database;
    final _data = <String, dynamic>{};

    _data['transactionID'] = trx.transactionID;
    _data['product'] = trx.product;
    _data['productCategory'] = trx.productCategory;
    _data['price'] = trx.price;
    _data['productCode'] = trx.productCode;
    _data['productDate'] = trx.productDate.replaceAll('-', '');
    _data['productGroupID'] = trx.productGroupID;

    await _database.insert(_tableProduct, _data);
  }

  Future<List<Map<String, dynamic>>> queryTransaction(
      String transactionID) async {
    final db = await DatabaseHelper.instance.database;
    return db.query(
      _tableTransaction,
      where: 'transactionID = ?',
      whereArgs: [transactionID],
    );
  }

  Future<List<Map<String, dynamic>>> queryTransactionProducts(
      String transactionID) async {
    final db = await DatabaseHelper.instance.database;
    return db.query(
      _tableProduct,
      where: 'transactionID = ?',
      whereArgs: [transactionID],
    );
  }

  Future createTransactionBackup(String transactionID) async {
    final db = await DatabaseHelper.instance.database;

    final List<Map<String, dynamic>> resultTransaction = await db.rawQuery(
        "SELECT * FROM $_tableTransaction WHERE transactionID = '$transactionID'");
    final List<Map<String, dynamic>> resultProducts = await db.rawQuery(
        "SELECT * FROM $_tableProduct WHERE transactionID = '$transactionID'");

    for (final Map<String, dynamic> row in resultTransaction) {
      await db.insert(_tableTransactionBackup, row);
    }

    for (final Map<String, dynamic> row in resultProducts) {
      await db.insert(_tableProductBackup, row);
    }
  }

  Future restoreBackUp(String transactionID) async {
    final db = await DatabaseHelper.instance.database;

    final List<Map<String, dynamic>> resultTransaction = await db.rawQuery(
        "SELECT * FROM $_tableTransactionBackup  WHERE transactionID = '$transactionID'");
    final List<Map<String, dynamic>> resultProducts = await db.rawQuery(
        "SELECT * FROM $_tableProductBackup  WHERE transactionID = '$transactionID'");

    for (final Map<String, dynamic> row in resultTransaction) {
      await db.insert(_tableTransaction, row);
    }

    for (final Map<String, dynamic> row in resultProducts) {
      await db.insert(_tableProduct, row);
    }

    await db.delete(_tableTransactionBackup,
        where: 'transactionID = ?', whereArgs: [transactionID]);

    await db.delete(_tableProductBackup,
        where: 'transactionID = ?', whereArgs: [transactionID]);
  }

  Future deleteBackUp() async {
    final db = await DatabaseHelper.instance.database;
    await db.delete(_tableProductBackup);
    await db.delete(_tableTransactionBackup);
  }

  Future deleteTransaction(String transactionID) async {
    final db = await DatabaseHelper.instance.database;
    await db.delete(_tableProduct,
        where: 'transactionID = ?', whereArgs: [transactionID]);
    await db.delete(_tableTransaction,
        where: 'transactionID = ?', whereArgs: [transactionID]);
    final _chartData = ChartData();
    _chartData.reset();
  }

  Future deleteTransactionWithBackup(String transactionID) async {
    await createTransactionBackup(transactionID);
    await deleteTransaction(transactionID);
  }

  Future<Map<String, dynamic>> getFirstAndLastDate() async {
    final db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> results =
        await db.query(_tableTransaction, orderBy: 'date', columns: ['date']);
    String first;
    String last;

    if (results.isNotEmpty) {
      first = results[0]['date'].toString();
      last = results[results.length - 1]['date'].toString();
    } else {
      first = DateTime.now().toString();
      last = DateTime.now().add(const Duration(days: 30)).toString();

      first = DateFormat('yyyy-MM-dd').format(DateTime.parse(first));
      last = DateFormat('yyyy-MM-dd').format(DateTime.parse(last));
    }
    return {'first': first, 'last': last};
  }

  Future<Map<String, dynamic>> getLowestAndHighestValue() async {
    final db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> results = await db.query(_tableTransaction,
        orderBy: 'totalPrice', columns: ['totalPrice']);

    String lowest;
    String highest;
    if (results.isNotEmpty) {
      lowest = results[0]['totalPrice'].toString();
      lowest = double.parse(lowest).toStringAsFixed(0);
      highest = results[results.length - 1]['totalPrice'].toString();
      highest = double.parse(highest).toStringAsFixed(0);
    } else {
      lowest = '0';
      highest = '0';
    }
    return {'lowest': lowest, 'highest': highest};
  }

  Future<String> getProductCode(String coicop) async {
    final db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> code = await db.query(
        'tblProduct' + '_' + translate.key,
        where: 'Coicop = ?',
        whereArgs: [coicop],
        columns: ['code']);

    // print(code);

    if (code != null) {
      if (code.isNotEmpty) {
        return code[0]['code'] as String;
      } else {
        //Coicop code is not seen in product table, therefore look it up in the coicop table:
        //mogelijk gaat hier iets verkeerd icm taal veranderen
        final List<Map<String, dynamic>> codeCoicop = await db.query(
            'tblCoicop' + '_' + translate.key,
            where: 'Coicop = ?',
            whereArgs: [coicop],
            columns: ['code']);
        return codeCoicop[0]['code'] as String;
      }
    } else {
      return '';
    }
  }

  Future<List<Map<String, dynamic>>> queryDailyTransactions(
      BuildContext context, String year, String month, String day) async {
    final _month = month.length == 1 ? '0$month' : month;
    final _day = day.length == 1 ? '0$day' : day;
    final db = await DatabaseHelper.instance.database;
    final query =
        'SELECT  * FROM $_tableTransaction WHERE date = $year$_month$_day';
    return db.rawQuery(query);
  }

  Future<String> lookupMainCategoryReceiptType(String lookupValue) async {
    final db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> categoryName = await db.rawQuery(
        "SELECT ${translate.key.toString()} FROM tblTranslations WHERE code = '$lookupValue'");
    final List<Map<String, dynamic>> receiptName = await db.rawQuery(
        "SELECT ${translate.key.toString()} FROM tblTranslations WHERE code = '00'");

    return '${receiptName[0][translate.key]} ${categoryName[0][translate.key]}';
  }
}
