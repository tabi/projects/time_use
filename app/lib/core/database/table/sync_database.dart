import 'dart:async';

import 'package:sqflite/sqflite.dart';

import 'package:tbo_app/core/database/database_helper.dart';

import 'package:tbo_app/core/database/datamodels/builtvalues/sync_sync.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_id.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_product.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_search_product.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_search_store.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_transaction.dart';

const created = 0;
const updated = 1;
const deleted = 2;

const dataGlobalType = 0;
const dataReceiptType = 1;
const dataProductType = 2;
const dataStoreType = 3;

class SyncDatabase {
  static const String tableSyncSync = 'tbl_sync_sync';
  static const String tableSyncId = 'tbl_sync_id';
  static const String tableSyncOrder = 'tbl_sync_order';

  Future<void> createTables(Database db) async {
    await db.execute('''
          CREATE TABLE $tableSyncSync (
            dataType int,
            dataIdentifier TEXT,
            syncOrder int,
            syncTime int,
            action int)
          ''');

    await db.execute('''
          CREATE TABLE $tableSyncId (
            userName TEXT,
            userPassword TEXT,
            phoneName TEXT)
          ''');

    await db.execute('''
          CREATE TABLE $tableSyncOrder (
            dataType int,
            syncOrder int)
          ''');

    await initSyncId();
    await initSyncOrder(dataGlobalType);
    await initSyncOrder(dataReceiptType);
    await initSyncOrder(dataProductType);
    await initSyncOrder(dataStoreType);
  }

  Future<void> initSyncId() async {
    final _database = await DatabaseHelper.instance.database;
    await _database.insert(
      tableSyncId,
      SyncId((b) => b
        ..userName = ''
        ..userPassword = ''
        ..phoneName = '').toJson(),
    );
  }

  Future<void> updateSyncId(SyncId syncId) async {
    final _database = await DatabaseHelper.instance.database;
    await _database.update(tableSyncId, syncId.toJson());
  }

  Future<SyncId> getSyncId() async {
    var syncId = SyncId();
    final _database = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> results =
        await _database.query(tableSyncId);
    if (results.isNotEmpty) {
      syncId = SyncId.fromJson(results.first);
    }
    return syncId;
  }

//####################################################################

  Future<void> initSyncOrder(int dataType) async {
    final _database = await DatabaseHelper.instance.database;
    final data = <String, dynamic>{};
    data['dataType'] = dataType;
    data['syncOrder'] = 0;
    await _database.insert(tableSyncOrder, data);
  }

  Future<void> updateSyncOrder(int dataType, int syncOrder) async {
    final _database = await DatabaseHelper.instance.database;
    final data = <String, dynamic>{};
    data['syncOrder'] = syncOrder;
    await _database.update(tableSyncOrder, data,
        where: 'dataType = ?', whereArgs: [dataType]);
  }

  Future<int> getSyncOrder(int dataType) async {
    int syncOrder = 0;
    final _database = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> results = await _database
        .query(tableSyncOrder, where: 'dataType = ?', whereArgs: [dataType]);
    if (results.isNotEmpty) {
      syncOrder = results[0]['syncOrder'] as int;
    }
    return syncOrder;
  }

  Future<int> getAndSetNextSyncOrder() async {
    int syncOrder = await getSyncOrder(dataGlobalType);
    syncOrder = syncOrder + 1;
    await updateSyncOrder(dataGlobalType, syncOrder);
    return syncOrder;
  }

//####################################################################

  Future<void> createSyncSync(
      int dataType, String dataIdentifier, int action) async {
    final _database = await DatabaseHelper.instance.database;
    final int syncOrder = await getAndSetNextSyncOrder();

    await _database.insert(
        tableSyncSync,
        SyncSync((b) => b
          ..dataType = dataType
          ..dataIdentifier = dataIdentifier
          ..syncOrder = syncOrder
          ..syncTime = DateTime.now().millisecondsSinceEpoch
          ..action = action).toJson());
  }

  Future<void> createImportedSyncSync(SyncSync syncSync) async {
    final _database = await DatabaseHelper.instance.database;
    await _database.insert(
        tableSyncSync, syncSync.rebuild((b) => b.syncOrder = 0).toJson());
  }

  Future<void> updateSyncSync(
      int dataType, String dataIdentifier, int action) async {
    final _database = await DatabaseHelper.instance.database;
    final data = <String, dynamic>{};
    data['syncOrder'] = await getAndSetNextSyncOrder();
    data['syncTime'] = DateTime.now().millisecondsSinceEpoch;
    data['action'] = action;
    await _database.update(tableSyncSync, data,
        where: 'dataType = ? AND dataIdentifier = ?',
        whereArgs: [dataType, dataIdentifier]);
  }

  Future<void> updateImportedSyncSync(SyncSync syncSync) async {
    final _database = await DatabaseHelper.instance.database;
    await _database.update(
        tableSyncSync, syncSync.rebuild((b) => b..syncOrder = 0).toJson(),
        where: 'dataType = ? AND dataIdentifier = ?',
        whereArgs: [syncSync.dataType, syncSync.dataIdentifier]);
  }

  Future<SyncSync> getNextSyncSyncToPush(int localPushed) async {
    var syncSync = SyncSync.newInstance();
    final _database = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> results = await _database.query(tableSyncSync,
        where: 'syncOrder > ?',
        whereArgs: [localPushed],
        orderBy: 'syncOrder');

    if (results.isNotEmpty) {
      syncSync = SyncSync.fromJson(results.first);
    }

    return syncSync;
  }

  Future<SyncSync> getSyncSync(int dataType, String dataIdentifier) async {
    var syncSync = SyncSync.newInstance();
    final _database = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> results = await _database.query(tableSyncSync,
        where: 'dataType = ? AND dataIdentifier = ?',
        whereArgs: [dataType, dataIdentifier]);
    if (results.isNotEmpty) {
      syncSync = SyncSync.fromJson(results.first);
    }
    return syncSync;
  }

//####################################################################

  Future<void> insertTransaction(SyncTransaction syncTransaction) async {
    final _database = await DatabaseHelper.instance.database;
    await _database.insert('transactions', syncTransaction.toJson());
  }

  Future<void> insertProduct(SyncProduct syncProduct) async {
    final _database = await DatabaseHelper.instance.database;
    await _database.insert('products', syncProduct.toJson());
  }

  Future<void> insertSearchProduct(SyncSearchProduct syncSearchProduct) async {
    final _database = await DatabaseHelper.instance.database;
    await _database.insert(
        'search_suggestions_products', syncSearchProduct.toJson());
  }

  Future<void> deleteSearchProduct(String productCode) async {
    final _database = await DatabaseHelper.instance.database;
    await _database.delete('search_suggestions_products',
        where: 'productCode = ?', whereArgs: [productCode]);
  }

  Future<void> insertSearchStore(SyncSearchStore syncSearchStore) async {
    final _database = await DatabaseHelper.instance.database;
    final data = <String, dynamic>{};

    data['storeName'] = syncSearchStore.storeName;
    data['storeType'] = syncSearchStore.storeType;
    data['lastAdded'] = syncSearchStore.lastAdded;
    data['count'] = syncSearchStore.count;

    await _database.insert('search_suggestions_stores', data);
  }

  Future<void> deleteSearchStore(String storeName) async {
    final _database = await DatabaseHelper.instance.database;
    await _database.delete('search_suggestions_stores',
        where: 'storeName = ?', whereArgs: [storeName]);
  }
}
