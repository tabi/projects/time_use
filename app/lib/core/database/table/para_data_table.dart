import 'package:sqflite/sqflite.dart';
import 'package:tbo_app/para_data/para_data.dart';

class ParaDataTable {
  final Database database;

  const ParaDataTable(this.database);

  static const paraDataTableName = 'paraData';

  Future<void> initialize() async {
    await database.execute('''
          CREATE TABLE $paraDataTableName (
            timestamp INT,
            objectName TEXT,
            action TEXT)
          ''');
  }

  Future<void> insert(ParaData data) async {
    await database.insert(paraDataTableName, data.toJson());
  }
}
