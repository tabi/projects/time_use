import '../database_helper.dart';

class TranslationTable {
  //### sql database interface ##########################################################################

  static const tblTranslation = 'tbl_Translation';

  static const colLanguage = 'language';
  static const colPageOrder = 'pageOrder';
  static const colPageKey = 'pageKey';
  static const colItemOrder = 'itemOrder';
  static const colItemKey = 'itemKey';
  static const colTranslation = 'translation';

  static Future<List<Map<String, dynamic>>> _select() async {
    final db = await DatabaseHelper.instance.database;
    return db.query(tblTranslation,
        orderBy:
            '$colLanguage AND $colPageOrder AND $colPageKey AND $colItemOrder AND $colItemKey');
  }

  //### class interface ##########################################################################

  String language;
  int pageOrder;
  String pageKey;
  int itemOrder;
  String itemKey;
  String translation;

  TranslationTable(this.language, this.pageOrder, this.pageKey, this.itemOrder,
      this.itemKey, this.translation);

  TranslationTable.fromRow(Map<String, dynamic> row) {
    language = row[colLanguage] as String;
    pageOrder = row[colPageOrder] as int;
    pageKey = row[colPageKey] as String;
    itemOrder = row[colItemOrder] as int;
    itemKey = row[colItemKey] as String;
    translation = row[colTranslation] as String;
  }

  Map<String, dynamic> toRow() {
    return {
      colLanguage: language,
      colPageOrder: pageOrder,
      colPageKey: pageKey,
      colItemOrder: itemOrder,
      colItemKey: itemKey,
      colTranslation: translation
    };
  }

  static Future<List<TranslationTable>> select() async {
    final rows = await _select();
    final List<TranslationTable> result = <TranslationTable>[];
    for (final Map<String, dynamic> row in rows) {
      result.add(TranslationTable.fromRow(row));
    }
    return result;
  }

  //############################################################################

}
