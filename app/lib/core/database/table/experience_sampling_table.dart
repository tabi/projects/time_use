import 'package:sqflite/sqflite.dart';

import '../database_helper.dart';

class ExperienceSamplingDb {
  static const String esResults = 'tbl_experience_sampling_results';
  static const String esTiming = 'tbl_experience_sampling_timing';

  Future<void> createTables(Database db) async {
    await db.execute('''
          CREATE TABLE $esResults (
            sendTime String,
            readTime String,
            isFeelingQuestionair INT,
            isFirstAttempt INT,            
            happy INT,
            energetic INT,
            relaxed INT,
            cheerful INT,
            socialMedia INT,
            texting INT,
            games INT,
            newsOnline INT,
            readingPaper INT           
            )
          ''');

    await db.execute('''
          CREATE TABLE $esTiming (
            id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
            date String,
            unixTime INT,
            questionairType TEXT,
            notificationType TEXT
            )
          ''');
  }

  static Future<void> addEsTiming(String date, int unixTime,
      String questionairType, String notificationType) async {
    final Database db = await DatabaseHelper.instance.database;
    db.insert(esTiming, {
      'date': date,
      'unixTime': unixTime,
      'questionairType': questionairType,
      'notificationType': notificationType,
    });
  }

  static Future<void> removeEsTimings() async {
    final Database db = await DatabaseHelper.instance.database;
    db.delete(esTiming);
    db.rawDelete(
        "DELETE FROM sqlite_sequence where name='$esTiming'"); //resets table index
  }

  static Future<List<Map<String, dynamic>>> getEsTimings() async {
    final Database db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> result = await db.query(esTiming);
     return result;
  }

  static Future<void> addQuestionairResult({
    String sendTime,
    String readTime,
    int isFirstAttempt,
    int isFeelingQuestionair,
    int happy,
    int energetic,
    int relaxed,
    int cheerful,
    int socialMedia,
    int texting,
    int games,
    int newsOnline,
    int readingPaper,
  }) async {
    final Database db = await DatabaseHelper.instance.database;
    db.insert(esResults, {
      'sendTime': sendTime,
      'readTime': readTime,
      'isFirstAttempt': isFirstAttempt,
      'isFeelingQuestionair': isFeelingQuestionair,
      'happy': happy,
      'energetic': energetic,
      'relaxed': relaxed,
      'cheerful': cheerful,
      'socialMedia': socialMedia,
      'texting': texting,
      'games': games,
      'newsOnline': newsOnline,
      'readingPaper': readingPaper,
    });
  }

  static Future<List<Map<String, dynamic>>> getQuestionairResults() async {
    final Database db = await DatabaseHelper.instance.database;
    final List<Map<String, dynamic>> result = await db.query(esResults);
    return result;
  }
}
