import 'dart:convert';

import 'package:tbo_app/core/input_type.dart';

import '../database_helper.dart';

class ActivitiesTable {
  //### sql database interface ##########################################################################

  static const tblActivities = 'tbl_Activities';

  static const colTreeCode = 'treeCode';
  static const colNodeCode = 'nodeCode';
  static const colNodeType = 'nodeType';
  static const colIconCode = 'iconCode';
  static const colColorCode = 'colorCode';
  static const colActivityCode = 'activityCode';
  static const colActivity = 'activity';
  static const colDescription = 'description';
  static const colParentId = 'parentId';
  static const colChildIds = 'childIds';

  static Future<List<Map<String, dynamic>>> _select(
      String treeCode, InputType inputType) async {
    final db = await DatabaseHelper.instance.database;

    if (inputType == InputType.closed) {
      return db.query(tblActivities,
          where: '$colTreeCode = ? AND $colNodeType <> ?',
          whereArgs: [treeCode, 'search'],
          orderBy: colNodeCode);
    } else {
      return db.query(tblActivities,
          where: '$colTreeCode = ?',
          whereArgs: [treeCode],
          orderBy: colNodeCode);
    }
  }

  static Future<int> _delete(String treeCode) async {
    final db = await DatabaseHelper.instance.database;
    return db.delete(tblActivities,
        where: '$colTreeCode = ?', whereArgs: [treeCode]);
  }

  static Future<int> _insert(Map<String, dynamic> row) async {
    final db = await DatabaseHelper.instance.database;
    return db.insert(tblActivities, row);
  }

  static Future<List<Map<String, dynamic>>> _selectAllTreeCodes() async {
    final db = await DatabaseHelper.instance.database;
    return db.rawQuery(
        'SELECT $colTreeCode FROM $tblActivities ORDER BY $colTreeCode');
  }

  //### class interface ##########################################################################

  String treeCode;
  String nodeCode;
  String nodeType;
  String iconCode;
  String colorCode;
  String activityCode;
  String activity;
  String description;
  int parentId;
  List<int> childIds;

  ActivitiesTable(this.treeCode, this.nodeCode, this.nodeType, this.iconCode,
      this.colorCode, this.activityCode, this.activity, this.description);

  ActivitiesTable.fromRow(Map<String, dynamic> row) {
    treeCode = row[colTreeCode] as String;
    nodeCode = row[colNodeCode] as String;
    nodeType = row[colNodeType] as String;
    iconCode = row[colIconCode] as String;
    colorCode = row[colColorCode] as String;
    activityCode = row[colActivityCode] as String;
    activity = row[colActivity] as String;
    description = row[colDescription] as String;
    if (row[colParentId] != null) {
      parentId = row[colParentId] as int;
    }
    if ((row[colChildIds] as String)?.isNotEmpty ?? false) {
      childIds = List<int>.from(
          json.decode(row[colChildIds] as String) as List<dynamic>);
    } else {
      childIds = <int>[];
    }
  }

  Map<String, dynamic> toRow() {
    return {
      colTreeCode: treeCode,
      colNodeCode: nodeCode,
      colNodeType: nodeType,
      colIconCode: iconCode,
      colColorCode: colorCode,
      colActivityCode: activityCode,
      colActivity: activity,
      colDescription: description,
      colParentId: parentId,
      colChildIds: childIds
    };
  }

  static Future<List<ActivitiesTable>> select(
      String treeCode, InputType inputType) async {
    final rows = await _select(treeCode, inputType);
    final List<ActivitiesTable> result = <ActivitiesTable>[];
    for (final Map<String, dynamic> row in rows) {
      result.add(ActivitiesTable.fromRow(row));
    }
    return result;
  }

  static Future<int> delete(String treeCode) async {
    final rowsDeleted = await _delete(treeCode);
    return rowsDeleted;
  }

  static Future<int> insert(List<ActivitiesTable> acts) async {
    int count = 0;
    for (final ActivitiesTable act in acts) {
      await _insert(act.toRow());
      count++;
    }
    return count;
  }

  static Future<List<String>> selectAllTreeCodes() async {
    final rows = await _selectAllTreeCodes();
    final List<String> result = <String>[];
    for (final Map<String, dynamic> row in rows) {
      result.add(row[colTreeCode] as String);
    }
    return result;
  }

  //############################################################################

}
