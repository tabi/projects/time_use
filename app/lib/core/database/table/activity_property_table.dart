import 'package:sqflite/sqflite.dart';
import 'package:tbo_app/core/database/agenda_database.dart';

class ActivityPropertyTable {
  //### sql database interface ##########################################################################

  static const tblActivityProperty = 'tbl_ActivityProperty';

  static const colTreeCode = 'treeCode';
  static const colDateId = 'dateId';
  static const colActivityType = 'activityType';
  static const colStartHour = 'startHour';
  static const colStartMinute = 'startMinute';
  static const colKey = 'key';
  static const colValue = 'value';

  static Future create(Database db, int version) async {
    await db.execute('''
          CREATE TABLE $tblActivityProperty (
            $colTreeCode TEXT NOT NULL,
            $colDateId INTEGER NOT NULL,
            $colActivityType INTEGER NOT NULL,
            $colStartHour INTEGER NOT NULL,
            $colStartMinute INTEGER NOT NULL,
            $colKey TEXT NOT NULL,
            $colValue TEXT NOT NULL
          )
          ''');
  }

  static Future<List<Map<String, dynamic>>> _select(
      String treeCode, int dateId, int activityType) async {
    final Database db = await AgendaDatabase.instance.database;
    return db.query(tblActivityProperty,
        where: '$colTreeCode = ? AND $colDateId = ? AND $colActivityType = ?',
        whereArgs: [treeCode, dateId, activityType]);
  }

  static Future<int> _delete(
      String treeCode, int dateId, int activityType) async {
    final db = await AgendaDatabase.instance.database;
    return db.delete(tblActivityProperty,
        where: '$colTreeCode = ? AND $colDateId = ? AND $colActivityType = ?',
        whereArgs: [treeCode, dateId, activityType]);
  }

  static Future<int> _insert(Map<String, dynamic> row) async {
    final db = await AgendaDatabase.instance.database;
    return db.insert(tblActivityProperty, row);
  }

  //### class interface ##########################################################################

  String treeCode;
  int dateId;
  int activityType;
  int startHour;
  int startMinute;
  String key;
  String value;

  ActivityPropertyTable(this.treeCode, this.dateId, this.activityType,
      this.startHour, this.startMinute, this.key, this.value);

  ActivityPropertyTable.fromRow(Map<String, dynamic> row) {
    treeCode = row[colTreeCode] as String;
    dateId = row[colDateId] as int;
    activityType = row[colActivityType] as int;
    startHour = row[colStartHour] as int;
    startMinute = row[colStartMinute] as int;
    key = row[colKey] as String;
    value = row[colValue] as String;
  }

  Map<String, dynamic> toRow() {
    return {
      colTreeCode: treeCode,
      colDateId: dateId,
      colActivityType: activityType,
      colStartHour: startHour,
      colStartMinute: startMinute,
      colKey: key,
      colValue: value
    };
  }

  static Future<List<ActivityPropertyTable>> select(
      String treeCode, int dateId, int activityType) async {
    final allRows = await _select(treeCode, dateId, activityType);
    final actProperties = <ActivityPropertyTable>[];
    for (final Map<String, dynamic> row in allRows) {
      actProperties.add(ActivityPropertyTable.fromRow(row));
    }
    return actProperties;
  }

  static Future<int> delete(
      String treeCode, int dateId, int activityType) async {
    final rowsDeleted = await _delete(treeCode, dateId, activityType);
    return rowsDeleted;
  }

  static Future<int> insert(List<ActivityPropertyTable> actProperties) async {
    int count = 0;
    for (final property in actProperties) {
      await _insert(property.toRow());
      count++;
    }
    return count;
  }
}
