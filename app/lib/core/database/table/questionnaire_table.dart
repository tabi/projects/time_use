import 'package:sqflite/sqflite.dart';
import 'package:tbo_app/home/start_questionnaire_result.dart';

class QuestionnaireTable {
  final Database database;

  const QuestionnaireTable(this.database);

  static const startQuestionnaireTableName = 'startQuestionnaire';

  Future<void> initialize() async {
    await database.execute('''
          CREATE TABLE $startQuestionnaireTableName (
            age INT,
            gender TEXT,
            livingSituation TEXT,
            partner INT,
            paidWork INT)
          ''');
  }

  Future<void> insert(StartQuestionnaireResult result) async {
    await database.insert(startQuestionnaireTableName, result.toJson());
  }

  Future<bool> hasStartQuestionnaireEntry() async {
    final _result = await database.query(startQuestionnaireTableName);
    return _result.isNotEmpty;
  }

  Future<StartQuestionnaireResult> startQuestionnaireEntry() async {
    if (await hasStartQuestionnaireEntry()) {
      final _result = await database.query(startQuestionnaireTableName);
      return StartQuestionnaireResult.fromJson(_result.first);
    }
    return null;
  }
}
