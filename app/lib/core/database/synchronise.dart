import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:built_collection/built_collection.dart';
import 'package:http/http.dart';

import 'package:tbo_app/core/database/table/sync_database.dart';

import 'package:tbo_app/core/database/datamodels/builtvalues/sync_sync.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_id.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_image.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_phone.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_phone_info.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_product.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_pull_body.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_push_receipt_body.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_receipt_data.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_register_body.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_register_data.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_search_product_data.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_search_store_data.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_transaction.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_user.dart';

import 'package:tbo_app/core/database/table/transaction_database.dart';
import 'package:path_provider/path_provider.dart';

const backendIsActive = true;
//const urlBase = 'http://172.16.37.446:8000';
//const urlBase = 'http://192.168.178.15:8000';
//const urlBase = 'http://cfdemobudget.cfapps.io';
const urlBase = 'https://budgetonderzoek.ontwikkel.cbs.nl';

const urlPushReceipt = '$urlBase/budget/data/pushreceipt';
const urlPullReceipt = '$urlBase/budget/data/pullreceipt';
const urlPushProduct = '$urlBase/budget/data/pushproduct';
const urlPullProduct = '$urlBase/budget/data/pullproduct';
const urlPushStore = '$urlBase/budget/data/pushstore';
const urlPullStore = '$urlBase/budget/data/pullstore';
const urlRegister = '$urlBase/budget/phone/register';

class Synchronise {
  //main functions

  static bool isBackendActive() {
    return backendIsActive;
  }

  static Future<void> synchronise() async {
    if (!backendIsActive) {
      return;
    }

    final _syncDatabase = SyncDatabase();
    final SyncId syncId = await _syncDatabase.getSyncId();

    if (syncId.userName != '' && syncId.phoneName != '') {
      final SyncUser user =
          SyncUser.newInstance(syncId.userName, syncId.userPassword);
      final SyncPhone phone = SyncPhone.newInstance(syncId.phoneName);
      await pullAllReceipts(user, phone);
      await pullAllSearchProducts(user, phone);
      await pullAllSearchStores(user, phone);
      await pushAllDataTypes(user, phone);
    }
  }

  //pull and push all datatypes

  static Future<void> pullAllReceipts(SyncUser user, SyncPhone phone) async {
    bool act = true;
    final _syncDatabase = SyncDatabase();
    while (act) {
      int syncOrder = await _syncDatabase.getSyncOrder(dataReceiptType);
      SyncReceiptData syncData = await pullOneReceipt(user, phone, syncOrder);
      if (syncData.synchronisation.dataIdentifier == null ||
          syncData.synchronisation.dataIdentifier == '') {
        act = false;
      } else {
        if (syncData.transaction.receiptLocation != '') {
          final String image = imageName(syncData.transaction.receiptLocation);
          final String receiptLocation = await imagePath(image);
          syncData = syncData.rebuild((b) => b.transaction = syncData
              .transaction
              .rebuild((b) => b.receiptLocation = receiptLocation)
              .toBuilder());
        }

        syncOrder = syncData.synchronisation.syncOrder;
        syncData = syncData.rebuild((b) => b.synchronisation = syncData
            .synchronisation
            .rebuild((b) => b.syncOrder = 0)
            .toBuilder());
        final SyncSync oldSyncSync = await _syncDatabase.getSyncSync(
            dataReceiptType, syncData.synchronisation.dataIdentifier);
        if (oldSyncSync.dataIdentifier == '') {
          await _syncDatabase.createImportedSyncSync(syncData.synchronisation);
          if (syncData.synchronisation.action != deleted) {
            await _syncDatabase.insertTransaction(syncData.transaction);
            for (final prod in syncData.products) {
              _syncDatabase.insertProduct(prod);
            }
          }
          if (syncData.transaction.receiptLocation != '') {
            final imageBytes = base64.decode(syncData.image.base64image);
            final File file = File(syncData.transaction.receiptLocation);
            await file.writeAsBytes(imageBytes);
          }
        } else {
          if (oldSyncSync.syncTime < syncData.synchronisation.syncTime) {
            _syncDatabase.updateImportedSyncSync(syncData.synchronisation);
            final _transactionDatabase = TransactionDatabase();
            await _transactionDatabase
                .deleteTransaction(syncData.synchronisation.dataIdentifier);
            if (syncData.synchronisation.action != deleted) {
              await _syncDatabase.insertTransaction(syncData.transaction);
              for (final prod in syncData.products) {
                _syncDatabase.insertProduct(prod);
              }
              if (syncData.transaction.receiptLocation != '') {
                final imageBytes = base64.decode(syncData.image.base64image);
                final File file = File(syncData.transaction.receiptLocation);
                await file.writeAsBytes(imageBytes);
              }
            }
          }
        }
        _syncDatabase.updateSyncOrder(dataReceiptType, syncOrder);
      }
    }
  }

  static Future<void> pullAllSearchProducts(
      SyncUser user, SyncPhone phone) async {
    bool act = true;
    final _syncDatabase = SyncDatabase();
    while (act) {
      int syncOrder = await _syncDatabase.getSyncOrder(dataProductType);
      SyncSearchProductData syncData =
          await pullOneSearchProduct(user, phone, syncOrder);
      if (syncData.synchronisation.dataIdentifier == null ||
          syncData.synchronisation.dataIdentifier == '') {
        act = false;
      } else {
        syncOrder = syncData.synchronisation.syncOrder;
        syncData = syncData.rebuild((b) => b.synchronisation = syncData
            .synchronisation
            .rebuild((b) => b.syncOrder = 0)
            .toBuilder());
        final SyncSync oldSyncSync = await _syncDatabase.getSyncSync(
            dataProductType, syncData.synchronisation.dataIdentifier);
        if (oldSyncSync.dataIdentifier == '') {
          await _syncDatabase.createImportedSyncSync(syncData.synchronisation);
          if (syncData.synchronisation.action != deleted) {
            await _syncDatabase.insertSearchProduct(syncData.searchProduct);
          }
        } else {
          if (oldSyncSync.syncTime < syncData.synchronisation.syncTime) {
            _syncDatabase.updateImportedSyncSync(syncData.synchronisation);
            await _syncDatabase
                .deleteSearchProduct(syncData.synchronisation.dataIdentifier);
            if (syncData.synchronisation.action != deleted) {
              await _syncDatabase.insertSearchProduct(syncData.searchProduct);
            }
          }
        }
        _syncDatabase.updateSyncOrder(dataProductType, syncOrder);
      }
    }
  }

  static Future<void> pullAllSearchStores(
      SyncUser user, SyncPhone phone) async {
    bool act = true;
    final _syncDatabase = SyncDatabase();
    while (act) {
      int syncOrder = await _syncDatabase.getSyncOrder(dataStoreType);
      SyncSearchStoreData syncData =
          await pullOneSearchStore(user, phone, syncOrder);
      if (syncData.synchronisation.dataIdentifier == null ||
          syncData.synchronisation.dataIdentifier == '') {
        act = false;
      } else {
        syncOrder = syncData.synchronisation.syncOrder;
        syncData = syncData.rebuild((b) => b.synchronisation = syncData
            .synchronisation
            .rebuild((b) => b.syncOrder = 0)
            .toBuilder());
        final SyncSync oldSyncSync = await _syncDatabase.getSyncSync(
            dataStoreType, syncData.synchronisation.dataIdentifier);
        if (oldSyncSync.dataIdentifier == '') {
          await _syncDatabase.createImportedSyncSync(syncData.synchronisation);
          if (syncData.synchronisation.action != deleted) {
            await _syncDatabase.insertSearchStore(syncData.searchStore);
          }
        } else {
          if (oldSyncSync.syncTime < syncData.synchronisation.syncTime) {
            _syncDatabase.updateImportedSyncSync(syncData.synchronisation);
            await _syncDatabase
                .deleteSearchStore(syncData.synchronisation.dataIdentifier);
            if (syncData.synchronisation.action != deleted) {
              await _syncDatabase.insertSearchStore(syncData.searchStore);
            }
          }
        }
        _syncDatabase.updateSyncOrder(dataStoreType, syncOrder);
      }
    }
  }

  static Future<void> pushAllDataTypes(SyncUser user, SyncPhone phone) async {
    int lastPushed = -1;
    int attempt = 0;

    bool act = true;

    while (act) {
      final SyncRegisterData syncRegisterData =
          await getPhonePushStatus(user, phone);
      final _syncDatabase = SyncDatabase();
      final SyncSync synchronisation = await _syncDatabase
          .getNextSyncSyncToPush(syncRegisterData.phone.syncOrder);

      if (lastPushed != synchronisation.syncOrder) {
        lastPushed = synchronisation.syncOrder;
        attempt = 1;
      } else if (attempt < 5) {
        attempt = attempt + 1;
        const Duration _duration = Duration(milliseconds: 100);
        sleep(_duration);
      } else {
        break;
      }

      if (synchronisation.dataIdentifier == '') {
        act = false;
      } else {
        if (synchronisation.dataType == dataReceiptType) {
          await pushOneReceipt(synchronisation, user, phone);
        }
      }
    }
  }

  //push one datatype

  static Future pushOneReceipt(
      SyncSync synchronisation, SyncUser user, SyncPhone phone) async {
    SyncTransaction transaction;
    final List<SyncProduct> products = <SyncProduct>[];
    SyncImage image;

    if (synchronisation.action != deleted) {
      final _transactionDatabase = TransactionDatabase();

      final List<Map<String, dynamic>> trans = await _transactionDatabase
          .queryTransaction(synchronisation.dataIdentifier);

      transaction = SyncTransaction.fromJson(trans.first);

      final List<Map<String, dynamic>> prods = await _transactionDatabase
          .queryTransactionProducts(synchronisation.dataIdentifier);

      for (final product in prods) {
        products.add(SyncProduct.fromJson(product));
      }

      image = SyncImage((b) => b
        ..transactionID = transaction.transactionID
        ..base64image = '');

      if (transaction.receiptLocation != '') {
        final File imageFile = File(transaction.receiptLocation);
        final List<int> imageBytes = imageFile.readAsBytesSync();
        image = image.rebuild((b) => b.base64image = base64.encode(imageBytes));
        transaction = transaction.rebuild(
            (b) => b.receiptLocation = imageName(transaction.receiptLocation));
      }
    } else {
      transaction = SyncTransaction.newInstance(synchronisation.dataIdentifier);
      image = SyncImage((b) => b
        ..transactionID = transaction.transactionID
        ..base64image = '');
    }

    final SyncPushReceiptBody body = SyncPushReceiptBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..syncOrder = synchronisation.syncOrder
      ..synchronisation = synchronisation.toBuilder()
      ..transaction = transaction.toBuilder()
      ..products = ListBuilder<SyncProduct>(products)
      ..image = image.toBuilder());

    await post(urlPushReceipt, body: jsonEncode(body));
  }

  //pull one datatype

  static Future<SyncReceiptData> pullOneReceipt(
      SyncUser user, SyncPhone phone, int syncOrder) async {
    final SyncPullBody body = SyncPullBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..syncOrder = syncOrder);
    final Response response =
        await post(urlPullReceipt, body: jsonEncode(body));
    return SyncReceiptData.fromJson(
        json.decode(response.body) as Map<String, dynamic>);
  }

  static Future<SyncSearchProductData> pullOneSearchProduct(
      SyncUser user, SyncPhone phone, int syncOrder) async {
    final SyncPullBody body = SyncPullBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..syncOrder = syncOrder);
    final Response response =
        await post(urlPullProduct, body: jsonEncode(body));
    return SyncSearchProductData.fromJson(
        json.decode(response.body) as Map<String, dynamic>);
  }

  static Future<SyncSearchStoreData> pullOneSearchStore(
      SyncUser user, SyncPhone phone, int syncOrder) async {
    final SyncPullBody body = SyncPullBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..syncOrder = syncOrder);
    final Response response = await post(urlPullStore, body: jsonEncode(body));
    return SyncSearchStoreData.fromJson(
        json.decode(response.body) as Map<String, dynamic>);
  }

  //file path functions

  static String imageName(String path) {
    const String prefix = '/Pictures/flutter_test/';
    int i = path.indexOf(prefix);
    if (i < 0) {
      return path;
    } else {
      i = i + prefix.length;
      return path.substring(i);
    }
  }

  static Future<String> imagePath(String name) async {
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Pictures/flutter_test';
    await Directory(dirPath).create(recursive: true);
    return '$dirPath/$name';
  }

  //register phone and get phone push status

  static Future<SyncRegisterData> getPhonePushStatus(
      SyncUser user, SyncPhone phone) async {
    final List<SyncPhoneInfo> phoneInfos = <SyncPhoneInfo>[];
    final SyncRegisterBody body = SyncRegisterBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..phoneInfos = ListBuilder<SyncPhoneInfo>(phoneInfos));
    final Response response = await post(urlRegister, body: jsonEncode(body));
    return SyncRegisterData.fromJson(
        json.decode(response.body) as Map<String, dynamic>);
  }

  static Future<SyncRegisterData> registerNewPhone(
      String userName,
      String userPassword,
      String phoneName,
      List<SyncPhoneInfo> phoneInfos) async {
    final SyncUser user = SyncUser.newInstance(userName, userPassword);
    final SyncPhone phone = SyncPhone.newInstance(phoneName);
    final SyncRegisterBody body = SyncRegisterBody((b) => b
      ..user = user.toBuilder()
      ..phone = phone.toBuilder()
      ..phoneInfos = ListBuilder<SyncPhoneInfo>(phoneInfos));

    final Response response = await post(urlRegister, body: jsonEncode(body));

    return SyncRegisterData.fromJson(
        json.decode(response.body) as Map<String, dynamic>);
  }
}
