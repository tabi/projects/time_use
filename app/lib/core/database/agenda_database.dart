import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:tbo_app/core/database/table/activity_date_table.dart';
import 'package:tbo_app/core/database/table/activity_info_table.dart';
import 'package:tbo_app/core/database/table/activity_property_table.dart';

class AgendaDatabase {
  static const _databaseName = 'agenda.db';
  static const _databaseVersion = 1;

  // make this a singleton class
  AgendaDatabase._privateConstructor();
  static final AgendaDatabase instance = AgendaDatabase._privateConstructor();

  // only have a single app-wide reference to the database
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    // lazily instantiate the db the first time it is accessed
    return _database = await _initDatabase();
  }

  // this opens the database (and creates it if it doesn't exist)
  Future<Database> _initDatabase() async {
    final Directory documentsDirectory =
        await getApplicationDocumentsDirectory();
    final String path = join(documentsDirectory.path, _databaseName);
    return openDatabase(path, version: _databaseVersion, onCreate: _onCreate);
  }

  // SQL code to create the database table
  Future _onCreate(Database db, int version) async {
    ActivityDateTable.create(db, version);
    ActivityInfoTable.create(db, version);
    ActivityPropertyTable.create(db, version);
  }
}
