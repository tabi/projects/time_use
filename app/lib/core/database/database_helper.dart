import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:tbo_app/core/database/table/experience_sampling_table.dart';
import 'package:tbo_app/core/database/table/para_data_table.dart';
import 'package:tbo_app/core/database/table/questionnaire_table.dart';
import 'package:tbo_app/core/database/table/sync_database.dart';
import 'package:tbo_app/core/database/table/transaction_database.dart';
import 'package:tbo_app/open_input/search_suggestions_table.dart';

class DatabaseHelper {
  DatabaseHelper._();

  static final DatabaseHelper instance = DatabaseHelper._();

  static Database _database;

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }
    return _database = await _initDatabase();
  }

  Future<Database> _initDatabase() async {
    final path = await _getDatabasePath();
    await _importDatabase();
    return openDatabase(path);
  }

  Future<String> _getDatabasePath() async {
    final databasesDir = await getApplicationDocumentsDirectory();
    final path = join(databasesDir.path, 'database.db');
    return path;
  }

  Future<void> _importDatabase() async {
    final path = await _getDatabasePath();
    if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound) {
      final data =
          await rootBundle.load(join('assets/databases', 'database.db'));
      final bytes =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      await File(path).writeAsBytes(bytes);
      _database = await openDatabase(path);

      await TransactionDatabase().createTables(_database);
      await SyncDatabase().createTables(_database);
      await SearchSuggestionsTable(_database).createTables();
      await QuestionnaireTable(_database).initialize();
      await ParaDataTable(_database).initialize();
      await ExperienceSamplingDb().createTables(_database);
    }
  }
}
