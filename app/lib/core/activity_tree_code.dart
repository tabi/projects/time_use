import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'activity_tree_code.g.dart';

class ActivityTreeCode extends EnumClass {
  static Serializer<ActivityTreeCode> get serializer =>
      _$activityTreeCodeSerializer;

  static const ActivityTreeCode MEDIA20201102 = _$MEDIA20201102;
  static const ActivityTreeCode MEDIA20201102N = _$MEDIA20201102N;
  static const ActivityTreeCode T20200908 = _$T20200908;
  static const ActivityTreeCode T20201004 = _$T20201004;

  const ActivityTreeCode._(String name) : super(name);

  static BuiltSet<ActivityTreeCode> get values => _$values;

  static ActivityTreeCode valueOf(String name) => _$valueOf(name);
}
// ignore_for_file: constant_identifier_names
