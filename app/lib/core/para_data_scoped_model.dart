import 'package:flutter/widgets.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:sqflite/sqflite.dart';
import 'package:tbo_app/core/database/database_helper.dart';
import 'package:tbo_app/core/database/table/para_data_table.dart';
import 'package:tbo_app/para_data/para_data.dart';
import 'package:tbo_app/para_data/para_data_navigator_observer.dart';

class ParaDataScopedModel extends Model {
  static ParaDataScopedModel of(BuildContext context) =>
      ScopedModel.of<ParaDataScopedModel>(context);

  Future<void> openScreen(String name) async {
    assert(name != null);
    final _table = await _paraDataTable();
    await _table.insert(ParaData((b) => b
      ..timestamp = DateTime.now().toUtc()
      ..objectName = name
      ..action = 'open screen'));
  }

  Future<Database> _database() async {
    return DatabaseHelper.instance.database;
  }

  Future<ParaDataTable> _paraDataTable() async {
    return ParaDataTable(await _database());
  }

  Future<void> closeScreen(String name) async {
    assert(name != null);
    final _table = await _paraDataTable();
    await _table.insert(ParaData((b) => b
      ..timestamp = DateTime.now().toUtc()
      ..objectName = name
      ..action = 'close screen'));
  }

  Future<void> onTap(String widgetType, String action) async {
    final _table = await _paraDataTable();
    await _table.insert(ParaData((b) => b
      ..timestamp = DateTime.now().toUtc()
      ..objectName = widgetType
      ..action = action));
  }

  NavigatorObserver observer() {
    return ParaDataNavigatorObserver(this);
  }
}
