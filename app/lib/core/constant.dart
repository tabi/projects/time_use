class Constant {
  static const String emptyString = '';
  static const String newLine = '\n';
  static const String codeLevelSeparator = '.';
  static const String codeLevelBottom = '';
}
