import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:tbo_app/activities/activity.dart';
import 'package:tbo_app/activities/activity_companion.dart';
import 'package:tbo_app/activities/activity_location.dart';
import 'package:tbo_app/activities/activity_node.dart';
import 'package:tbo_app/activities/activity_type.dart';
import 'package:tbo_app/activities/medium.dart';
import 'package:tbo_app/core/activity_tree_code.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_group_info.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_id.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_image.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_phone.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_phone_info.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_product.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_pull_body.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_push_receipt_body.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_push_search_product_body.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_push_search_store_body.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_receipt_data.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_register_body.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_register_data.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_search_product.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_search_product_data.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_search_store.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_search_store_data.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_sync.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_transaction.dart';
import 'package:tbo_app/core/database/datamodels/builtvalues/sync_user.dart';
import 'package:tbo_app/core/database/datamodels/user_progress.dart';
import 'package:tbo_app/home/gender.dart';
import 'package:tbo_app/home/living_situation.dart';
import 'package:tbo_app/home/start_questionnaire_result.dart';
import 'package:tbo_app/para_data/para_data.dart';

part 'serializers.g.dart';

@SerializersFor([
  Gender,
  LivingSituation,
  StartQuestionnaireResult,
  SyncSync,
  SyncGroupInfo,
  SyncId,
  SyncImage,
  SyncPhone,
  SyncPhoneInfo,
  SyncProduct,
  SyncPullBody,
  SyncPushReceiptBody,
  SyncPushSearchProductBody,
  SyncPushSearchStoreBody,
  SyncReceiptData,
  SyncRegisterBody,
  SyncRegisterData,
  SyncSearchProduct,
  SyncSearchProductData,
  SyncSearchStore,
  SyncSearchStoreData,
  SyncTransaction,
  SyncUser,
  UserProgress,
  ActivityNode,
  Activity,
  ActivityCompanion,
  ActivityType,
  ActivityLocation,
  Medium,
  ActivityTreeCode,
  ParaData,
])
final Serializers serializers =
    (_$serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
