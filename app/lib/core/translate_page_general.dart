import 'package:tbo_app/core/translate_page_base.dart';

class TranslatePageGeneral extends TranslatePageBase {
  TranslatePageGeneral._privateConstructor();

  static final TranslatePageGeneral instance =
      TranslatePageGeneral._privateConstructor();

  @override
  void initPage() {
    pageKey = 'general';
  }

  @override
  void initKeys() {
    keys.add('yes');
    keys.add('no');
    keys.add('ok');
    keys.add('cancel');
    keys.add('save');
    keys.add('mainActivity');
    keys.add('sideActivity');
    keys.add('mainActivities');
    keys.add('sideActivities');
    keys.add('warning');
    keys.add('create');
    keys.add('attention');
    keys.add('explanation');
    keys.add('activity');
    keys.add('and');
  }

  String get and {
    return translation('and');
  }

  String get activity {
    return translation('activity');
  }

  String get explanation {
    return translation('explanation');
  }

  String get attention {
    return translation('attention');
  }

  String get create {
    return translation('create');
  }

  String get yes {
    return translation('yes');
  }

  String get no {
    return translation('no');
  }

  String get ok {
    return translation('ok');
  }

  String get cancel {
    return translation('cancel');
  }

  String get save {
    return translation('save');
  }

  String get mainActivity {
    return translation('mainActivity');
  }

  String get sideActivity {
    return translation('sideActivity');
  }

  String get mainActivities {
    return translation('mainActivities');
  }

  String get sideActivities {
    return translation('sideActivities');
  }

  String get warning {
    return translation('warning');
  }
}
