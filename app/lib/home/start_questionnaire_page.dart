import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/core/questionnaire.dart';
import 'package:tbo_app/core/translate.dart';
import 'package:tbo_app/home/gender.dart';
import 'package:tbo_app/home/living_situation.dart';
import 'package:tbo_app/home/start_questionnaire_result.dart';
import 'package:tbo_app/home/start_questionnaire_scoped_model.dart';

class StartQuestionnairePage extends StatefulWidget {
  @override
  _StartQuestionnairePageState createState() => _StartQuestionnairePageState();
}

class _StartQuestionnairePageState extends State<StartQuestionnairePage>
    with TickerProviderStateMixin {
  int _age;
  Gender _gender;
  LivingSituation _livingSituation;
  bool _partner;
  bool _paidWork;

  final _questionnaireKey = GlobalKey<QuestionnaireState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: ScopedModelDescendant<StartQuestionnaireModel>(
          builder: (context, child, startQuestionnaireModel) {
        return Questionnaire(
          key: _questionnaireKey,
          onComplete: () async {
            await startQuestionnaireModel
                .insert(StartQuestionnaireResult((b) => b
                  ..age = _age
                  ..gender = _gender
                  ..livingSituation = _livingSituation
                  ..partner = _partner ? 1 : 0
                  ..paidWork = _paidWork ? 1 : 0));
            Navigator.of(context).pop();
          },
          itemCount: 5,
          itemBuilder: (context, index) {
            return [
              _AgeQuestion(
                age: _age,
                questionAnswered: (age) {
                  setState(() {
                    _age = int.parse(age);
                  });
                  _questionnaireKey.currentState.nextQuestion(1);
                },
              ),
              _GenderQuestion(
                gender: _gender,
                questionAnswered: (gender) {
                  setState(() {
                    _gender = gender;
                  });
                  _questionnaireKey.currentState.nextQuestion(2);
                },
              ),
              _LivingSituationQuestion(
                livingSituation: _livingSituation,
                questionAnswered: (livingSituation) {
                  setState(() {
                    _livingSituation = livingSituation;
                  });
                  _questionnaireKey.currentState.nextQuestion(3);
                },
              ),
              _PartnerQuestion(
                partner: _partner,
                questionAnswered: (partner) {
                  setState(() {
                    _partner = partner;
                  });
                  _questionnaireKey.currentState.nextQuestion(4);
                },
              ),
              _EmploymentQuestion(
                paidWork: _paidWork,
                questionAnswered: (paidWork) {
                  setState(() {
                    _paidWork = paidWork;
                  });
                  _questionnaireKey.currentState.nextQuestion(5);
                },
              ),
            ][index];
          },
        );
      }),
    );
  }
}

class _AgeQuestion extends StatefulWidget {
  final int age;
  final bool active;
  final ValueChanged<String> questionAnswered;
  const _AgeQuestion(
      {Key key, this.active = false, this.questionAnswered, this.age})
      : super(key: key);
  @override
  __AgeQuestionState createState() => __AgeQuestionState();
}

class __AgeQuestionState extends State<_AgeQuestion>
    with AutomaticKeepAliveClientMixin, SingleTickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> _opacityAnimation;
  final _ageTextEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _ageTextEditingController.text = widget.age?.toString() ?? '';
    _animationController = AnimationController(
        duration: const Duration(milliseconds: 200), vsync: this);
    _opacityAnimation =
        Tween<double>(begin: 0.0, end: 1.0).animate(_animationController);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 21.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            '${translate.question} 1'.toUpperCase(),
            style: Theme.of(context).textTheme.caption,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 12),
            child: Text(translate.ageQuestion,
                style: Theme.of(context).textTheme.headline6),
          ),
          Stack(
            children: [
              TextFormField(
                controller: _ageTextEditingController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(hintText: translate.enterAge),
                onFieldSubmitted: widget.questionAnswered,
                onChanged: (value) {
                  if (value?.isNotEmpty ?? false) {
                    _animationController.forward();
                  }
                },
              ),
              Align(
                alignment: Alignment.centerRight,
                child: AnimatedBuilder(
                  builder: (context, child) {
                    return AnimatedOpacity(
                      opacity: _opacityAnimation.value,
                      duration: const Duration(milliseconds: 330),
                      child: child,
                    );
                  },
                  animation: _opacityAnimation,
                  child: FlatButton(
                    onPressed: () {
                      if (_ageTextEditingController.text.isNotEmpty) {
                        FocusScope.of(context).unfocus();
                        widget.questionAnswered(_ageTextEditingController.text);
                      }
                    },
                    child: Text(
                      translate.next,
                      style: Theme.of(context).textTheme.button,
                    ),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class _GenderQuestion extends StatefulWidget {
  final Gender gender;
  final bool active;
  final ValueChanged<Gender> questionAnswered;
  const _GenderQuestion(
      {Key key, this.active = false, this.gender, this.questionAnswered})
      : super(key: key);
  @override
  __GenderQuestionState createState() => __GenderQuestionState();
}

class __GenderQuestionState extends State<_GenderQuestion>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 23.0),
          child: Text(
            '${translate.question} 2'.toUpperCase(),
            style: Theme.of(context).textTheme.caption,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 23.0, top: 12),
          child: Text(
            translate.genderQuestion,
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Row(
            children: <Widget>[
              Radio(
                value: Gender.male,
                groupValue: widget.gender,
                onChanged: widget.questionAnswered,
              ),
              Text(translate.male),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Row(
            children: <Widget>[
              Radio(
                value: Gender.female,
                groupValue: widget.gender,
                onChanged: widget.questionAnswered,
              ),
              Text(translate.female),
            ],
          ),
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class _LivingSituationQuestion extends StatefulWidget {
  final LivingSituation livingSituation;
  final bool active;
  final ValueChanged<LivingSituation> questionAnswered;
  const _LivingSituationQuestion(
      {Key key,
      this.active = false,
      this.livingSituation,
      this.questionAnswered})
      : super(key: key);
  @override
  __LivingSituationQuestionState createState() =>
      __LivingSituationQuestionState();
}

class __LivingSituationQuestionState extends State<_LivingSituationQuestion>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 23.0),
          child: Text(
            '${translate.question} 3'.toUpperCase(),
            style: Theme.of(context).textTheme.caption,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 23.0, top: 12, bottom: 12),
          child: Text(
            translate.livingSituationQuestion,
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8.0, bottom: 12),
          child: Row(
            children: <Widget>[
              Radio(
                value: LivingSituation.aloneWithoutChildren,
                groupValue: widget.livingSituation,
                onChanged: widget.questionAnswered,
              ),
              SizedBox(
                width: 240,
                child: Text(
                  translate.aloneWithoutChildren,
                  maxLines: 2,
                  softWrap: true,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8.0, bottom: 12),
          child: Row(
            children: <Widget>[
              Radio(
                value: LivingSituation.aloneWithChildren,
                groupValue: widget.livingSituation,
                onChanged: widget.questionAnswered,
              ),
              SizedBox(
                width: 240,
                child: Text(
                  translate.aloneWithChildren,
                  maxLines: 2,
                  softWrap: true,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8.0, bottom: 12),
          child: Row(
            children: <Widget>[
              Radio(
                value: LivingSituation.livingTogetherWithoutChildren,
                groupValue: widget.livingSituation,
                onChanged: widget.questionAnswered,
              ),
              SizedBox(
                width: 240,
                child: Text(
                  translate.livingTogetherWithoutChildren,
                  maxLines: 3,
                  softWrap: true,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8.0, bottom: 12),
          child: Row(
            children: <Widget>[
              Radio(
                value: LivingSituation.livingTogetherWithChildren,
                groupValue: widget.livingSituation,
                onChanged: widget.questionAnswered,
              ),
              SizedBox(
                width: 240,
                child: Text(
                  translate.livingTogetherWithChildren,
                  maxLines: 3,
                  softWrap: true,
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8.0, bottom: 12),
          child: Row(
            children: <Widget>[
              Radio(
                value: LivingSituation.other,
                groupValue: widget.livingSituation,
                onChanged: widget.questionAnswered,
              ),
              SizedBox(
                width: 240,
                child: Text(
                  translate.other,
                  maxLines: 2,
                  softWrap: true,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class _PartnerQuestion extends StatefulWidget {
  final bool partner;
  final ValueChanged<bool> questionAnswered;
  final bool active;
  const _PartnerQuestion(
      {Key key, this.active = false, this.partner, this.questionAnswered})
      : super(key: key);
  @override
  _PartnerQuestionState createState() => _PartnerQuestionState();
}

class _PartnerQuestionState extends State<_PartnerQuestion>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 23.0),
          child: Text(
            '${translate.question} 4'.toUpperCase(),
            style: Theme.of(context).textTheme.caption,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 23.0, top: 12),
          child: Text(
            translate.partnerQuestion,
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Row(
            children: <Widget>[
              Radio(
                value: true,
                groupValue: widget.partner,
                onChanged: widget.questionAnswered,
              ),
              Text(translate.yes),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Row(
            children: <Widget>[
              Radio(
                value: false,
                groupValue: widget.partner,
                onChanged: widget.questionAnswered,
              ),
              Text(translate.no),
            ],
          ),
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}

class _EmploymentQuestion extends StatefulWidget {
  final bool paidWork;
  final ValueChanged<bool> questionAnswered;
  final bool active;
  const _EmploymentQuestion(
      {Key key, this.active = false, this.paidWork, this.questionAnswered})
      : super(key: key);
  @override
  _EmploymentQuestionState createState() => _EmploymentQuestionState();
}

class _EmploymentQuestionState extends State<_EmploymentQuestion>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 23.0),
          child: Text(
            '${translate.question} 5'.toUpperCase(),
            style: Theme.of(context).textTheme.caption,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 23.0, top: 12),
          child: Text(
            translate.paidWorkQuestion,
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
        const SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 23.0),
          child: Text(
            translate.paidWorkExplanation,
            style: Theme.of(context)
                .textTheme
                .bodyText2
                .copyWith(fontStyle: FontStyle.italic),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Row(
            children: <Widget>[
              Radio(
                value: true,
                groupValue: widget.paidWork,
                onChanged: widget.questionAnswered,
              ),
              Text(translate.yes),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Row(
            children: <Widget>[
              Radio(
                value: false,
                groupValue: widget.paidWork,
                onChanged: widget.questionAnswered,
              ),
              Text(translate.no),
            ],
          ),
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
