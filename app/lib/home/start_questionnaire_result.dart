import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:tbo_app/core/serializers.dart';
import 'package:tbo_app/home/gender.dart';
import 'package:tbo_app/home/living_situation.dart';

part 'start_questionnaire_result.g.dart';

abstract class StartQuestionnaireResult
    implements
        Built<StartQuestionnaireResult, StartQuestionnaireResultBuilder> {
  static Serializer<StartQuestionnaireResult> get serializer =>
      _$startQuestionnaireResultSerializer;

  int get age;
  Gender get gender;
  LivingSituation get livingSituation;
  num get partner;
  num get paidWork;

  @memoized
  bool get hasPartner =>
      partner == 1 ||
      LivingSituation.livingTogetherWithoutChildren == livingSituation ||
      LivingSituation.livingTogetherWithChildren == livingSituation;

  @memoized
  bool get hasChildren =>
      LivingSituation.aloneWithoutChildren != livingSituation &&
      LivingSituation.livingTogetherWithoutChildren != livingSituation &&
      LivingSituation.other != livingSituation;

  @memoized
  bool get hasOtherLivingPartners => livingSituation == LivingSituation.other;

  factory StartQuestionnaireResult(
          [Function(StartQuestionnaireResultBuilder b) updates]) =
      _$StartQuestionnaireResult;

  StartQuestionnaireResult._();

  factory StartQuestionnaireResult.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map<String, dynamic> toJson() {
    return serializers.serializeWith(serializer, this) as Map<String, dynamic>;
  }
}
