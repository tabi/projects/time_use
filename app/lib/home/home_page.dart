import 'dart:async';

import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/activities/activities_page.dart';
import 'package:tbo_app/activities/activity_tree_scoped_model.dart';
import 'package:tbo_app/activities/agendas_scoped_model.dart';
import 'package:tbo_app/core/color_pallet.dart';
import 'package:tbo_app/core/configuration.dart';
import 'package:tbo_app/core/configuration_state.dart';
import 'package:tbo_app/core/database/datamodels/progress_statistics.dart';
import 'package:tbo_app/core/database/user_progress_database.dart';
import 'package:tbo_app/core/input_type.dart';
import 'package:tbo_app/core/para_data_scoped_model.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/core/translate.dart';
import 'package:tbo_app/core/translate_init.dart';
import 'package:tbo_app/home/bottom_navigation_bar.dart'
    as custom_bottom_navigation_bar;
import 'package:tbo_app/home/page_model.dart';
import 'package:tbo_app/home/start_questionnaire_page.dart';
import 'package:tbo_app/home/start_questionnaire_scoped_model.dart';
import 'package:tbo_app/insights/incomplete_insights_dialog.dart';
import 'package:tbo_app/insights/insights_page.dart';
import 'package:tbo_app/insights/insights_scoped_model.dart';
import 'package:tbo_app/insights/insights_visibility.dart';
import 'package:tbo_app/overview/overview_page.dart';
import 'package:tbo_app/para_data/para_data_name.dart';
import 'package:tbo_app/settings/settings.dart';
import 'package:tbo_app/settings/settings_page.dart';

import '../core/database/database_helper.dart';

GlobalKey keyButton4 = GlobalKey();

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  bool _showingQuestionnaire = false;
  int _currentPageIndex = 0;
  List<Widget> _pages = [];

  @override
  void initState() {
    super.initState();
    prepareTus();
  }

  Future<void> _showStartQuestionnaire(BuildContext context) async {
    final _startQuestionnaireModel = StartQuestionnaireModel.of(context);
    final _hasEntry =
        await _startQuestionnaireModel.hasStartQuestionnaireEntry();
    if (!_hasEntry && !_showingQuestionnaire) {
      setState(() {
        _showingQuestionnaire = true;
      });
      Navigator.of(context).push(MaterialPageRoute(
        settings: const RouteSettings(name: 'StartQuestionnairePage'),
        builder: (BuildContext context) {
          return StartQuestionnairePage();
        },
      ));
    }
  }

  Future<bool> prepareTus() async {
    await Settings.instance.init();
    await DatabaseHelper.instance.database;
    final tx = TranslateInit.instance;
    await tx.init('NL');
    final _configuration = Configuration.of(context);
    await _configuration.load();
    final _stats = await UserProgressDatabase().getProgressStats();
    final _remaining = (_stats.remaining ?? 7) > 0;
    setState(() {
      _pages = [
        OverviewPage(
          keyButton4,
          dateSelected: (date) {
            setState(() {
              _currentPageIndex = 1;
            });
            PageModel.of(context).selectedDate = date;
          },
        ),
        ActivitiesPage(
            treeCode: Configuration.of(context).mainActivityTree.name,
            testVersion: 'V0VO',
            closable: false),
        if (_showInsights(_configuration, _remaining)) InsightsPage(),
        SettingsPage(),
      ];
    });

    return true;
  }

  bool _showInsights(ConfigurationState _configuration, bool _remaining) {
    return InsightsVisibility.directly == _configuration.insightsVisibility ||
        (InsightsVisibility.afterExperiment ==
                _configuration.insightsVisibility &&
            _remaining);
  }

  void _initializeActivityTrees() {
    if (Configuration.of(context).mainActivityTree != null) {
      ActivityTreeScopedModel.of(context).mainActivityTree.init(
          Configuration.of(context).mainActivityTree.name,
          Configuration.of(context).inputType);
      ActivityTreeScopedModel.of(context).sideActivityTree.init(
          Configuration.of(context).sideActivityTree.name,
          Configuration.of(context).inputType);
      AgendasScopedModel.of(context).mainActivityTree =
          ActivityTreeScopedModel.of(context).mainActivityTree;
      AgendasScopedModel.of(context).sideActivityTree =
          ActivityTreeScopedModel.of(context).sideActivityTree;
      InsightsScopedModel.of(context).activityTree =
          ActivityTreeScopedModel.of(context).mainActivityTree;
    }
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _showStartQuestionnaire(context);
      _initializeActivityTrees();
    });
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Stack(
        children: <Widget>[
          Positioned(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Container(
                    color: ColorPallet.primaryColor,
                  ),
                ),
                Expanded(
                  child: Container(
                    color: Colors.white,
                  ),
                )
              ],
            ),
          ),
          Positioned(
            bottom: 10 * y,
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    key: keyButton4,
                    height: 50.0 * y,
                    width: 56.0 * x,
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            child: SafeArea(
              child: Scaffold(
                body: Container(
                  color: ColorPallet.primaryColor,
                  child: SafeArea(
                    child: Builder(
                      builder: (context) {
                        if (_pages.isEmpty) {
                          return Container(color: Colors.white);
                        }
                        return Container(
                            color: Colors.white,
                            child: _pages[_currentPageIndex]);
                      },
                    ),
                  ),
                ),
                bottomNavigationBar: ScopedModelDescendant<AgendasScopedModel>(
                    builder: (context, child, agendasModel) {
                  final _configuration = Configuration.of(context);
                  return FutureBuilder<ProgressStatistics>(
                      future: UserProgressDatabase().getProgressStats(),
                      builder: (context, snapshot) {
                        final _remaining = (snapshot?.data?.remaining ?? 7) > 0;
                        return Theme(
                          data: Theme.of(context).copyWith(
                              canvasColor: Colors.white,
                              primaryColor: Colors.blue,
                              textTheme: Theme.of(context).textTheme.copyWith(
                                  caption:
                                      const TextStyle(color: Colors.grey))),
                          child: Container(
                            decoration: BoxDecoration(
                              color: Colors.white,
                              boxShadow: [
                                BoxShadow(
                                    color: ColorPallet.veryLightGray,
                                    offset: Offset(0.0 * x, -2.2 * y),
                                    blurRadius: 1.7 * y,
                                    spreadRadius: 1.7 * y)
                              ],
                            ),
                            child: custom_bottom_navigation_bar
                                .BottomNavigationBar(
                              elevation: 0,
                              iconSize: 30.0 * x,
                              onTap: (index) async {
                                final InputType inputType =
                                    Configuration.of(context).inputType;
                                final picked = DateTime.now();
                                await agendasModel.load(
                                    Configuration.of(context)
                                        .mainActivityTree
                                        .name,
                                    Configuration.of(context)
                                        .sideActivityTree
                                        .name,
                                    picked,
                                    inputType);
                                ParaDataScopedModel.of(context).onTap(
                                    (_pages[_currentPageIndex] as ParaDataName)
                                        .name,
                                    'openTab');
                                if (index == 2 &&
                                    _hideInsights(translate.insightsPage,
                                        _configuration, _remaining)) {
                                  await showDialog<bool>(
                                      context: context,
                                      routeSettings: const RouteSettings(
                                          name: 'IncompleteInsightsDialog'),
                                      builder: (BuildContext context) {
                                        return const IncompleteInsightsDialog();
                                      });
                                } else {
                                  setState(() {
                                    _currentPageIndex = index;
                                  });
                                }
                              },
                              fixedColor: ColorPallet.primaryColor,
                              type: custom_bottom_navigation_bar
                                  .BottomNavigationBarType.fixed,
                              currentIndex: _currentPageIndex,
                              items: [
                                {
                                  'icon': Icons.event_note,
                                  'text': translate.overviewPage
                                },
                                {
                                  'icon': Icons.view_day,
                                  'text': 'Activiteiten'
                                },
                                if (_showInsights(_configuration, _remaining))
                                  {
                                    'icon': Icons.equalizer,
                                    'text': translate.insightsPage
                                  },
                                {
                                  'icon': Icons.settings,
                                  'text': translate.settings
                                },
                              ].map(
                                (Map<String, dynamic> values) {
                                  return BottomNavigationBarItem(
                                    icon: Icon(values['icon'] as IconData,
                                        color: (_hideInsights(
                                                values['text'] as String,
                                                _configuration,
                                                _remaining))
                                            ? const Color(0x99AAAAAA)
                                            : null),
                                    label: values['text'] as String,
                                  );
                                },
                              ).toList(),
                            ),
                          ),
                        );
                      });
                }),
              ),
            ),
          ),
        ],
      ),
    );
  }

  bool _hideInsights(
      String page, ConfigurationState _configuration, bool _remaining) {
    return page == translate.insightsPage &&
        _configuration.insightsVisibility ==
            InsightsVisibility.afterExperiment &&
        _remaining;
  }
}
