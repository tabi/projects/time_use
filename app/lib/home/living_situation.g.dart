// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'living_situation.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const LivingSituation _$aloneWithoutChildren =
    const LivingSituation._('aloneWithoutChildren');
const LivingSituation _$aloneWithChildren =
    const LivingSituation._('aloneWithChildren');
const LivingSituation _$livingTogetherWithoutChildren =
    const LivingSituation._('livingTogetherWithoutChildren');
const LivingSituation _$livingTogetherWithChildren =
    const LivingSituation._('livingTogetherWithChildren');
const LivingSituation _$other = const LivingSituation._('other');

LivingSituation _$valueOf(String name) {
  switch (name) {
    case 'aloneWithoutChildren':
      return _$aloneWithoutChildren;
    case 'aloneWithChildren':
      return _$aloneWithChildren;
    case 'livingTogetherWithoutChildren':
      return _$livingTogetherWithoutChildren;
    case 'livingTogetherWithChildren':
      return _$livingTogetherWithChildren;
    case 'other':
      return _$other;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<LivingSituation> _$values =
    new BuiltSet<LivingSituation>(const <LivingSituation>[
  _$aloneWithoutChildren,
  _$aloneWithChildren,
  _$livingTogetherWithoutChildren,
  _$livingTogetherWithChildren,
  _$other,
]);

Serializer<LivingSituation> _$livingSituationSerializer =
    new _$LivingSituationSerializer();

class _$LivingSituationSerializer
    implements PrimitiveSerializer<LivingSituation> {
  @override
  final Iterable<Type> types = const <Type>[LivingSituation];
  @override
  final String wireName = 'LivingSituation';

  @override
  Object serialize(Serializers serializers, LivingSituation object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  LivingSituation deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      LivingSituation.valueOf(serialized as String);
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
