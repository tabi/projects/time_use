// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'start_questionnaire_result.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<StartQuestionnaireResult> _$startQuestionnaireResultSerializer =
    new _$StartQuestionnaireResultSerializer();

class _$StartQuestionnaireResultSerializer
    implements StructuredSerializer<StartQuestionnaireResult> {
  @override
  final Iterable<Type> types = const [
    StartQuestionnaireResult,
    _$StartQuestionnaireResult
  ];
  @override
  final String wireName = 'StartQuestionnaireResult';

  @override
  Iterable<Object> serialize(
      Serializers serializers, StartQuestionnaireResult object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'age',
      serializers.serialize(object.age, specifiedType: const FullType(int)),
      'gender',
      serializers.serialize(object.gender,
          specifiedType: const FullType(Gender)),
      'livingSituation',
      serializers.serialize(object.livingSituation,
          specifiedType: const FullType(LivingSituation)),
      'partner',
      serializers.serialize(object.partner, specifiedType: const FullType(num)),
      'paidWork',
      serializers.serialize(object.paidWork,
          specifiedType: const FullType(num)),
    ];

    return result;
  }

  @override
  StartQuestionnaireResult deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new StartQuestionnaireResultBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'age':
          result.age = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'gender':
          result.gender = serializers.deserialize(value,
              specifiedType: const FullType(Gender)) as Gender;
          break;
        case 'livingSituation':
          result.livingSituation = serializers.deserialize(value,
                  specifiedType: const FullType(LivingSituation))
              as LivingSituation;
          break;
        case 'partner':
          result.partner = serializers.deserialize(value,
              specifiedType: const FullType(num)) as num;
          break;
        case 'paidWork':
          result.paidWork = serializers.deserialize(value,
              specifiedType: const FullType(num)) as num;
          break;
      }
    }

    return result.build();
  }
}

class _$StartQuestionnaireResult extends StartQuestionnaireResult {
  @override
  final int age;
  @override
  final Gender gender;
  @override
  final LivingSituation livingSituation;
  @override
  final num partner;
  @override
  final num paidWork;
  bool __hasPartner;
  bool __hasChildren;
  bool __hasOtherLivingPartners;

  factory _$StartQuestionnaireResult(
          [void Function(StartQuestionnaireResultBuilder) updates]) =>
      (new StartQuestionnaireResultBuilder()..update(updates)).build();

  _$StartQuestionnaireResult._(
      {this.age,
      this.gender,
      this.livingSituation,
      this.partner,
      this.paidWork})
      : super._() {
    if (age == null) {
      throw new BuiltValueNullFieldError('StartQuestionnaireResult', 'age');
    }
    if (gender == null) {
      throw new BuiltValueNullFieldError('StartQuestionnaireResult', 'gender');
    }
    if (livingSituation == null) {
      throw new BuiltValueNullFieldError(
          'StartQuestionnaireResult', 'livingSituation');
    }
    if (partner == null) {
      throw new BuiltValueNullFieldError('StartQuestionnaireResult', 'partner');
    }
    if (paidWork == null) {
      throw new BuiltValueNullFieldError(
          'StartQuestionnaireResult', 'paidWork');
    }
  }

  @override
  bool get hasPartner => __hasPartner ??= super.hasPartner;

  @override
  bool get hasChildren => __hasChildren ??= super.hasChildren;

  @override
  bool get hasOtherLivingPartners =>
      __hasOtherLivingPartners ??= super.hasOtherLivingPartners;

  @override
  StartQuestionnaireResult rebuild(
          void Function(StartQuestionnaireResultBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  StartQuestionnaireResultBuilder toBuilder() =>
      new StartQuestionnaireResultBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is StartQuestionnaireResult &&
        age == other.age &&
        gender == other.gender &&
        livingSituation == other.livingSituation &&
        partner == other.partner &&
        paidWork == other.paidWork;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, age.hashCode), gender.hashCode),
                livingSituation.hashCode),
            partner.hashCode),
        paidWork.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('StartQuestionnaireResult')
          ..add('age', age)
          ..add('gender', gender)
          ..add('livingSituation', livingSituation)
          ..add('partner', partner)
          ..add('paidWork', paidWork))
        .toString();
  }
}

class StartQuestionnaireResultBuilder
    implements
        Builder<StartQuestionnaireResult, StartQuestionnaireResultBuilder> {
  _$StartQuestionnaireResult _$v;

  int _age;
  int get age => _$this._age;
  set age(int age) => _$this._age = age;

  Gender _gender;
  Gender get gender => _$this._gender;
  set gender(Gender gender) => _$this._gender = gender;

  LivingSituation _livingSituation;
  LivingSituation get livingSituation => _$this._livingSituation;
  set livingSituation(LivingSituation livingSituation) =>
      _$this._livingSituation = livingSituation;

  num _partner;
  num get partner => _$this._partner;
  set partner(num partner) => _$this._partner = partner;

  num _paidWork;
  num get paidWork => _$this._paidWork;
  set paidWork(num paidWork) => _$this._paidWork = paidWork;

  StartQuestionnaireResultBuilder();

  StartQuestionnaireResultBuilder get _$this {
    if (_$v != null) {
      _age = _$v.age;
      _gender = _$v.gender;
      _livingSituation = _$v.livingSituation;
      _partner = _$v.partner;
      _paidWork = _$v.paidWork;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(StartQuestionnaireResult other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$StartQuestionnaireResult;
  }

  @override
  void update(void Function(StartQuestionnaireResultBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$StartQuestionnaireResult build() {
    final _$result = _$v ??
        new _$StartQuestionnaireResult._(
            age: age,
            gender: gender,
            livingSituation: livingSituation,
            partner: partner,
            paidWork: paidWork);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
