import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'living_situation.g.dart';

class LivingSituation extends EnumClass {
  static Serializer<LivingSituation> get serializer =>
      _$livingSituationSerializer;

  static const LivingSituation aloneWithoutChildren = _$aloneWithoutChildren;
  static const LivingSituation aloneWithChildren = _$aloneWithChildren;
  static const LivingSituation livingTogetherWithoutChildren =
      _$livingTogetherWithoutChildren;
  static const LivingSituation livingTogetherWithChildren =
      _$livingTogetherWithChildren;
  static const LivingSituation other = _$other;

  const LivingSituation._(String name) : super(name);

  static BuiltSet<LivingSituation> get values => _$values;

  static LivingSituation valueOf(String name) => _$valueOf(name);
}
