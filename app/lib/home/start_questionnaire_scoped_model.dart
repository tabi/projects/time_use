import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:sqflite/sqflite.dart';
import 'package:tbo_app/core/database/database_helper.dart';
import 'package:tbo_app/core/database/table/questionnaire_table.dart';
import 'package:tbo_app/home/start_questionnaire_result.dart';

class StartQuestionnaireModel extends Model {
  static StartQuestionnaireModel of(BuildContext context) =>
      ScopedModel.of<StartQuestionnaireModel>(context);

  Future<Database> _database() async {
    return DatabaseHelper.instance.database;
  }

  Future<QuestionnaireTable> _questionnaireTable() async {
    return QuestionnaireTable(await _database());
  }

  Future<bool> hasStartQuestionnaireEntry() async {
    final _table = await _questionnaireTable();
    return _table.hasStartQuestionnaireEntry();
  }

  Future<StartQuestionnaireResult> startQuestionnaireEntry() async {
    final _table = await _questionnaireTable();
    return _table.startQuestionnaireEntry();
  }

  Future<void> insert(StartQuestionnaireResult result) async {
    final _table = await _questionnaireTable();
    await _table.insert(result);
  }
}
