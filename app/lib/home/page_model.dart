import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class PageModel extends Model {
  DateTime _selectedDate;

  DateTime get selectedDate => _selectedDate ?? DateTime.now();
  set selectedDate(DateTime selectedDate) {
    _selectedDate = selectedDate;
    notifyListeners();
  }

  static PageModel of(BuildContext context) =>
      ScopedModel.of<PageModel>(context);
}
