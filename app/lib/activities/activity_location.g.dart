// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activity_location.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const ActivityLocation _$home = const ActivityLocation._('home');
const ActivityLocation _$work = const ActivityLocation._('work');
const ActivityLocation _$schoolStudy = const ActivityLocation._('schoolStudy');
const ActivityLocation _$other = const ActivityLocation._('other');

ActivityLocation _$valueOf(String name) {
  switch (name) {
    case 'home':
      return _$home;
    case 'work':
      return _$work;
    case 'schoolStudy':
      return _$schoolStudy;
    case 'other':
      return _$other;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<ActivityLocation> _$values =
    new BuiltSet<ActivityLocation>(const <ActivityLocation>[
  _$home,
  _$work,
  _$schoolStudy,
  _$other,
]);

Serializer<ActivityLocation> _$activityLocationSerializer =
    new _$ActivityLocationSerializer();

class _$ActivityLocationSerializer
    implements PrimitiveSerializer<ActivityLocation> {
  @override
  final Iterable<Type> types = const <Type>[ActivityLocation];
  @override
  final String wireName = 'ActivityLocation';

  @override
  Object serialize(Serializers serializers, ActivityLocation object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  ActivityLocation deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ActivityLocation.valueOf(serialized as String);
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
