import 'dart:core';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/activities/activity.dart';
import 'package:tbo_app/activities/activity_agenda.dart';
import 'package:tbo_app/activities/activity_companion_dialog.dart';
import 'package:tbo_app/activities/activity_node.dart';
import 'package:tbo_app/activities/activity_tree.dart';
import 'package:tbo_app/activities/activity_tree_scoped_model.dart';
import 'package:tbo_app/activities/activity_type.dart';
import 'package:tbo_app/activities/agendas_scoped_model.dart';
import 'package:tbo_app/activities/select_activity_page.dart';
import 'package:tbo_app/activities/translate_page_activity_entry.dart';
import 'package:tbo_app/core/app_colors.dart';
import 'package:tbo_app/core/app_icons.dart';
import 'package:tbo_app/core/configuration.dart';
import 'package:tbo_app/core/constant.dart';
import 'package:tbo_app/core/extensions.dart';
import 'package:tbo_app/core/input_type.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/open_input/search_widget.dart';

class TimeActivityEntryForm extends StatefulWidget {
  final ActivityAgendas agendas;
  final DateTime chosenDateTime;

  const TimeActivityEntryForm({Key key, this.agendas, this.chosenDateTime})
      : super(key: key);

  @override
  _TimeActivityEntryFormState createState() => _TimeActivityEntryFormState();
}

class _TimeActivityEntryFormState extends State<TimeActivityEntryForm> {
  ActivityAgendas _agendas;
  TranslatePageActivityEntry translate;
  ScrollController pageScrollController = ScrollController();

  bool get isChanged {
    if (!widget.agendas.main.selectedActivity
        .isEqualTo(_agendas.main.selectedActivity)) {
      return true;
    }

    if (widget.agendas.side.activities.length !=
        _agendas.side.activities.length) {
      return true;
    } else {
      for (int i = 0; i < widget.agendas.side.activities.length; i++) {
        if (!widget.agendas.side.activities[i]
            .isEqualTo(_agendas.side.activities[i])) {
          return true;
        }
      }
    }

    return false;
  }

  bool get isComplete {
    return _agendas.main.selectedActivity.activityNode != null &&
        (_agendas.main.selectedActivity.companion?.isNotEmpty ?? false);
  }

  bool isSaved = false;
  bool isSaveClicked = false;

  static final _darkBlueStyle = TextStyle(
      color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
      fontSize: 18.0 * f,
      height: 1.25,
      fontWeight: FontWeight.w600);

  static final _darkBlueStyleHeader = TextStyle(
      color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
      fontSize: 18.0 * f,
      height: 1.25,
      fontWeight: FontWeight.w700);

  static const _whiteStyle = TextStyle(
      color: Colors.white,
      fontSize: 18.0,
      height: 1.25,
      fontWeight: FontWeight.w600);

  @override
  void initState() {
    super.initState();
    _agendas =
        ActivityAgendas(widget.agendas.main.copy, widget.agendas.side.copy);
    translate = TranslatePageActivityEntry.instance;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        child: Material(
          type: MaterialType.transparency,
          child: Container(
            color: Colors.white,
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.91,
                  child: SingleChildScrollView(
                    controller: pageScrollController,
                    child: Column(
                      children: <Widget>[
                        _myAppBar(context),
                        SizedBox(height: 20 * y),
                        _line1aMainActivityHeader(),
                        SizedBox(height: 16 * y),
                        _line1bSelectActivity(),
                        SizedBox(height: 12 * y),
                        _line1cPickTime(context),
                        SizedBox(height: 30 * y),
                        _line2aParticipantsHeader(),
                        SizedBox(height: 16 * y),
                        _line2bParticipants(),
                        SizedBox(height: 30 * y),
                        _line3aSideActivityHeader(),
                        _line3bSideActivities(),
                      ],
                    ),
                  ),
                ),
                _line8bottomBar(context),
              ],
            ),
          ),
        ));
  }

  Widget _myAppBar(BuildContext context) {
    final _dateText =
        DateFormat('E d MMMM y', 'nl').format(widget.chosenDateTime);
    final _firstLetter = _dateText.substring(0, 1).toLowerCase();
    final _date = '$_firstLetter${_dateText.substring(1)}';
    return AppBar(
      title: Text('${translate.general.activity} $_date'),
      leading: SizedBox(
        width: 50,
        child: InkWell(
          onTap: () {
            _leavePage(context);
          },
          child: Icon(AppIcons.appIconData(enumIcon.ICO_ARROWLEFT), size: 30),
        ),
      ),
    );
  }

  Future<void> _leavePage(BuildContext context) async {
    if (!isChanged) {
      Navigator.of(context).pop();
    } else {
      showDialog<int>(
        context: context,
        routeSettings: const RouteSettings(name: 'LeavePageDialog'),
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(translate.general.warning),
            content: Text(translate.saveQuestion),
            actions: [
              FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  Navigator.of(context).pop();
                },
                child: Text(translate.general.no),
              ),
              FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text(translate.general.cancel),
              ),
              FlatButton(
                onPressed: () async {
                  isSaveClicked = true;
                  Navigator.of(context).pop();
                  await _saveActivity(context, actionAfterwards: () {
                    Navigator.of(context).pop();
                  });
                },
                child: Text(translate.general.yes),
              ),
            ],
          );
        },
      );
    }
  }

  Future<void> _saveActivity(BuildContext context,
      {VoidCallback actionAfterwards}) async {
    final AgendasScopedModel activitiesModel = AgendasScopedModel.of(context);
    if (!isComplete) {
      setState(() {
        _showNotCompleteWarningDialog();
      });
      return false;
    }
    _agendas.main.replaceActivity(_agendas.main.selectedActivity);
    _agendas.side.selectedActivity = null;
    activitiesModel.replaceAgendas(_agendas);
    await activitiesModel.save();
    actionAfterwards?.call();
  }

  Widget _line1aMainActivityHeader() {
    return Center(
      child: Text(
        'Wat heeft u gedaan?',
        style: _darkBlueStyleHeader,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }

  Widget _line1bSelectActivity() {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () async {
        final _activityNode = await _launchActivityPickerPage(
            context, ActivityTreeScopedModel.of(context).mainActivityTree);
        if (_activityNode != null) {
          setState(() {
            final _mainActivity =
                _agendas.main.selectedActivity.rebuild((b) => b
                  ..activityNode = _activityNode.toBuilder()
                  ..activity = _activityNode.activity);

            _agendas.main.activities.replace(_agendas.main.selectedActivity,
                _mainActivity.rebuild((b) => b));

            _agendas.main.selectedActivity = _mainActivity.rebuild((b) => b);
          });
        }
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 20.0 * x),
        height: _agendas.main.selectedActivity.activityNode != null
            ? 90 * y
            : 45 * y,
        decoration: BoxDecoration(
          border: Border.all(
            color: _agendas.main.selectedActivity.activityNode == null &&
                    isSaveClicked
                ? AppColors.appColor(enumColor.COL_RED)
                : AppColors.appColor(enumColor.COL_GRAY_BORDER),
          ),
          borderRadius: BorderRadius.circular(5),
          color: Colors.white,
        ),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 42 * y,
              child: Row(
                children: <Widget>[
                  SizedBox(
                    width: 40 * x,
                    child: Center(
                      child: AppIcons.appIcon(
                          enumIcon.ICO_RUNNING,
                          AppColors.appColor(enumColor.COL_BLUE_DARKER),
                          22 * f),
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerLeft,
                    child: SizedBox(
                      width: 300 * x,
                      child: Text(
                        _agendas.main.selectedActivity.activityNode != null
                            ? _agendas.main.selectedActivity
                                .activity //JJAN _activity.activityNode.activity
                            : translate.selectActivity,
                        style: _darkBlueStyle,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  )
                ],
              ),
            ),
            if (_agendas.main.selectedActivity.activityNode == null)
              const SizedBox()
            else
              SizedBox(
                height: _agendas.main.selectedActivity.activityNode != null
                    ? 37
                    : 0,
                child: Row(
                  children: <Widget>[
                    SizedBox(
                      width: 40 * x,
                      child: Center(
                        child: AppIcons.appIconFromIconCode(
                          ActivityTreeScopedModel.of(context)
                              .mainActivityTree
                              .activityTree()
                              .firstWhere((element) {
                            return _agendas
                                .main.selectedActivity.activityNode.nodeCode
                                .startsWith(element.nodeCode);
                          }).iconCode,
                          AppColors.appColorFromColorCode(
                              ActivityTreeScopedModel.of(context)
                                  .mainActivityTree
                                  .activityTree()
                                  .firstWhere((element) {
                            return _agendas
                                .main.selectedActivity.activityNode.nodeCode
                                .startsWith(element.nodeCode);
                          }).colorCode),
                          20 * f,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: SizedBox(
                        width: 300,
                        child: Text(
                          _agendas.main.selectedActivity.activityNode != null
                              ? ActivityTreeScopedModel.of(context)
                                  .mainActivityTree
                                  .activityTree()
                                  .firstWhere((element) {
                                  return _agendas.main.selectedActivity
                                      .activityNode.nodeCode
                                      .startsWith(element.nodeCode);
                                }).activity
                              : Constant.emptyString,
                          style: _darkBlueStyle,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    )
                  ],
                ),
              )
          ],
        ),
      ),
    );
  }

  Future<ActivityNode> _launchActivityPickerPage(
      BuildContext context, ActivityTree activityTree) async {
    final InputType inputType = Configuration.of(context).inputType;
    final SelectActivityPageResult result = await Navigator.push(
        context,
        MaterialPageRoute(
          settings: RouteSettings(
              name: inputType == InputType.closed
                  ? 'SelectActivityPage'
                  : 'SearchWidget'),
          builder: (context) => inputType == InputType.closed
              ? SelectActivityPage(
                  activityTree: activityTree,
                  activityNode: _agendas.main.selectedActivity.activityNode)
              : SearchWidget(),
        ));
    return result.activityNode;
  }

  Widget _line1cPickTime(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20.0 * x),
      height: 45 * y,
      decoration: BoxDecoration(
        border: Border.all(
          color: AppColors.appColor(enumColor.COL_GRAY_DARKER),
        ),
        borderRadius: BorderRadius.circular(5),
        color: Colors.white,
      ),
      child: _plusMinTimePick(context),
    );
  }

  Widget _plusMinTimePick(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        SizedBox(
          width: 40 * x,
          child: Center(
            child: Icon(AppIcons.appIconData(enumIcon.ICO_SOLID_CLOCK),
                size: 20 * f,
                color: AppColors.appColor(enumColor.COL_BLUE_DARKER)),
          ),
        ),
        _decreaseStartDateArrow(),
        const Spacer(),
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () async {
            final _timeOfDay = await _activityTimePicker(
                initialTime: TimeOfDay(
                    hour:
                        _agendas.main.selectedActivity.startDate.toLocal().hour,
                    minute: _agendas.main.selectedActivity.startDate
                        .toLocal()
                        .minute));
            if (_timeOfDay != null) {
              final DateTime _newStartDate =
                  _pickedStartDate(roundTimeOfDay(_timeOfDay));
              if (_newStartDate.isBefore(_agendas.main.minStartDate()) ||
                  _newStartDate.isAfter(_agendas.main.maxStartDate())) {
                //NOOP
              } else {
                setState(() {
                  _agendas.main.setStartDate(_newStartDate);
                });
              }
            }
          },
          child: Text(
            DateFormat('HH:mm')
                .format(_agendas.main.selectedActivity.startDate.toLocal()),
            style: _darkBlueStyle,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        const Spacer(),
        _increaseStartDateArrow(),
        const Spacer(),
        Text(
          translate.until,
          style: _darkBlueStyle,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
        const Spacer(),
        _decreaseEndDateArrow(),
        const Spacer(),
        GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: () async {
            final _timeOfDay = await _activityTimePicker(
                initialTime: TimeOfDay(
                    hour: _agendas.main.selectedActivity.endDate.toLocal().hour,
                    minute: _agendas.main.selectedActivity.endDate
                        .toLocal()
                        .minute));
            if (_timeOfDay != null) {
              final DateTime _newEndDate =
                  _pickedEndDate(roundTimeOfDay(_timeOfDay));
              if (_newEndDate.isBefore(_agendas.main.minEndDate()) ||
                  _newEndDate.isAfter(_agendas.main.maxEndDate())) {
                //NOOP
              } else {
                setState(() {
                  _agendas.main.setEndDate(_newEndDate);
                });
              }
            }
          },
          child: Text(
            DateFormat('HH:mm')
                .format(_agendas.main.selectedActivity.endDate.toLocal()),
            style: _darkBlueStyle,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        const Spacer(),
        _increaseEndDateArrow(),
      ],
    );
  }

  TimeOfDay roundTimeOfDay(TimeOfDay timeOfDay) {
    if (timeOfDay.minute % 10 < 5) {
      return TimeOfDay(
          hour: timeOfDay.hour,
          minute: timeOfDay.minute - timeOfDay.minute % 10);
    }
    if (timeOfDay.minute < 50) {
      return TimeOfDay(
          hour: timeOfDay.hour,
          minute: 10 + timeOfDay.minute - timeOfDay.minute % 10);
    } else {
      return TimeOfDay(hour: 1 + timeOfDay.hour, minute: 0);
    }
  }

  DateTime _pickedStartDate(TimeOfDay timeOfDay) {
    DateTime _dt = DateTime(
        widget.chosenDateTime.year,
        widget.chosenDateTime.month,
        widget.chosenDateTime.day,
        timeOfDay.hour,
        timeOfDay.minute);
    if (timeOfDay.hour < 4) {
      _dt = _dt.add(const Duration(days: 1));
    }
    return _dt;
  }

  DateTime _pickedEndDate(TimeOfDay timeOfDay) {
    DateTime _dt = DateTime(
        widget.chosenDateTime.year,
        widget.chosenDateTime.month,
        widget.chosenDateTime.day,
        timeOfDay.hour,
        timeOfDay.minute);
    if (timeOfDay.hour <= 4) {
      _dt = _dt.add(const Duration(hours: 24));
    }
    return _dt;
  }

  Widget _decreaseStartDateArrow() {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        if (_agendas.main.canDecreaseStartDate()) {
          setState(() {
            _agendas.main.decreaseStartDate();
          });
        }
      },
      child: SizedBox(
        width: 45 * x,
        child: Center(
          child: Icon(
            AppIcons.appIconData(enumIcon.ICO_CHEVRONLEFT),
            size: 18 * f,
            color: _agendas.main.canDecreaseStartDate()
                ? AppColors.appColor(enumColor.COL_BLUE_DARKER)
                : AppColors.appColor(enumColor.COL_GRAY_BORDER),
          ),
        ),
      ),
    );
  }

  Widget _increaseStartDateArrow() {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        if (_agendas.main.canIncreaseStartDate()) {
          setState(() {
            _agendas.main.increaseStartDate();
          });
        }
      },
      child: SizedBox(
        width: 45 * x,
        child: Center(
          child: Icon(
            AppIcons.appIconData(enumIcon.ICO_CHEVRONRIGHT),
            size: 18 * f,
            color: _agendas.main.canIncreaseStartDate()
                ? AppColors.appColor(enumColor.COL_BLUE_DARKER)
                : AppColors.appColor(enumColor.COL_GRAY_BORDER),
          ),
        ),
      ),
    );
  }

  Widget _decreaseEndDateArrow() {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        if (_agendas.main.canDecreaseEndDate()) {
          setState(() {
            _agendas.main.decreaseEndDate();
          });
        }
      },
      child: SizedBox(
        width: 45 * x,
        child: Center(
          child: Icon(
            AppIcons.appIconData(enumIcon.ICO_CHEVRONLEFT),
            size: 18 * f,
            color: _agendas.main.canDecreaseEndDate()
                ? AppColors.appColor(enumColor.COL_BLUE_DARKER)
                : AppColors.appColor(enumColor.COL_GRAY_BORDER),
          ),
        ),
      ),
    );
  }

  Widget _increaseEndDateArrow() {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        if (_agendas.main.canIncreaseEndDate()) {
          setState(() {
            _agendas.main.increaseEndDate();
          });
        }
      },
      child: SizedBox(
        width: 45 * x,
        child: Center(
          child: Icon(
            AppIcons.appIconData(enumIcon.ICO_CHEVRONRIGHT),
            size: 18 * f,
            color: _agendas.main.canIncreaseEndDate()
                ? AppColors.appColor(enumColor.COL_BLUE_DARKER)
                : AppColors.appColor(enumColor.COL_GRAY_BORDER),
          ),
        ),
      ),
    );
  }

  Future<TimeOfDay> _activityTimePicker({TimeOfDay initialTime}) async {
    return showTimePicker(
        context: context,
        initialTime: initialTime,
        routeSettings: const RouteSettings(name: 'ActivityTimePicker'),
        builder: (BuildContext context, Widget child) {
          return MediaQuery(
            data: MediaQuery.of(context).copyWith(alwaysUse24HourFormat: true),
            child: child,
          );
        });
  }

  Widget _line2aParticipantsHeader() {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            translate.togetherWith,
            style: _darkBlueStyleHeader,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          SizedBox(width: 10 * x),
          Container(
            color: Colors.transparent,
            width: 40 * x,
            child: GestureDetector(
              onTap: () {
                _showWhoInfoDialog(context);
              },
              child: AppIcons.appIcon(enumIcon.ICO_INFO,
                  AppColors.appColor(enumColor.COL_BLUE_DARKER), 25.0 * f),
            ),
          ),
        ],
      ),
    );
  }

  void _showWhoInfoDialog(BuildContext context) {
    showDialog(
      context: context,
      routeSettings: const RouteSettings(name: 'WhoInfoDialog'),
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AppIcons.appIcon(enumIcon.ICO_INFO,
                        AppColors.appColor(enumColor.COL_BLUE_DARKER), 25.0),
                    SizedBox(
                      height: 20 * y,
                    ),
                    Text(
                      translate.whoInfo,
                      style: TextStyle(
                        color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
                        fontSize: 18.0 * f,
                        height: 1.25,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(
                      height: 20 * y,
                    ),
                    Text(
                      '${translate.general.translation("explanation")}: ${translate.whoInfoExtra}',
                      style: TextStyle(
                        color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
                        fontSize: 18.0 * f,
                        height: 1.25,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(
                      height: 20 * y,
                    ),
                    Row(
                      children: <Widget>[
                        const Spacer(),
                        FlatButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            translate.general.ok,
                            style: TextStyle(
                              color: Colors.blue,
                              fontSize: 18.0 * f,
                              height: 1.25,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }

  Widget _line2bParticipants() {
    return InkWell(
      onTap: () async {
        final _result = await showDialog<Activity>(
          context: context,
          routeSettings: const RouteSettings(name: 'ParticipantsDialog'),
          builder: (BuildContext context) {
            return _line2bParticipantsDialog();
          },
        );
        if (_result != null) {
          setState(() {
            final _mainActivity = _agendas.main.selectedActivity
                .rebuild((b) => b..companion = _result.companion?.toBuilder());

            _agendas.main.activities.replace(_agendas.main.selectedActivity,
                _mainActivity.rebuild((b) => b));

            _agendas.main.selectedActivity = _mainActivity.rebuild((b) => b);
          });
        }
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 20.0 * x),
        decoration: BoxDecoration(
          border: Border.all(
            color:
                (_agendas.main.selectedActivity.companion?.isEmpty ?? true) &&
                        isSaveClicked
                    ? AppColors.appColor(enumColor.COL_RED)
                    : AppColors.appColor(enumColor.COL_GRAY_BORDER),
          ),
          borderRadius: BorderRadius.circular(5),
          color: Colors.white,
        ),
        child: SizedBox(
          height: 45 * y,
          child: Row(
            children: <Widget>[
              SizedBox(
                width: 40 * x,
                child: Center(
                  child: (_agendas.main.selectedActivity.companion?.isEmpty ??
                          true)
                      ? Icon(
                          AppIcons.appIconData(enumIcon.ICO_MALE),
                          size: 22 * f,
                          color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
                        )
                      : _agendas.main.selectedActivity.companion[0].imageIcon(),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: SizedBox(
                  width: 300 * x,
                  child: (_agendas.main.selectedActivity.companion?.isEmpty ??
                          true)
                      ? Text('Meerdere keuzes mogelijk', style: _darkBlueStyle)
                      : Text(
                          _agendas.main.selectedActivity.companion[0].label +
                              (_agendas.main.selectedActivity.companion.length >
                                      1
                                  ? ' ...'
                                  : ''),
                          style: _darkBlueStyle),
                ),
              ),
              Align(
                  alignment: Alignment.centerRight,
                  child: Icon(
                    Icons.arrow_drop_down,
                    color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
                  ))
            ],
          ),
        ),
      ),
    );
  }

  Widget _line2bParticipantsDialog() {
    return ActivityCompanionDialog(
      activity: _agendas.main.selectedActivity.rebuild((b) => b),
    );
  }

  Widget _line3aSideActivityHeader() {
    return Center(
      child: Text(
        'Wat deed u daarnaast?',
        style: _darkBlueStyleHeader,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }

  Widget _line3bSideActivities() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5.0 * x),
      child: Column(
        children: <Widget>[
          _sideActivityListView(),
          _line6bAddSideActivity(),
          SizedBox(height: 16 * y),
        ],
      ),
    );
  }

  Widget _sideActivityListView() {
    return ScopedModelDescendant<AgendasScopedModel>(
        builder: (context, child, activitiesModel) {
      final _sideActivities =
          _agendas.side.overlappingActivities(_agendas.main.selectedActivity);
      return Column(
        children: <Widget>[
          ListView.separated(
            physics: const NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            separatorBuilder: (context, index) => SizedBox(height: 10 * y),
            itemCount: _sideActivities.length,
            itemBuilder: (context, index) {
              return _sideActivityContainer(
                  _sideActivities[index], activitiesModel);
            },
          ),
          if (_sideActivities.isNotEmpty) SizedBox(height: 10 * y),
        ],
      );
    });
  }

  Widget _sideActivityContainer(
      Activity sideActivity, AgendasScopedModel activitiesModel) {
    return Container(
      height: 45 * y,
      padding: EdgeInsets.only(bottom: 10 * y),
      margin: EdgeInsets.symmetric(horizontal: 15.0 * x),
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.transparent,
          width: 1.5,
        ),
        borderRadius: BorderRadius.circular(5),
        color: sideActivity.activityNode.colorActivity(),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          SizedBox(
            width: 10 * x,
          ),
          Expanded(
            child: GestureDetector(
              onTap: () async {
                _agendas.side.selectActivity(sideActivity);
                _agendas.side.resetDrag();
                final _activityNode = await _launchActivityPickerPage(context,
                    ActivityTreeScopedModel.of(context).sideActivityTree);
                if (_activityNode != null) {
                  setState(() {
                    final _sideActivity = sideActivity.rebuild(
                        (b) => b.activityNode = _activityNode.toBuilder());
                    _agendas.side.activities
                        .replace(sideActivity, _sideActivity);
                  });
                }
              },
              child: Text(
                sideActivity.activityNode.activity,
                style: _whiteStyle,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
          SizedBox(
            width: 10 * x,
          ),
          GestureDetector(
            onTap: () async {
              final _timeOfDay = await _activityTimePicker(
                  initialTime: TimeOfDay(
                      hour: sideActivity.startDate.toLocal().hour,
                      minute: sideActivity.startDate.toLocal().minute));
              _agendas.side.selectActivity(sideActivity);
              _agendas.side.resetDrag();
              if (_timeOfDay != null) {
                final DateTime _newStartDate =
                    _pickedStartDate(roundTimeOfDay(_timeOfDay));
                if (_newStartDate.isBefore(_agendas.side.minStartDate()) ||
                    _newStartDate.isAfter(_agendas.side.maxStartDate())) {
                  //NOOP
                } else {
                  setState(() {
                    _agendas.side.setStartDate(_newStartDate);
                  });
                }
              }
            },
            child: Text(
              DateFormat('HH:mm').format(sideActivity.startDate.toLocal()),
              style: _whiteStyle,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          const Text(
            ' - ',
            style: _whiteStyle,
            maxLines: 1,
            overflow: TextOverflow.ellipsis,
          ),
          GestureDetector(
            onTap: () async {
              final _timeOfDay = await _activityTimePicker(
                  initialTime: TimeOfDay(
                      hour: sideActivity.endDate.toLocal().hour,
                      minute: sideActivity.endDate.toLocal().minute));
              _agendas.side.selectActivity(sideActivity);
              _agendas.side.resetDrag();
              if (_timeOfDay != null) {
                final DateTime _newEndDate =
                    _pickedEndDate(roundTimeOfDay(_timeOfDay));
                if (_newEndDate.isBefore(_agendas.side.minEndDate()) ||
                    _newEndDate.isAfter(_agendas.side.maxEndDate())) {
                  //NOOP
                } else {
                  setState(() {
                    _agendas.side.setEndDate(_newEndDate);
                  });
                }
              }
            },
            child: Text(
              DateFormat('HH:mm').format(sideActivity.endDate.toLocal()),
              style: _whiteStyle,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ),
          SizedBox(
            width: 10 * x,
          ),
          GestureDetector(
              onTap: () async {
                _agendas.side.selectActivity(sideActivity);
                _agendas.side.resetDrag();
                _showDeleteWarningDialog(context);
              },
              child: const SizedBox(
                width: 40,
                child: Icon(Icons.delete, color: Colors.white, size: 25),
              )),
        ],
      ),
    );
  }

  void _showDeleteWarningDialog(BuildContext context) {
    showDialog(
      context: context,
      routeSettings: const RouteSettings(name: 'DeleteWarningDialog'),
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(translate.general.warning),
          content: Text(
              'Wilt U nevenactiviteit: \n\n${_agendas.side.selectedActivity.activityNode.activity}\n\nverwijderen?'),
          actions: [
            FlatButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(translate.general.cancel),
            ),
            ScopedModelDescendant<AgendasScopedModel>(
                builder: (context, child, activitiesModel) {
              return FlatButton(
                onPressed: () {
                  setState(() {
                    _agendas.side.activities
                        .remove(_agendas.side.selectedActivity);
                    _agendas.side.deselect();
                  });
                  Navigator.of(context).pop();
                },
                child: Text(translate.general.ok),
              );
            }),
          ],
        );
      },
    );
  }

  Widget _line6bAddSideActivity() {
    return ScopedModelDescendant<AgendasScopedModel>(
        builder: (context, child, activitiesModel) {
      return GestureDetector(
        onTap: () async {
          if (_canAddSideActivity(activitiesModel)) {
            await _createSideActivity(context, activitiesModel);
            setState(() {});
          } else {
            _showCanNotAddSideActivity();
          }
        },
        child: Container(
          margin: EdgeInsets.symmetric(horizontal: 15.0 * x),
          height: 47 * y,
          child: DottedBorder(
            color: AppColors.appColor(enumColor.COL_GRAY_BORDER),
            strokeWidth: 1.4,
            dashPattern: const [5, 5],
            radius: const Radius.circular(4),
            strokeCap: StrokeCap.round,
            borderType: BorderType.RRect,
            child: Row(
              children: <Widget>[
                SizedBox(
                  width: 10 * x,
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: SizedBox(
                    width: 300 * x,
                    child: Text(
                      translate.addSideActivities,
                      style: _darkBlueStyle,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
                const Spacer(),
                SizedBox(
                  width: 40 * x,
                  child: Center(
                    child: Icon(
                      AppIcons.appIconData(enumIcon.ICO_ADDBOX),
                      color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
    });
  }

  bool _canAddSideActivity(AgendasScopedModel activitiesModel) {
    return endDateLastSideActivityOfActivity()
        .isBefore(_agendas.main.selectedActivity.endDate);
  }

  DateTime endDateLastSideActivityOfActivity() {
    final _sideActivitiesForActivity =
        _agendas.side.overlappingActivities(_agendas.main.selectedActivity);
    if (_sideActivitiesForActivity.isEmpty) {
      return _agendas.main.selectedActivity.startDate;
    } else {
      _sideActivitiesForActivity
          .sort((lhs, rhs) => lhs.endDate.compareTo(rhs.endDate));
      return _sideActivitiesForActivity.last.endDate;
    }
  }

  Future<void> _showCanNotAddSideActivity() async {
    showDialog<int>(
      context: context,
      routeSettings: const RouteSettings(name: 'CanNotAddSideActivityDialog'),
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(translate.general.warning),
          content: Text(translate.noMoreSideactivities),
          actions: [
            FlatButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(translate.general.ok),
            ),
          ],
        );
      },
    );
  }

  Future<void> _createSideActivity(
      BuildContext context, AgendasScopedModel activitiesModel) async {
    final _activityNode = await _launchActivityPickerPage(
        context, ActivityTreeScopedModel.of(context).sideActivityTree);
    if (_activityNode != null) {
      final _startTime = TimeOfDay(
          hour: endDateLastSideActivityOfActivity().toUtc().toLocal().hour,
          minute: endDateLastSideActivityOfActivity().toUtc().toLocal().minute);
      final _endTime = TimeOfDay(
          hour: _agendas.main.selectedActivity.endDate.toLocal().hour,
          minute: _agendas.main.selectedActivity.endDate.toLocal().minute);
      DateTime _newStartDate = _pickedStartDate(roundTimeOfDay(_startTime));
      DateTime _newEndDate = _pickedStartDate(roundTimeOfDay(_endTime));

      if (_newStartDate.isBefore(_agendas.side.minStartDate()) ||
          _newStartDate.isAfter(_agendas.side.maxStartDate()) ||
          _newEndDate.isBefore(_agendas.side.minEndDate()) ||
          _newEndDate.isAfter(_agendas.side.maxEndDate())) {
        _newStartDate = endDateLastSideActivityOfActivity();
        _newEndDate = _agendas.main.selectedActivity.endDate;
      }

      final _sideActivity = Activity((b) => b
        ..type = ActivityType.side
        ..startDate = _newStartDate.toUtc()
        ..endDate = _newEndDate.toUtc()
        ..activityNode = _activityNode.toBuilder());
      _agendas.side.activities.add(_sideActivity);
    }
  }

  Widget _line8bottomBar(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10.0),
      height: 40,
      decoration: BoxDecoration(
        border: Border.all(
          color: Colors.transparent,
          width: 1.5,
        ),
        borderRadius: BorderRadius.circular(5),
        color: Colors.white,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          const Spacer(),
          GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () async {
              setState(() {
                isSaveClicked = true;
              });
              await _saveActivity(context, actionAfterwards: () {
                Navigator.of(context).pop();
              });
            },
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 10.0),
              height: 40,
              decoration: BoxDecoration(
                border: Border.all(
                  color: isComplete ? Colors.blue : Colors.grey,
                  width: 1.5,
                ),
                borderRadius: BorderRadius.circular(5),
                color: isComplete ? Colors.blue : Colors.grey,
              ),
              child: Row(
                children: <Widget>[
                  const SizedBox(
                    width: 20,
                  ),
                  Center(
                    child: Text(
                      translate.general.save,
                      style: const TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          height: 1.25,
                          fontWeight: FontWeight.w600),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                ],
              ),
            ),
          ),
          const Spacer(),
        ],
      ),
    );
  }

  Future<void> _showNotCompleteWarningDialog() async {
    showDialog<int>(
      context: context,
      routeSettings: const RouteSettings(name: 'NotCompleteWarningDialog'),
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(translate.general.warning),
          content: Text(translate.missingData),
          actions: [
            FlatButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(translate.general.ok),
            ),
          ],
        );
      },
    );
  }
}
