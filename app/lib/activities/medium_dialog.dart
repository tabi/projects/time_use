import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/activities/medium.dart';
import 'package:tbo_app/activities/medium_dialog_item.dart';
import 'package:tbo_app/home/start_questionnaire_result.dart';
import 'package:tbo_app/home/start_questionnaire_scoped_model.dart';

class MediumDialog extends StatelessWidget {
  const MediumDialog({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      child: ScopedModelDescendant<StartQuestionnaireModel>(
          builder: (context, child, startQuestionnaireModel) {
        return FutureBuilder<StartQuestionnaireResult>(
            future: startQuestionnaireModel.startQuestionnaireEntry(),
            builder: (context, snapshot) {
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: ListView(
                  children: <Widget>[
                    MediumDialogItem(
                      medium: Medium.desktop,
                      onTap: _mediumSelected,
                    ),
                    MediumDialogItem(
                      medium: Medium.laptop,
                      onTap: _mediumSelected,
                    ),
                    MediumDialogItem(
                      medium: Medium.smartphone,
                      onTap: _mediumSelected,
                    ),
                    MediumDialogItem(
                      medium: Medium.television,
                      onTap: _mediumSelected,
                    ),
                    MediumDialogItem(
                      medium: Medium.console,
                      onTap: _mediumSelected,
                    ),
                    MediumDialogItem(
                      medium: Medium.tablet,
                      onTap: _mediumSelected,
                    ),
                    MediumDialogItem(
                      medium: Medium.carRadio,
                      onTap: _mediumSelected,
                    ),
                    MediumDialogItem(
                      medium: Medium.paper,
                      onTap: _mediumSelected,
                    ),
                    MediumDialogItem(
                      medium: Medium.eReader,
                      onTap: _mediumSelected,
                    ),
                    MediumDialogItem(
                      medium: Medium.handheld,
                      onTap: _mediumSelected,
                    ),
                    MediumDialogItem(
                      medium: Medium.radio,
                      onTap: _mediumSelected,
                    ),
                    MediumDialogItem(
                      medium: Medium.phone,
                      onTap: _mediumSelected,
                    ),
                    MediumDialogItem(
                      medium: Medium.portableRadio,
                      onTap: _mediumSelected,
                    ),
                    MediumDialogItem(
                      medium: Medium.none,
                      onTap: _mediumSelected,
                    ),
                  ],
                ),
              );
            });
      }),
    );
  }

  void _mediumSelected(BuildContext context, Medium medium) {
    Navigator.of(context).pop(medium);
  }
}
