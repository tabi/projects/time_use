import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'medium.g.dart';

class Medium extends EnumClass {
  static Serializer<Medium> get serializer => _$mediumSerializer;

  static const Medium desktop = _$desktop;
  static const Medium laptop = _$laptop;
  static const Medium smartphone = _$smartphone;
  static const Medium television = _$television;
  static const Medium console = _$console;
  static const Medium tablet = _$tablet;
  static const Medium carRadio = _$carRadio;
  static const Medium paper = _$paper;
  static const Medium eReader = _$eReader;
  static const Medium handheld = _$handheld;
  static const Medium radio = _$radio;
  static const Medium phone = _$phone;
  static const Medium portableRadio = _$portableRadio;
  static const Medium none = _$none;

  const Medium._(String name) : super(name);

  static BuiltSet<Medium> get values => _$values;

  static Medium valueOf(String name) => _$valueOf(name);
}
