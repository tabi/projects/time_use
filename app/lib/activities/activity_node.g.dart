// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activity_node.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ActivityNode> _$activityNodeSerializer =
    new _$ActivityNodeSerializer();

class _$ActivityNodeSerializer implements StructuredSerializer<ActivityNode> {
  @override
  final Iterable<Type> types = const [ActivityNode, _$ActivityNode];
  @override
  final String wireName = 'ActivityNode';

  @override
  Iterable<Object> serialize(Serializers serializers, ActivityNode object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'parentId',
      serializers.serialize(object.parentId,
          specifiedType: const FullType(int)),
      'childIds',
      serializers.serialize(object.childIds,
          specifiedType:
              const FullType(BuiltList, const [const FullType(int)])),
      'nodeCode',
      serializers.serialize(object.nodeCode,
          specifiedType: const FullType(String)),
      'nodeType',
      serializers.serialize(object.nodeType,
          specifiedType: const FullType(String)),
      'iconCode',
      serializers.serialize(object.iconCode,
          specifiedType: const FullType(String)),
      'colorCode',
      serializers.serialize(object.colorCode,
          specifiedType: const FullType(String)),
      'activityCode',
      serializers.serialize(object.activityCode,
          specifiedType: const FullType(String)),
      'activity',
      serializers.serialize(object.activity,
          specifiedType: const FullType(String)),
      'description',
      serializers.serialize(object.description,
          specifiedType: const FullType(String)),
      'selected',
      serializers.serialize(object.selected,
          specifiedType: const FullType(bool)),
      'visible',
      serializers.serialize(object.visible,
          specifiedType: const FullType(bool)),
      'open',
      serializers.serialize(object.open, specifiedType: const FullType(bool)),
    ];

    return result;
  }

  @override
  ActivityNode deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ActivityNodeBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'parentId':
          result.parentId = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'childIds':
          result.childIds.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(int)]))
              as BuiltList<Object>);
          break;
        case 'nodeCode':
          result.nodeCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'nodeType':
          result.nodeType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'iconCode':
          result.iconCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'colorCode':
          result.colorCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'activityCode':
          result.activityCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'activity':
          result.activity = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'selected':
          result.selected = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'visible':
          result.visible = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case 'open':
          result.open = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }

    return result.build();
  }
}

class _$ActivityNode extends ActivityNode {
  @override
  final int parentId;
  @override
  final BuiltList<int> childIds;
  @override
  final String nodeCode;
  @override
  final String nodeType;
  @override
  final String iconCode;
  @override
  final String colorCode;
  @override
  final String activityCode;
  @override
  final String activity;
  @override
  final String description;
  @override
  final bool selected;
  @override
  final bool visible;
  @override
  final bool open;

  factory _$ActivityNode([void Function(ActivityNodeBuilder) updates]) =>
      (new ActivityNodeBuilder()..update(updates)).build();

  _$ActivityNode._(
      {this.parentId,
      this.childIds,
      this.nodeCode,
      this.nodeType,
      this.iconCode,
      this.colorCode,
      this.activityCode,
      this.activity,
      this.description,
      this.selected,
      this.visible,
      this.open})
      : super._() {
    if (parentId == null) {
      throw new BuiltValueNullFieldError('ActivityNode', 'parentId');
    }
    if (childIds == null) {
      throw new BuiltValueNullFieldError('ActivityNode', 'childIds');
    }
    if (nodeCode == null) {
      throw new BuiltValueNullFieldError('ActivityNode', 'nodeCode');
    }
    if (nodeType == null) {
      throw new BuiltValueNullFieldError('ActivityNode', 'nodeType');
    }
    if (iconCode == null) {
      throw new BuiltValueNullFieldError('ActivityNode', 'iconCode');
    }
    if (colorCode == null) {
      throw new BuiltValueNullFieldError('ActivityNode', 'colorCode');
    }
    if (activityCode == null) {
      throw new BuiltValueNullFieldError('ActivityNode', 'activityCode');
    }
    if (activity == null) {
      throw new BuiltValueNullFieldError('ActivityNode', 'activity');
    }
    if (description == null) {
      throw new BuiltValueNullFieldError('ActivityNode', 'description');
    }
    if (selected == null) {
      throw new BuiltValueNullFieldError('ActivityNode', 'selected');
    }
    if (visible == null) {
      throw new BuiltValueNullFieldError('ActivityNode', 'visible');
    }
    if (open == null) {
      throw new BuiltValueNullFieldError('ActivityNode', 'open');
    }
  }

  @override
  ActivityNode rebuild(void Function(ActivityNodeBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ActivityNodeBuilder toBuilder() => new ActivityNodeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ActivityNode &&
        parentId == other.parentId &&
        childIds == other.childIds &&
        nodeCode == other.nodeCode &&
        nodeType == other.nodeType &&
        iconCode == other.iconCode &&
        colorCode == other.colorCode &&
        activityCode == other.activityCode &&
        activity == other.activity &&
        description == other.description &&
        selected == other.selected &&
        visible == other.visible &&
        open == other.open;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc($jc(0, parentId.hashCode),
                                                childIds.hashCode),
                                            nodeCode.hashCode),
                                        nodeType.hashCode),
                                    iconCode.hashCode),
                                colorCode.hashCode),
                            activityCode.hashCode),
                        activity.hashCode),
                    description.hashCode),
                selected.hashCode),
            visible.hashCode),
        open.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ActivityNode')
          ..add('parentId', parentId)
          ..add('childIds', childIds)
          ..add('nodeCode', nodeCode)
          ..add('nodeType', nodeType)
          ..add('iconCode', iconCode)
          ..add('colorCode', colorCode)
          ..add('activityCode', activityCode)
          ..add('activity', activity)
          ..add('description', description)
          ..add('selected', selected)
          ..add('visible', visible)
          ..add('open', open))
        .toString();
  }
}

class ActivityNodeBuilder
    implements Builder<ActivityNode, ActivityNodeBuilder> {
  _$ActivityNode _$v;

  int _parentId;
  int get parentId => _$this._parentId;
  set parentId(int parentId) => _$this._parentId = parentId;

  ListBuilder<int> _childIds;
  ListBuilder<int> get childIds => _$this._childIds ??= new ListBuilder<int>();
  set childIds(ListBuilder<int> childIds) => _$this._childIds = childIds;

  String _nodeCode;
  String get nodeCode => _$this._nodeCode;
  set nodeCode(String nodeCode) => _$this._nodeCode = nodeCode;

  String _nodeType;
  String get nodeType => _$this._nodeType;
  set nodeType(String nodeType) => _$this._nodeType = nodeType;

  String _iconCode;
  String get iconCode => _$this._iconCode;
  set iconCode(String iconCode) => _$this._iconCode = iconCode;

  String _colorCode;
  String get colorCode => _$this._colorCode;
  set colorCode(String colorCode) => _$this._colorCode = colorCode;

  String _activityCode;
  String get activityCode => _$this._activityCode;
  set activityCode(String activityCode) => _$this._activityCode = activityCode;

  String _activity;
  String get activity => _$this._activity;
  set activity(String activity) => _$this._activity = activity;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  bool _selected;
  bool get selected => _$this._selected;
  set selected(bool selected) => _$this._selected = selected;

  bool _visible;
  bool get visible => _$this._visible;
  set visible(bool visible) => _$this._visible = visible;

  bool _open;
  bool get open => _$this._open;
  set open(bool open) => _$this._open = open;

  ActivityNodeBuilder();

  ActivityNodeBuilder get _$this {
    if (_$v != null) {
      _parentId = _$v.parentId;
      _childIds = _$v.childIds?.toBuilder();
      _nodeCode = _$v.nodeCode;
      _nodeType = _$v.nodeType;
      _iconCode = _$v.iconCode;
      _colorCode = _$v.colorCode;
      _activityCode = _$v.activityCode;
      _activity = _$v.activity;
      _description = _$v.description;
      _selected = _$v.selected;
      _visible = _$v.visible;
      _open = _$v.open;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ActivityNode other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ActivityNode;
  }

  @override
  void update(void Function(ActivityNodeBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ActivityNode build() {
    _$ActivityNode _$result;
    try {
      _$result = _$v ??
          new _$ActivityNode._(
              parentId: parentId,
              childIds: childIds.build(),
              nodeCode: nodeCode,
              nodeType: nodeType,
              iconCode: iconCode,
              colorCode: colorCode,
              activityCode: activityCode,
              activity: activity,
              description: description,
              selected: selected,
              visible: visible,
              open: open);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'childIds';
        childIds.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ActivityNode', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
