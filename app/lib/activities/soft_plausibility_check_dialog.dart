import 'package:flutter/material.dart';
import 'package:tbo_app/activities/translate_page_activity_agenda.dart';

class SoftPlausibilityCheckDialog extends StatelessWidget {
  const SoftPlausibilityCheckDialog({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final _translate = TranslatePageActivityAgenda.instance;
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 20,
                ),
                Text(
                  _translate.dayNotComplete,
                  style: const TextStyle(
                    color: Colors.black,
                    fontSize: 18.0,
                    height: 1.25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  _translate.closeDayQuestion,
                  style: const TextStyle(
                    color: Colors.black,
                    fontSize: 18.0,
                    height: 1.25,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: <Widget>[
                    const Spacer(),
                    FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop(true);
                      },
                      child: Text(
                        _translate.general.yes,
                        style: const TextStyle(
                          color: Colors.blue,
                          fontSize: 18.0,
                          height: 1.25,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop(false);
                      },
                      child: Text(
                        _translate.general.no,
                        style: const TextStyle(
                          color: Colors.blue,
                          fontSize: 18.0,
                          height: 1.25,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
