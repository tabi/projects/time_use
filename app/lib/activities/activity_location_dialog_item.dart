import 'package:flutter/material.dart';
import 'package:tbo_app/activities/activity_location.dart';
import 'package:tbo_app/core/app_colors.dart';
import 'package:tbo_app/core/extensions.dart';

typedef ActivityLocationDialogItemCallback = void Function(
    BuildContext context, ActivityLocation location);

class ActivityLocationDialogItem extends StatelessWidget {
  final ActivityLocation location;
  final ActivityLocationDialogItemCallback onTap;

  const ActivityLocationDialogItem({Key key, this.location, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTap(context, location);
      },
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 5),
        height: 50,
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: location.imageIcon(),
              ),
            ),
            Text(
              location.label,
              style: TextStyle(
                      color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
                      fontSize: 18.0,
                      height: 1.25,
                      fontWeight: FontWeight.w600)
                  .apply(fontSizeFactor: 0.9),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
      ),
    );
  }
}
