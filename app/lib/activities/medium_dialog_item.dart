import 'package:flutter/material.dart';
import 'package:tbo_app/activities/medium.dart';
import 'package:tbo_app/core/app_colors.dart';
import 'package:tbo_app/core/extensions.dart';

typedef MediumDialogItemCallback = void Function(
    BuildContext context, Medium medium);

class MediumDialogItem extends StatelessWidget {
  final Medium medium;
  final MediumDialogItemCallback onTap;

  const MediumDialogItem({Key key, this.medium, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        onTap(context, medium);
      },
      child: LayoutBuilder(builder: (context, constraints) {
        return Container(
          margin: const EdgeInsets.symmetric(horizontal: 5),
          height: 50,
          width: constraints.maxWidth,
          child: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: medium.imageIcon(),
                ),
              ),
              SizedBox(
                width: constraints.maxWidth - 80,
                child: Text(
                  medium.label,
                  style: TextStyle(
                          color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
                          fontSize: 18.0,
                          height: 1.25,
                          fontWeight: FontWeight.w600)
                      .apply(fontSizeFactor: 0.9),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
        );
      }),
    );
  }
}
