import 'package:tbo_app/core/translate_page_base.dart';

class TranslatePageActivityEntry extends TranslatePageBase {
  TranslatePageActivityEntry._privateConstructor();
  static final TranslatePageActivityEntry instance =
      TranslatePageActivityEntry._privateConstructor();

  @override
  void initPage() {
    pageKey = 'activityEntry';
  }

  @override
  void initKeys() {
    keys.add('selectActivity');
    keys.add('from');
    keys.add('until');
    keys.add('togetherWith');
    keys.add('moreChoicesPossible');
    keys.add('alone');
    keys.add('partner');
    keys.add('parent');
    keys.add('children');
    keys.add('housemates');
    keys.add('others');
    keys.add('addSideActivities');
    keys.add('ready');
    keys.add('saveQuestion');
    keys.add('occupiedPeriod');
    keys.add('missingData');
    keys.add('noMoreSideactivities');
    keys.add('whoInfo');
    keys.add('whoInfoExtra');
  }

  String get whoInfo {
    return translation('whoInfo');
  }

  String get whoInfoExtra {
    return translation('whoInfoExtra');
  }

  String get noMoreSideactivities {
    return translation('noMoreSideactivities');
  }

  String get missingData {
    return translation('missingData');
  }

  String get occupiedPeriod {
    return translation('occupiedPeriod');
  }

  String get saveQuestion {
    return translation('saveQuestion');
  }

  String get ready {
    return translation('ready');
  }

  String get selectActivity {
    return translation('selectActivity');
  }

  String get from {
    return translation('from');
  }

  String get until {
    return translation('until');
  }

  String get togetherWith {
    return translation('togetherWith');
  }

  String get moreChoicesPossible {
    return translation('moreChoicesPossible');
  }

  String get addSideActivities {
    return translation('addSideActivities');
  }

  String get alone {
    return translation('alone');
  }

  String get partner {
    return translation('partner');
  }

  String get parent {
    return translation('parent');
  }

  String get children {
    return translation('children');
  }

  String get housemates {
    return translation('housemates');
  }

  String get others {
    return translation('others');
  }
}
