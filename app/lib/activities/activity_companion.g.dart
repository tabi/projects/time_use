// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activity_companion.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const ActivityCompanion _$none = const ActivityCompanion._('none');
const ActivityCompanion _$partner = const ActivityCompanion._('partner');
const ActivityCompanion _$parents = const ActivityCompanion._('parents');
const ActivityCompanion _$children = const ActivityCompanion._('children');
const ActivityCompanion _$houseMates = const ActivityCompanion._('houseMates');
const ActivityCompanion _$other = const ActivityCompanion._('other');

ActivityCompanion _$valueOf(String name) {
  switch (name) {
    case 'none':
      return _$none;
    case 'partner':
      return _$partner;
    case 'parents':
      return _$parents;
    case 'children':
      return _$children;
    case 'houseMates':
      return _$houseMates;
    case 'other':
      return _$other;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<ActivityCompanion> _$values =
    new BuiltSet<ActivityCompanion>(const <ActivityCompanion>[
  _$none,
  _$partner,
  _$parents,
  _$children,
  _$houseMates,
  _$other,
]);

Serializer<ActivityCompanion> _$activityCompanionSerializer =
    new _$ActivityCompanionSerializer();

class _$ActivityCompanionSerializer
    implements PrimitiveSerializer<ActivityCompanion> {
  @override
  final Iterable<Type> types = const <Type>[ActivityCompanion];
  @override
  final String wireName = 'ActivityCompanion';

  @override
  Object serialize(Serializers serializers, ActivityCompanion object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  ActivityCompanion deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ActivityCompanion.valueOf(serialized as String);
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
