import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'activity_companion.g.dart';

class ActivityCompanion extends EnumClass {
  static Serializer<ActivityCompanion> get serializer =>
      _$activityCompanionSerializer;

  static const ActivityCompanion none = _$none;
  static const ActivityCompanion partner = _$partner;
  static const ActivityCompanion parents = _$parents;
  static const ActivityCompanion children = _$children;
  static const ActivityCompanion houseMates = _$houseMates;
  static const ActivityCompanion other = _$other;

  const ActivityCompanion._(String name) : super(name);

  static BuiltSet<ActivityCompanion> get values => _$values;

  static ActivityCompanion valueOf(String name) => _$valueOf(name);
}
