import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'activity_location.g.dart';

class ActivityLocation extends EnumClass {
  static Serializer<ActivityLocation> get serializer =>
      _$activityLocationSerializer;

  static const ActivityLocation home = _$home;
  static const ActivityLocation work = _$work;
  static const ActivityLocation schoolStudy = _$schoolStudy;
  static const ActivityLocation other = _$other;

  const ActivityLocation._(String name) : super(name);

  static BuiltSet<ActivityLocation> get values => _$values;

  static ActivityLocation valueOf(String name) => _$valueOf(name);
}
