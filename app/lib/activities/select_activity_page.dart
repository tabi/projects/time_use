import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:tbo_app/activities/activity_node.dart';
import 'package:tbo_app/activities/activity_tree.dart';
import 'package:tbo_app/activities/activity_tree_scoped_model.dart';
import 'package:tbo_app/activities/medium.dart';
import 'package:tbo_app/activities/medium_check_dialog.dart';
import 'package:tbo_app/activities/medium_dialog.dart';
import 'package:tbo_app/activities/translate_page_select_activity.dart';
import 'package:tbo_app/core/app_colors.dart';
import 'package:tbo_app/core/app_icons.dart';
import 'package:tbo_app/core/configuration.dart';
import 'package:tbo_app/core/constant.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/settings/settings.dart';

class SelectActivityPageResult {
  final ActivityNode activityNode;
  final Medium medium;

  SelectActivityPageResult(this.activityNode, this.medium);
}

class SelectActivityPage extends StatefulWidget {
  final ActivityTree activityTree;
  final ActivityNode activityNode;
  final bool includeMedium;

  const SelectActivityPage(
      {this.activityNode,
      this.includeMedium = false,
      @required this.activityTree});

  @override
  State<StatefulWidget> createState() {
    return _SelectActivityPageState();
  }
}

class _SelectActivityPageState extends State<SelectActivityPage> {
  TranslatePageSelectActivity translate;

  ScrollController _scrollController;

  @override
  void initState() {
    super.initState();
    _scrollController = ScrollController();
    translate = TranslatePageSelectActivity.instance;
    widget.activityTree.select(widget.activityNode?.activityCode ?? '');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          translate.selectActivity,
          style: TextStyle(fontWeight: FontWeight.w600, fontSize: 23.0 * f),
        ),
        leading: SizedBox(
            width: 50 * x,
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: AppIcons.appIcon(
                  enumIcon.ICO_ARROWLEFT, Colors.white, 30 * f),
            )),
      ),
      body: ListView.builder(
        padding: const EdgeInsets.only(bottom: 8.0),
        controller: _scrollController,
        itemCount: widget.activityTree.activityTree().length,
        itemBuilder: (context, index) {
          final node = widget.activityTree.activityTree()[index];
          return _selectActivityContainer(node);
        },
      ),
    );
  }

  Widget _selectActivityContainer(ActivityNode node) {
    return !node.visible
        ? Container()
        : GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTap: () {
              if (node.isOpenInput) {
                createActivityDialog(context, node).then((activity) {
                  if (activity != Constant.emptyString) {
                    setState(() {
                      widget.activityTree
                          .addCustomActivity(node.nodeCode, activity);
                      widget.activityTree.setActivityNodeProperties(
                          Configuration.of(context).inputType);
                      _returnToPreviousPage(context, node);
                      // _returnToPreviousPage(
                      //     context, node.rebuild((b) => b.activity = activity));
                    });
                  }
                });
              } else {
                setState(() {
                  if (node.open) {
                    widget.activityTree.select(node.parentCode);
                  } else if (node.childIds.isEmpty) {
                    if (widget.includeMedium) {
                      showDialog<bool>(
                              context: context,
                              builder: (context) => const MediumCheckDialog())
                          .then((includeMedium) {
                        if (includeMedium) {
                          showDialog<Medium>(
                                  context: context,
                                  builder: (context) => const MediumDialog())
                              .then((medium) {
                            if (medium != null) {
                              _returnToPreviousPage(context, node,
                                  medium: medium);
                            }
                          });
                        } else {
                          _returnToPreviousPage(context, node);
                        }
                      });
                    } else {
                      _returnToPreviousPage(context, node);
                    }
                  } else {
                    widget.activityTree.select(node.nodeCode);
                  }
                  scrollToSelection();
                });
              }
            },
            onLongPress: () {
              _showActivityDescriptionDialog(context, node);
            },
            child: Container(
              color: node.colorBackground(),
              height: heightContainerBig(node),
              child: Row(
                children: <Widget>[
                  SizedBox(width: node.widthLeftMargin()),
                  SizedBox(
                    width: node.widthActivityIcon(),
                    child: node.level != 0
                        ? Container()
                        : Icon(
                            node.iconData(),
                            color: node.colorActivity(),
                            size: 25,
                          ),
                  ),
                  SizedBox(width: node.widthLeftText()),
                  if (node.isOpenInput)
                    _editableActivity(node)
                  else
                    _clickableActivity(node),
                  SizedBox(width: node.widthRightText()),
                  SizedBox(
                    width: node.widthInfoIcon(),
                    child: node.description == ''
                        ? Container()
                        : Container(
                            color: Colors.transparent,
                            width: node.widthInfoIcon(),
                            child: GestureDetector(
                              onTap: () {
                                _showActivityDescriptionDialog(context, node);
                              },
                              child: AppIcons.appIcon(
                                  enumIcon.ICO_INFO,
                                  AppColors.appColor(enumColor.COL_BLUE_DARKER),
                                  25.0 * f),
                            ),
                          ),
                  ),
                  SizedBox(width: node.widthRightIcon()),
                  SizedBox(
                    width: node.widthOpenCloseIcon(),
                    child: node.childIds.isEmpty //.level == 2
                        ? Container()
                        : Container(
                            color: Colors.transparent,
                            width: node.widthOpenCloseIcon(),
                            child: Icon(
                              treeIcon(node),
                              size: 22 * f,
                              color:
                                  AppColors.appColor(enumColor.COL_BLUE_DARKER),
                            ),
                          ),
                  ),
                  SizedBox(width: node.widthRightMargin()),
                ],
              ),
            ),
          );
  }

  void _returnToPreviousPage(BuildContext context, ActivityNode node,
      {Medium medium}) {
    Navigator.pop(context, SelectActivityPageResult(node, medium));
  }

  void scrollToSelection() {
    double height = 0.0;
    if (Settings.instance.getSetting(enumSetting.settingMainVisibleYn) != 'Y') {
      _scrollController.jumpTo(height);
    } else {
      bool found = false;
      for (final ActivityNode node in widget.activityTree.activityTree()) {
        if (!found && node.selected) {
          found = true;
        } else if (!found) {
          if (node.visible) {
            height += 30.0 + (maxLines(node) * 22.5);
          }
        }
      }
      height = !found ? 0 : height;
      _scrollController.animateTo(height,
          duration: const Duration(seconds: 1), curve: Curves.ease);
    }
  }

  void _showActivityDescriptionDialog(BuildContext context, ActivityNode act) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AppIcons.appIcon(enumIcon.ICO_INFO,
                        AppColors.appColor(enumColor.COL_BLUE_DARKER), 25.0),
                    SizedBox(
                      height: 2 * y,
                    ),
                    Text(
                      act.activity,
                      style: TextStyle(
                        color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
                        fontSize: 18.0,
                        height: 1.25,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(
                      height: 20 * y,
                    ),
                    if (act.description == '')
                      Container()
                    else
                      Text(
                        '${translate.general.translation('explanation')}: ${act.description}',
                        style: TextStyle(
                          color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
                          fontSize: 18.0 * f,
                          height: 1.25,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    SizedBox(
                      height: 20 * y,
                    ),
                    Row(
                      children: <Widget>[
                        const Spacer(),
                        FlatButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            translate.general.ok,
                            style: const TextStyle(
                              color: Colors.blue,
                              fontSize: 18.0,
                              height: 1.25,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }

  Future<String> createActivityDialog(BuildContext context, ActivityNode node) {
    final TextEditingController myController = TextEditingController();
    return showDialog(
        context: context,
        builder: (context) {
          return Dialog(
            child: SizedBox(
              height: 320,
              child: Column(
                children: <Widget>[
                  Container(
                    height: 80,
                    color: Colors.blue,
                    child: Row(
                      children: <Widget>[
                        const SizedBox(
                          width: 20,
                        ),
                        Text(
                          translate.createActivity,
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 24.0,
                            height: 1.25,
                            fontWeight: FontWeight.w600,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        const Spacer(),
                        InkWell(
                          onTap: () {
                            Navigator.of(context).pop(Constant.emptyString);
                          },
                          child: const Icon(Icons.close,
                              color: Colors.white, size: 30),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(20.0),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
                      ),
                      borderRadius:
                          const BorderRadius.all(Radius.circular(10.0)),
                    ),
                    height: 120,
                    child: Column(
                      children: <Widget>[
                        const SizedBox(
                          height: 10,
                        ),
                        Row(
                          children: <Widget>[
                            SizedBox(
                              width: 40,
                              child: Center(
                                child: AppIcons.appIcon(
                                    enumIcon.ICO_RUNNING,
                                    AppColors.appColor(
                                        enumColor.COL_BLUE_DARKER),
                                    30 * f),
                              ),
                            ),
                            Flexible(
                              child: TextField(
                                controller: myController,
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Row(
                          children: <Widget>[
                            SizedBox(
                              width: 40,
                              child: Center(
                                  child: AppIcons.appIconFromIconCode(
                                ActivityTreeScopedModel.of(context)
                                    .mainActivityTree
                                    .rootNode(node.rootCode)
                                    .iconCode,
                                AppColors.appColorFromColorCode(
                                    ActivityTreeScopedModel.of(context)
                                        .mainActivityTree
                                        .rootNode(node.rootCode)
                                        .colorCode),
                                20,
                              )),
                            ),
                            Flexible(
                              child: Text(
                                ActivityTreeScopedModel.of(context)
                                    .mainActivityTree
                                    .rootNode(node.rootCode)
                                    .activity,
                                style: TextStyle(
                                  color: AppColors.appColorFromColorCode(
                                      ActivityTreeScopedModel.of(context)
                                          .mainActivityTree
                                          .rootNode(node.rootCode)
                                          .colorCode),
                                  fontSize: 18.0,
                                  height: 1.25,
                                  fontWeight: FontWeight.w600,
                                ),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 80,
                    child: Row(
                      children: <Widget>[
                        const Spacer(),
                        FlatButton(
                          onPressed: () {
                            Navigator.of(context).pop(Constant.emptyString);
                          },
                          child: Text(
                            translate.general.cancel,
                            style: const TextStyle(
                              color: Colors.blue,
                              fontSize: 18.0,
                              height: 1.25,
                              fontWeight: FontWeight.w600,
                            ),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        FlatButton(
                          onPressed: () {
                            Navigator.of(context)
                                .pop(myController.text.toString());
                          },
                          child: Text(
                            translate.general.create,
                            style: const TextStyle(
                              color: Colors.blue,
                              fontSize: 18.0,
                              height: 1.25,
                              fontWeight: FontWeight.w600,
                            ),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  IconData treeIcon(ActivityNode node) {
    if (node.childIds.isEmpty) {
      return node.selected
          ? Icons.radio_button_checked
          : Icons.radio_button_unchecked;
    } else {
      return node.open
          ? FontAwesomeIcons.chevronCircleUp
          : FontAwesomeIcons.chevronCircleDown;
    }
  }

  Widget _clickableActivity(ActivityNode node) {
    return Container(
      height: heightContainerSmall(node),
      width: node.widthTextBox(context),
      decoration: BoxDecoration(
        border: Border.all(
          color: node.colorBorder(),
          width: 0,
        ),
        borderRadius: BorderRadius.circular(8),
        color:
            node.childIds.isEmpty ? node.colorActivity() : Colors.transparent,
      ),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: node.widthBeforeText(),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: SizedBox(
              width: node.widthText(context),
              child: Text(
                node.activity,
                style: TextStyle(
                  color: node.childIds.isEmpty
                      ? Colors.white
                      : AppColors.appColor(enumColor.COL_BLUE_DARKER),
                  fontSize: 18.0 * f,
                  height: 1.25,
                  fontWeight: FontWeight.w600,
                ),
                maxLines: maxLines(node),
                overflow: TextOverflow.ellipsis,
              ),
            ),
          )
        ],
      ),
    );
  }

  int maxLines(ActivityNode node) {
    final String line =
        Settings.instance.getSetting(enumSetting.settingLinesVisibleCode);
    if (line == '1') {
      return 1;
    } else if (line == '2') {
      return 2;
    } else if (line == '3') {
      return 3;
    } else if (line == 'X') {
      return nLines(
          node.activity,
          TextStyle(
            color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
            fontSize: 18.0 * f,
            height: 1.25,
            fontWeight: FontWeight.w600,
          ),
          node.widthText(context));
    } else if (line == 'V') {
      return nLines(
          node.activity,
          TextStyle(
            color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
            fontSize: 18.0 * f,
            height: 1.25,
            fontWeight: FontWeight.w600,
          ),
          node.widthText(context));
    } else {
      return 1;
    }
  }

  int nLines(String text, TextStyle style, double width) {
    final TextSpan span = TextSpan(text: text, style: style);

    int n = 1;
    TextPainter tp =
        TextPainter(text: span, maxLines: n, textDirection: TextDirection.ltr);
    tp.layout(maxWidth: width);
    while (tp.didExceedMaxLines && n < 10) {
      n++;
      tp = TextPainter(
          text: span, maxLines: n, textDirection: TextDirection.ltr);
      tp.layout(maxWidth: width);
    }

    return n;
  }

  double heightContainerBig(ActivityNode node) {
    return 30.0 + (maxLines(node) * 22.5);
  }

  double heightContainerSmall(ActivityNode node) {
    return 20.0 + (maxLines(node) * 22.5);
  }

  Widget _editableActivity(ActivityNode node) {
    return SizedBox(
        height: heightContainerSmall(node),
        width: node.widthTextBox(context),
        child: DottedBorder(
          color: node.colorBorder(),
          strokeWidth: 1.8,
          dashPattern: const [6, 6],
          radius: const Radius.circular(8),
          child: Row(
            children: <Widget>[
              SizedBox(
                width: node.widthBeforeText() - 5,
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: SizedBox(
                  width: node.widthText(context) - 5,
                  child: Text(
                    node.activity,
                    style: TextStyle(
                      color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
                      fontSize: 18.0 * f,
                      height: 1.25,
                      fontWeight: FontWeight.w600,
                    ),
                    maxLines: maxLines(node),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              )
            ],
          ),
        ));
  }

  Widget myAppBar(BuildContext context) {
    return AppBar(
      title: Text(
        translate.selectActivity,
        style: TextStyle(fontWeight: FontWeight.w600, fontSize: 23.0 * f),
      ),
      leading: SizedBox(
          width: 50 * x,
          child: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child:
                AppIcons.appIcon(enumIcon.ICO_ARROWLEFT, Colors.white, 30 * f),
          )),
    );
  }
}
