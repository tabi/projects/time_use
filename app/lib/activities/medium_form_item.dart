import 'package:flutter/material.dart';
import 'package:tbo_app/activities/medium.dart';
import 'package:tbo_app/activities/medium_dialog.dart';
import 'package:tbo_app/core/app_colors.dart';
import 'package:tbo_app/core/extensions.dart';

class MediumFormItem extends StatelessWidget {
  static final _darkBlueStyle = TextStyle(
      color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
      fontSize: 18.0,
      height: 1.25,
      fontWeight: FontWeight.w600);

  final Medium medium;
  final ValueChanged<Medium> onChanged;
  final bool showErrorBorder;

  const MediumFormItem(
      {Key key, this.onChanged, this.showErrorBorder = false, this.medium})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        final _result = await showDialog<Medium>(
          context: context,
          builder: (BuildContext context) {
            return const MediumDialog();
          },
        );
        if (_result != null) {
          onChanged(_result);
        }
      },
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: showErrorBorder
                ? AppColors.appColor(enumColor.COL_RED)
                : AppColors.appColor(enumColor.COL_GRAY_BORDER),
          ),
          borderRadius: BorderRadius.circular(5),
          color: Colors.white,
        ),
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 8),
          height: 45,
          child: Row(
            children: <Widget>[
              if (medium != null)
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8),
                  child: medium.imageIcon(),
                ),
              Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  child: medium == null
                      ? Text('Meerdere keuzes mogelijk', style: _darkBlueStyle)
                      : Text(medium.label, style: _darkBlueStyle),
                ),
              ),
              const Spacer(),
              Align(
                  alignment: Alignment.centerRight,
                  child: Icon(
                    Icons.arrow_drop_down,
                    color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
