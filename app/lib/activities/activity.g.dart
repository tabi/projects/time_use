// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activity.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<Activity> _$activitySerializer = new _$ActivitySerializer();

class _$ActivitySerializer implements StructuredSerializer<Activity> {
  @override
  final Iterable<Type> types = const [Activity, _$Activity];
  @override
  final String wireName = 'Activity';

  @override
  Iterable<Object> serialize(Serializers serializers, Activity object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'type',
      serializers.serialize(object.type,
          specifiedType: const FullType(ActivityType)),
      'startDate',
      serializers.serialize(object.startDate,
          specifiedType: const FullType(DateTime)),
      'endDate',
      serializers.serialize(object.endDate,
          specifiedType: const FullType(DateTime)),
    ];
    if (object.id != null) {
      result
        ..add('id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.activity != null) {
      result
        ..add('activity')
        ..add(serializers.serialize(object.activity,
            specifiedType: const FullType(String)));
    }
    if (object.activityNode != null) {
      result
        ..add('activityNode')
        ..add(serializers.serialize(object.activityNode,
            specifiedType: const FullType(ActivityNode)));
    }
    if (object.companion != null) {
      result
        ..add('companion')
        ..add(serializers.serialize(object.companion,
            specifiedType: const FullType(
                BuiltList, const [const FullType(ActivityCompanion)])));
    }
    if (object.location != null) {
      result
        ..add('location')
        ..add(serializers.serialize(object.location,
            specifiedType: const FullType(ActivityLocation)));
    }
    if (object.medium != null) {
      result
        ..add('medium')
        ..add(serializers.serialize(object.medium,
            specifiedType: const FullType(Medium)));
    }
    return result;
  }

  @override
  Activity deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ActivityBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(ActivityType)) as ActivityType;
          break;
        case 'startDate':
          result.startDate = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'endDate':
          result.endDate = serializers.deserialize(value,
              specifiedType: const FullType(DateTime)) as DateTime;
          break;
        case 'activity':
          result.activity = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'activityNode':
          result.activityNode.replace(serializers.deserialize(value,
              specifiedType: const FullType(ActivityNode)) as ActivityNode);
          break;
        case 'companion':
          result.companion.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(ActivityCompanion)]))
              as BuiltList<Object>);
          break;
        case 'location':
          result.location = serializers.deserialize(value,
                  specifiedType: const FullType(ActivityLocation))
              as ActivityLocation;
          break;
        case 'medium':
          result.medium = serializers.deserialize(value,
              specifiedType: const FullType(Medium)) as Medium;
          break;
      }
    }

    return result.build();
  }
}

class _$Activity extends Activity {
  @override
  final String id;
  @override
  final ActivityType type;
  @override
  final DateTime startDate;
  @override
  final DateTime endDate;
  @override
  final String activity;
  @override
  final ActivityNode activityNode;
  @override
  final BuiltList<ActivityCompanion> companion;
  @override
  final ActivityLocation location;
  @override
  final Medium medium;

  factory _$Activity([void Function(ActivityBuilder) updates]) =>
      (new ActivityBuilder()..update(updates)).build();

  _$Activity._(
      {this.id,
      this.type,
      this.startDate,
      this.endDate,
      this.activity,
      this.activityNode,
      this.companion,
      this.location,
      this.medium})
      : super._() {
    if (type == null) {
      throw new BuiltValueNullFieldError('Activity', 'type');
    }
    if (startDate == null) {
      throw new BuiltValueNullFieldError('Activity', 'startDate');
    }
    if (endDate == null) {
      throw new BuiltValueNullFieldError('Activity', 'endDate');
    }
  }

  @override
  Activity rebuild(void Function(ActivityBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ActivityBuilder toBuilder() => new ActivityBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Activity &&
        id == other.id &&
        type == other.type &&
        startDate == other.startDate &&
        endDate == other.endDate &&
        activity == other.activity &&
        activityNode == other.activityNode &&
        companion == other.companion &&
        location == other.location &&
        medium == other.medium;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc($jc($jc(0, id.hashCode), type.hashCode),
                                startDate.hashCode),
                            endDate.hashCode),
                        activity.hashCode),
                    activityNode.hashCode),
                companion.hashCode),
            location.hashCode),
        medium.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Activity')
          ..add('id', id)
          ..add('type', type)
          ..add('startDate', startDate)
          ..add('endDate', endDate)
          ..add('activity', activity)
          ..add('activityNode', activityNode)
          ..add('companion', companion)
          ..add('location', location)
          ..add('medium', medium))
        .toString();
  }
}

class ActivityBuilder implements Builder<Activity, ActivityBuilder> {
  _$Activity _$v;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  ActivityType _type;
  ActivityType get type => _$this._type;
  set type(ActivityType type) => _$this._type = type;

  DateTime _startDate;
  DateTime get startDate => _$this._startDate;
  set startDate(DateTime startDate) => _$this._startDate = startDate;

  DateTime _endDate;
  DateTime get endDate => _$this._endDate;
  set endDate(DateTime endDate) => _$this._endDate = endDate;

  String _activity;
  String get activity => _$this._activity;
  set activity(String activity) => _$this._activity = activity;

  ActivityNodeBuilder _activityNode;
  ActivityNodeBuilder get activityNode =>
      _$this._activityNode ??= new ActivityNodeBuilder();
  set activityNode(ActivityNodeBuilder activityNode) =>
      _$this._activityNode = activityNode;

  ListBuilder<ActivityCompanion> _companion;
  ListBuilder<ActivityCompanion> get companion =>
      _$this._companion ??= new ListBuilder<ActivityCompanion>();
  set companion(ListBuilder<ActivityCompanion> companion) =>
      _$this._companion = companion;

  ActivityLocation _location;
  ActivityLocation get location => _$this._location;
  set location(ActivityLocation location) => _$this._location = location;

  Medium _medium;
  Medium get medium => _$this._medium;
  set medium(Medium medium) => _$this._medium = medium;

  ActivityBuilder();

  ActivityBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _type = _$v.type;
      _startDate = _$v.startDate;
      _endDate = _$v.endDate;
      _activity = _$v.activity;
      _activityNode = _$v.activityNode?.toBuilder();
      _companion = _$v.companion?.toBuilder();
      _location = _$v.location;
      _medium = _$v.medium;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Activity other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Activity;
  }

  @override
  void update(void Function(ActivityBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Activity build() {
    _$Activity _$result;
    try {
      _$result = _$v ??
          new _$Activity._(
              id: id,
              type: type,
              startDate: startDate,
              endDate: endDate,
              activity: activity,
              activityNode: _activityNode?.build(),
              companion: _companion?.build(),
              location: location,
              medium: medium);
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'activityNode';
        _activityNode?.build();
        _$failedField = 'companion';
        _companion?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Activity', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
