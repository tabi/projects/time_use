import 'package:tbo_app/core/translate_page_base.dart';

class TranslatePageActivityAgenda extends TranslatePageBase {
  TranslatePageActivityAgenda._privateConstructor();
  static final TranslatePageActivityAgenda instance =
      TranslatePageActivityAgenda._privateConstructor();

  @override
  void initPage() {
    pageKey = 'activityAgenda';
  }

  @override
  void initKeys() {
    keys.add('dayNotComplete');
    keys.add('closeDayQuestion');
    keys.add('dayAlreadyClosed');
    keys.add('openDayQuestion');
    keys.add('close');
    keys.add('closed');
    keys.add('dayClosed');
  }

  String get dayClosed {
    return translation('dayClosed');
  }

  String get closed {
    return translation('closed');
  }

  String get close {
    return translation('close');
  }

  String get openDayQuestion {
    return translation('openDayQuestion');
  }

  String get dayAlreadyClosed {
    return translation('dayAlreadyClosed');
  }

  String get closeDayQuestion {
    return translation('closeDayQuestion');
  }

  String get dayNotComplete {
    return translation('dayNotComplete');
  }
}
