import 'package:tbo_app/core/translate_page_base.dart';

class TranslatePageSelectActivity extends TranslatePageBase {
  TranslatePageSelectActivity._privateConstructor();
  static final TranslatePageSelectActivity instance =
      TranslatePageSelectActivity._privateConstructor();

  @override
  void initPage() {
    pageKey = 'selectActivity';
  }

  @override
  void initKeys() {
    keys.add('selectActivity');
    keys.add('fullDescription');
    keys.add('createActivity');
  }

  String get selectActivity {
    return translation('selectActivity');
  }

  String get fullDescription {
    return translation('fullDescription');
  }

  String get createActivity {
    return translation('createActivity');
  }
}
