import 'dart:async';

import 'package:built_collection/built_collection.dart';
import 'package:tbo_app/activities/activity.dart';
import 'package:tbo_app/activities/activity_companion.dart';
import 'package:tbo_app/activities/activity_node.dart';
import 'package:tbo_app/activities/activity_tree.dart';
import 'package:tbo_app/activities/activity_type.dart';
import 'package:tbo_app/core/database/table/activity_info_table.dart';
import 'package:tbo_app/core/database/table/activity_property_table.dart';
import 'package:tbo_app/core/extensions.dart';
import 'package:tbo_app/core/input_type.dart';

import 'activity_companion.dart';

class ActivityAgendas {
  ActivityAgenda main;
  ActivityAgenda side;
  ActivityAgendas(this.main, this.side);
}

class ActivityAgenda {
  static const int _mainActivityType = 1;
  static const int _sideActivityType = 2;
  static const Duration _tenMinutes = Duration(minutes: 10);

  ActivityTree activityTree;

  String _treeCode;
  DateTime _currentDateTime;
  ActivityType _activityType;

  DateTime get dayStartDate => DateTime(
      _currentDateTime.year, _currentDateTime.month, _currentDateTime.day, 4);
  DateTime get dayEndDate => dayStartDate.add(const Duration(days: 1));

  List<Activity> activities = <Activity>[];

  Activity selectedActivity;
  bool get hasSelectedActivity => selectedActivity != null;

  Activity _originalActivity;
  Activity _originalActivityAfter;
  Activity _originalActivityBefore;

  Future<void> load(String treeCode, DateTime dateTime,
      ActivityType activityType, InputType inputType) async {
    _treeCode = treeCode;
    _currentDateTime = DateTime(dateTime.year, dateTime.month, dateTime.day);
    _activityType = activityType;
    selectedActivity = null;
    await reload();
  }

  Future<void> reload() async {
    final List<ActivityInfoTable> _activitiesInfos =
        await ActivityInfoTable.select(
            _treeCode,
            _currentDateTime.millisecondsSinceEpoch,
            _activityType == ActivityType.main
                ? _mainActivityType
                : _sideActivityType);

    final List<ActivityPropertyTable> _activityProperties =
        await ActivityPropertyTable.select(
            _treeCode,
            _currentDateTime.millisecondsSinceEpoch,
            _activityType == ActivityType.main
                ? _mainActivityType
                : _sideActivityType);

    activities.clear();
    for (final ActivityInfoTable _activityInfo in _activitiesInfos) {
      final ListBuilder<ActivityCompanion> companions =
          ListBuilder<ActivityCompanion>();
      for (final ActivityPropertyTable _activityProperty
          in _activityProperties) {
        if (_activityProperty.startHour == _activityInfo.startHour &&
            _activityProperty.startMinute == _activityInfo.startMinute) {
          //TODO: add property type conversions

          for (final ActivityCompanion companion in ActivityCompanion.values) {
            if (_activityProperty.key == companionKey(companion)) {
              companions.add(companion);
            }
          }
        }
      }
      activities.add(_activityInfo.createActivity(
          _findActivityNode(_activityInfo),
          activityCompanion: companions));
    }
  }

  ActivityNode _findActivityNode(ActivityInfoTable _activityInfo) {
    return activityTree.activityTree().firstWhere((activityNode) =>
        activityNode.nodeCode == _activityInfo.nodeCode &&
        activityNode.activity == _activityInfo.description);
  }

  String companionKey(ActivityCompanion companion) {
    return 'companion_$companion';
  }

  Future<void> save() async {
    await saveActivityInfoTable();
    await saveActivityPropertyTable();
  }

  Future<void> saveActivityInfoTable() async {
    final List<ActivityInfoTable> activityInfos = activities
        .where((activity) => activity.activityNode != null)
        .map((activity) => _activityInfoTable(activity, _activityType))
        .toList();

    await ActivityInfoTable.delete(
        _treeCode,
        _currentDateTime.millisecondsSinceEpoch,
        _activityType == ActivityType.main
            ? _mainActivityType
            : _sideActivityType);
    await ActivityInfoTable.insert(activityInfos);
  }

  Future<void> saveActivityPropertyTable() async {
    final List<ActivityPropertyTable> activityProperties =
        <ActivityPropertyTable>[];
    for (final Activity activity in activities) {
      if (activity.activityNode != null && activity.companion != null) {
        //TODO add property type conversions

        for (final ActivityCompanion companion in activity.companion) {
          activityProperties.add(_activityPropertyTable(
              activity, _activityType, companionKey(companion), '1'));
        }
      }
    }

    await ActivityPropertyTable.delete(
        _treeCode,
        _currentDateTime.millisecondsSinceEpoch,
        _activityType == ActivityType.main
            ? _mainActivityType
            : _sideActivityType);
    await ActivityPropertyTable.insert(activityProperties);
  }

  ActivityInfoTable _activityInfoTable(
      Activity activity, ActivityType activityType) {
    return ActivityInfoTable(
        _treeCode,
        _currentDateTime.millisecondsSinceEpoch,
        activityType == ActivityType.main
            ? _mainActivityType
            : _sideActivityType,
        activity.activityNode.nodeCode,
        activity.activityNode.activity,
        0,
        0,
        activity.startDate.toUtc().millisecondsSinceEpoch, //start hour
        activity.startDate.toUtc().minute, //start minute
        activity.endDate.toUtc().millisecondsSinceEpoch, //end hour
        activity.endDate.toUtc().minute,
        medium: activity.medium,
        location: activity.location);
  }

  ActivityPropertyTable _activityPropertyTable(
      Activity activity, ActivityType activityType, String key, String value) {
    return ActivityPropertyTable(
        _treeCode,
        _currentDateTime.millisecondsSinceEpoch,
        activityType == ActivityType.main
            ? _mainActivityType
            : _sideActivityType,
        activity.startDate.toUtc().millisecondsSinceEpoch, //start hour
        activity.startDate.toUtc().minute, //start minute
        key,
        value);
  }

  bool isMissingEntries() {
    final _endDate = DateTime(_currentDateTime.year, _currentDateTime.month,
            _currentDateTime.day, 4)
        .add(const Duration(days: 1));
    return activities.areMissing(_endDate);
  }

  void replaceActivity(Activity to) {
    activities.replace(selectedActivity, to);
    selectedActivity = to;
  }

  void selectActivity(Activity activity) {
    activities.reset();
    if (activity != null) {
      selectedActivity = activity.rebuild((b) => b);
      activities.replace(activity, selectedActivity);
    } else {
      selectedActivity = null;
    }
  }

  void deselect() {
    selectedActivity = null;
  }

  void resetDrag() {
    _originalActivity = selectedActivity.rebuild((b) => b);
    _originalActivityAfter =
        activities.after(selectedActivity)?.rebuild((b) => b);
    _originalActivityBefore =
        activities.before(selectedActivity)?.rebuild((b) => b);
  }

  bool matchesCurrentDate(DateTime date) {
    return (selectedActivity?.startDate?.isAtSameMomentAs(date) ?? false) ||
        (selectedActivity?.endDate?.isAtSameMomentAs(date) ?? false);
  }

  Activity selectActivityWithStartDate(DateTime date) {
    return activities.selectActivityWithStartDate(date, _activityType);
  }

  bool isStartOfActivity(DateTime date) {
    return activities.isStartOfActivity(date);
  }

  bool isWithinActivity(DateTime date) {
    return activities.isWithinActivity(date);
  }

  bool increaseStartDateOnActivity(Duration duration) {
    if (selectedActivity.startDate.isBefore(_originalActivity.startDate)) {
      final _activityBefore = activities.before(selectedActivity);
      final _updatedActivity = selectedActivity.rebuild(
          (b) => b.startDate = selectedActivity.startDate.add(duration));
      if (_activityBefore != null &&
          _activityBefore.endDate.isBefore(_originalActivityBefore.endDate)) {
        activities.replace(
            _activityBefore,
            _activityBefore
                .rebuild((b) => b.endDate = _updatedActivity.startDate));
      }
      replaceActivity(_updatedActivity);
      return true;
    } else if (selectedActivity.canChangeDuration()) {
      final _updatedActivity = selectedActivity.rebuild(
          (b) => b.startDate = selectedActivity.startDate.add(duration));
      replaceActivity(_updatedActivity);
      return true;
    } else {
      final _activityAfter = activities.after(selectedActivity);
      final _updatedActivity = selectedActivity.rebuild((b) => b
        ..startDate = selectedActivity.startDate.add(duration)
        ..endDate = selectedActivity.endDate.add(duration));
      if (_updatedActivity.startDate.isBefore(dayEndDate)) {
        if (_activityAfter != null) {
          if (_activityAfter.startDate.isBefore(_updatedActivity.endDate)) {
            if (_activityAfter.canChangeDuration()) {
              activities.replace(
                  _activityAfter,
                  _activityAfter
                      .rebuild((b) => b.startDate = _updatedActivity.endDate));
              replaceActivity(_updatedActivity);
              return true;
            }
          } else {
            replaceActivity(_updatedActivity);
            return true;
          }
        } else {
          replaceActivity(_updatedActivity);
          return true;
        }
      }
    }
    return false;
  }

  bool decreaseEndDateOnActivity(Duration duration) {
    if (selectedActivity.endDate.isAfter(_originalActivity.endDate)) {
      final _activityAfter = activities.after(selectedActivity);
      final _updatedActivity = selectedActivity.rebuild(
          (b) => b.endDate = selectedActivity.endDate.subtract(duration));
      if (_activityAfter != null &&
          _activityAfter.startDate.isAfter(_originalActivityAfter.startDate)) {
        activities.replace(
            _activityAfter,
            _activityAfter
                .rebuild((b) => b.startDate = _updatedActivity.endDate));
      }
      replaceActivity(_updatedActivity);
      return true;
    } else if (selectedActivity.canChangeDuration()) {
      final _updatedActivity = selectedActivity.rebuild(
          (b) => b.endDate = selectedActivity.endDate.subtract(duration));
      replaceActivity(_updatedActivity);
      return true;
    } else {
      final _activityBefore = activities.before(selectedActivity);
      final _updatedActivity = selectedActivity.rebuild((b) => b
        ..startDate = selectedActivity.startDate.subtract(duration)
        ..endDate = selectedActivity.endDate.subtract(duration));
      if (_updatedActivity.endDate.isAfter(dayStartDate)) {
        if (_activityBefore != null) {
          if (_activityBefore.endDate.isAfter(_updatedActivity.startDate)) {
            if (_activityBefore.canChangeDuration()) {
              activities.replace(
                  _activityBefore,
                  _activityBefore
                      .rebuild((b) => b.endDate = _updatedActivity.startDate));
              replaceActivity(_updatedActivity);
              return true;
            }
          } else {
            replaceActivity(_updatedActivity);
            return true;
          }
        } else {
          replaceActivity(_updatedActivity);
          return true;
        }
      }
    }
    return false;
  }

  bool decreaseStartDateOnActivity(Duration duration) {
    if (selectedActivity.endDate.isAfter(_originalActivity.endDate)) {
      final _activityAfter = activities.after(selectedActivity);
      final _updatedActivity = selectedActivity.rebuild((b) => b
        ..startDate = selectedActivity.startDate.subtract(duration)
        ..endDate = selectedActivity.endDate.subtract(duration));
      if (_activityAfter != null &&
          _activityAfter.startDate.isAfter(_originalActivityAfter.startDate)) {
        activities.replace(
            _activityAfter,
            _activityAfter
                .rebuild((b) => b.startDate = _updatedActivity.endDate));
      }
      replaceActivity(_updatedActivity);
      return true;
    } else if (selectedActivity.startDate
        .isAfter(_originalActivity.startDate)) {
      final _updatedActivity = selectedActivity.rebuild(
          (b) => b.startDate = selectedActivity.startDate.subtract(duration));
      replaceActivity(_updatedActivity);
      return true;
    } else {
      final _activityBefore = activities.before(selectedActivity);
      final _updatedActivity = selectedActivity.rebuild(
          (b) => b.startDate = selectedActivity.startDate.subtract(duration));
      if (_updatedActivity.startDate.isAfter(dayStartDate) ||
          _updatedActivity.startDate.isAtSameMomentAs(dayStartDate)) {
        if (_activityBefore != null) {
          if (_activityBefore.endDate.isBefore(_updatedActivity.startDate) ||
              _activityBefore.endDate
                  .isAtSameMomentAs(_updatedActivity.startDate)) {
            replaceActivity(_updatedActivity);
            return true;
          } else if (_activityBefore.canChangeDuration()) {
            activities.replace(
                _activityBefore,
                _activityBefore
                    .rebuild((b) => b.endDate = _updatedActivity.startDate));
            replaceActivity(_updatedActivity);
            return true;
          }
        } else {
          replaceActivity(_updatedActivity);
          return true;
        }
      }
    }
    return false;
  }

  bool increaseEndDateOnActivity(Duration duration) {
    if (selectedActivity.startDate.isBefore(_originalActivity.startDate)) {
      final _activityBefore = activities.before(selectedActivity);
      final _updatedActivity = selectedActivity.rebuild((b) => b
        ..startDate = selectedActivity.startDate.add(duration)
        ..endDate = selectedActivity.endDate.add(duration));
      if (_activityBefore != null &&
          _activityBefore.endDate.isBefore(_originalActivityBefore.endDate)) {
        activities.replace(
            _activityBefore,
            _activityBefore
                .rebuild((b) => b.endDate = _updatedActivity.startDate));
      }
      replaceActivity(_updatedActivity);
      return true;
    } else if (selectedActivity.endDate.isBefore(_originalActivity.endDate)) {
      final _updatedActivity = selectedActivity
          .rebuild((b) => b.endDate = selectedActivity.endDate.add(duration));
      replaceActivity(_updatedActivity);
      return true;
    } else {
      final _activityAfter = activities.after(selectedActivity);
      final _updatedActivity = selectedActivity
          .rebuild((b) => b.endDate = selectedActivity.endDate.add(duration));
      if (_updatedActivity.endDate.isBefore(dayEndDate) ||
          _updatedActivity.endDate.isAtSameMomentAs(dayEndDate)) {
        if (_activityAfter != null) {
          if (_activityAfter.startDate.isAfter(_updatedActivity.endDate) ||
              _activityAfter.startDate
                  .isAtSameMomentAs(_updatedActivity.endDate)) {
            replaceActivity(_updatedActivity);
            return true;
          } else if (_activityAfter.canChangeDuration()) {
            activities.replace(
                _activityAfter,
                _activityAfter
                    .rebuild((b) => b.startDate = _updatedActivity.endDate));
            replaceActivity(_updatedActivity);
            return true;
          }
        } else {
          replaceActivity(_updatedActivity);
          return true;
        }
      }
    }
    return false;
  }

  //############################################

  DateTime minStartDate() {
    return _originalActivityBefore != null
        ? _originalActivityBefore.endDate
        : dayStartDate;
  }

  bool canDecreaseStartDate() {
    return selectedActivity.startDate.isAfter(minStartDate());
  }

  void decreaseStartDate() {
    final _updatedActivity = selectedActivity.rebuild(
        (b) => b.startDate = selectedActivity.startDate.subtract(_tenMinutes));
    replaceActivity(_updatedActivity);
  }

  DateTime maxStartDate() {
    return (_originalActivityAfter != null
            ? _originalActivityAfter.startDate
            : dayEndDate)
        .subtract(_tenMinutes);
  }

  bool canIncreaseStartDate() {
    return selectedActivity.startDate.isBefore(maxStartDate());
  }

  void increaseStartDate() {
    setStartDate(selectedActivity.startDate.add(_tenMinutes));
  }

  void setStartDate(DateTime startDate) {
    Activity _updatedActivity;
    if (startDate.isBefore(selectedActivity.endDate)) {
      _updatedActivity =
          selectedActivity.rebuild((b) => b.startDate = startDate);
    } else {
      _updatedActivity = selectedActivity.rebuild((b) => b
        ..startDate = startDate
        ..endDate = startDate.add(_tenMinutes));
    }
    replaceActivity(_updatedActivity);
  }

  DateTime maxEndDate() {
    return _originalActivityAfter != null
        ? _originalActivityAfter.startDate
        : dayEndDate;
  }

  bool canIncreaseEndDate() {
    return selectedActivity.endDate.isBefore(maxEndDate());
  }

  void increaseEndDate() {
    final _updatedActivity = selectedActivity
        .rebuild((b) => b.endDate = selectedActivity.endDate.add(_tenMinutes));
    replaceActivity(_updatedActivity);
  }

  DateTime minEndDate() {
    return (_originalActivityBefore != null
            ? _originalActivityBefore.endDate
            : dayStartDate)
        .add(_tenMinutes);
  }

  bool canDecreaseEndDate() {
    return selectedActivity.endDate.isAfter(minEndDate());
  }

  void decreaseEndDate() {
    setEndDate(selectedActivity.endDate.subtract(_tenMinutes));
  }

  void setEndDate(DateTime endDate) {
    Activity _updatedActivity;
    if (endDate.isAfter(selectedActivity.startDate)) {
      _updatedActivity = selectedActivity.rebuild((b) => b.endDate = endDate);
    } else {
      _updatedActivity = selectedActivity.rebuild((b) => b
        ..startDate = endDate.subtract(_tenMinutes)
        ..endDate = endDate);
    }
    replaceActivity(_updatedActivity);
  }

  ActivityAgenda get copy {
    final ActivityAgenda _copy = ActivityAgenda();
    _copy._treeCode = _treeCode;
    _copy._currentDateTime = _currentDateTime;
    _copy._activityType = _activityType;
    for (final Activity a in activities) {
      _copy.activities.add(a.rebuild((b) => b));
    }
    if (hasSelectedActivity) {
      _copy.selectedActivity = selectedActivity.rebuild((b) => b);
      _copy.resetDrag();
    }
    return _copy;
  }

  void replace(ActivityAgenda agenda) {
    _treeCode = agenda._treeCode;
    _currentDateTime = agenda._currentDateTime;
    _activityType = agenda._activityType;
    selectedActivity = null;
    activities.clear();
    for (final Activity a in agenda.activities) {
      activities.add(a.rebuild((b) => b));
    }
    if (agenda.hasSelectedActivity) {
      selectedActivity = agenda.selectedActivity.rebuild((b) => b);
      resetDrag();
    }
  }

  List<Activity> overlappingActivities(Activity activity) {
    return activities.where((act) {
      return act.startDate.isBefore(activity.endDate) &&
          (act.endDate.isAfter(activity.startDate));
    }).toList();
  }
}
