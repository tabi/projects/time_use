import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:flutter/material.dart' hide Builder;
import 'package:tbo_app/core/app_colors.dart';
import 'package:tbo_app/core/app_icons.dart';
import 'package:tbo_app/core/constant.dart';
import 'package:tbo_app/core/serializers.dart';

part 'activity_node.g.dart';

abstract class ActivityNode
    implements Built<ActivityNode, ActivityNodeBuilder> {
  static Serializer<ActivityNode> get serializer => _$activityNodeSerializer;

  int get parentId;
  BuiltList<int> get childIds;
  String get nodeCode;
  String get nodeType;
  String get iconCode;
  String get colorCode;
  String get activityCode;
  String get activity;
  String get description;
  bool get selected;
  bool get visible;
  bool get open;

  bool get isOpenInput {
    if (nodeType.isEmpty) {
      return false;
    } else {
      return nodeType.toUpperCase().substring(0, 1) == 'O';
    }
  }

  String _parentCode(String nodeCode) {
    final List<String> levels = nodeCode.split(Constant.codeLevelSeparator);
    if (levels.length == 1) {
      return Constant.codeLevelBottom;
    } else {
      final _buffer = StringBuffer();
      for (int i = 0; i < levels.length - 1; i++) {
        _buffer.write(Constant.codeLevelSeparator + levels[i]);
      }
      final _parentCode = _buffer.toString().substring(1);
      return _parentCode;
    }
  }

  String get parentCode {
    return _parentCode(nodeCode);
  }

  String get rootCode {
    String result = nodeCode;
    while (_parentCode(result) != Constant.codeLevelBottom) {
      result = _parentCode(result);
    }
    return result;
  }

  int get level {
    return nodeCode.split(Constant.codeLevelSeparator).length - 1;
  }

  // ### icon and color ###########################################################################

  IconData iconData() {
    return AppIcons.appIconDataFromIconCode(iconCode);
  }

  Color colorActivity() {
    return AppColors.appColorFromColorCode(colorCode);
  }

  Color colorBackground() {
    return level == 0
        ? Colors.white
        : AppColors.appColor(enumColor.COL_OFF_WHITE);
  }

  Color colorBorder() {
    return childIds.isEmpty ? colorActivity() : Colors.transparent;
  }

  factory ActivityNode([Function(ActivityNodeBuilder b) updates]) =
      _$ActivityNode;

  ActivityNode._();

  factory ActivityNode.remainingActivity() {
    return ActivityNode((b) => b
      ..parentId = -100
      ..childIds = ListBuilder<int>([])
      ..nodeCode = '_01.01'
      ..nodeType = ''
      ..iconCode = ''
      ..colorCode = ''
      ..activityCode = ''
      ..activity = ''
      ..description = ''
      ..selected = false
      ..visible = false
      ..open = false);
  }

  factory ActivityNode.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map toJson() {
    return serializers.serializeWith(serializer, this) as Map;
  }

  // ### width ###########################################################################

  static const double _widthLeftMargin = 10;
  static const double _widthActivityIcon = 30;
  static const double _widthLeftText = 20;
  static const double _widthBeforeText = 10;
  static const double _widthRightText = 10.0;
  static const double _widthInfoIcon = 30;
  static const double _widthRightInfo = 10;
  static const double _widthOpenCloseIcon = 30;
  static const double _widthRightMargin = 10.0;

  //top: Lm Licon Lt [text] Rt Iicon Ri Ricon Rm
  //sub: Lm    [text      ] Rt Iicon Ri Ricon Rm
  //bot: Lm       [Bt text         ] Rm

  //bot: Lm       [Bt text         ] Rt Iicon Ri Rm

  double widthLeftMargin() {
    return level == 0 ? _widthLeftMargin : level * 2 * _widthLeftMargin;
  }

  double widthActivityIcon() {
    return level == 0 ? _widthActivityIcon : 0;
  }

  double widthInfoIcon() {
    return description == '' ? 0 : _widthInfoIcon;
  }

  double widthRightIcon() {
    return description == '' ? 0 : (childIds.isEmpty ? 0 : _widthRightInfo);
  }

  double widthLeftText() {
    return level == 0 ? _widthLeftText : 0;
  }

  double widthBeforeText() {
    return childIds.isEmpty ? _widthBeforeText : 0;
  }

  double widthRightText() {
    return description == ''
        ? (childIds.isEmpty ? 0 : _widthRightText)
        : _widthRightText;
  }

  double widthOpenCloseIcon() {
    return childIds.isEmpty ? 0 : _widthOpenCloseIcon;
  }

  double widthRightMargin() {
    return _widthRightMargin;
  }

  double widthTextBox(BuildContext context) {
    return MediaQuery.of(context).size.width -
        widthLeftMargin() -
        widthActivityIcon() -
        widthLeftText() -
        widthRightText() -
        widthInfoIcon() -
        widthRightIcon() -
        widthOpenCloseIcon() -
        widthRightMargin();
  }

  double widthText(BuildContext context) {
    return widthTextBox(context) - widthBeforeText();
  }
}
