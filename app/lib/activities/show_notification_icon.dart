import 'package:flutter/material.dart';
import 'package:tbo_app/activities/translate_page_activity_agenda.dart';
import 'package:tbo_app/core/app_colors.dart';
import 'package:tbo_app/core/app_icons.dart';

class ShowNotificationIcon {
  Future<void> show(BuildContext context) async {
    final OverlayState overlayState = Overlay.of(context);
    final OverlayEntry overlayEntry = OverlayEntry(builder: _build);

    overlayState.insert(overlayEntry);

    await Future.delayed(const Duration(seconds: 2));

    overlayEntry.remove();
  }

  Widget _build(BuildContext context) {
    return Center(
      child: Material(
        color: Colors.transparent,
        //child: new Icon(Icons.warning, color: Colors.purple),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.black12, width: 2),
            borderRadius: BorderRadius.circular(12.0),
            color: Colors.white,
          ),
          height: 210,
          width: 270,
          child: Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 30,
                ),
                Center(
                  child: Container(
                    height: 90,
                    width: 90,
                    decoration: BoxDecoration(
                      shape: BoxShape
                          .circle, // You can use like this way or like the below line
                      //borderRadius: new BorderRadius.circular(30.0),
                      color: AppColors.appColor(enumColor.COL_GREEN_LIGHT),
                    ),
                    child:
                        AppIcons.appIcon(enumIcon.ICO_CHECK, Colors.white, 60),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Center(
                  child: Text(
                    TranslatePageActivityAgenda.instance.dayClosed,
                    style: TextStyle(
                      color: AppColors.appColor(enumColor.COL_GREEN_LIGHT),
                      fontSize: 18.0,
                      height: 1.25,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
