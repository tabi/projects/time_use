import 'package:flutter/material.dart';

class DrawResizeDots extends CustomPainter {
  Paint _paintOut;
  Paint _paintIn;
  double _height;
  double _width6;

  DrawResizeDots(double height, double width) {
    _height = height;
    _width6 = width / 6.0;

    _paintOut = Paint()
      ..color = Colors.white
      ..strokeWidth = 10.0
      ..style = PaintingStyle.fill;

    _paintIn = Paint()
      ..color = Colors.black
      ..strokeWidth = 10.0
      ..style = PaintingStyle.fill;
  }

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawCircle(Offset(1.0 * _width6, 0.0 - 1.0), 5.0, _paintOut);
    canvas.drawCircle(Offset(1.0 * _width6, 0.0 - 1.0), 3.0, _paintIn);

    canvas.drawCircle(Offset(5.0 * _width6, _height - 5.0), 5.0, _paintOut);
    canvas.drawCircle(Offset(5.0 * _width6, _height - 5.0), 3.0, _paintIn);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class DrawResizeStartDateDot extends CustomPainter {
  Paint _paintOut;
  Paint _paintIn;
  double _height;
  double _width;

  DrawResizeStartDateDot(double height, double width) {
    _height = height / 2.0;
    _width = width / 2.0;

    _paintOut = Paint()
      ..color = Colors.white
      ..strokeWidth = 10.0
      ..style = PaintingStyle.fill;

    _paintIn = Paint()
      ..color = Colors.black
      ..strokeWidth = 10.0
      ..style = PaintingStyle.fill;
  }

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawCircle(Offset(_width, _height + 1.0), 5.0, _paintOut);
    canvas.drawCircle(Offset(_width, _height + 1.0), 3.0, _paintIn);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class DrawResizeEndDateDot extends CustomPainter {
  Paint _paintOut;
  Paint _paintIn;
  double _height;
  double _width;

  DrawResizeEndDateDot(double height, double width) {
    _height = height / 2.0;
    _width = width / 2.0;

    _paintOut = Paint()
      ..color = Colors.white
      ..strokeWidth = 10.0
      ..style = PaintingStyle.fill;

    _paintIn = Paint()
      ..color = Colors.black
      ..strokeWidth = 10.0
      ..style = PaintingStyle.fill;
  }

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawCircle(Offset(_width, _height - 1.0), 5.0, _paintOut);
    canvas.drawCircle(Offset(_width, _height - 1.0), 3.0, _paintIn);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
