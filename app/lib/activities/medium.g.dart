// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'medium.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const Medium _$desktop = const Medium._('desktop');
const Medium _$laptop = const Medium._('laptop');
const Medium _$smartphone = const Medium._('smartphone');
const Medium _$television = const Medium._('television');
const Medium _$console = const Medium._('console');
const Medium _$tablet = const Medium._('tablet');
const Medium _$carRadio = const Medium._('carRadio');
const Medium _$paper = const Medium._('paper');
const Medium _$eReader = const Medium._('eReader');
const Medium _$handheld = const Medium._('handheld');
const Medium _$radio = const Medium._('radio');
const Medium _$phone = const Medium._('phone');
const Medium _$portableRadio = const Medium._('portableRadio');
const Medium _$none = const Medium._('none');

Medium _$valueOf(String name) {
  switch (name) {
    case 'desktop':
      return _$desktop;
    case 'laptop':
      return _$laptop;
    case 'smartphone':
      return _$smartphone;
    case 'television':
      return _$television;
    case 'console':
      return _$console;
    case 'tablet':
      return _$tablet;
    case 'carRadio':
      return _$carRadio;
    case 'paper':
      return _$paper;
    case 'eReader':
      return _$eReader;
    case 'handheld':
      return _$handheld;
    case 'radio':
      return _$radio;
    case 'phone':
      return _$phone;
    case 'portableRadio':
      return _$portableRadio;
    case 'none':
      return _$none;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<Medium> _$values = new BuiltSet<Medium>(const <Medium>[
  _$desktop,
  _$laptop,
  _$smartphone,
  _$television,
  _$console,
  _$tablet,
  _$carRadio,
  _$paper,
  _$eReader,
  _$handheld,
  _$radio,
  _$phone,
  _$portableRadio,
  _$none,
]);

Serializer<Medium> _$mediumSerializer = new _$MediumSerializer();

class _$MediumSerializer implements PrimitiveSerializer<Medium> {
  @override
  final Iterable<Type> types = const <Type>[Medium];
  @override
  final String wireName = 'Medium';

  @override
  Object serialize(Serializers serializers, Medium object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  Medium deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      Medium.valueOf(serialized as String);
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
