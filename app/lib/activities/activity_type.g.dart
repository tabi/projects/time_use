// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activity_type.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const ActivityType _$main = const ActivityType._('main');
const ActivityType _$side = const ActivityType._('side');

ActivityType _$valueOf(String name) {
  switch (name) {
    case 'main':
      return _$main;
    case 'side':
      return _$side;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<ActivityType> _$values =
    new BuiltSet<ActivityType>(const <ActivityType>[
  _$main,
  _$side,
]);

Serializer<ActivityType> _$activityTypeSerializer =
    new _$ActivityTypeSerializer();

class _$ActivityTypeSerializer implements PrimitiveSerializer<ActivityType> {
  @override
  final Iterable<Type> types = const <Type>[ActivityType];
  @override
  final String wireName = 'ActivityType';

  @override
  Object serialize(Serializers serializers, ActivityType object,
          {FullType specifiedType = FullType.unspecified}) =>
      object.name;

  @override
  ActivityType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ActivityType.valueOf(serialized as String);
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
