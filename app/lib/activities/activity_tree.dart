import 'package:built_collection/built_collection.dart';
import 'package:tbo_app/activities/activity_node.dart';
import 'package:tbo_app/core/constant.dart';
import 'package:tbo_app/core/database/table/activities_table.dart';
import 'package:tbo_app/core/database/table/activity_info_table.dart';
import 'package:tbo_app/core/input_type.dart';
import 'package:tbo_app/settings/settings.dart';

class ActivityTree {
  String _treeCode;

  List<ActivityNode> _tree;

  List<ActivityNode> activityTree() {
    return _tree;
  }

  //### general ##########################################################################################

  ActivityNode node(String code) {
    if (code == null) {
      return null;
    }
    try {
      return _tree[_id(code)];
    } catch (e) {
      return null;
    }
  }

  ActivityNode rootNode(String code) {
    return node(node(code).rootCode);
  }

  int _id(String code) {
    final _node = _tree.firstWhere((element) => element.nodeCode == code,
        orElse: () => null);
    return _node != null ? _tree.indexOf(_node) : -1;
  }

  List<int> _childIds(String code) {
    final result = <int>[];
    for (int i = 0; i < _tree.length; i++) {
      if (_tree[i].parentCode == code) {
        result.add(i);
      }
    }
    return result;
  }

  ActivityNode openChild(String code) {
    for (final int i in _childIds(code)) {
      if (_tree[i].nodeType.isNotEmpty) {
        if (_tree[i].nodeType.toUpperCase().substring(0, 1) == 'O') {
          return _tree[i];
        }
      }
    }
    return null;
  }

  //### init ##########################################################################################

  Future init(String treeCode, InputType inputType) async {
    if (_tree != null) {
      return null;
    }

    _treeCode = treeCode;
    _tree = <ActivityNode>[];

    await _addActivitiesFromDatabase(inputType);
    _addMissingActivities();
    setActivityNodeProperties(inputType);
    await _addCustomActivities();
    setActivityNodeProperties(inputType);
  }

  Future _addCustomActivities() async {
    final infos = await ActivityInfoTable.selectAll(_treeCode);
    for (final ActivityInfoTable info in infos) {
      addCustomActivity(info.nodeCode, info.description);
    }
  }

  void addCustomActivity(String code, String activity) {
    for (int i = 0; i < _tree.length; i++) {
      if (_tree[i].nodeCode == code && _tree[i].activity == activity) {
        return;
      }
    }
    final copy = node(code);
    if (copy == null) {
      return;
    }
    final customAct = ActivityNode((b) => b
      ..nodeCode = copy.nodeCode
      ..nodeType = ''
      ..iconCode = copy.iconCode
      ..colorCode = copy.colorCode
      ..activityCode = copy.activityCode
      ..activity = activity
      ..description = copy.description
      ..parentId = -1
      ..childIds = ListBuilder<int>()
      ..selected = false
      ..visible = false
      ..open = false);

    bool found = false;
    bool inserted = false;
    for (int i = 0; i < _tree.length; i++) {
      // achter de node
      // if (_tree[i].nodeCode == customAct.nodeCode) {
      //   found = true;
      // } else if (found && _tree[i].nodeCode != customAct.nodeCode) {
      //   _tree.insert(i, customAct);
      //   inserted = true;
      //   break;
      // }

      // voor de node
      if (_tree[i].nodeCode == customAct.nodeCode) {
        _tree.insert(i, customAct);
        found = true;
        inserted = true;
        break;
      }
    }

    if (found && !inserted) {
      _tree.add(customAct);
    }
  }

  void addNewHalfOpenActivity(ActivityNode node) {
    _tree.add(node);
  }

  void _addMissingActivities() {
    int i = 0;
    while (i < _tree.length) {
      final parentCode = _tree[i].parentCode;
      final parentId = _id(parentCode);
      if (parentCode != Constant.codeLevelBottom && parentId == -1) {
        _tree.insert(
          i,
          ActivityNode((b) => b
            ..nodeCode = parentCode
            ..nodeType = ''
            ..iconCode = _tree[i].iconCode
            ..colorCode = _tree[i].colorCode
            ..activityCode = '${_tree[i].activityCode} ^'
            ..activity = '${_tree[i].activity} ^'
            ..description = _tree[i].description
            ..parentId = -1
            ..childIds = ListBuilder<int>()
            ..selected = false
            ..visible = false
            ..open = false),
        );
      } else {
        i++;
      }
    }
  }

  void setActivityNodeProperties(InputType inputType) {
    if (inputType == InputType.closed) {
      for (int i = 0; i < _tree.length; i++) {
        final ActivityNode node = _tree[i];
        _tree[i] = node.rebuild((b) => b
          ..parentId = _id(node.parentCode)
          ..childIds = ListBuilder<int>(_childIds(node.nodeCode)));
      }
    }
  }

  //### select ##########################################################################################

  void select(String code) {
    _resetSelection();

    final id = _id(code);
    if (id != -1) {
      _tree[id] = _tree[id].rebuild((b) => b.selected = true);
    }
    _setVisibility(_visibleIds(code), true);
    _setOpen(_openIds(code), true);
  }

  void _resetSelection() {
    for (int i = 0; i < _tree.length; i++) {
      final ActivityNode node = _tree[i];
      _tree[i] = node.rebuild((b) => b
        ..selected = false
        ..visible = false
        ..open = false);
    }
  }

  void _setVisibility(List<int> ids, bool visible) {
    for (final int id in ids) {
      _tree[id] = _tree[id].rebuild((b) => b.visible = visible);
    }
  }

  void _setOpen(List<int> ids, bool open) {
    for (final int id in ids) {
      _tree[id] = _tree[id].rebuild((b) => b.open = open);
    }
  }

  List<int> _visibleIds(String code) {
    final result = <int>[];
    int id = _id(code);
    while (id != -1) {
      result.add(id);
      result.addAll(_tree[id].childIds);
      id = _tree[id].parentId;
    }
    if (result.isEmpty ||
        Settings.instance.getSetting(enumSetting.settingMainVisibleYn) == 'Y') {
      result.addAll(_childIds(Constant.codeLevelBottom));
    }
    return result;
  }

  List<int> _openIds(String code) {
    final result = <int>[];
    int id = _id(code);
    while (id != -1) {
      result.add(id);
      id = _tree[id].parentId;
    }
    return result;
  }

  Future _addActivitiesFromDatabase(InputType inputType) async {
    final rows = await ActivitiesTable.select(_treeCode, inputType);

    for (final ActivitiesTable row in rows) {
      _tree.add(ActivityNode((b) => b
        ..nodeCode = row.nodeCode
        ..nodeType = row.nodeType
        ..iconCode = row.iconCode
        ..colorCode = row.colorCode
        ..activityCode = row.activityCode
        ..activity = row.activity
        ..description = row.description
        ..parentId = row.parentId
        ..childIds = ListBuilder<int>(row.childIds)
        ..selected = false
        ..visible = false
        ..open = false));
    }
  }
}
