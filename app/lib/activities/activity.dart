import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:tbo_app/activities/activity_companion.dart';
import 'package:tbo_app/activities/activity_location.dart';
import 'package:tbo_app/activities/activity_node.dart';
import 'package:tbo_app/activities/activity_type.dart';
import 'package:tbo_app/activities/medium.dart';
import 'package:tbo_app/core/serializers.dart';

part 'activity.g.dart';

abstract class Activity implements Built<Activity, ActivityBuilder> {
  static Serializer<Activity> get serializer => _$activitySerializer;

  @nullable
  String get id;

  ActivityType get type;
  DateTime get startDate;
  DateTime get endDate;

  //JJAN
  @nullable
  String get activity;

  @nullable
  ActivityNode get activityNode;

  @nullable
  BuiltList<ActivityCompanion> get companion;

  @nullable
  ActivityLocation get location;

  @nullable
  Medium get medium;

  factory Activity([Function(ActivityBuilder b) updates]) = _$Activity;

  Activity._();

  factory Activity.fromJson(Map json) {
    return serializers.deserializeWith(serializer, json);
  }

  Map toJson() {
    return serializers.serializeWith(serializer, this) as Map;
  }

  Activity increaseStartDate(Duration duration) {
    return rebuild((b) => b.startDate = startDate.add(duration));
  }

  Activity decreaseStartDate(Duration duration) {
    return rebuild((b) => b.startDate = startDate.subtract(duration));
  }

  Activity increaseEndDate(Duration duration) {
    return rebuild((b) => b.endDate = endDate.add(duration));
  }

  Activity decreaseEndDate(Duration duration) {
    return rebuild((b) => b.endDate = endDate.subtract(duration));
  }

  bool canChangeDuration() {
    final _difference = endDate.difference(startDate).inMinutes.abs();
    return _difference > 10;
  }

  bool isEqualTo(Activity b) {
    if (type != b.type) return false;
    if (!startDate.isAtSameMomentAs(b.startDate)) return false;
    if (!endDate.isAtSameMomentAs(b.endDate)) return false;
    if (activity != b.activity) return false;
    if (companion != b.companion) return false;
    if (activityNode == null && b.activityNode != null) return false;
    if (activityNode != null && b.activityNode == null) return false;
    if (activityNode != null && b.activityNode != null) {
      if (activityNode.parentId != b.activityNode.parentId) return false;
      if (activityNode.nodeCode != b.activityNode.nodeCode) return false;
      if (activityNode.activity != b.activityNode.activity) return false;
    }
    return true;
  }
}
