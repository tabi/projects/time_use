import 'dart:core';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/activities/activity.dart';
import 'package:tbo_app/activities/activity_entry_page.dart';
import 'package:tbo_app/activities/activity_tree_scoped_model.dart';
import 'package:tbo_app/activities/agendas_scoped_model.dart';
import 'package:tbo_app/activities/draw_resize_dots.dart';
import 'package:tbo_app/activities/hard_plausibility_check_dialog.dart';
import 'package:tbo_app/activities/select_activity_page.dart';
import 'package:tbo_app/activities/show_notification_icon.dart';
import 'package:tbo_app/activities/soft_plausibility_check_dialog.dart';
import 'package:tbo_app/activities/translate_page_activity_agenda.dart';
import 'package:tbo_app/core/app_colors.dart';
import 'package:tbo_app/core/app_icons.dart';
import 'package:tbo_app/core/configuration.dart';
import 'package:tbo_app/core/configuration_state.dart';
import 'package:tbo_app/core/extensions.dart';
import 'package:tbo_app/core/input_type.dart';
import 'package:tbo_app/core/para_data_scoped_model.dart';
import 'package:tbo_app/core/plausibility_check_type.dart';
import 'package:tbo_app/home/page_model.dart';
import 'package:tbo_app/open_input/search_widget.dart';
import 'package:tbo_app/para_data/para_data_name.dart';

class ActivitiesPage extends StatefulWidget with ParaDataName {
  final String treeCode;
  final String testVersion;
  final bool closable;

  const ActivitiesPage({this.treeCode, this.testVersion, this.closable});

  @override
  ActivitiesPageState createState() {
    return ActivitiesPageState();
  }
}

class ActivitiesPageState extends State<ActivitiesPage> {
  static const _activityHeight = 40.0;
  final List<DateTime> _timeIntervals = [];

  TranslatePageActivityAgenda _translate;

  bool _scrollSyncBusy = false;
  ScrollController _controllerTick;
  ScrollController _controllerMain;
  ScrollController _controllerSide;

  double _sumDy = 0.0;
  double _offset = 0.0;

  InputType inputType;

  InputType _getInputType() {
    final _configuration = Configuration.of(context);
    return _configuration.inputType;
  }

  @override
  void initState() {
    super.initState();
    _translate = TranslatePageActivityAgenda.instance;
    _controllerTick = ScrollController();
    _controllerTick.addListener(_scrollListenerTick);
    _controllerMain = ScrollController();
    _controllerMain.addListener(_scrollListenerMain);
    _controllerSide = ScrollController();
    _controllerSide.addListener(_scrollListenerSide);
    _scrollSyncBusy = false;
  }

  void _initTimeIntervals(BuildContext context) {
    if (_timeIntervals.isEmpty) {
      final _date = PageModel.of(context).selectedDate;
      for (int hour = 0; hour < 24; hour++) {
        for (int minute = 0; minute < 6; minute++) {
          _timeIntervals.add(DateTime(
                  _date.year, _date.month, _date.day, hour + 4, minute * 10)
              .toUtc());
        }
      }
      _timeIntervals
          .add(DateTime(_date.year, _date.month, _date.day, 24 + 4).toUtc());
    }
  }

  void _scrollListenerTick() {
    _synchroniseControllers(0, _controllerTick.offset);
  }

  void _scrollListenerMain() {
    _synchroniseControllers(1, _controllerMain.offset);
  }

  void _scrollListenerSide() {
    _synchroniseControllers(2, _controllerSide.offset);
  }

  void _synchroniseControllers(int list, double offset) {
    if (!_scrollSyncBusy) {
      _scrollSyncBusy = true;
      if (list != 0) {
        _controllerTick.jumpTo(offset);
      }
      if (list != 1) {
        _controllerMain.jumpTo(offset);
      }
      if (list != 2) {
        _controllerSide.jumpTo(offset);
      }
      _offset = offset;
      _scrollSyncBusy = false;
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    _initTimeIntervals(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              _agendaTopBar(context),
              Builder(builder: (context) {
                return _myAgenda();
              }),
            ],
          ),
        ),
      ),
    );
  }

  Widget _agendaTopBar(BuildContext context) {
    return Container(
      color: AppColors.appColor(enumColor.COL_BLUE_MIDDLE),
      child: Column(
        children: [
          _agendaTopBarLine1(context),
          _agendaTopBarLine2(context),
        ],
      ),
    );
  }

  Widget _agendaTopBarLine1(BuildContext context) {
    return ScopedModelDescendant<AgendasScopedModel>(
        builder: (context, child, activitiesModel) {
      return SizedBox(
        height: 60,
        child: Row(
          children: <Widget>[
            Container(),
            const Spacer(),
            InkWell(
              onTap: () async {
                await _closeDay(context, activitiesModel);
              },
              child: FutureBuilder<bool>(
                  future: activitiesModel.canCloseDay,
                  builder: (context, snapshot) {
                    final _canCloseDay = snapshot.data ?? true;
                    return Container(
                      height: 34,
                      width: 140,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: _canCloseDay
                            ? AppColors.appColor(enumColor.COL_BLUE_DARKER)
                            : AppColors.appColor(enumColor.COL_GREEN_MIDDLE),
                      ),
                      child: Center(
                        child: Row(
                          children: <Widget>[
                            const Spacer(),
                            if (_canCloseDay)
                              Container()
                            else
                              AppIcons.appIcon(
                                  enumIcon.ICO_CHECK, Colors.white, 20),
                            if (_canCloseDay)
                              Container()
                            else
                              const SizedBox(width: 10),
                            if (_canCloseDay)
                              Text(
                                _translate.close,
                                style: TextStyle(
                                    color:
                                        AppColors.appColor(enumColor.COL_WHITE),
                                    fontSize: 18.0,
                                    height: 1.25),
                              )
                            else
                              Text(
                                _translate.closed,
                                style: TextStyle(
                                    color:
                                        AppColors.appColor(enumColor.COL_WHITE),
                                    fontSize: 18.0,
                                    height: 1.25),
                              ),
                            const Spacer(),
                          ],
                        ),
                      ),
                    );
                  }),
            ),
            const SizedBox(
              width: 20,
            ),
          ],
        ),
      );
    });
  }

  Future<void> _closeDay(
      BuildContext context, AgendasScopedModel activitiesModel) async {
    final _isOpen = await activitiesModel.isDayOpen();
    ParaDataScopedModel.of(context)
        .onTap('closeDayButton', '${_isOpen ? 'close' : 'open'} day');
    if (!_isOpen) {
      final doOpen = await _showDayOpenDialog(context);

      if (doOpen) {
        await activitiesModel.openDay();
      }
    } else {
      bool doClose = true;
      if (activitiesModel.isMissingEntries()) {
        doClose = await _showDayCloseFailsDialog(context);
      }
      if (doClose) {
        await activitiesModel.closeDay();
        final _showNotificationIcon = ShowNotificationIcon();
        await _showNotificationIcon.show(context);
      }
    }
  }

  Future<bool> _showDayOpenDialog(BuildContext context) async {
    bool close = false;
    await showDialog(
      context: context,
      routeSettings: const RouteSettings(name: 'DayOpenDialog'),
      builder: (BuildContext context) {
        return Dialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      _translate.dayAlreadyClosed,
                      style: const TextStyle(
                        color: Colors.black,
                        fontSize: 18.0,
                        height: 1.25,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      _translate.openDayQuestion,
                      style: const TextStyle(
                        color: Colors.black,
                        fontSize: 18.0,
                        height: 1.25,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      children: <Widget>[
                        const Spacer(),
                        FlatButton(
                          onPressed: () {
                            close = true;
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            _translate.general.yes,
                            style: const TextStyle(
                              color: Colors.blue,
                              fontSize: 18.0,
                              height: 1.25,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        FlatButton(
                          onPressed: () {
                            close = false;
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            _translate.general.no,
                            style: const TextStyle(
                              color: Colors.blue,
                              fontSize: 18.0,
                              height: 1.25,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
    return close;
  }

  Future<bool> _showDayCloseFailsDialog(BuildContext context) async {
    final _plausibilityCheckType =
        Configuration.of(context).plausibilityCheckType;
    final close = await showDialog<bool>(
        context: context,
        routeSettings: const RouteSettings(name: 'DayCloseFailsDialog'),
        builder: (BuildContext context) {
          return _plausibilityCheckType == PlausibilityCheckType.soft
              ? const SoftPlausibilityCheckDialog()
              : const HardPlausibilityCheckDialog();
        });
    return close ?? false;
  }

  Widget _agendaTopBarLine2(BuildContext context) {
    final _dateText = DateFormat('E d MMMM y', 'nl')
        .format(PageModel.of(context).selectedDate);
    final _firstLetter = _dateText.substring(0, 1).toUpperCase();
    final _date = '$_firstLetter${_dateText.substring(1)}';
    return ScopedModelDescendant<AgendasScopedModel>(
        builder: (context, child, activityModel) {
      return SizedBox(
        height: 60,
        //color: Colors.yellow,
        child: Row(
          children: <Widget>[
            const SizedBox(
              width: 20,
            ),
            InkWell(
              child: Row(
                children: <Widget>[
                  Text(
                    _date,
                    style: TextStyle(
                        color: AppColors.appColor(enumColor.COL_WHITE),
                        fontSize: 23.0,
                        height: 1.25),
                  ),
                  //AppIcons.appIcon(enumIcon.ICO_CARETDOWN, Colors.white, 25),
                ],
              ),
            ),
            const Spacer(),
            SizedBox(
              width: 50,
              child: activityModel.canPasteActivity
                  ? InkWell(
                      onTap: () async {
                        activityModel.paste();
                        await activityModel.save();
                      },
                      child: const Icon(Icons.content_paste,
                          color: Colors.white, size: 30),
                    )
                  : Container(),
            ),
            SizedBox(
              width: 50,
              child: activityModel.canCopyActivity
                  ? InkWell(
                      onTap: () {
                        activityModel.copy();
                      },
                      child: const Icon(Icons.content_copy,
                          color: Colors.white, size: 30),
                    )
                  : Container(),
            ),
            SizedBox(
              width: 50,
              child: activityModel.canDeleteActivity
                  ? InkWell(
                      onTap: () {
                        _showDeleteWarningDialog(context, activityModel);
                      },
                      child: const Icon(Icons.delete,
                          color: Colors.white, size: 30),
                    )
                  : Container(),
            ),
            const SizedBox(
              width: 20,
            ),
          ],
        ),
      );
    });
  }

  void _showDeleteWarningDialog(
      BuildContext context, AgendasScopedModel activitiesScopedModel) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(_translate.general.warning),
          content: Text(
              'Wilt U activiteit: \n\n${activitiesScopedModel.mainAgendaSelectedActivity?.activityNode?.activity ?? activitiesScopedModel.sideAgendaSelectedActivity?.activityNode?.activity}\n\nverwijderen?'),
          actions: [
            FlatButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text(_translate.general.cancel),
            ),
            ScopedModelDescendant<AgendasScopedModel>(
                builder: (context, child, activitiesModel) {
              return FlatButton(
                onPressed: () async {
                  setState(() {
                    activitiesModel.delete();
                  });
                  await activitiesModel.save();
                  Navigator.of(context).pop();
                },
                child: Text(_translate.general.ok),
              );
            }),
          ],
        );
      },
    );
  }

  Widget _myAgenda() {
    return GestureDetector(
      onScaleUpdate: (ScaleUpdateDetails details) {
        setState(() {
          //TODO: set scale?
        });
      },
      child: ScopedModelDescendant<AgendasScopedModel>(
          builder: (context, child, activitiesModel) {
        return SizedBox(
          height: MediaQuery.of(context).size.height -
              120 +
              MediaQuery.of(context).padding.top -
              (widget.closable ? 0 : 90) -
              70,
          child: Stack(
            children: <Widget>[
              Positioned(
                left: 2.0,
                width: 70.0,
                top: 0.0,
                bottom: 0.0,
                child: GestureDetector(
                  onTap: () {
                    activitiesModel.deselect();
                  },
                  child: _tickListView(),
                ),
              ),
              Positioned(
                left: 72,
                width: MediaQuery.of(context).size.width - 72,
                top: 0.0,
                bottom: 0.0,
                child: _mainActivityListView(),
              ),
              Positioned(
                left: 72 + (MediaQuery.of(context).size.width - 72) / 2,
                width: (MediaQuery.of(context).size.width - 72) / 2,
                top: 0.0,
                bottom: 0.0,
                child: _sideActivityListView(),
              ),
              _mainActivityDragCircles(),
              _sideActivityDragCircles(),
            ],
          ),
        );
      }),
    );
  }

  Widget _mainActivityDragCircles() {
    return ScopedModelDescendant<AgendasScopedModel>(
        builder: (context, child, activitiesModel) {
      if (!activitiesModel.mainAgendaHasSelectedActivity) {
        return Container();
      } else {
        final int startMinutes = activitiesModel
            .mainAgendaSelectedActivityStartDate
            .difference(_timeIntervals[0])
            .inMinutes;
        final int stopMinutes = activitiesModel
            .mainAgendaSelectedActivityEndDate
            .difference(_timeIntervals[0])
            .inMinutes;
        final double activityWidth =
            (MediaQuery.of(context).size.width - 72) / 2.0;

        return Stack(
          children: <Widget>[
            Positioned(
              left: 72 - 20 + (1.0 * activityWidth / 6.0),
              top: startMinutes * 4 - _offset,
              width: 40,
              height: 40,
              child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onVerticalDragStart: (DragStartDetails details) {
                    setState(() {
                      activitiesModel.mainAgendaResetDrag();
                      _sumDy = 0.0;
                    });
                  },
                  onVerticalDragUpdate: (DragUpdateDetails details) {
                    setState(() {
                      _sumDy += details.delta.dy;
                      const _duration = Duration(minutes: 10);
                      bool _bussy = true;
                      while (_bussy) {
                        _bussy = false;
                        if (_sumDy > _activityHeight / 2.0) {
                          if (activitiesModel
                              .mainAgendaIncreaseStartDateOnActivity(
                                  _duration)) {
                            _sumDy -= _activityHeight;
                            _bussy = true;
                          }
                        } else if (_sumDy < -_activityHeight / 2.0) {
                          if (activitiesModel
                              .mainAgendaDecreaseStartDateOnActivity(
                                  _duration)) {
                            _sumDy += _activityHeight;
                            _bussy = true;
                          }
                        }
                      }
                    });
                  },
                  onVerticalDragEnd: (DragEndDetails details) async {
                    if (activitiesModel
                            .mainAgendaSelectedActivityActivityNode !=
                        null) {
                      await activitiesModel.save();
                    }
                    setState(() {
                      //activitiesModel.save();
                    });
                  },
                  child: Container(
                    width: 20,
                    height: 20,
                    color: Colors.transparent,
                    child: CustomPaint(
                      painter: DrawResizeStartDateDot(40.0, 40.0),
                    ),
                  )),
            ),
            Positioned(
              left: 72 - 20 + (5.0 * activityWidth / 6.0),
              top: stopMinutes * 4 - _offset,
              width: 40,
              height: 40,
              child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onVerticalDragStart: (DragStartDetails details) {
                    setState(() {
                      activitiesModel.mainAgendaResetDrag();
                      _sumDy = 0.0;
                    });
                  },
                  onVerticalDragUpdate: (DragUpdateDetails details) {
                    setState(() {
                      _sumDy += details.delta.dy;
                      const _duration = Duration(minutes: 10);
                      bool _bussy = true;
                      while (_bussy) {
                        _bussy = false;
                        if (_sumDy > _activityHeight / 2.0) {
                          if (activitiesModel
                              .mainAgendaIncreaseEndDateOnActivity(_duration)) {
                            _sumDy -= _activityHeight;
                            _bussy = true;
                          }
                        } else if (_sumDy < -_activityHeight / 2.0) {
                          if (activitiesModel
                              .mainAgendaDecreaseEndDateOnActivity(_duration)) {
                            _sumDy += _activityHeight;
                            _bussy = true;
                          }
                        }
                      }
                    });
                  },
                  onVerticalDragEnd: (DragEndDetails details) async {
                    await activitiesModel.save();
                    setState(() {
                      //activitiesModel.save();
                    });
                  },
                  child: Container(
                    width: 20,
                    height: 20,
                    color: Colors.transparent,
                    child: CustomPaint(
                      painter: DrawResizeEndDateDot(40.0, 40.0),
                    ),
                  )),
            )
          ],
        );
      }
    });
  }

  Widget _sideActivityDragCircles() {
    return ScopedModelDescendant<AgendasScopedModel>(
        builder: (context, child, activitiesModel) {
      if (!activitiesModel.sideAgendaHasSelectedActivity) {
        return Container();
      } else {
        final int startMinutes = activitiesModel
            .sideAgendaSelectedActivityStartDate
            .difference(_timeIntervals[0])
            .inMinutes;
        final int stopMinutes = activitiesModel
            .sideAgendaSelectedActivityEndDate
            .difference(_timeIntervals[0])
            .inMinutes;
        final double activityWidth =
            (MediaQuery.of(context).size.width - 72) / 2.0;

        return Stack(
          children: <Widget>[
            Positioned(
              left: 72 + activityWidth - 20 + (1.0 * activityWidth / 6.0),
              top: startMinutes * 4 - _offset,
              width: 40,
              height: 40,
              child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onVerticalDragStart: (DragStartDetails details) {
                    setState(() {
                      activitiesModel.sideAgendaResetDrag();
                      _sumDy = 0.0;
                    });
                  },
                  onVerticalDragUpdate: (DragUpdateDetails details) {
                    setState(() {
                      _sumDy += details.delta.dy;
                      const _duration = Duration(minutes: 10);
                      bool _bussy = true;
                      while (_bussy) {
                        _bussy = false;
                        if (_sumDy > _activityHeight / 2.0) {
                          if (activitiesModel
                              .sideAgendaIncreaseStartDateOnActivity(
                                  _duration)) {
                            _sumDy -= _activityHeight;
                            _bussy = true;
                          }
                        } else if (_sumDy < -_activityHeight / 2.0) {
                          if (activitiesModel
                              .sideAgendaDecreaseStartDateOnActivity(
                                  _duration)) {
                            _sumDy += _activityHeight;
                            _bussy = true;
                          }
                        }
                      }
                    });
                  },
                  onVerticalDragEnd: (DragEndDetails details) async {
                    await activitiesModel.save();
                    setState(() {
                      //await activitiesModel.save();
                    });
                  },
                  child: Container(
                    width: 20,
                    height: 20,
                    color: Colors.transparent,
                    child: CustomPaint(
                      painter: DrawResizeStartDateDot(40.0, 40.0),
                    ),
                  )),
            ),
            Positioned(
              left: 72 + activityWidth - 20 + (5.0 * activityWidth / 6.0),
              top: stopMinutes * 4 - _offset,
              width: 40,
              height: 40,
              child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  onVerticalDragStart: (DragStartDetails details) {
                    setState(() {
                      activitiesModel.sideAgendaResetDrag();
                      _sumDy = 0.0;
                    });
                  },
                  onVerticalDragUpdate: (DragUpdateDetails details) {
                    setState(() {
                      _sumDy += details.delta.dy;
                      const _duration = Duration(minutes: 10);
                      bool _bussy = true;
                      while (_bussy) {
                        _bussy = false;
                        if (_sumDy > _activityHeight / 2.0) {
                          if (activitiesModel
                              .sideAgendaIncreaseEndDateOnActivity(_duration)) {
                            _sumDy -= _activityHeight;
                            _bussy = true;
                          }
                        } else if (_sumDy < -_activityHeight / 2.0) {
                          if (activitiesModel
                              .sideAgendaDecreaseEndDateOnActivity(_duration)) {
                            _sumDy += _activityHeight;
                            _bussy = true;
                          }
                        }
                      }
                    });
                  },
                  onVerticalDragEnd: (DragEndDetails details) async {
                    await activitiesModel.save();
                    setState(() {
                      //activitiesModel.save();
                    });
                  },
                  child: Container(
                    width: 20,
                    height: 20,
                    color: Colors.transparent,
                    child: CustomPaint(
                      painter: DrawResizeEndDateDot(40.0, 40.0),
                    ),
                  )),
            )
          ],
        );
      }
    });
  }

  Widget _tickListView() {
    return ListView.builder(
      padding: const EdgeInsets.only(bottom: 20),
      controller: _controllerTick,
      itemCount: _timeIntervals.length,
      itemBuilder: (context, index) {
        return _tickContainer(_timeIntervals[index].toLocal());
      },
    );
  }

  Widget _tickContainer(DateTime date) {
    return ScopedModelDescendant<AgendasScopedModel>(
        builder: (context, child, activitiesModel) {
      return Container(
        color: Colors.white,
        height: _activityHeight,
        child: Row(
          children: <Widget>[
            const SizedBox(width: 4.0),
            Align(
                alignment: Alignment.centerRight,
                child: Text(
                  DateFormat('HH:mm').format(date),
                  style: TextStyle(
                      color: activitiesModel.matchesCurrentDate(date)
                          ? AppColors.appColor(enumColor.COL_BLUE_LIGHTER)
                          : AppColors.appColor(enumColor.COL_BLUE_DARKER)),
                )),
          ],
        ),
      );
    });
  }

  Widget _mainActivityListView() {
    return ScopedModelDescendant<AgendasScopedModel>(
        builder: (context, child, activitiesModel) {
      return ListView.builder(
        padding: const EdgeInsets.only(top: 20),
        controller: _controllerMain,
        itemCount: _timeIntervals.length,
        itemBuilder: (context, index) {
          final _date = _timeIntervals[index].toUtc();
          final _activity =
              activitiesModel.mainAgendaSelectActivityWithStartDate(_date);
          if (activitiesModel.mainAgendaIsStartOfActivity(_date) ||
              !activitiesModel.mainAgendaIsWithinActivity(_date)) {
            return _mainActivityBar(_activity, activitiesModel,
                onTap: () async {
              if (_activity == activitiesModel.mainAgendaSelectedActivity) {
                await Navigator.push(
                    context,
                    MaterialPageRoute(
                        settings:
                            const RouteSettings(name: 'ActivityEntryPage'),
                        builder: (context) => ActivityEntryPage(
                              agendas: activitiesModel.copyAgendas,
                              chosenDateTime:
                                  PageModel.of(context).selectedDate,
                            )));
              } else {
                activitiesModel.selectMainActivity(_activity);
              }
            });
          } else {
            return const SizedBox();
          }
        },
      );
    });
  }

  Widget _mainActivityBar(Activity activity, AgendasScopedModel activitiesModel,
      {VoidCallback onTap}) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onTap,
      child: Container(
        height: max(
                1.0,
                activity.endDate.difference(activity.startDate).inMinutes /
                    10) *
            _activityHeight,
        width: MediaQuery.of(context).size.width - 72,
        decoration: BoxDecoration(
          border: Border.all(
            color: activity.startDate ==
                    activitiesModel.mainAgendaSelectedActivity?.startDate
                ? AppColors.appColor(enumColor.COL_BLACK)
                : activity.activity != null
                    ? AppColors.appColor(enumColor.COL_WHITE)
                    : Colors.transparent,
            width: 2,
          ),
          borderRadius: BorderRadius.circular(8),
          color: _activityColorBackground(
              activity,
              activitiesModel.mainAgendaSelectedActivity?.startDate,
              activity.activityNode == null
                  ? AppColors.colorCode(enumColor.COL_WHITE)
                  : ActivityTreeScopedModel.of(context)
                      .mainActivityTree
                      .rootNode(activity.activityNode.rootCode)
                      .colorCode),
        ),
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 3,
              left: 6,
              width:
                  activitiesModel.sideActivitiesForActivity(activity).isNotEmpty
                      ? (MediaQuery.of(context).size.width - 72) / 2 - 15
                      : (MediaQuery.of(context).size.width - 72) - 15,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    child: Text(
                      activity.activityNode != null
                          ? activity
                              .activity //JJAN activity.activityNode.activity
                          : activity.startDate ==
                                  activitiesModel
                                      .mainAgendaSelectedActivity?.startDate
                              ? '+ Nieuwe activiteit'
                              : '', // act.description,
                      style: TextStyle(
                          color: AppColors.appColor(enumColor.COL_WHITE),
                          fontSize: 18.0,
                          height: 1.25),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.left,
                    ),
                  ),
                  if (activity.activityNode == null)
                    Container()
                  else
                    Row(
                      children: [
                        ...activity.companion
                                ?.map((companion) => companion.imageIcon())
                                ?.toList() ??
                            []
                      ],
                    )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Color _activityColorBackground(
      Activity activity, DateTime currentStartDate, String colorCode) {
    if (activity.activityNode != null) {
      return AppColors.appColorFromColorCode(colorCode);
    } else if (activity.startDate == currentStartDate) {
      return AppColors.appColor(enumColor.COL_BLUE_LIGHTER);
    } else {
      return Colors.transparent;
    }
  }

  Widget _sideActivityListView() {
    return ScopedModelDescendant<AgendasScopedModel>(
        builder: (context, child, activitiesModel) {
      return ListView.builder(
        padding: const EdgeInsets.only(top: 20),
        controller: _controllerSide,
        itemCount: _timeIntervals.length,
        itemBuilder: (context, index) {
          final _date = _timeIntervals[index];
          final _sideActivity =
              activitiesModel.sideAgendaSelectActivityWithStartDate(_date);
          if (activitiesModel.sideAgendaIsStartOfActivity(_date) ||
              !activitiesModel.sideAgendaIsWithinActivity(_date)) {
            return _sideActivityBar(_sideActivity, activitiesModel,
                onTap: () async {
              final InputType inputType = _getInputType();
              if (_sideActivity == activitiesModel.sideAgendaSelectedActivity) {
                final SelectActivityPageResult _result = await Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => inputType == InputType.closed
                          ? SelectActivityPage(
                              activityTree: ActivityTreeScopedModel.of(context)
                                  .sideActivityTree,
                              activityNode: activitiesModel
                                  .mainAgendaSelectedActivity?.activityNode,
                              includeMedium:
                                  Configuration.of(context).appFlavor ==
                                      AppFlavor.media,
                            )
                          : SearchWidget(),
                    ));
                if (_result?.activityNode != null) {
                  activitiesModel.setActivityNodeForSideActivity(
                      _date,
                      _sideActivity.rebuild((b) => b.medium = _result.medium),
                      _result.activityNode);
                  await activitiesModel.save();
                }
              } else {
                activitiesModel.selectSideActivity(_sideActivity);
              }
            });
          } else {
            return const SizedBox();
          }
        },
      );
    });
  }

  Widget _sideActivityBar(
      Activity sideActivity, AgendasScopedModel activitiesModel,
      {VoidCallback onTap}) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onTap,
      child: Container(
        height: max(
                1.0,
                sideActivity.endDate
                        .difference(sideActivity.startDate)
                        .inMinutes /
                    10) *
            _activityHeight,
        width: (MediaQuery.of(context).size.width - 72) / 2,
        decoration: BoxDecoration(
          border: Border.all(
            color: sideActivity.startDate ==
                    activitiesModel.sideAgendaSelectedActivity?.startDate
                ? AppColors.appColor(enumColor.COL_BLACK)
                : sideActivity.activityNode != null
                    ? AppColors.appColor(enumColor.COL_WHITE)
                    : Colors.transparent,
            width: 2,
          ),
          borderRadius: BorderRadius.circular(8),
          color: _activityColorBackground(
              sideActivity,
              activitiesModel.sideAgendaSelectedActivity?.startDate,
              sideActivity.activityNode == null
                  ? AppColors.colorCode(enumColor.COL_WHITE)
                  : ActivityTreeScopedModel.of(context)
                      .sideActivityTree
                      .rootNode(sideActivity.activityNode.rootCode)
                      .colorCode),
        ),
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 3,
              left: 6,
              width: ((MediaQuery.of(context).size.width - 72) / 2) - 15,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Flexible(
                    child: Text(
                      sideActivity.activityNode != null
                          ? sideActivity.activityNode.activity
                          : sideActivity.startDate ==
                                  activitiesModel
                                      .sideAgendaSelectedActivity?.startDate
                              ? '+ Nieuwe activiteit'
                              : '', // act.description,
                      style: TextStyle(
                          color: AppColors.appColor(enumColor.COL_WHITE),
                          fontSize: 18.0,
                          height: 1.25),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      textAlign: TextAlign.left,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
