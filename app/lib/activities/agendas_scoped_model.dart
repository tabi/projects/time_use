import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/activities/activity.dart';
import 'package:tbo_app/activities/activity_agenda.dart';
import 'package:tbo_app/activities/activity_node.dart';
import 'package:tbo_app/activities/activity_tree.dart';
import 'package:tbo_app/activities/activity_type.dart';
import 'package:tbo_app/core/database/user_progress_database.dart';
import 'package:tbo_app/core/extensions.dart';
import 'package:tbo_app/core/input_type.dart';

class AgendasScopedModel extends Model {
  static AgendasScopedModel of(BuildContext context) =>
      ScopedModel.of<AgendasScopedModel>(context);

  DateTime _currentDateTime;

  final ActivityAgenda _mainAgenda = ActivityAgenda();
  final ActivityAgenda _sideAgenda = ActivityAgenda();

  Future<bool> get canCloseDay => isDayOpen();

  // ignore: avoid_setters_without_getters
  set mainActivityTree(ActivityTree activityTree) {
    _mainAgenda.activityTree = activityTree;
  }

  // ignore: avoid_setters_without_getters
  set sideActivityTree(ActivityTree activityTree) {
    _sideAgenda.activityTree = activityTree;
  }

  //### load + save ###################################################

  Future<void> load(String mainTreeCode, String sideTreeCode, DateTime dateTime,
      InputType inputType) async {
    _currentDateTime = dateTime;
    await _mainAgenda.load(
        mainTreeCode, dateTime, ActivityType.main, inputType);
    await _sideAgenda.load(
        sideTreeCode, dateTime, ActivityType.side, inputType);
    notifyListeners();
  }

  Future<void> reload(InputType inputType) async {
    await _mainAgenda.reload();
    await _sideAgenda.reload();
    notifyListeners();
  }

  Future<void> save() async {
    await _mainAgenda.save();
    await _sideAgenda.save();
    notifyListeners();
  }

  //### copy + paste + delete ###################################################

  Activity _copiedActivity;

  bool get hasActivityOnClipboard => _copiedActivity != null;
  bool get canCopyActivity =>
      _sideAgenda.selectedActivity?.activityNode != null;
  bool get canPasteActivity =>
      hasActivityOnClipboard && _sideAgenda.hasSelectedActivity;
  bool get canDeleteActivity =>
      _mainAgenda.selectedActivity?.activityNode != null ||
      _sideAgenda.selectedActivity?.activityNode != null;

  void copy() {
    if (_mainAgenda.hasSelectedActivity) {
      _copiedActivity = _mainAgenda.selectedActivity.rebuild((b) => b);
    }
    if (_sideAgenda.hasSelectedActivity) {
      _copiedActivity = _sideAgenda.selectedActivity.rebuild((b) => b);
    }
    notifyListeners();
  }

  void paste() {
    _sideAgenda.replaceActivity(_copiedActivity.rebuild((b) => b
      ..startDate = _sideAgenda.selectedActivity.startDate
      ..endDate = _sideAgenda.selectedActivity.endDate));
    //_copiedActivity = null;
    notifyListeners();
  }

  void delete() {
    if (_mainAgenda.hasSelectedActivity) {
      _mainAgenda.activities.remove(_mainAgenda.selectedActivity);
    }
    if (_sideAgenda.hasSelectedActivity) {
      _sideAgenda.activities.remove(_sideAgenda.selectedActivity);
    }
    deselect();
  }

  //### ###################################################

  Future<void> openDay() async {
    final _userProgress = UserProgressDatabase();
    await _userProgress.unmarkDayComplete(_currentDateTime.year.toString(),
        _currentDateTime.month.toString(), _currentDateTime.day.toString());
    notifyListeners();
  }

  Future<bool> isDayOpen() async {
    final _userProgress = UserProgressDatabase();
    final _completed = await _userProgress.isDayCompleted(
        _currentDateTime.year.toString(),
        _currentDateTime.month.toString(),
        _currentDateTime.day.toString());
    return !_completed;
  }

  bool isMissingEntries() {
    _currentDateTime ??= DateTime.now();
    final _endDate = DateTime(_currentDateTime.year, _currentDateTime.month,
            _currentDateTime.day, 4)
        .add(const Duration(days: 1));
    return _mainAgenda.activities.areMissing(_endDate);
  }

  Future<void> closeDay() async {
    final _userProgress = UserProgressDatabase();
    await _userProgress.markDayComplete(_currentDateTime.year.toString(),
        _currentDateTime.month.toString(), _currentDateTime.day.toString());
    notifyListeners();
  }

  void add(Activity activity) {
    if (activity.type == ActivityType.main) {
      _mainAgenda.activities.add(activity);
    } else {
      _sideAgenda.activities.add(activity);
    }
    notifyListeners();
  }

  void replaceMainActivity(Activity to) {
    _mainAgenda.replaceActivity(to);
    notifyListeners();
  }

  ActivityAgendas get copyAgendas {
    return ActivityAgendas(_mainAgenda.copy, _sideAgenda.copy);
  }

  void replaceAgendas(ActivityAgendas agendas) {
    _mainAgenda.replace(agendas.main);
    _sideAgenda.replace(agendas.side);
    notifyListeners();
  }

  void replaceSideActivity(Activity to) {
    _sideAgenda.replaceActivity(to);
    notifyListeners();
  }

  void deselect() {
    _mainAgenda.deselect();
    _sideAgenda.deselect();
    notifyListeners();
  }

  void selectMainActivity(Activity activity) {
    _mainAgenda.selectActivity(activity);
    _sideAgenda.selectActivity(null);
    notifyListeners();
  }

  void selectSideActivity(Activity activity) {
    _sideAgenda.selectActivity(activity);
    _mainAgenda.selectActivity(null);
    notifyListeners();
  }

  void updateSideActivity(Activity from, Activity to) {
    _sideAgenda.activities.replace(from, to);
    notifyListeners();
  }

  bool matchesCurrentDate(DateTime date) {
    return _mainAgenda.matchesCurrentDate(date) ||
        _sideAgenda.matchesCurrentDate(date);
  }

  void setActivityNodeForSideActivity(
      DateTime date, Activity sideActivity, ActivityNode activityNode) {
    _sideAgenda.activities.replace(sideActivity,
        sideActivity.rebuild((b) => b.activityNode = activityNode.toBuilder()));
    _sideAgenda.selectedActivity = _sideAgenda.selectedActivity
        .rebuild((b) => b.activityNode = activityNode.toBuilder());
    notifyListeners();
  }

  List<Activity> sideActivitiesForActivity(Activity activity) {
    return _sideAgenda.activities.where((sideActivity) {
      return sideActivity.startDate.isAtSameMomentAs(activity.startDate) ||
          (sideActivity.startDate.isAfter(activity.startDate) &&
                  sideActivity.startDate.isBefore(activity.endDate) ||
              (sideActivity.endDate.isAfter(activity.startDate) &&
                  (sideActivity.endDate.isAtSameMomentAs(activity.endDate) ||
                      (sideActivity.endDate.isBefore(activity.endDate)))));
    }).toList();
  }

  //### Access to Main and Side Agenda ##################################################

  void mainAgendaResetDrag() {
    _mainAgenda.resetDrag();
  }

  void sideAgendaResetDrag() {
    _sideAgenda.resetDrag();
  }

  bool mainAgendaIncreaseEndDateOnActivity(Duration duration) {
    return _mainAgenda.increaseEndDateOnActivity(duration);
  }

  bool sideAgendaIncreaseEndDateOnActivity(Duration duration) {
    return _sideAgenda.increaseEndDateOnActivity(duration);
  }

  bool mainAgendaDecreaseEndDateOnActivity(Duration duration) {
    return _mainAgenda.decreaseEndDateOnActivity(duration);
  }

  bool sideAgendaDecreaseEndDateOnActivity(Duration duration) {
    return _sideAgenda.decreaseEndDateOnActivity(duration);
  }

  Activity mainAgendaSelectActivityWithStartDate(DateTime date) {
    return _mainAgenda.selectActivityWithStartDate(date);
  }

  Activity sideAgendaSelectActivityWithStartDate(DateTime date) {
    return _sideAgenda.selectActivityWithStartDate(date);
  }

  bool mainAgendaIsStartOfActivity(DateTime date) {
    return _mainAgenda.isStartOfActivity(date);
  }

  bool sideAgendaIsStartOfActivity(DateTime date) {
    return _sideAgenda.isStartOfActivity(date);
  }

  bool mainAgendaIsWithinActivity(DateTime date) {
    return _mainAgenda.isWithinActivity(date);
  }

  bool sideAgendaIsWithinActivity(DateTime date) {
    return _sideAgenda.isWithinActivity(date);
  }

  Activity get mainAgendaSelectedActivity {
    return _mainAgenda.selectedActivity;
  }

  Activity get sideAgendaSelectedActivity {
    return _sideAgenda.selectedActivity;
  }

  bool get mainAgendaActivitiesIsNotEmpty {
    return _mainAgenda.activities.isNotEmpty;
  }

  bool get sideAgendaActivitiesIsNotEmpty {
    return _sideAgenda.activities.isNotEmpty;
  }

  bool get mainAgendaHasSelectedActivity {
    return _mainAgenda.hasSelectedActivity;
  }

  bool get sideAgendaHasSelectedActivity {
    return _sideAgenda.hasSelectedActivity;
  }

  DateTime get mainAgendaSelectedActivityStartDate {
    return _mainAgenda.selectedActivity.startDate;
  }

  DateTime get sideAgendaSelectedActivityStartDate {
    return _sideAgenda.selectedActivity.startDate;
  }

  DateTime get mainAgendaSelectedActivityEndDate {
    return _mainAgenda.selectedActivity.endDate;
  }

  DateTime get sideAgendaSelectedActivityEndDate {
    return _sideAgenda.selectedActivity.endDate;
  }

  bool mainAgendaIncreaseStartDateOnActivity(Duration duration) {
    return _mainAgenda.increaseStartDateOnActivity(duration);
  }

  bool sideAgendaIncreaseStartDateOnActivity(Duration duration) {
    return _sideAgenda.increaseStartDateOnActivity(duration);
  }

  bool mainAgendaDecreaseStartDateOnActivity(Duration _duration) {
    return _mainAgenda.decreaseStartDateOnActivity(_duration);
  }

  bool sideAgendaDecreaseStartDateOnActivity(Duration _duration) {
    return _sideAgenda.decreaseStartDateOnActivity(_duration);
  }

  ActivityNode get mainAgendaSelectedActivityActivityNode {
    return _mainAgenda.selectedActivity.activityNode;
  }
}
