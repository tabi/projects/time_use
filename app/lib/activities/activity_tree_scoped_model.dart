import 'package:flutter/widgets.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/activities/activity_tree.dart';
import 'package:tbo_app/core/configuration.dart';

class ActivityTreeScopedModel extends Model {
  final ActivityTree mainActivityTree;
  final ActivityTree sideActivityTree;

  ActivityTreeScopedModel(this.mainActivityTree, this.sideActivityTree);

  static ActivityTreeScopedModel of(BuildContext context) =>
      ScopedModel.of<ActivityTreeScopedModel>(context);

  Future<void> load(BuildContext context) async {
    final _configuration = Configuration.of(context);
    mainActivityTree.init(
        _configuration.mainActivityTree.name, _configuration.inputType);
    sideActivityTree.init(
        _configuration.sideActivityTree.name, _configuration.inputType);
  }
}
