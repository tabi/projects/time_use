import 'dart:core';

import 'package:flutter/material.dart';
import 'package:tbo_app/activities/activity_agenda.dart';
import 'package:tbo_app/activities/media_activity_entry_form.dart';
import 'package:tbo_app/activities/time_activity_entry_form.dart';
import 'package:tbo_app/activities/translate_page_activity_entry.dart';
import 'package:tbo_app/core/configuration.dart';
import 'package:tbo_app/core/configuration_state.dart';

class ActivityEntryPage extends StatefulWidget {
  final ActivityAgendas agendas;
  final DateTime chosenDateTime;

  const ActivityEntryPage({this.agendas, this.chosenDateTime});

  @override
  State<StatefulWidget> createState() {
    return _ActivityEntryPageState();
  }
}

class _ActivityEntryPageState extends State<ActivityEntryPage> {
  ActivityAgendas _agendas;
  TranslatePageActivityEntry translate;
  ScrollController pageScrollController = ScrollController();

  bool get isChanged {
    if (!widget.agendas.main.selectedActivity
        .isEqualTo(_agendas.main.selectedActivity)) {
      return true;
    }

    if (widget.agendas.side.activities.length !=
        _agendas.side.activities.length) {
      return true;
    } else {
      for (int i = 0; i < widget.agendas.side.activities.length; i++) {
        if (!widget.agendas.side.activities[i]
            .isEqualTo(_agendas.side.activities[i])) {
          return true;
        }
      }
    }

    return false;
  }

  bool get isComplete {
    return _agendas.main.selectedActivity.activityNode != null &&
        (_agendas.main.selectedActivity.companion?.isNotEmpty ?? false);
  }

  bool isSaved = false;
  bool isSaveClicked = false;

  @override
  void initState() {
    super.initState();
    _agendas =
        ActivityAgendas(widget.agendas.main.copy, widget.agendas.side.copy);
    translate = TranslatePageActivityEntry.instance;
  }

  @override
  Widget build(BuildContext context) {
    return Configuration.of(context).appFlavor == AppFlavor.time
        ? TimeActivityEntryForm(
            agendas: widget.agendas,
            chosenDateTime: widget.chosenDateTime,
          )
        : MediaActivityEntryForm(
            agendas: widget.agendas,
            chosenDateTime: widget.chosenDateTime,
          );
  }
}
