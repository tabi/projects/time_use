import 'package:built_collection/built_collection.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/activities/activity.dart';
import 'package:tbo_app/activities/activity_companion.dart';
import 'package:tbo_app/core/app_colors.dart';
import 'package:tbo_app/core/app_icons.dart';
import 'package:tbo_app/core/extensions.dart';
import 'package:tbo_app/home/start_questionnaire_result.dart';
import 'package:tbo_app/home/start_questionnaire_scoped_model.dart';

class ActivityCompanionDialog extends StatefulWidget {
  final Activity activity;

  const ActivityCompanionDialog({Key key, this.activity}) : super(key: key);

  @override
  _ActivityCompanionDialogState createState() =>
      _ActivityCompanionDialogState();
}

class _ActivityCompanionDialogState extends State<ActivityCompanionDialog> {
  Activity _activity;

  static final _darkBlueStyle = TextStyle(
      color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
      fontSize: 18.0,
      height: 1.25,
      fontWeight: FontWeight.w600);

  static final _darkBlueStyleHeader = TextStyle(
      color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
      fontSize: 18.0,
      height: 1.25,
      fontWeight: FontWeight.w700);

  static final _midBlueStyle = TextStyle(
      color: AppColors.appColor(enumColor.COL_BLUE_MIDDLE),
      fontSize: 18.0,
      height: 1.25,
      fontWeight: FontWeight.w600);

  @override
  void initState() {
    super.initState();
    _activity = widget.activity;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      child: ScopedModelDescendant<StartQuestionnaireModel>(
          builder: (context, child, startQuestionnaireModel) {
        return FutureBuilder<StartQuestionnaireResult>(
            future: startQuestionnaireModel.startQuestionnaireEntry(),
            builder: (context, snapshot) {
              final _startQuestionnaireEntry = snapshot.data;
              return Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const SizedBox(height: 20),
                  Text('Selecteer met wie u was:', style: _darkBlueStyleHeader),
                  const SizedBox(height: 20),
                  _whoChoise(ActivityCompanion.none),
                  if (_startQuestionnaireEntry?.hasPartner ?? true)
                    _whoChoise(ActivityCompanion.partner),
                  _whoChoise(ActivityCompanion.parents),
                  if (_startQuestionnaireEntry?.hasChildren ?? false)
                    _whoChoise(ActivityCompanion.children),
                  if (_startQuestionnaireEntry?.hasOtherLivingPartners ?? false)
                    _whoChoise(ActivityCompanion.houseMates),
                  _whoChoise(ActivityCompanion.other),
                  const SizedBox(height: 20),
                  InkWell(
                    onTap: () {
                      Navigator.of(context).pop(_activity);
                    },
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Gereed', style: _midBlueStyle),
                    ),
                  ),
                  const SizedBox(height: 20),
                ],
              );
            });
      }),
    );
  }

  Widget _whoChoise(ActivityCompanion companion) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        setState(() {
          final _builder = _activity.companion?.toBuilder() ??
              ListBuilder<ActivityCompanion>();
          if (_activity.companion?.contains(companion) ?? false) {
            _builder.remove(companion);
          } else {
            if (companion == ActivityCompanion.none) {
              _builder.clear();
            } else {
              _builder.remove(ActivityCompanion.none);
            }
            _builder.add(companion);
          }
          _activity = _activity.rebuild((b) => b.companion = _builder);
        });
      },
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 5),
        height: 50,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(
              width: 50,
              child: Center(
                child: companion.imageIcon(),
              ),
            ),
            Text(
              companion.label,
              style: _darkBlueStyle.apply(fontSizeFactor: 0.9),
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
            ),
            const Spacer(),
            SizedBox(
              width: 20,
              child: Center(
                child: Icon(
                  (_activity.companion?.contains(companion) ?? false)
                      ? AppIcons.appIconData(ActivityCompanion.none == companion
                          ? enumIcon.ICO_DOTCIRCLE
                          : enumIcon.ICO_CHECKSQUARE)
                      : AppIcons.appIconData(ActivityCompanion.none == companion
                          ? enumIcon.ICO_CIRCLE
                          : enumIcon.ICO_SQUARE),
                  color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
                ),
              ),
            ),
            SizedBox(
              width: ActivityCompanion.none == companion ? 12 : 10,
            )
          ],
        ),
      ),
    );
  }
}
