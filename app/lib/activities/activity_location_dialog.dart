import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/activities/activity.dart';
import 'package:tbo_app/activities/activity_location.dart';
import 'package:tbo_app/activities/activity_location_dialog_item.dart';
import 'package:tbo_app/home/start_questionnaire_result.dart';
import 'package:tbo_app/home/start_questionnaire_scoped_model.dart';

class ActivityLocationDialog extends StatelessWidget {
  final Activity activity;

  const ActivityLocationDialog({Key key, this.activity}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      child: ScopedModelDescendant<StartQuestionnaireModel>(
          builder: (context, child, startQuestionnaireModel) {
        return FutureBuilder<StartQuestionnaireResult>(
            future: startQuestionnaireModel.startQuestionnaireEntry(),
            builder: (context, snapshot) {
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 20),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    ActivityLocationDialogItem(
                      location: ActivityLocation.home,
                      onTap: _locationSelected,
                    ),
                    ActivityLocationDialogItem(
                      location: ActivityLocation.work,
                      onTap: _locationSelected,
                    ),
                    ActivityLocationDialogItem(
                      location: ActivityLocation.schoolStudy,
                      onTap: _locationSelected,
                    ),
                    ActivityLocationDialogItem(
                      location: ActivityLocation.other,
                      onTap: _locationSelected,
                    ),
                  ],
                ),
              );
            });
      }),
    );
  }

  void _locationSelected(BuildContext context, ActivityLocation location) {
    Navigator.of(context).pop(activity.rebuild((b) => b.location = location));
  }
}
