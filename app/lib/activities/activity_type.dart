import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'activity_type.g.dart';

class ActivityType extends EnumClass {
  static Serializer<ActivityType> get serializer => _$activityTypeSerializer;

  static const ActivityType main = _$main;
  static const ActivityType side = _$side;

  const ActivityType._(String name) : super(name);

  static BuiltSet<ActivityType> get values => _$values;

  static ActivityType valueOf(String name) => _$valueOf(name);
}
