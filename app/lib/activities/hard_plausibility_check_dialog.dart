import 'package:flutter/material.dart';
import 'package:tbo_app/activities/translate_page_activity_agenda.dart';

class HardPlausibilityCheckDialog extends StatelessWidget {
  const HardPlausibilityCheckDialog({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final _translate = TranslatePageActivityAgenda.instance;
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 20,
                ),
                Text(
                  _translate.dayNotComplete,
                  style: const TextStyle(
                    color: Colors.black,
                    fontSize: 18.0,
                    height: 1.25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                const Text(
                  'Je hebt de hele dag nog niet ingevuld. Vul eerst de hele dag in voordat je de dag afsluit.',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18.0,
                    height: 1.25,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                Row(
                  children: <Widget>[
                    const Spacer(),
                    FlatButton(
                      onPressed: () {
                        Navigator.of(context).pop(false);
                      },
                      child: Text(
                        _translate.general.ok,
                        style: const TextStyle(
                          color: Colors.blue,
                          fontSize: 18.0,
                          height: 1.25,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
