import 'dart:async';
import 'dart:io' show Platform;

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tbo_app/core/activity_tree_code.dart';
import 'package:tbo_app/core/color_pallet.dart';
import 'package:tbo_app/core/configuration.dart';
import 'package:tbo_app/core/configuration_state.dart';
import 'package:tbo_app/core/database/user_progress_database.dart';
import 'package:tbo_app/core/translate.dart';
import 'package:tbo_app/tbo_app.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = await SharedPreferences.getInstance();
  translate.treePreference =
      prefs.getString('treePreference') ?? ActivityTreeCode.T20201004.name;
  translate.languagePreference = prefs.getString('languagePreference') ?? 'en';
  translate.tablePreference = prefs.getString('tablePreference') ?? 'en';
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  if (Platform.isAndroid) {
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: ColorPallet.primaryColor,
        statusBarBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
      ),
    );
  } else {
    SystemChrome.setSystemUIOverlayStyle(
      const SystemUiOverlayStyle(
        statusBarColor: ColorPallet.primaryColor,
        statusBarBrightness: Brightness.dark,
        statusBarIconBrightness: Brightness.dark,
      ),
    );
  }

  final _userProgressDatabase = UserProgressDatabase();
  final initialized = await _userProgressDatabase.isInitialized();

  runApp(
    Configuration(
      state: ConfigurationState(
        environment: Environment.development,
        appFlavor: AppFlavor.time,
      ),
      child: TBOApp(
        initialized: initialized,
      ),
    ),
  );
}
