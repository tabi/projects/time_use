enum enumSetting {
  settingDoesNotExist,
  settingLanguage,
  settingAtivityTree,
  settingLinesVisibleCode,
  settingMainVisibleYn,
  settingSideMarginYn,
  settingChosenDate
}

class Settings {
  Settings._privateConstructor();
  static final Settings instance = Settings._privateConstructor();

  Map<enumSetting, String> _settings;

  Future init() async {
    if (_settings == null) {
      _settings = <enumSetting, String>{};

      for (final enumSetting setting in enumSetting.values) {
        _settings[setting] = '';
      }

      _defaults();

      await _loadSetting();
    }
  }

  void _defaults() {
    _settings[enumSetting.settingLanguage] = 'NL';
    _settings[enumSetting.settingAtivityTree] = 'T20201004';
    _settings[enumSetting.settingLinesVisibleCode] = 'V'; // 1 2 3 X V
    _settings[enumSetting.settingMainVisibleYn] = 'Y'; // Y N
    _settings[enumSetting.settingSideMarginYn] = 'N'; // Y N
  }

  Future<void> setSetting(enumSetting setting, String value) async {
    _settings[setting] = value;
  }

  Future setAndSaveSetting(enumSetting setting, String value) async {
    _settings[setting] = value;
    await _saveSetting(setting, value);
  }

  String getSetting(enumSetting setting) {
    return _settings[setting];
  }

  Future _loadSetting() async {
    //### TBD load rows ##################################################
    final List<Map<String, dynamic>> rows = <Map<String, dynamic>>[];
    for (final Map<String, dynamic> row in rows) {
      _saveSetting(row['key'] as enumSetting, row['value'] as String);
    }
  }

  Future _saveSetting(enumSetting setting, String value) async {
    final List<Map<String, dynamic>> rows = <Map<String, dynamic>>[];
    final Map<String, dynamic> row = <String, dynamic>{};
    row[_settingCode(setting)] = value;
    rows.add(row);
    //### TBD save rows ##################################################
  }

  enumSetting _setting(String code) {
    return enumSetting.values.firstWhere(
        (setting) => _settingCode(setting) == code,
        orElse: () => enumSetting.settingDoesNotExist);
  }

  String _settingCode(enumSetting setting) {
    final String code = setting.toString();
    return code.substring(code.indexOf('.') + 1);
  }

  bool isValidSettingCode(String code) {
    return _setting(code) != enumSetting.settingDoesNotExist;
  }
}
