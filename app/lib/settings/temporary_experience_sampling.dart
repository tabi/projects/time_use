import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tbo_app/core/database/table/experience_sampling_table.dart';
import 'package:tbo_app/intro_slider/intro_slider.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;

import '../core/color_pallet.dart';
import '../core/responsive_ui.dart';
import '../experience_sampling/notification.dart';
import 'settings_scoped_model.dart';

class _SettingsBoxWidget extends StatelessWidget {
  const _SettingsBoxWidget({@required this.settingsWidget});

  final Widget settingsWidget;

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<SettingsModel>(
        builder: (context, child, model) {
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 8.0 * x),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: ColorPallet.veryLightGray,
                offset: Offset(5.0 * x, 1.0 * y),
                blurRadius: 2.0 * x,
                spreadRadius: 3.0 * x)
          ],
        ),
        child: settingsWidget,
      );
    });
  }
}

class ExperienceSamplingWidget extends StatefulWidget {
  @override
  _ExperienceSamplingWidgetState createState() =>
      _ExperienceSamplingWidgetState();
}

class _ExperienceSamplingWidgetState extends State<ExperienceSamplingWidget> {
  TextStyle headerText = TextStyle(
      color: ColorPallet.darkTextColor,
      fontSize: 18.0 * f,
      fontWeight: FontWeight.w700);
  TextStyle settingText = TextStyle(
      color: ColorPallet.darkTextColor,
      fontSize: 16.0 * f,
      fontWeight: FontWeight.w600);
  TextStyle valueText = TextStyle(
      color: ColorPallet.midGray,
      fontSize: 16.0 * f,
      fontWeight: FontWeight.w600);
  TextStyle valueTextBlue = TextStyle(
      color: ColorPallet.primaryColor,
      fontSize: 16.0 * f,
      fontWeight: FontWeight.w600);

  Future<bool> getExperienceSamplingSetting() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool('experienceSamplingSetting') ?? false;
  }

  @override
  Widget build(BuildContext context) {
    return _SettingsBoxWidget(
      settingsWidget: Container(
        margin: EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.timeline,
                    color: ColorPallet.darkTextColor, size: 24.0 * x),
                SizedBox(width: 10.0 * x),
                Text('Experience sampling', style: headerText),
              ],
            ),
            SizedBox(height: 24.0 * x),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
            //   children: <Widget>[
            //     Text('Experience sampling', style: settingText),
            //     Padding(
            //       padding: EdgeInsets.only(right: 9.5 * x),
            //       child: FutureBuilder(
            //         future: getExperienceSamplingSetting(),
            //         builder:
            //             (BuildContext context, AsyncSnapshot<bool> snapshot) {
            //           if (!snapshot.hasData) {
            //             return SizedBox();
            //           } else {
            //             return Switch(
            //               onChanged: (value) async {
            //                 final SharedPreferences prefs =
            //                     await SharedPreferences.getInstance();
            //                 setState(() {
            //                   prefs.setBool('experienceSamplingSetting', value);
            //                 });
            //                 if (value) {
            //                   scheduleESNotifcations();
            //                 } else {
            //                   removeESNotifications();
            //                 }
            //               },
            //               value: snapshot.data,
            //               activeColor: ColorPallet.pink,
            //             );
            //           }
            //         },
            //       ),
            //     ),
            //   ],
            // ),
            // SizedBox(height: 8.0 * x),
            // Container(height: 1.0 * y, color: ColorPallet.lightGray),
            // SizedBox(height: 8.0 * x),
            Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    RaisedButton(
                      onPressed: () {
                        zonedScheduleNotification(
                          1000,
                          tz.TZDateTime.now(tz.local)
                              .add(const Duration(seconds: 1)),
                          'Hoe voelt u zich?',
                          'Wij stellen u 5 vragen over hoe u zich nu voelt. Het invullen duurt heel kort.',
                          {
                            'id': 1000,
                            'date': DateTime.now().toString(),
                            'unixTime': DateTime.now().millisecondsSinceEpoch,
                            'questionairType': 'feelings',
                            'notificationType': 'secondAttempt'
                          },
                        );
                      },
                      child: Text('Questionair 1', style: settingText),
                    ),
                    RaisedButton(
                      onPressed: () {
                        zonedScheduleNotification(
                          1001,
                          tz.TZDateTime.now(tz.local)
                              .add(const Duration(seconds: 1)),
                          'Welke activiteiten heeft u gedaan?',
                          'Wij vragen u welke activiteiten u het afgelopen uur heeft gedaan. Het invullen duurt heel kort.',
                          {
                            'id': 1001,
                            'date': DateTime.now().toString(),
                            'unixTime': DateTime.now().millisecondsSinceEpoch,
                            'questionairType': 'activities',
                            'notificationType': 'secondAttempt'
                          },
                        );
                      },
                      child: Text('Questionair 2', style: settingText),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    RaisedButton(
                      onPressed: () async {
                        await showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return const EsTiming();
                          },
                        );
                      },
                      child: Text('Show schedule', style: settingText),
                    ),
                    RaisedButton(
                      onPressed: () async {
                        await showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return const EsResults();
                          },
                        );
                      },
                      child: Text('Show results', style: settingText),
                    ),
                  ],
                ),
                RaisedButton(
                  onPressed: () async {
                    await Navigator.push(
                      context,
                      MaterialPageRoute(
                        settings: const RouteSettings(name: 'IntroSlider'),
                        builder: (context) => IntroSlider(),
                      ),
                    );
                  },
                  child:
                      Text('Show intro/onboarding slider', style: settingText),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class EsTiming extends StatelessWidget {
  const EsTiming({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextStyle settingText = TextStyle(
        color: ColorPallet.darkTextColor,
        fontSize: 16.0 * f,
        fontWeight: FontWeight.w600);

    return AlertDialog(
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: const Text('Close'),
        ),
      ],
      content: Container(
        height: 500,
        width: 380,
        color: Colors.white,
        child: FutureBuilder(
          future: ExperienceSamplingDb.getEsTimings(),
          builder: (BuildContext context,
              AsyncSnapshot<List<Map<String, dynamic>>> snapshot) {
            if (!snapshot.hasData || snapshot.data.isEmpty) {
              return Container();
            }
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                if (snapshot.data[index]['notificationType'] ==
                    'firstAttempt') {
                  final String s = snapshot.data[index]['date'] as String;
                  return SizedBox(
                    width: 330,
                    height: 17,
                    child: Row(
                      children: <Widget>[
                        Text(s.substring(0, 16)),
                        const SizedBox(width: 15),
                        Text(snapshot.data[index]['questionairType'] as String,
                            style: settingText),
                        Expanded(child: Container()),
                      ],
                    ),
                  );
                } else {
                  return Container();
                }
              },
            );
          },
        ),
      ),
    );
  }
}

class EsResults extends StatelessWidget {
  const EsResults({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextStyle settingText = TextStyle(
        color: ColorPallet.darkTextColor,
        fontSize: 16.0 * f,
        fontWeight: FontWeight.w600);

    return AlertDialog(
      actions: <Widget>[
        FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          child: const Text('Close'),
        ),
      ],
      content: Container(
        height: 500,
        width: 380,
        color: Colors.white,
        child: FutureBuilder(
          future: ExperienceSamplingDb.getQuestionairResults(),
          builder: (BuildContext context,
              AsyncSnapshot<List<Map<String, dynamic>>> snapshot) {
            if (!snapshot.hasData || snapshot.data.isEmpty) {
              return Container();
            }
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: SizedBox(
                    width: 380,
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Text('Datum: ',
                                style: settingText.apply(fontWeightDelta: 2)),
                            Text(
                                (snapshot.data[index]['sendTime'] as String)
                                    .substring(0, 10),
                                style: settingText),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text('Send time: ',
                                style: settingText.apply(fontWeightDelta: 2)),
                            Text(
                                (snapshot.data[index]['sendTime'] as String)
                                    .substring(10, 16),
                                style: settingText),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text('Read time: ',
                                style: settingText.apply(fontWeightDelta: 2)),
                            Text(
                                (snapshot.data[index]['readTime'] as String)
                                    .substring(10, 16),
                                style: settingText),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text('Is first attempt: ',
                                style: settingText.apply(fontWeightDelta: 2)),
                            Text(
                                snapshot.data[index]['isFirstAttempt'] == 1
                                    ? 'Yes'
                                    : 'No',
                                style: settingText),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text('Is feeling questionair: ',
                                style: settingText.apply(fontWeightDelta: 2)),
                            Text(
                                snapshot.data[index]['isFeelingQuestionair'] ==
                                        1
                                    ? 'Yes'
                                    : 'No',
                                style: settingText),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            if (snapshot.data[index]['isFeelingQuestionair'] ==
                                1)
                              Text(
                                'happy:${snapshot.data[index]['happy'].toString()} energetic:${snapshot.data[index]['energetic'].toString()} \nrelaxed:${snapshot.data[index]['relaxed'].toString()} cheerful:${snapshot.data[index]['cheerful'].toString()}',
                              )
                            else
                              Text(
                                  'social:${snapshot.data[index]['socialMedia'].toString()} texting:${snapshot.data[index]['texting'].toString()} \ngames:${snapshot.data[index]['games'].toString()} news:${snapshot.data[index]['newsOnline'].toString()} paper:${snapshot.data[index]['readingPaper'].toString()}',
                                  style: settingText)
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              },
            );
          },
        ),
      ),
    );
  }
}
