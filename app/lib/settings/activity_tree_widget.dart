import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/core/color_pallet.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/core/translate.dart';
import 'package:tbo_app/settings/settings_box_widget.dart';
import 'package:tbo_app/settings/settings_scoped_model.dart';

class ActivityTreeWidget extends StatefulWidget {
  final Key keyButton4;
  final TextStyle headerText;
  final TextStyle settingText;

  const ActivityTreeWidget(
      {Key key,
      @required this.keyButton4,
      @required this.headerText,
      @required this.settingText})
      : super(key: key);

  @override
  ActivityTreeWidgetState createState() => ActivityTreeWidgetState();
}

class ActivityTreeWidgetState extends State<ActivityTreeWidget> {
  String keyToTree(String key) {
    switch (key) {
      case 'NL1':
        return 'NL1';
      case 'Open invoer':
        return 'Open invoer';
      case 'Een niveau':
        return 'Een niveau';
      case 'Twee niveaux':
        return 'Twee niveaux';
      case 'T20201004':
        return 'T20201004';
      case 'lugtig_o1':
        return 'lugtig_o1';
      case 'lugtig_o3':
        return 'lugtig_o3';
      default:
        return 'T20201004';
    }
  }

  String treeToKey(String country) {
    switch (country) {
      case 'NL1':
        return 'NL1';
      case 'Open invoer':
        return 'Open invoer';
      case 'Een niveau':
        return 'Een niveau';
      case 'Twee niveaux':
        return 'Twee niveaux';
      case 'T20201004':
        return 'T20201004';
      case 'lugtig_o1':
        return 'lugtig_o1';
      case 'lugtig_o3':
        return 'lugtig_o3';
      default:
        return 'T20201004';
    }
  }

  @override
  Widget build(BuildContext context) {
    return SettingsBoxWidget(
        settingsWidget: Container(
      margin: EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
      child: ScopedModelDescendant<SettingsModel>(
        builder: (context, child, model) => Column(
          key: widget.keyButton4,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.ac_unit,
                    color: ColorPallet.darkTextColor, size: 24.0 * x),
                SizedBox(width: 10.0 * x),
                Text('Activiteiten lijst', style: widget.headerText),
              ],
            ),
            SizedBox(height: 24.0 * y),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Activiteiten lijst', style: widget.settingText),
                DropdownButton<String>(
                  value: keyToTree(translate.treePreference),
                  items: <String>[
                    'NL1',
                    'Open invoer',
                    'Een niveau',
                    'Twee niveaux',
                    'T20201004',
                    'lugtig_o1',
                    'lugtig_o3'
                  ].map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Padding(
                        padding: EdgeInsets.only(right: 9.5 * x),
                        child: Text(value,
                            style: TextStyle(
                                color: ColorPallet.pink,
                                fontSize: 16.0 * f,
                                fontWeight: FontWeight.w600)),
                      ),
                    );
                  }).toList(),
                  onChanged: (newValue) async {
                    setState(() {
                      translate.treePreference = treeToKey(newValue);
                    });
                    await model.saveActivityTreePreference();
                  },
                ),
              ],
            ),
            SizedBox(height: 0.0 * y),
            Container(height: 1.0 * y, color: ColorPallet.lightGray),
          ],
        ),
      ),
    ));
  }
}
