import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/core/color_pallet.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/core/translate.dart';
import 'package:tbo_app/settings/settings_box_widget.dart';
import 'package:tbo_app/settings/settings_scoped_model.dart';

class VersionWidget extends StatelessWidget {
  final TextStyle headerText;
  final TextStyle settingText;

  const VersionWidget(
      {Key key, @required this.headerText, @required this.settingText})
      : super(key: key);

  Future<String> getVersionNumber() async {
    final packageInfo = await PackageInfo.fromPlatform();
    final versionName = packageInfo.version;
    final versionCode = packageInfo.buildNumber;
    return '$versionName+$versionCode';
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<SettingsModel>(
        builder: (context, child, model) {
      return SettingsBoxWidget(
        settingsWidget: Container(
          margin:
              EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.info_outline,
                      color: ColorPallet.darkTextColor, size: 24.0 * x),
                  SizedBox(width: 10.0 * x),
                  Text(translate.information, style: headerText),
                ],
              ),
              SizedBox(height: 24.0 * x),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(translate.versionNumber, style: settingText),
                  Padding(
                    padding: EdgeInsets.only(right: 9.5 * x),
                    child: FutureBuilder(
                      future: getVersionNumber(),
                      builder: (BuildContext context,
                          AsyncSnapshot<String> snapshot) {
                        if (!snapshot.hasData) {
                          return const Text('');
                        } else {
                          return Text(snapshot.data, style: settingText);
                        }
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(height: 8.0 * x),
              Container(height: 1.0 * y, color: ColorPallet.lightGray),
            ],
          ),
        ),
      );
    });
  }
}
