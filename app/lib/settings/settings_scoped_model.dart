import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tbo_app/core/translate.dart';

class SettingsModel extends Model {
  static SettingsModel of(BuildContext context) =>
      ScopedModel.of<SettingsModel>(context);

  Future<void> saveLanguagePreference() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('languagePreference', translate.languagePreference);
  }

  Future<void> saveActivityTreePreference() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('treePreference', translate.treePreference);
  }

  Future<void> saveTablePreference() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('tablePreference', translate.tablePreference);
  }
}
