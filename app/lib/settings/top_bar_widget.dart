import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/core/color_pallet.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/core/translate.dart';
import 'package:tbo_app/settings/settings_scoped_model.dart';

class TopBarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: ColorPallet.primaryColor,
      height: 50 * y,
      child: Row(
        children: <Widget>[
          SizedBox(width: 22.0 * x),
          ScopedModelDescendant<SettingsModel>(
              builder: (context, child, model) {
            return Text(
              translate.settings,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 22.0 * f,
                  fontWeight: FontWeight.w600),
            );
          }),
          SizedBox(width: 10 * x),
          // InkWell(
          //     onTap: () {
          //       showTutorial(context);
          //     },
          //     child: Icon(Icons.info, color: Colors.white, size: 28.0 * x)),
        ],
      ),
    );
  }
}
