import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/core/color_pallet.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/core/translate.dart';
import 'package:tbo_app/settings/settings_box_widget.dart';
import 'package:tbo_app/settings/settings_scoped_model.dart';

class LanguageWidget extends StatefulWidget {
  final Key keyButton3;
  final TextStyle headerText;
  final TextStyle settingText;

  const LanguageWidget(
      {Key key,
      @required this.keyButton3,
      @required this.headerText,
      @required this.settingText})
      : super(key: key);
  @override
  _LanguageWidgetState createState() => _LanguageWidgetState();
}

class _LanguageWidgetState extends State<LanguageWidget> {
  String keyToCountry(String key) {
    switch (key) {
      case 'nl':
        return 'Nederlands';
      case 'sl':
        return 'Slovensko';
      case 'fi':
        return 'Suomalainen';
      case 'en':
        return 'English';
      default:
        return 'English';
    }
  }

  String countryToKey(String country) {
    switch (country) {
      case 'Nederlands':
        return 'nl';
      case 'Slovensko':
        return 'sl';
      case 'Suomalainen':
        return 'fi';
      case 'English':
        return 'en';
      default:
        return 'en';
    }
  }

  @override
  Widget build(BuildContext context) {
    return SettingsBoxWidget(
        settingsWidget: Container(
      margin: EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
      child: ScopedModelDescendant<SettingsModel>(
        builder: (context, child, model) => Column(
          key: widget.keyButton3,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.language,
                    color: ColorPallet.darkTextColor, size: 24.0 * x),
                SizedBox(width: 10.0 * x),
                Text(translate.language, style: widget.headerText),
              ],
            ),
            SizedBox(height: 24.0 * y),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(translate.language, style: widget.settingText),
                DropdownButton<String>(
                  value: keyToCountry(translate.languagePreference),
                  items: <String>[
                    'Nederlands',
                    'Suomalainen',
                    'Slovensko',
                    'English'
                  ].map((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Padding(
                        padding: EdgeInsets.only(right: 9.5 * x),
                        child: Text(value,
                            style: TextStyle(
                                color: ColorPallet.pink,
                                fontSize: 16.0 * f,
                                fontWeight: FontWeight.w600)),
                      ),
                    );
                  }).toList(),
                  onChanged: (newValue) async {
                    setState(() {
                      translate.languagePreference = countryToKey(newValue);
                    });
                    await model.saveLanguagePreference();
                  },
                ),
              ],
            ),
            SizedBox(height: 0.0 * y),
            Container(height: 1.0 * y, color: ColorPallet.lightGray),
          ],
        ),
      ),
    ));
  }
}
