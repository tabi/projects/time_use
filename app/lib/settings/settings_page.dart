import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tbo_app/core/color_pallet.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/para_data/para_data_name.dart';
import 'package:tbo_app/settings/contact_widget.dart';
import 'package:tbo_app/settings/settings_page_tutorial.dart';
import 'package:tbo_app/settings/temporary_experience_sampling.dart';
import 'package:tbo_app/settings/top_bar_widget.dart';
import 'package:tbo_app/settings/version_widget.dart';

class SettingsPage extends StatefulWidget with ParaDataName {
  @override
  State<StatefulWidget> createState() {
    return _SettingsPageState();
  }
}

class _SettingsPageState extends State<SettingsPage> {
  final headerText = TextStyle(
      color: ColorPallet.darkTextColor,
      fontSize: 18.0 * f,
      fontWeight: FontWeight.w700);
  final settingText = TextStyle(
      color: ColorPallet.darkTextColor,
      fontSize: 16.0 * f,
      fontWeight: FontWeight.w600);
  final valueText = TextStyle(
      color: ColorPallet.midGray,
      fontSize: 16.0 * f,
      fontWeight: FontWeight.w600);
  final valueTextBlue = TextStyle(
      color: ColorPallet.primaryColor,
      fontSize: 16.0 * f,
      fontWeight: FontWeight.w600);
  final GlobalKey keyButton1 = GlobalKey();
  final GlobalKey keyButton2 = GlobalKey();
  final GlobalKey keyButton3 = GlobalKey();
  final GlobalKey keyButton4 = GlobalKey();

  @override
  void initState() {
    initTargets(keyButton1, keyButton2, keyButton3);
    super.initState();

    Future.delayed(const Duration(milliseconds: 200), () {
      showInitialTutorial(context);
    });
  }

  Future<void> showInitialTutorial(BuildContext context) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String status = prefs.getString('mainSettingsTutorial');
    if (status == null) {
      prefs.setString('mainSettingsTutorial', 'shown');
      showTutorial(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        TopBarWidget(),
        Expanded(
          child: Container(
            margin: const EdgeInsets.only(bottom: 8.0),
            child: ListView(
              children: <Widget>[
                SizedBox(height: 8.0 * y),
                // NotificationWidget(
                //   keyButton1: keyButton1,
                //   headerText: headerText,
                //   settingText: settingText,
                //   valueText: valueText,
                // ),
                ExperienceSamplingWidget(),
                // SizedBox(height: 8.0 * y),
                // LanguageWidget(
                //   keyButton3: keyButton3,
                //   headerText: headerText,
                //   settingText: settingText,
                // ),
                SizedBox(height: 8.0 * y),
                // ActivityTreeWidget(
                //   keyButton4: keyButton4,
                //   headerText: headerText,
                //   settingText: settingText,
                // ),
                SizedBox(height: 8.0 * y),
                ContactWidget(
                  keyButton2: keyButton2,
                  headerText: headerText,
                  settingText: settingText,
                  valueTextBlue: valueTextBlue,
                ),
                SizedBox(height: 8.0 * y),
                VersionWidget(
                  headerText: headerText,
                  settingText: settingText,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
