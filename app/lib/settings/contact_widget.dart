import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/core/color_pallet.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/core/translate.dart';
import 'package:tbo_app/settings/settings_box_widget.dart';
import 'package:tbo_app/settings/settings_scoped_model.dart';
import 'package:url_launcher/url_launcher.dart';

class ContactWidget extends StatelessWidget {
  final Key keyButton2;
  final TextStyle headerText;
  final TextStyle settingText;
  final TextStyle valueTextBlue;

  const ContactWidget(
      {Key key,
      @required this.keyButton2,
      @required this.headerText,
      @required this.settingText,
      @required this.valueTextBlue})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<SettingsModel>(
        builder: (context, child, model) {
      return SettingsBoxWidget(
        settingsWidget: Container(
          key: keyButton2,
          margin:
              EdgeInsets.symmetric(horizontal: 20.0 * x, vertical: 20.0 * y),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.forum,
                      color: ColorPallet.darkTextColor, size: 24.0 * x),
                  SizedBox(width: 10.0 * x),
                  Text(translate.contact, style: headerText),
                ],
              ),
              SizedBox(height: 24.0 * x),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(translate.phoneNumber, style: settingText),
                  InkWell(
                      onTap: () async {
                        final String url = 'tel:${translate.phoneNumberDigits}';
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not launch $url';
                        }
                      },
                      child: Padding(
                        padding: EdgeInsets.only(right: 9.5 * x),
                        child: Text(translate.phoneNumberDigits,
                            style: valueTextBlue),
                      ))
                ],
              ),
              SizedBox(height: 8.0 * x),
              Container(height: 1.0 * y, color: ColorPallet.lightGray),
              SizedBox(height: 24.0 * y),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(translate.email, style: settingText),
                  InkWell(
                      onTap: () async {
                        final DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

                        String deviceInfoString;
                        if (Platform.isAndroid) {
                          final AndroidDeviceInfo androidInfo =
                              await deviceInfo.androidInfo;
                          deviceInfoString = androidInfo.model;
                        } else if (Platform.isIOS) {
                          final IosDeviceInfo iosInfo =
                              await deviceInfo.iosInfo;
                          deviceInfoString = iosInfo.model;
                        }
                        final String url =
                            'mailto:${translate.emailAdress}?subject=HBS App&body=\n\n\nSend from $deviceInfoString';
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not launch $url';
                        }
                      },
                      child: Padding(
                        padding: EdgeInsets.only(right: 9.5 * x),
                        child:
                            Text(translate.emailAdress, style: valueTextBlue),
                      ))
                ],
              ),
              SizedBox(height: 8.0 * y),
              Container(height: 1.0 * y, color: ColorPallet.lightGray),
            ],
          ),
        ),
      );
    });
  }
}
