import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/core/color_pallet.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/settings/settings_scoped_model.dart';

class SettingsBoxWidget extends StatelessWidget {
  const SettingsBoxWidget({@required this.settingsWidget});

  final Widget settingsWidget;

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<SettingsModel>(
        builder: (context, child, model) {
      return Container(
        margin: EdgeInsets.symmetric(horizontal: 8.0 * x),
        decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                color: ColorPallet.veryLightGray,
                offset: Offset(5.0 * x, 1.0 * y),
                blurRadius: 2.0 * x,
                spreadRadius: 3.0 * x)
          ],
        ),
        child: settingsWidget,
      );
    });
  }
}
