import 'dart:async';

import 'package:sqflite/sqflite.dart';
import 'package:tbo_app/activities/activity_node.dart';
import 'package:tbo_app/activities/activity_tree.dart';

class SearchSuggestionsTable {
  final String tblSearchSuggestions = 'tbl_search_suggestions';
  final Database database;

  SearchSuggestionsTable(this.database);

  Future<void> createTables() async {
    await database.execute('''
          CREATE TABLE $tblSearchSuggestions (
            activity TEXT,
            lastAdded INT,
            count INT)
          ''');
  }

  Future<void> addSearch(String activity) async {
    final int searchCount = await _getSearchCount(activity);
    if (searchCount == 0) {
      final Map<String, dynamic> data = <String, dynamic>{};
      data['activity'] = activity;
      data['lastAdded'] = DateTime.now().millisecondsSinceEpoch;
      data['count'] = 1;
      await database.insert(tblSearchSuggestions, data);
    } else {
      await database.rawUpdate('''
            UPDATE $tblSearchSuggestions 
            SET count = ${searchCount + 1}, lastAdded = ${DateTime.now().millisecondsSinceEpoch} 
            WHERE activity='$activity'
            ''');
    }
  }

  Future<int> _getSearchCount(String activity) async {
    final List<Map<String, dynamic>> result = await database.rawQuery(
        "Select count FROM $tblSearchSuggestions WHERE activity='$activity'");
    return result.isEmpty ? 0 : result[0]['count'] as int;
  }

  Future<List<ActivityNode>> getMostRecentSearches(ActivityTree tree) async {
    final List<Map<String, dynamic>> result = await database.rawQuery(
        'Select activity FROM $tblSearchSuggestions ORDER BY lastAdded DESC');
    final List<ActivityNode> activityNodes = _getActivityNodes(tree, result);
    return activityNodes;
  }

  Future<List<ActivityNode>> getMostFrequentSearches(ActivityTree tree) async {
    final List<Map<String, dynamic>> result = await database.rawQuery(
        'Select activity FROM $tblSearchSuggestions ORDER BY count DESC');
    final List<ActivityNode> activityNodes = _getActivityNodes(tree, result);
    return activityNodes;
  }

  List<ActivityNode> _getActivityNodes(
      ActivityTree tree, List<Map<String, dynamic>> activities) {
    final List<ActivityNode> result = [];

    for (final Map<String, dynamic> activityInfo in activities) {
      final String activity = activityInfo['activity'] as String;
      for (final ActivityNode node in tree.activityTree()) {
        if (activity == node.activity) {
          result.add(node);
          break;
        }
      }
    }
    return result;
  }
}
