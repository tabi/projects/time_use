String textSanitizer(String str) {
  final String sanitizedString =
      str.replaceAll(RegExp('[^a-zA-Z 0-9\x80-\xFF]+'), '');
  if (sanitizedString.isNotEmpty) {
    return sanitizedString;
  } else {
    return '';
  }
}
