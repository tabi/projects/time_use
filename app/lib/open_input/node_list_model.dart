import 'package:built_collection/built_collection.dart';
import 'package:tbo_app/activities/activity_node.dart';
import 'package:tbo_app/activities/activity_tree.dart';

String getParentNode(ActivityTree tree, ActivityNode selectedNode) {
  final List<ActivityNode> allActivityNodes = tree.activityTree();
  for (final ActivityNode node in allActivityNodes) {
    if (node.nodeCode == selectedNode.parentCode) {
      return node.activity;
    }
  }
  return selectedNode.activity;
}

Future<List<ActivityNode>> getSearchinputMatchedActivities(
    ActivityTree tree, String searchInput) async {
  final List<ActivityNode> all = tree.activityTree();
  final List<ActivityNode> result = [];
  final List<String> stopDuplicates = [];
  for (final ActivityNode node in all) {
    //Include only those activities which match the search string
    if (node.activity.toLowerCase().contains(searchInput.toLowerCase())) {
      //Do not add duplicate nodes
      if (!result.contains(node) && !stopDuplicates.contains(node.activity)) {
        //Do not add 'open'/'maak activiteit aan' nodes
        if (node.nodeType == 'search') {
          // Do not add parent nodes
          if (node.parentCode != '') {
            result.add(node);
            stopDuplicates.add(node.activity);
          }
        }
      }
    }
  }
  return result;
}

List<ActivityNode> getAllParentNodes(ActivityTree tree) {
  final List<ActivityNode> result = [];
  for (final ActivityNode node in tree.activityTree()) {
    if (node.iconCode != '') {
      result.add(node);
    }
  }
  return result;
}

ActivityNode addNewActivity(
    ActivityTree tree, ActivityNode parentNode, String activity) {
  final ActivityNode childThroughParent =
      tree.activityTree()[parentNode.childIds[0]];
  final int parentId = childThroughParent.parentId;

  final String newNodeCode = '${parentNode.nodeCode}.999';

  final ActivityNode newActivityNode = parentNode.rebuild((b) {
    b.parentId = parentId;
    b.childIds = ListBuilder<int>();
    b.nodeCode = newNodeCode;
    b.nodeType = 'search';
    b.iconCode = '';
    b.colorCode = '';
    b.activityCode = 'new';
    b.activity = activity;
    b.selected = false;
    b.visible = false;
    b.open = false;
  });

  tree.addNewHalfOpenActivity(newActivityNode);
  return newActivityNode;
}
