import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:tbo_app/activities/activity_node.dart';
import 'package:tbo_app/activities/activity_tree_scoped_model.dart';
import 'package:tbo_app/core/app_colors.dart';
import 'package:tbo_app/core/app_icons.dart';
import 'package:tbo_app/core/database/database_helper.dart';
import 'package:tbo_app/core/responsive_ui.dart';
import 'package:tbo_app/insights/draggable_scrollbar.dart';
import 'package:tbo_app/open_input/node_list_model.dart';
import 'package:tbo_app/open_input/search_suggestions_table.dart';
import 'package:tbo_app/open_input/util.dart';

class SearchWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: AppColors.appColor(enumColor.COL_BLUE_MIDDLE),
        child: SafeArea(
          child: Container(
            color: AppColors.appColor(enumColor.COL_WHITE),
            child: ScopedModel<SearchWidgetUIModel>(
              model: SearchWidgetUIModel(),
              child: ScopedModelDescendant<SearchWidgetUIModel>(
                builder: (_, __, ___) => Column(
                  children: [
                    _SearchBar(),
                    _QuickListSettings(),
                    _SearchList(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _SearchBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 73.0 * y,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        border: Border.all(
            width: 13.0 * x,
            color: AppColors.appColor(enumColor.COL_BLUE_MIDDLE)),
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              alignment: Alignment.centerLeft,
              child: TextField(
                autocorrect: false,
                focusNode: SearchWidgetUIModel.of(context).myFocusNode,
                keyboardType: TextInputType.text,
                onSubmitted: (value) {},
                onChanged: (userInput) {
                  final String sanitizedUserInput = textSanitizer(userInput);
                  SearchWidgetUIModel.of(context).searchString =
                      sanitizedUserInput;
                },
                autofocus: true,
                style: TextStyle(
                    color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
                    fontSize: 20.0 * f,
                    fontWeight: FontWeight.w500),
                decoration: InputDecoration(
                  hintText: 'Activiteit zoeken',
                  hintStyle: TextStyle(
                      color: AppColors.appColor(enumColor.COL_GRAY_MIDDLE),
                      fontSize: 18.0 * f),
                  prefixIcon: InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Icon(
                      Icons.arrow_back,
                      color: AppColors.appColor(enumColor.COL_GRAY_MIDDLE),
                      size: 24 * x,
                    ),
                  ),
                  border: InputBorder.none,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _QuickListSettings extends StatelessWidget {
  Color _getQuickListSettingColor(BuildContext context, {bool isRecentItems}) {
    if (SearchWidgetUIModel.of(context).isRecentItemsQuickList ==
        isRecentItems) {
      return AppColors.appColor(enumColor.COL_BLUE_DARKER);
    } else {
      return AppColors.appColor(enumColor.COL_GRAY_MIDDLE);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (SearchWidgetUIModel.of(context).searchString != '') {
      return Container();
    }
    return GestureDetector(
      onTap: () {
        SearchWidgetUIModel.of(context).isRecentItemsQuickList =
            !SearchWidgetUIModel.of(context).isRecentItemsQuickList;
      },
      child: SizedBox(
        height: 50 * y,
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              children: <Widget>[
                Text(
                  'Recente items',
                  style: TextStyle(
                      color: _getQuickListSettingColor(context,
                          isRecentItems: true),
                      fontWeight: FontWeight.w600,
                      fontSize: 18 * f),
                ),
                SizedBox(width: 3 * x),
                Icon(
                  Icons.arrow_downward,
                  color:
                      _getQuickListSettingColor(context, isRecentItems: true),
                  size: 15 * x,
                ),
                SizedBox(width: 30 * x),
                Text(
                  'Frequente items',
                  style: TextStyle(
                      color: _getQuickListSettingColor(context,
                          isRecentItems: false),
                      fontWeight: FontWeight.w600,
                      fontSize: 18 * f),
                ),
                SizedBox(width: 3 * x),
                Icon(
                  Icons.arrow_downward,
                  color:
                      _getQuickListSettingColor(context, isRecentItems: false),
                  size: 15 * x,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class _SearchList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Listener(
        onPointerDown: (v) {
          SearchWidgetUIModel.of(context).unfocusKeyboard();
        },
        child: FutureBuilder(
          future: getSearchinputMatchedActivities(
              ActivityTreeScopedModel.of(context).mainActivityTree,
              SearchWidgetUIModel.of(context).searchString),
          builder: (BuildContext context,
              AsyncSnapshot<List<ActivityNode>> snapshot) {
            if (SearchWidgetUIModel.of(context).searchString.isEmpty) {
              return _QuickSuggestionsList();
            } else if (snapshot.data.isEmpty) {
              return _AddNewActivityButton();
            } else {
              return _ActivitySearchList(snapshot.data);
            }
          },
        ),
      ),
    );
  }
}

class _QuickSuggestionsList extends StatelessWidget {
  Future<List<ActivityNode>> _getQuicklistActivities(
      BuildContext context) async {
    final db = await DatabaseHelper.instance.database;

    if (SearchWidgetUIModel.of(context).isRecentItemsQuickList) {
      return SearchSuggestionsTable(db).getMostRecentSearches(
          ActivityTreeScopedModel.of(context).mainActivityTree);
    } else {
      return SearchSuggestionsTable(db).getMostFrequentSearches(
          ActivityTreeScopedModel.of(context).mainActivityTree);
    }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _getQuicklistActivities(context),
      builder: (_, AsyncSnapshot<List<ActivityNode>> snapshot) {
        if (snapshot.data == null) {
          return Container();
        } else if (snapshot.data.isEmpty) {
          return Container();
        } else {
          return _ActivitySearchList(snapshot.data);
        }
      },
    );
  }
}

class _AddNewActivityButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final SearchWidgetUIModel searchWidgetUIModel =
        SearchWidgetUIModel.of(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          'assets/images/cant_find_activity.png',
          width: 50 * x,
        ),
        SizedBox(height: 10 * y),
        Text(
          'We kunnen deze activiteit niet vinden',
          style: TextStyle(
            color: AppColors.appColor(enumColor.COL_GRAY_DARKER),
            fontWeight: FontWeight.w600,
            fontSize: 18 * f,
          ),
        ),
        SizedBox(height: 10 * y),
        RaisedButton(
          color: AppColors.appColor(enumColor.COL_BLUE_MIDDLE),
          onPressed: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                searchWidgetUIModel.unfocusKeyboard();
                return _ActivityNotFoundDialog(searchWidgetUIModel);
              },
            );
          },
          child: Text(
            'Zelf toevoegen',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w600,
              fontSize: 18 * f,
            ),
          ),
        ),
      ],
    );
  }
}

class _ActivitySearchList extends StatelessWidget {
  _ActivitySearchList(this.activitySearchListData);

  final List<ActivityNode> activitySearchListData;
  final ScrollController _arrowsController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return DraggableScrollbar.rrect(
      alwaysVisibleScrollThumb: true,
      heightScrollThumb: 50.0,
      backgroundColor: Colors.grey,
      padding: EdgeInsets.only(right: 4.0 * x),
      controller: _arrowsController,
      child: ListView.builder(
        controller: _arrowsController,
        itemCount: activitySearchListData.length,
        itemBuilder: (BuildContext context, int index) {
          return _ActivitySearchTile(
            activitySearchListData[index],
          );
        },
      ),
    );
  }
}

class _ActivitySearchTile extends StatelessWidget {
  const _ActivitySearchTile(this.node);

  final ActivityNode node;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
          onTap: () async {
            final db = await DatabaseHelper.instance.database;
            SearchSuggestionsTable(db).addSearch(node.activity);
            Navigator.pop(context, node);
          },
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 5,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.symmetric(
                          horizontal: 17 * x, vertical: 2 * y),
                      child: Text(
                        node.activity,
                        style: TextStyle(
                            fontSize: 16 * f,
                            fontWeight: FontWeight.w700,
                            color:
                                AppColors.appColor(enumColor.COL_BLUE_DARKER)),
                      ),
                    ),
                    SizedBox(height: 3 * y),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 17 * x),
                      child: Text(
                        getParentNode(
                            ActivityTreeScopedModel.of(context)
                                .mainActivityTree,
                            node),
                        style: TextStyle(
                            fontSize: 12.5 * f,
                            color: AppColors.appColor(enumColor.COL_BLUE_DARKER)
                                .withOpacity(0.8),
                            fontWeight: FontWeight.w600),
                      ),
                    ),
                    SizedBox(height: 3 * y),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 17 * x, vertical: 2 * y),
          width: MediaQuery.of(context).size.width,
          height: 1 * y,
          color: AppColors.appColor(enumColor.COL_GRAY_LIGHT),
        ),
      ],
    );
  }
}

class _ActivityNotFoundDialog extends StatelessWidget {
  const _ActivityNotFoundDialog(this.searchWidgetUIModel);

  final SearchWidgetUIModel searchWidgetUIModel;

  @override
  Widget build(BuildContext context) {
    final List<ActivityNode> parentNodes =
        getAllParentNodes(ActivityTreeScopedModel.of(context).mainActivityTree);

    return Dialog(
      child: Container(
        height: MediaQuery.of(context).size.height * 0.9,
        width: MediaQuery.of(context).size.width * 0.8,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: EdgeInsets.only(
                left: 20.0 * x,
                top: 20 * y,
                bottom: 10 * y,
              ),
              child: Text(
                '${searchWidgetUIModel.searchString} behoort tot',
                style: TextStyle(
                  color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
                  fontSize: 19.0 * f,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: parentNodes.length,
                itemBuilder: (_, int index) => _NotFoundTile(
                  parentNodes[index],
                  searchWidgetUIModel.searchString,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _NotFoundTile extends StatelessWidget {
  const _NotFoundTile(this.parentNode, this.searchInput);

  final ActivityNode parentNode;
  final String searchInput;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        final db = await DatabaseHelper.instance.database;
        final ActivityNode newActivityNode = addNewActivity(
            ActivityTreeScopedModel.of(context).mainActivityTree,
            parentNode,
            searchInput);
        SearchSuggestionsTable(db).addSearch(newActivityNode.activity);
        Navigator.pop(context);
        Navigator.pop(context, newActivityNode);
      },
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: 11.0 * y,
          horizontal: 20 * x,
        ),
        child: Row(
          children: [
            AppIcons.appIconFromIconCode(
              parentNode.iconCode,
              AppColors.appColorFromColorCode(parentNode.colorCode),
              27 * f,
            ),
            SizedBox(width: 20 * x),
            Expanded(
              child: Text(
                parentNode.activity,
                style: TextStyle(
                  color: AppColors.appColor(enumColor.COL_BLUE_DARKER),
                  fontSize: 17.0 * f,
                  fontWeight: FontWeight.w500,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class SearchWidgetUIModel extends Model {
  final FocusNode _myFocusNode = FocusNode();
  String _searchString = '';
  bool _isRecentItemsQuickList = true;

  FocusNode get myFocusNode => _myFocusNode;

  void unfocusKeyboard() {
    _myFocusNode.unfocus();
    notifyListeners();
  }

  String get searchString => _searchString;

  set searchString(String searchString) {
    _searchString = searchString;
    notifyListeners();
  }

  bool get isRecentItemsQuickList => _isRecentItemsQuickList;

  set isRecentItemsQuickList(bool isRecentItemsQuickList) {
    _isRecentItemsQuickList = isRecentItemsQuickList;
    notifyListeners();
  }

  static SearchWidgetUIModel of(BuildContext context) =>
      ScopedModel.of<SearchWidgetUIModel>(context);
}
